﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Path = System.IO.Path;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for AI_list.xaml
    /// </summary>
    public partial class AI_list : Window
    {
        System.Data.DataSet Ads = new System.Data.DataSet();
        DataAccessLayer.DAL_ArtificialIntelligence aiDAL = new DataAccessLayer.DAL_ArtificialIntelligence();
        System.Data.DataSet ai_ds = new System.Data.DataSet();
        DataAccessLayer.DAL_ArtificialIntelligence ai_DAL = new DataAccessLayer.DAL_ArtificialIntelligence();
        string format = "dd/MM/yyyy";
        string _UserId = string.Empty;
        int File_Type = 0;

        List<string> strList = new List<string>();
        public AI_list(string userID)
        {
            InitializeComponent();
            _UserId = userID;
            Ads = aiDAL.Get_AIimagelist(userID);
            listview_AI();

        }
        public void listview_AI()
        {
            try
            {
                var MimgL = new List<MLA_Aerodyne.BusinessAccessLayer.AIListData>();
                DataTable Mdt = Ads.Tables[0];
                for (int i = 0; i < Mdt.Rows.Count; i++)
                {
                    MimgL.Add(new MLA_Aerodyne.BusinessAccessLayer.AIListData
                    {
                        ID = Ads.Tables[0].Rows[i]["File_ID"].ToString(),
                        Mission_Name = Ads.Tables[0].Rows[i]["Mission_Name"].ToString(),
                        Paddock_Name = Ads.Tables[0].Rows[i]["Paddock_Name"].ToString(),
                        File_Type = Ads.Tables[0].Rows[i]["File_Type"].ToString(),
                        Mission_Type = Ads.Tables[0].Rows[i]["MissionType"].ToString(),
                        Paddock_Id= Ads.Tables[0].Rows[i]["Paddock_Id"].ToString(),
                        imageCapturedDate = Ads.Tables[0].Rows[i]["File_Captured_Date"].ToString(),

                    }); 

                    if(MimgL[i].File_Type ==".jpeg" || MimgL[i].File_Type == ".jpg"|| MimgL[i].File_Type == ".JPG" || MimgL[i].File_Type == ".JPEG")
                    {
                        MimgL[i].File_Type = "Image";
                        File_Type = 1;
                    }
                    else
                    {
                        MimgL[i].File_Type = "Video";
                        File_Type = 2;
                    }                  
                }
               
                var AI_View = CollectionViewSource.GetDefaultView(MimgL);
                AI_View.SortDescriptions.Add(new SortDescription("imageCapturedDate", ListSortDirection.Descending));
                AIlistView1.ItemsSource = AI_View;
                
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
            }
        }

        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
            }
        }

        private void btnAI_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CloseAllWindows();
                MLA_Aerodyne.BusinessAccessLayer.AIListData aiList = ((Button)sender).DataContext as MLA_Aerodyne.BusinessAccessLayer.AIListData;
                string aiID = aiList.ID.ToString();
                string Mission_Name = aiList.Mission_Name;
                string Paddock_id = aiList.Paddock_Id;
                string Paddock_name = aiList.Paddock_Name;
                string File_Captured_Date = aiList.imageCapturedDate.ToString();
                string Mission_Type = aiList.Mission_Type;
                int import = 0;


                if (Mission_Type == "Detect and Count")
                {
                    if (File_Type == 1)
                    {
                        var arr = new string[] { Mission_Name.Replace(" ", ""), Paddock_name.Replace(" ", ""), File_Captured_Date.Replace(" ", ""), "Image" };

                        string path = Convert.ToString(ConfigurationManager.AppSettings["AI_batfile"].ToString());
                        const string argsSeparator = " ";
                        string args = string.Join(argsSeparator, arr);

                        //Process.Start(path, args);
                        var process = new Process
                        {
                            StartInfo = new ProcessStartInfo
                            {
                                FileName = path,
                                Arguments = args
                                //UseShellExecute = false,
                                //RedirectStandardOutput = true,
                                //RedirectStandardError = true,
                            }
                        };
                        //process.OutputDataReceived += CaptureOutput;
                        //process.ErrorDataReceived += CaptureError;
                        process.Start();
                        //process.BeginOutputReadLine();
                        //process.BeginErrorReadLine();

                        process.WaitForExit();
                        //MessageBox message1 = new MessageBox(strList.ToString());
                        //message1.Show();

                        // string result= process.StandardOutput.ReadToEnd();
                        DataAccessLayer.DAL_SaveAICattleCount SaveCat = new DataAccessLayer.DAL_SaveAICattleCount();
                        import = SaveCat.Select_CattleCount(_UserId, Paddock_id, Paddock_name, File_Captured_Date, Mission_Name, Mission_Type,File_Type);
                        this.Visibility = Visibility.Hidden;
                        MapLoad mview = new MapLoad(_UserId, "", "");
                        mview.Show();
                        ((MapLoad)this.Owner).closebutton();
                        if (import == 1)
                        {
                            MessageBox message = new MessageBox("AI process for Detect and Count images completed.");
                            message.Show();
                            ((MapLoad)this.Owner).closebutton();
                            import = 0;
                            File_Type = 0;
                        }
                        else
                        {
                            MessageBox message = new MessageBox("AI process for Detect and Count images aborted.");
                            message.Show();
                            ((MapLoad)this.Owner).closebutton();
                            File_Type = 0;
                        }
                    }
                    if (File_Type == 2)
                    {
                        var arr = new string[] { Mission_Name.Replace(" ", ""), Paddock_name.Replace(" ", ""), File_Captured_Date.Replace(" ", ""), "Video" };

                        string path = Convert.ToString(ConfigurationManager.AppSettings["AI_batfile_Video"].ToString());
                        const string argsSeparator = " ";
                        string args = string.Join(argsSeparator, arr);

                        //Process.Start(path, args);
                        var process = new Process
                        {
                            StartInfo = new ProcessStartInfo
                            {
                                FileName = path,
                                Arguments = args
                                //UseShellExecute = false,
                                //RedirectStandardOutput = true,
                                //RedirectStandardError = true,
                            }
                        };
                        //process.OutputDataReceived += CaptureOutput;
                        //process.ErrorDataReceived += CaptureError;
                        process.Start();
                        //process.BeginOutputReadLine();
                        //process.BeginErrorReadLine();

                        process.WaitForExit();
                        //MessageBox message1 = new MessageBox(strList.ToString());
                        //message1.Show();

                        // string result= process.StandardOutput.ReadToEnd();
                        DataAccessLayer.DAL_SaveAICattleCount SaveCat = new DataAccessLayer.DAL_SaveAICattleCount();
                        import = SaveCat.Select_CattleCount(_UserId, Paddock_id, Paddock_name, File_Captured_Date, Mission_Name, Mission_Type,File_Type);
                        this.Visibility = Visibility.Hidden;
                        MapLoad mview = new MapLoad(_UserId, "", "");
                        mview.Show();
                        ((MapLoad)this.Owner).closebutton();
                        if (import == 1)
                        {
                            MessageBox message = new MessageBox("AI process for Detect and Count videos completed.");
                            message.Show();
                            ((MapLoad)this.Owner).closebutton();
                            import = 0;
                            File_Type = 0;
                        }
                        else
                        {
                            MessageBox message = new MessageBox("AI process for Detect and Count videos aborted.");
                            message.Show();
                            ((MapLoad)this.Owner).closebutton();
                            File_Type = 0;
                        }
                    }

                }

                if (Mission_Type == "Fence Monitoring")
                {
                    if(File_Type==1)
                    {
                        var arr = new string[] { Mission_Name.Replace(" ", ""), Paddock_name.Replace(" ", ""), File_Captured_Date.Replace(" ", ""), "Image" };
                        //const string path = @"C:\Program Files (x86)\MLA_Aerodyne\MLA_Aerodyne_Setup\test1.bat";
                        string path = Convert.ToString(ConfigurationManager.AppSettings["AI_batfile_Fence"].ToString());
                        const string argsSeparator = " ";
                        string args = string.Join(argsSeparator, arr);

                        //Process.Start(path, args);
                        var process = new Process
                        {
                            StartInfo = new ProcessStartInfo
                            {
                                FileName = path,
                                Arguments = args

                            }
                        };

                        process.Start();
                        process.WaitForExit();
                        DataAccessLayer.DAL_AIFenceMonitoring SaveCat = new DataAccessLayer.DAL_AIFenceMonitoring();
                        import = SaveCat.Select_DamageCount(_UserId, Paddock_id, Paddock_name, File_Captured_Date, Mission_Name, Mission_Type,File_Type);
                        this.Visibility = Visibility.Hidden;
                        MapLoad mview = new MapLoad(_UserId, "", "");
                        mview.Show();
                        ((MapLoad)this.Owner).closebutton();
                        if (import == 1)
                        {
                            MessageBox message = new MessageBox("AI process for Fence Monitoring videos completed.");
                            message.Show();
                            ((MapLoad)this.Owner).closebutton();
                            import = 0;
                            File_Type = 0;
                        }
                        else
                        {
                            MessageBox message = new MessageBox("AI process for Fence Monitoring videos aborted.");
                            message.Show();
                            ((MapLoad)this.Owner).closebutton();
                            File_Type = 0;
                        }
                    }
                        
                if(File_Type==2)
                    {
                        var arr = new string[] { Mission_Name.Replace(" ", ""), Paddock_name.Replace(" ", ""), File_Captured_Date.Replace(" ", ""), "Video" };
                        string path = Convert.ToString(ConfigurationManager.AppSettings["AI_batfile_Fence_Video"].ToString());
                        const string argsSeparator = " ";
                        string args = string.Join(argsSeparator, arr);

                        //Process.Start(path, args);
                        var process = new Process
                        {
                            StartInfo = new ProcessStartInfo
                            {
                                FileName = path,
                                Arguments = args

                            }
                        };

                        process.Start();
                        process.WaitForExit();
                        DataAccessLayer.DAL_AIFenceMonitoring SaveCat = new DataAccessLayer.DAL_AIFenceMonitoring();
                        import = SaveCat.Select_DamageCount(_UserId, Paddock_id, Paddock_name, File_Captured_Date, Mission_Name, Mission_Type,File_Type);
                        this.Visibility = Visibility.Hidden;
                        MapLoad mview = new MapLoad(_UserId, "", "");
                        mview.Show();
                        ((MapLoad)this.Owner).closebutton();
                        if (import == 1)
                        {
                            MessageBox message = new MessageBox("AI process for Fence Monitoring videos completed.");
                            message.Show();
                            ((MapLoad)this.Owner).closebutton();
                            import = 0;
                            File_Type = 0;
                        }
                        else
                        {
                            MessageBox message = new MessageBox("AI process for Fence Monitoring videos aborted.");
                            message.Show();
                            ((MapLoad)this.Owner).closebutton();
                            File_Type = 0;
                        }
                    }
                }

             
                if (Mission_Type == "Weed Monitoring")
                {
                    if(File_Type==1)
                    {
                        var arr = new string[] { Mission_Name.Replace(" ", ""), Paddock_name.Replace(" ", ""), File_Captured_Date.Replace(" ", ""), "Image" };
                        //const string path = @"C:\Program Files (x86)\MLA_Aerodyne\MLA_Aerodyne_Setup\test1.bat";
                        string path = Convert.ToString(ConfigurationManager.AppSettings["AI_batfile_Weed"].ToString());
                        const string argsSeparator = " ";
                        string args = string.Join(argsSeparator, arr);

                        //Process.Start(path, args);
                        var process = new Process
                        {
                            StartInfo = new ProcessStartInfo
                            {
                                FileName = path,
                                Arguments = args
                            }
                        };

                        process.Start();
                        process.WaitForExit();

                        DataAccessLayer.DAL_AIWeedMonitoring SaveCat = new DataAccessLayer.DAL_AIWeedMonitoring();
                        import = SaveCat.Select_WeedCount(_UserId, Paddock_id, Paddock_name, File_Captured_Date, Mission_Name, Mission_Type,File_Type);
                        this.Visibility = Visibility.Hidden;
                        MapLoad mview = new MapLoad(_UserId, "", "");
                        mview.Show();
                        ((MapLoad)this.Owner).closebutton();
                        if (import == 1)
                        {
                            MessageBox message = new MessageBox("AI process for weed monitoring images completed.");
                            message.Show();
                            ((MapLoad)this.Owner).closebutton();
                            import = 0;
                            File_Type = 0;
                        }
                        else
                        {
                            MessageBox message = new MessageBox("AI process for weed monitoring images aborted.");
                            message.Show();
                            ((MapLoad)this.Owner).closebutton();
                            File_Type = 0;
                        }
                    }
                
                    if(File_Type==2)
                    {
                        var arr = new string[] { Mission_Name.Replace(" ", ""), Paddock_name.Replace(" ", ""), File_Captured_Date.Replace(" ", ""), "Video" };
                        //const string path = @"C:\Program Files (x86)\MLA_Aerodyne\MLA_Aerodyne_Setup\test1.bat";
                        string path = Convert.ToString(ConfigurationManager.AppSettings["AI_batfile_Weed_Video"].ToString());
                        const string argsSeparator = " ";
                        string args = string.Join(argsSeparator, arr);

                        //Process.Start(path, args);
                        var process = new Process
                        {
                            StartInfo = new ProcessStartInfo
                            {
                                FileName = path,
                                Arguments = args
                            }
                        };

                        process.Start();
                        process.WaitForExit();

                        DataAccessLayer.DAL_AIWeedMonitoring SaveCat = new DataAccessLayer.DAL_AIWeedMonitoring();
                        import = SaveCat.Select_WeedCount(_UserId, Paddock_id, Paddock_name, File_Captured_Date, Mission_Name, Mission_Type,File_Type);
                        this.Visibility = Visibility.Hidden;
                        MapLoad mview = new MapLoad(_UserId, "", "");
                        mview.Show();
                        ((MapLoad)this.Owner).closebutton();
                        if (import == 1)
                        {
                            MessageBox message = new MessageBox("AI process for weed monitoring videos completed.");
                            message.Show();
                            ((MapLoad)this.Owner).closebutton();
                            import = 0;
                            File_Type = 0;
                        }
                        else
                        {
                            MessageBox message = new MessageBox("AI process for weed monitoring videos aborted.");
                            message.Show();
                            ((MapLoad)this.Owner).closebutton();
                            File_Type = 0;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
            }
        }
        public void CaptureOutput(object sender, DataReceivedEventArgs e)
        {
            ShowOutput(e.Data, ConsoleColor.White);
        }

        public void CaptureError(object sender, DataReceivedEventArgs e)
        {
            ShowOutput(e.Data, ConsoleColor.Red);
        }

        public void ShowOutput(string data, ConsoleColor color)
        {
            if (data != null)
            {
                ConsoleColor oldColor = Console.ForegroundColor;
                Console.ForegroundColor = color;
                Console.WriteLine("Received: {0}", data);
                Console.ForegroundColor = oldColor;
                strList.Add(data);
            }
        }
        protected bool ExecuteBatchFile(string batchFileName, string argumentsToBatchFile)
        {
            string argumentsString = string.Empty;
            try
            {
                //Add up all arguments as string with space separator between the arguments
                if (argumentsToBatchFile != null)
                {

                    argumentsString += " ";
                    argumentsString += argumentsToBatchFile.Replace(" ", "");
                    //argumentsString += "\"";

                }
                //Create process start information
                System.Diagnostics.ProcessStartInfo DBProcessStartInfo = new System.Diagnostics.ProcessStartInfo(batchFileName, argumentsString);

                //Redirect the output to standard window
                DBProcessStartInfo.RedirectStandardOutput = true;

                //The output display window need not be falshed onto the front.
                DBProcessStartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
                DBProcessStartInfo.UseShellExecute = false;

                //Create the process and run it
                System.Diagnostics.Process dbProcess;
                dbProcess = System.Diagnostics.Process.Start(DBProcessStartInfo);

                //Catch the output text from the console so that if error happens, the output text can be logged.
                System.IO.StreamReader standardOutput = dbProcess.StandardOutput;

                /* Wait as long as the DB Backup or Restore or Repair is going on. 
                Ping once in every 2 seconds to check whether process is completed. */
                while (!dbProcess.HasExited)
                    dbProcess.WaitForExit(2000);

                if (dbProcess.HasExited)
                {
                    string consoleOutputText = standardOutput.ReadToEnd();
                    //TODO - log consoleOutputText to the log file.
                }
                return true;
            }
            // Catch the SQL exception and throw the customized exception made out of that
            catch (Exception ex)
            {
                throw ex;
            }
            // Catch all general exceptions

        }

        private void CloseAllWindows()
        {
            try
            {
                for (int intCounter = App.Current.Windows.Count - 1; intCounter > 0; intCounter--)
                    App.Current.Windows[intCounter].Close();
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
            }
        }
    }
    public static class FileSizeFormatter
    {
        // Load all suffixes in an array  
        static readonly string[] suffixes =
        { "Bytes", "KB", "MB", "GB", "TB", "PB" };
        public static string FormatSize(Int64 bytes)
        {
            int counter = 0;
            decimal number = (decimal)bytes;
            while (Math.Round(number / 1024) >= 1)
            {
                number = number / 1024;
                counter++;
            }
            return string.Format("{0:n1}{1}{2}", number, " ", suffixes[counter]);
        }
    }
}
