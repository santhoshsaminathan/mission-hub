﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using Newtonsoft.Json;
using System.Data;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for CLandmarkWindow.xaml
    /// </summary>
    public partial class CLandmarkWindow : Window
    {
        public dynamic Waypoints;

        public string User_Id;

        public string AllWaypoints;

        public string CapImage_Path;

        public string SelectMapArea;

        public string PaddockId;
        string PlotID;
        public string UserId { get; set; }
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        public CLandmarkWindow()
        {
            InitializeComponent();
            NameTxt.Focus();
        }

        public void GetUserIDFromLogin(string cUserId)
        {
            UserId = cUserId;
        }

        public void GetpaddockFromDatabase()
        {
            try
            {
                DataAccessLayer.DAL_CreateLankmark DAL_CreateLankmark = new DataAccessLayer.DAL_CreateLankmark();
                DataSet ds_Combo = DAL_CreateLankmark.GetLandmarkFromDB(User_Id);
                LandmarkListBox.ItemsSource = ds_Combo.Tables[0].DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "CLandmarkWindow", User_Id);
                return;
            }
        }

        public void ValueFromMapLoad(string UserId, string Waypoint, string CapImagePath, string Paddock_Id)
        {
            try
            {
                Waypoints = JsonConvert.DeserializeObject(Waypoint);

                AllWaypoints = Waypoint;

                User_Id = UserId;

                CapImage_Path = CapImagePath;

                PaddockId = Paddock_Id;

                DataAccessLayer.DAL_CreatePaddock DAL_CreatePaddock = new DataAccessLayer.DAL_CreatePaddock();
                string LoadMapName = DAL_CreatePaddock.GetSelectPaddockNameFromDB(Paddock_Id, User_Id);
                PlotID= DAL_CreatePaddock.GetPlotIDFromDB(Paddock_Id, User_Id);

                foreach (var val in Waypoints)
                {
                    string Name = val.name;
                    string CreatedBy = val.CreatedBy;
                    string[] points = val.points;
                }

                MapAreaNameTxt.Text = LoadMapName;

                GetpaddockFromDatabase();

                //GetMapAreaFromDatabase();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "CLandmarkWindow", User_Id);
                return;
            }
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (NameTxt.Text != "")
                {
                    DateTime now = DateTime.Now;
                    string CreatedOn = now.ToString();
                    string LandmarkName = NameTxt.Text;
                    DataAccessLayer.DAL_CreateLankmark DAL_CreateLankmark = new DataAccessLayer.DAL_CreateLankmark();
                    DAL_CreateLankmark.Save_CreateLandmark(User_Id, LandmarkName, AllWaypoints, CapImage_Path, PaddockId,PlotID);
					this.Visibility = Visibility.Hidden;
					MessageBox message = new MessageBox("Landmark saved successfully.");
					message.Show();
                    ((MapLoad)this.Owner).clearLandmark();
                }
                else
                {
                    MessageBox message = new MessageBox("Please enter landmark name.");
					message.Show();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "CLandmarkWindow", User_Id);
                return;
            }
        }

        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NameTxt.Text = "";
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).clearLandmark();
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "CLandmarkWindow", User_Id);
            }
        }
    }
}
