﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for VMapAreaWindow.xaml
    /// </summary>
    public partial class VMapAreaWindow : Window
    {
        string userID = "";
        public string ViewMaparea;

        public VMapAreaWindow(string Vmap)
        {
            InitializeComponent();
            userID = Vmap;
            BindComboBox(Vmap);
        }

        public void BindComboBox(string vbuserID)
        {
            try
            {
                DataAccessLayer.ViewMaparea vmap = new DataAccessLayer.ViewMaparea();
                DataSet Vds = new DataSet();
                Vds = vmap.BindCombobox_ViewMaparea(vbuserID);
                MapareaCombo.DataContext = Vds.Tables[0].DefaultView;
                MapareaCombo.DisplayMemberPath = "Plot_Name";
                MapareaCombo.SelectedValuePath = "Plot_ID";
            }
            catch(Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return;
            }
        }

        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ViewMaparea = MapareaCombo.SelectedValue.ToString();
                string MenuFlag = "editMaparea";
                if (MapareaCombo.Text != "")
                {
                    CloseAllWindows();
                    if (MapareaCombo.Text != "")
                    {
                        MapLoad mview = new MapLoad(userID, ViewMaparea, MenuFlag);
                        mview.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return;
            }
        }

        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }

        private void MapareaCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            MapareaCheckbox.Content = "Disable";
        }

        private void MapareaCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            MapareaCheckbox.Content = "Enable";
        }

        private void MapareaCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                string chosenValue = MapareaCombo.Text;
                string v = MapareaCombo.SelectedValue.ToString();
                DataAccessLayer.ViewMaparea imgObj = new DataAccessLayer.ViewMaparea();
                string imgSource = imgObj.getI_magePath(v);
                if(!String.IsNullOrEmpty(imgSource))
                {
                    Uri resourceUri = new Uri(imgSource, UriKind.Absolute);
                    imgMaparea.Source = new BitmapImage(resourceUri);
                }
                else
                {
                    imgMaparea.Source = new BitmapImage(new Uri(System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImgNull"].ToString())));
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return;
            }
        }

        private void ViewBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ViewMaparea = MapareaCombo.SelectedValue.ToString();
                string MenuFlag = "VMaparea";
                if (MapareaCombo.Text != "")
                {
                    CloseAllWindows();
                    if (MapareaCombo.Text != "")
                    {
                        MapLoad mview = new MapLoad(userID, ViewMaparea, MenuFlag);
                        mview.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return;
            }
        }

        private void CloseAllWindows()
        {
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > 0; intCounter--)
                App.Current.Windows[intCounter].Close();
        }
    }
}
