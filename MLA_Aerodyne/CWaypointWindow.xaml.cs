﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Globalization;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for CWaypointWindow.xaml
    /// </summary>
    public partial class CWaypointWindow : Window
    {
        public dynamic Waypoints;
        public string User_Id;
        public string AllWaypoints;
        public string _missiontype;
        public string CapImage_Path;
        public string SelectPaddock;
        public string Paddock_Id;
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        private const double EarthRadius = 3958.756;
        public string UserId { get; set; }
        public CWaypointWindow()
        {
            InitializeComponent();

            NameTxt.Focus();
        }

        public void GetUserIDFromLogin(string cUserId)
        {
            UserId = cUserId;
        }

        public void GetpaddockFromDatabase()
        {
            try
            {
                DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                DataSet ds_Combo = DAL_CreateWayPoint.GetWayPointsFromDB(User_Id);
                WaypointListBox.ItemsSource = ds_Combo.Tables[0].DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: "+ ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "CWaypointWindow", User_Id);
                return;
            }
        }

        public void ValueFromMapLoad(string UserId, string Waypoint, string CapImagePath, string PaddockId,string missiontype)
        {
            try
            {
                Waypoints = JsonConvert.DeserializeObject(Waypoint);
                AllWaypoints = Waypoint;
                User_Id = UserId;
                _missiontype = missiontype;
                CapImage_Path = CapImagePath;
                Paddock_Id = PaddockId;

                DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                string LoadPaddockName = DAL_CreateWayPoint.GetSelectPaddockNameFromDB(PaddockId);
                SelectPaddock = PaddockId;

                PaddockNameTxt.Text = LoadPaddockName;

                GetpaddockFromDatabase();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: "+ ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "CWaypointWindow", User_Id);
                return;
            }
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (NameTxt.Text != "")
                {
                    DateTime now = DateTime.Now;
                    string CreatedOn = now.ToString();
                    string WaypointName = NameTxt.Text;
                    dynamic Way_Points = JsonConvert.DeserializeObject(AllWaypoints);
                    IList<PointLatLng> coordinates = new List<PointLatLng>();
                    foreach (var val in Way_Points)
                    {
                        if(val != null)
                        {
                            PointLatLng pointLatLng = new PointLatLng();
                            pointLatLng.Latitude = val.lat;
                            pointLatLng.Longitude = val.lng;
                            coordinates.Add(pointLatLng);
                        }
                    }

                    var latlon1 = coordinates.OfType<PointLatLng>().First();
                    var latlon2 = coordinates.OfType<PointLatLng>().Last();
                    double lat1 = Convert.ToDouble(latlon1.Latitude);
                    double lon1 = Convert.ToDouble(latlon1.Longitude);
                    double lat2 = Convert.ToDouble(latlon2.Latitude);
                    double lon2 = Convert.ToDouble(latlon2.Longitude);
                    double distance = Haversine(lat1, lon1, lat2, lon2);
                    double distancekm = ConvertDistance.ConvertMilesToKilometers(distance);

                    distancekm = Math.Round(distancekm, 2);
                    double area = MLA_Aerodyne.DataAccessLayer.CalculateAreaOfPolygon.ComputeSignedArea(coordinates);
                    area = Math.Round(area, 2);
                    area = Math.Abs(area);
                    area = area / (double)10000;
                    NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
                    nfi.NumberDecimalSeparator = " ";
                    string _area = area.ToString("N", nfi);
                    DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                    DAL_CreateWayPoint.Save_CreateWayPoint(User_Id, WaypointName, AllWaypoints, CapImage_Path, SelectPaddock,_area.Replace(" ", "."), distancekm.ToString(), _missiontype);
                    this.Visibility = Visibility.Hidden;
					MessageBox message = new MessageBox("Waypoint saved successfully.");
					message.Show();
                    ((MapLoad)this.Owner).clearWaypoint();
                }
                else
                {
                    MessageBox message = new MessageBox("Please enter waypoint name.");
					message.Show();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: "+ ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "CWaypointWindow", User_Id);
                return;
            }
        }

        private double Haversine(double lat1, double lon1, double lat2, double lon2)
        {
            double dlat = DegreesToRadians(lat2 - lat1);
            double dlon = DegreesToRadians(lon2 - lon1);
            double a = Math.Sin(dlat / 2) * Math.Sin(dlat / 2) +
                Math.Cos(DegreesToRadians(lat1)) *
                Math.Cos(DegreesToRadians(lat2)) *
                Math.Sin(dlon / 2) * Math.Sin(dlon / 2);
            return 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a)) *
                EarthRadius;
        }

        // Convert degrees into radians.
        private double DegreesToRadians(double degrees)
        {
            return degrees / 180 * Math.PI;
        }


        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            NameTxt.Text = "";
            this.Visibility = Visibility.Hidden;
            ((MapLoad)this.Owner).clearWaypoint();
        }
    }
}
