﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Management;
using System.Windows.Interop;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Configuration;
using Newtonsoft.Json;
using System.Net.Http;
using System.Data.SQLite;
using System.Data.Odbc;
using MLA_Aerodyne.BusinessAccessLayer;
using CsvHelper;
using Microsoft.VisualBasic.FileIO;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for MissionHub.xaml
    /// </summary>
    public partial class MissionHub : Window
    {
        string missionUserid;
        string MissionName;
        string missionString;
        string PaddockId;
        string _userID;
        string WaypointId;
        int _status;
        int _Mission_ID;
        string plotID;
        DataSet VdsPaddock = new DataSet();
        DataSet VMissionds = new DataSet();
        DataSet VdsMaparea = new DataSet();
        DataSet VdsWaypoint = new DataSet();
        DataSet Vdslandmark = new DataSet();
        DataSet VdsPilot = new DataSet();
        DataSet vdsMissionList = new DataSet();
        DataSet vdsMissionListData = new DataSet();
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();
        DataAccessLayer.ViewMissionType Objvm = new DataAccessLayer.ViewMissionType();
        DataTable dt = new DataTable();
        BuisnessAccessLayer.MissionHubDetails missionObj = new BuisnessAccessLayer.MissionHubDetails();
        public MissionHub(string userID, string Mission_Name, string padID, int status)
        {
            InitializeComponent();
            try
            {
                string UnikKey = Objvm.Mission_Count();
                MissionName = Mission_Name;
                missionUserid = userID;
                _status = status;
                PaddockId = padID;
                _userID = userID;
                BindComboBox_Maparea(userID);
                BindComboBox_Import_Paddock(userID);
                MissionNametxt.Text = UnikKey;
                //MmapareaCombo.SelectedIndex = 1;
               // this.DataContext = this;

                if (MissionName == "6")
                {
                    BindComboBox_Missiontype();
                }
                else
                {
                    MmissiontypeCombo.Visibility = Visibility.Hidden;
                    Mmissiontypetext.Visibility = Visibility.Visible;
                    int caseSwitch = Convert.ToInt32(Mission_Name);
                    switch (caseSwitch)
                    {
                        case 1:
                            missionString = "LoRa Gateway";
                            break;
                        case 2:
                            missionString = "Detect & Count";
                            break;
                        case 3:
                            missionString = "Fence Monitoring";
                            break;

                        case 4:
                            missionString = "Water Monitoring";
                            break;
                        case 5:
                            missionString = "Weed Monitoring";
                            break;
                        default:
                            missionString = "";
                            break;
                    }
                    Mmissiontypetext.Text = missionString;
                }


                BindComboBox_Pilotdetails();
                Createdontxt.Text = DateTime.Now.ToString();
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", userID);
            }
        }

        private void MyComboBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            MmapareaCombo.IsDropDownOpen = true;
        }

        public static string GetUniqueKey(int maxSize)
        {
            try
            {
                char[] chars = new char[62];
                chars = "123456789".ToCharArray();
                byte[] data = new byte[1];
                RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
                StringBuilder result = new StringBuilder(maxSize);
                foreach (byte b in data)
                {
                    result.Append(chars[b % (chars.Length)]);
                }
                return result.ToString();
            }
            
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }
      

        public void BindComboBox_Import_Paddock(string vbuserID)
        {
            try
            {
                DataAccessLayer.ViewPaddock vpaddock = new DataAccessLayer.ViewPaddock();
                VdsPaddock = vpaddock.BindCombobox_Import_Paddock(vbuserID);
                importPaddockCombo.DataContext = VdsPaddock.Tables[0].DefaultView;
                importPaddockCombo.DisplayMemberPath = "Paddock_Name";
                importPaddockCombo.SelectedValuePath = "Paddock_Id";
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        public void BindComboBox_Landmark(string vbuserID, string plotID)
        {
            try
            {
                DataAccessLayer.ViewLandMark vlandmark = new DataAccessLayer.ViewLandMark();
                Vdslandmark = vlandmark.BindCombobox_Landmark(vbuserID, plotID);
                MlandmarkCombo.DataContext = Vdslandmark.Tables[0].DefaultView;
                MlandmarkCombo.DisplayMemberPath = "Landmark_Name";
                MlandmarkCombo.SelectedValuePath = "LM_ID";
            }
           
             catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        public void BindComboBox_Paddock(string vbuserID, string padID)
        {
            try
            {
                if (_status == 0)
                {
                    DataAccessLayer.ViewPaddock vpaddock = new DataAccessLayer.ViewPaddock();
                    VdsPaddock = vpaddock.BindCombobox_Paddock(vbuserID, padID);
                    MpaddockCombo.DataContext = VdsPaddock.Tables[0].DefaultView;
                    MpaddockCombo.DisplayMemberPath = "Paddock_Name";
                    MpaddockCombo.SelectedValuePath = "Paddock_Id";
                }
                else
                {
                    DataAccessLayer.ViewPaddock vpaddock = new DataAccessLayer.ViewPaddock();
                    VdsPaddock = vpaddock.Bind_Paddock(vbuserID, PaddockId);
                    MpaddockCombo.DataContext = VdsPaddock.Tables[0].DefaultView;
                    MpaddockCombo.DisplayMemberPath = "Paddock_Name";
                    MpaddockCombo.SelectedValuePath = "Paddock_Id";
                    MpaddockCombo.Visibility = Visibility.Hidden;
                    paddocktypetext.Text = VdsPaddock.Tables[0].Rows[0]["Paddock_Name"].ToString();
                    paddocktypetext.Visibility = Visibility.Visible;
                    plotID = VdsPaddock.Tables[0].Rows[0]["Plot_ID"].ToString();

                    if (plotID != null)
                    {
                        DataAccessLayer.ViewMaparea vmap = new DataAccessLayer.ViewMaparea();
                        VdsMaparea = vmap.Bind_Maparea(vbuserID, plotID);
                        MmapareaCombo.Visibility = Visibility.Hidden;
                        Maptypetext.Text = VdsMaparea.Tables[0].Rows[0]["Plot_Name"].ToString();
                        Maptypetext.Visibility = Visibility.Visible;
                    }
                }
            }
           
             catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        public void BindComboBox_Maparea(string vbuserID)
        {
            try
            {
                DataAccessLayer.ViewMaparea vmap = new DataAccessLayer.ViewMaparea();
                VdsMaparea = vmap.BindCombobox_ViewMaparea(vbuserID);
                MmapareaCombo.DataContext = VdsMaparea.Tables[0].DefaultView;
                MmapareaCombo.DisplayMemberPath = "Plot_Name";
                MmapareaCombo.SelectedValuePath = "Plot_ID";
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }
        public void BindComboBox_Waypoint(string vbuserID, string padID)
        {
            try
            {
                MLA_Aerodyne.BuisnessAccessLayer.UserDetails Vuserdetails = new MLA_Aerodyne.BuisnessAccessLayer.UserDetails();
                DataAccessLayer.ViewWaypoint vway = new DataAccessLayer.ViewWaypoint();
                VdsWaypoint = vway.BindCombobox_ViewWaypoint(vbuserID, padID);
                MwaypointCombo.DataContext = VdsWaypoint.Tables[0].DefaultView;
                MwaypointCombo.DisplayMemberPath = "WayPoint_Name";
                MwaypointCombo.SelectedValuePath = "WayPoint_Id";
            }
           
             catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }
        public void BindComboBox_Missiontype()
        {
            try
            {
                MLA_Aerodyne.BuisnessAccessLayer.UserDetails Vuserdetails = new MLA_Aerodyne.BuisnessAccessLayer.UserDetails();
                DataAccessLayer.ViewMissionType vMission = new DataAccessLayer.ViewMissionType();
                VMissionds = vMission.BindCombobox_ViewMission();
                MmissiontypeCombo.DataContext = VMissionds.Tables[0].DefaultView;
                MmissiontypeCombo.DisplayMemberPath = "MissionType";
                MmissiontypeCombo.SelectedValuePath = "MisMaster_ID";
                MmissiontypeCombo.SelectedIndex = 0;
            }
            
             catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);

            }
        }

        public void BindComboBox_Missionselect()
        {
            try
            {
                DataAccessLayer.ViewMissionType gMission = new DataAccessLayer.ViewMissionType();
                string _mission_Select = gMission.GetMission(missionObj.Waypoint_ID.ToString());
                if(_mission_Select== "Customize")
                {
                    BindComboBox_Missiontype();
                }
                else
                {
                    DataAccessLayer.ViewMissionType vMission = new DataAccessLayer.ViewMissionType();
                    VMissionds = vMission.BindCombobox_SelectMission(_mission_Select);                   
                    MmissiontypeCombo.DataContext = VMissionds.Tables[0].DefaultView;
                    MmissiontypeCombo.DisplayMemberPath = "MissionType";
                    MmissiontypeCombo.SelectedValuePath = "MisMaster_ID";
                    MmissiontypeCombo.SelectedIndex = 0;
                    MmissiontypeCombo.IsEditable = false;
                }
               
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }
        public void BindComboBox_Pilotdetails()
        {
            try
            {
                MLA_Aerodyne.BuisnessAccessLayer.UserDetails Vuserdetails = new MLA_Aerodyne.BuisnessAccessLayer.UserDetails();
                DataAccessLayer.ViewPilotDetails vPilot = new DataAccessLayer.ViewPilotDetails();
                VdsPilot = vPilot.BindCombobox_ViewPilotDetails();
                MpilotCombo.DataContext = VdsPilot.Tables[0].DefaultView;
                MpilotCombo.DisplayMemberPath = "Pilot_Name";
                MpilotCombo.SelectedValuePath = "Pilot_Id";
            }
            
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }
        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            ((MapLoad)this.Owner).closebutton();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            ((MapLoad)this.Owner).closebutton();
        }

        private void createMissionBtn_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                importLoraCanvas.Visibility = Visibility.Hidden;
                viewMissionCanvas.Visibility = Visibility.Hidden;
                createMissionCanvas.Visibility = Visibility.Visible;
                ExportcsvCanvas.Visibility = Visibility.Hidden;
                string UnikKey = Objvm.Mission_Count();
                MissionNametxt.Text = UnikKey;
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        private void CreateMissionBtn_MouseEnter(object sender, MouseEventArgs e)
        {
            try
            {
                string UnikKey = Objvm.Mission_Count();
                MissionNametxt.Text = UnikKey;
                datePicker1.Text = "";
                alertTextbox.Text = "";
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        private void missionSaveBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MissionNametxt.Text != "" && datePicker1.SelectedDate != null)
                {
                    missionObj.userID = Convert.ToInt32(missionUserid);
                    missionObj.Mission_Name = MissionNametxt.Text;
                    missionObj.CreatedOn = Convert.ToDateTime(Createdontxt.Text);
                    missionObj.DateOfFlight = Convert.ToDateTime(datePicker1.Text);
                    missionObj.Alert = alertTextbox.Text;
                    missionObj.Status = 1;
                    missionObj.Plot_ID = Convert.ToInt32(MmapareaCombo.SelectedValue);
                    missionObj.Paddock_ID = Convert.ToInt32(MpaddockCombo.SelectedValue);
                    missionObj.Waypoint_ID = Convert.ToInt32(MwaypointCombo.SelectedValue);
                    missionObj.Pilot_ID = Convert.ToInt32(MpilotCombo.SelectedValue);
                    if (MissionName == "6")
                    {
                        missionObj.Flying_ID = Convert.ToInt32(MmissiontypeCombo.SelectedValue);
                    }
                    else
                    {
                        missionObj.Flying_ID = Convert.ToInt32(MissionName);
                    }
                    DataAccessLayer.ViewMissionType vMt = new DataAccessLayer.ViewMissionType();
                    int missionSave = vMt.Insert_CreateMission(missionObj);
                    if (missionSave != 0)
                    {
                        MessageBox message = new MessageBox("Mission saved successfully.");
                        message.Show();
                        ((MapLoad)this.Owner).closebutton();
                        string UnikKey = Objvm.Mission_Count();
                        MissionNametxt.Text = UnikKey;
                        datePicker1.Text = "";
                        alertTextbox.Text = "";
                        this.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        MessageBox message = new MessageBox("Mission not saved.");
                        message.Show();
                        this.Visibility = Visibility.Hidden;
                        ((MapLoad)this.Owner).closebutton();
                    }
                }
                else
                {
                    if (MissionNametxt.Text == "")
                    {
                        MessageBox message = new MessageBox("Please enter the mission name.");
                        message.Show();
                    }
                    if (datePicker1.SelectedDate == null)
                    {
                        MessageBox message = new MessageBox("Please select the date of flight.");
                        message.Show();
                    }
                }
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        private void MmapareaCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                missionObj.Plot_ID = Convert.ToInt32(MmapareaCombo.SelectedValue);
                BindComboBox_Paddock(missionUserid, Convert.ToString(missionObj.Plot_ID));
                MpaddockCombo.SelectedIndex = 0;
                MwaypointCombo.SelectedIndex = 0;
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        private void MpaddockCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                missionObj.Paddock_ID = Convert.ToInt32(MpaddockCombo.SelectedValue);
                BindComboBox_Waypoint(missionUserid, Convert.ToString(missionObj.Paddock_ID));
                MwaypointCombo.SelectedIndex = 0;
                //missionObj.Landmark_ID = Convert.ToInt32(MlandmarkCombo.SelectedValue);
                BindComboBox_Landmark(missionUserid, Convert.ToString(missionObj.Paddock_ID));
                MlandmarkCombo.SelectedIndex = 0;
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        private void MwaypointCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                missionObj.Waypoint_ID = Convert.ToInt32(MwaypointCombo.SelectedValue);
                BindComboBox_Missionselect();
            }
           
              catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        private void MmissiontypeCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                missionObj.Mission_ID = Convert.ToInt32(MmissiontypeCombo.SelectedValue);
            }
          
             catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        private void MpilotCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                missionObj.Pilot_ID = Convert.ToInt32(MpilotCombo.SelectedValue);
            }
       
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        private void ViewMissionBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                importLoraCanvas.Visibility = Visibility.Hidden;
                createMissionCanvas.Visibility = Visibility.Hidden;
                viewMissionCanvas.Visibility = Visibility.Visible;
                ExportcsvCanvas.Visibility = Visibility.Hidden;
                string UnikKey = Objvm.Mission_Count();
                MissionNametxt.Text = UnikKey;
                datePicker1.Text = "";
                alertTextbox.Text = "";
                DataAccessLayer.ViewMissionType listMission = new DataAccessLayer.ViewMissionType();
                vdsMissionList = listMission.BindListBox_ViewMission(missionUserid);
                missionListbx.DataContext = vdsMissionList.Tables[0].DefaultView;
                missionListbx.DisplayMemberPath = "Mission_Name";
                missionListbx.SelectedValuePath = "Mission_ID";
                if (this.missionListbx.Items.Count > 0)
                    this.missionListbx.SelectedIndex = 0;
                int iMission = Convert.ToInt32(missionListbx.SelectedValue);
                MissionView_List(iMission);
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        private void MissionView_List(int iMission)
        {
            missionDatatxtblk.Text = "";
            try
            {
                if (iMission != 0)
                {
                    DataAccessLayer.ViewMissionType missionData = new DataAccessLayer.ViewMissionType();
                    MLA_Aerodyne.BuisnessAccessLayer.MissionHubDetails mdetails = new BuisnessAccessLayer.MissionHubDetails();
                    vdsMissionListData = missionData.BindTextBlock_ViewMission(iMission);
                    if(vdsMissionListData.Tables[0].Rows.Count > 0)
                    {
                    mdetails.Mission_ID = Convert.ToInt32( vdsMissionListData.Tables[0].Rows[0]["Mission_ID"]);
                    _Mission_ID = mdetails.Mission_ID;
                    mdetails.Mission_Name = vdsMissionListData.Tables[0].Rows[0]["Mission_Name"].ToString();
                    mdetails.Mission_Type = vdsMissionListData.Tables[0].Rows[0]["MissionType"].ToString();
                    mdetails.Paddock_Name = vdsMissionListData.Tables[0].Rows[0]["Paddock_Name"].ToString();
                    mdetails.Waypoint_Name = vdsMissionListData.Tables[0].Rows[0]["WayPoint_Name"].ToString();
                    mdetails.Pilot_Name = vdsMissionListData.Tables[0].Rows[0]["Pilot_Name"].ToString();
                    mdetails.DateOfFlight = Convert.ToDateTime(vdsMissionListData.Tables[0].Rows[0]["DateOfFlight"]);
                    mdetails.File_Path = vdsMissionListData.Tables[0].Rows[0]["Csv_FilePath"].ToString();
                    mdetails.Alert = vdsMissionListData.Tables[0].Rows[0]["Alert"].ToString();
                    mdetails.Paddock_ID = Convert.ToInt32(vdsMissionListData.Tables[0].Rows[0]["Paddock_Id"]);
                    WaypointId = vdsMissionListData.Tables[0].Rows[0]["WayPoint_Id"].ToString();
                    PaddockId = vdsMissionListData.Tables[0].Rows[0]["Paddock_Id"].ToString();
                    missionDatatxtblk.TextWrapping = missionDatatxtblk.TextWrapping = TextWrapping.Wrap;
                   
                    missionDatatxtblk.FontSize = 14; // 24 points
                    missionDatatxtblk.Inlines.Add(new Run("\n"));
                    missionDatatxtblk.Inlines.Add(new Run("                         "));
                    missionDatatxtblk.Inlines.Add(new Bold((new Underline(new Run(mdetails.Mission_Name)))));
                    missionDatatxtblk.Inlines.Add(new Run("\n"));
                    missionDatatxtblk.Inlines.Add(new Run("\n"));
                    missionDatatxtblk.Inlines.Add(new Run("  "));
                    missionDatatxtblk.Inlines.Add(new Bold((new Underline(new Run("Mission Type:")))));
                    missionDatatxtblk.Inlines.Add(new Run("  "));
                    missionDatatxtblk.Inlines.Add(new Run(mdetails.Mission_Type));
                    missionDatatxtblk.Inlines.Add(new Run("\n"));
                    missionDatatxtblk.Inlines.Add(new Run("\n"));
                    missionDatatxtblk.Inlines.Add(new Run("  "));
                    missionDatatxtblk.Inlines.Add(new Bold((new Underline(new Run("Paddock Name:")))));
                    missionDatatxtblk.Inlines.Add(new Run("  "));
                    missionDatatxtblk.Inlines.Add(new Run(mdetails.Paddock_Name));
                    missionDatatxtblk.Inlines.Add(new Run("\n"));
                    missionDatatxtblk.Inlines.Add(new Run("\n"));
                    missionDatatxtblk.Inlines.Add(new Run("  "));
                    missionDatatxtblk.Inlines.Add(new Bold((new Underline(new Run("Waypoint Name:")))));
                    missionDatatxtblk.Inlines.Add(new Run("  "));
                    missionDatatxtblk.Inlines.Add(new Run(mdetails.Waypoint_Name));
                    missionDatatxtblk.Inlines.Add(new Run("\n"));
                    missionDatatxtblk.Inlines.Add(new Run("\n"));
                    missionDatatxtblk.Inlines.Add(new Run("  "));
                    missionDatatxtblk.Inlines.Add(new Bold((new Underline(new Run("Pilot Name:")))));
                    missionDatatxtblk.Inlines.Add(new Run("  "));
                    missionDatatxtblk.Inlines.Add(new Run(mdetails.Pilot_Name));
                    missionDatatxtblk.Inlines.Add(new Run("\n"));
                    missionDatatxtblk.Inlines.Add(new Run("\n"));
                    missionDatatxtblk.Inlines.Add(new Run("  "));
                    missionDatatxtblk.Inlines.Add(new Bold((new Underline(new Run("Date of Flight:")))));
                    missionDatatxtblk.Inlines.Add(new Run("  "));
                    missionDatatxtblk.Inlines.Add(new Run(mdetails.DateOfFlight.ToString()));
                    missionDatatxtblk.Inlines.Add(new Run("\n"));
                    missionDatatxtblk.Inlines.Add(new Run("\n"));
                    missionDatatxtblk.Inlines.Add(new Run("  "));
                    missionDatatxtblk.Inlines.Add(new Bold((new Underline(new Run("Alert:")))));
                    missionDatatxtblk.Inlines.Add(new Run("  "));
                    missionDatatxtblk.Inlines.Add(new Run(mdetails.Alert));
                    missionDatatxtblk.Inlines.Add(new Run("\n"));
                    missionDatatxtblk.Inlines.Add(new Run("\n"));
                    missionDatatxtblk.Inlines.Add(new Run("  "));
                    }
                }
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }
        private void MissionListbx_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                int iMission = Convert.ToInt32(missionListbx.SelectedValue);
                MissionView_List(iMission);
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        private void GenerateMissionCombobx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (generateMissionCombobx.Text.ToString() != "")
                {
                    missionObj.Mission_ID = Convert.ToInt32(generateMissionCombobx.SelectedValue);
                }
            }
            
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }
        private void CloseAllWindows()
        {
            try
            {
                for (int intCounter = App.Current.Windows.Count - 1; intCounter > 0; intCounter--)
                    App.Current.Windows[intCounter].Close();
            }

             catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }
        private void ExportBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string UnikKey = Objvm.Mission_Count();
                MissionNametxt.Text = UnikKey;
                int iMission = Convert.ToInt32(generateMissionCombobx.SelectedValue);
                DataAccessLayer.ViewMissionType missionData = new DataAccessLayer.ViewMissionType();
                MLA_Aerodyne.BuisnessAccessLayer.MissionHubDetails mdetails = new BuisnessAccessLayer.MissionHubDetails();
                vdsMissionListData = missionData.ExportCSV_ViewMission(iMission);
                DataTable csvDatatable = vdsMissionListData.Tables[0];
                csvDatatable.ToCSV();
                MessageBox message = new MessageBox("CSV file saved successfully.");
                message.Show();
                datePicker1.Text = "";
                alertTextbox.Text = "";
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }
           
             catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }
        private void BindMissionExport()
        {
            try
            {
                DataAccessLayer.ViewMissionType listMission = new DataAccessLayer.ViewMissionType();
                vdsMissionList = listMission.BindListBox_ViewMission(missionUserid);
                generateMissionCombobx.DataContext = vdsMissionList.Tables[0].DefaultView;
                generateMissionCombobx.DisplayMemberPath = "Mission_Name";
                generateMissionCombobx.SelectedValuePath = "Mission_ID";
            }
  
             catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        private void ExportmainBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                importLoraCanvas.Visibility = Visibility.Hidden;
                viewMissionCanvas.Visibility = Visibility.Hidden;
                createMissionCanvas.Visibility = Visibility.Hidden;
                ExportcsvCanvas.Visibility = Visibility.Visible;
                BindMissionExport();
                generateMissionCombobx.SelectedIndex = 0;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        private void missionListbx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //int iMission = Convert.ToInt32(((DataRowView)missionListbx.SelectedValue)["Mission_ID"]);
            //MissionView_List(iMission);
        }

        private void configureMissionBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnSimulate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CloseAllWindows();
                string MenuFlag = "SimulateFlight";
                string wayID = WaypointId;
                MapLoad mview = new MapLoad(missionUserid, wayID, MenuFlag);
                mview.Show();
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }
        
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        private void MlandmarkCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                missionObj.Landmark_ID = Convert.ToInt32(MlandmarkCombo.SelectedValue);
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        private void importMainBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                viewMissionCanvas.Visibility = Visibility.Hidden;
                createMissionCanvas.Visibility = Visibility.Hidden;
                ExportcsvCanvas.Visibility = Visibility.Hidden;
                importLoraCanvas.Visibility = Visibility.Visible;
                string UnikKey = Objvm.Mission_Count();
                MissionNametxt.Text = UnikKey;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        private void importPaddockCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private static DataTable GetDataTabletFromCSVFile(string csv_file_path)
        {
            DataTable csvData = new DataTable();
            try
            {
                using (TextFieldParser csvReader = new TextFieldParser(csv_file_path))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        DataColumn datecolumn = new DataColumn(column);
                        datecolumn.AllowDBNull = true;
                        csvData.Columns.Add(datecolumn);
                    }

                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        //Making empty value as null

                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = null;
                            }
                        }
                        csvData.Rows.Add(fieldData);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return csvData;
        }

        private void browsedrive_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                OpenFileDialog op = new OpenFileDialog();
                op.Title = "Select a CSV file";
                op.Filter = "csv files (*.csv)|*.csv";

                if (op.ShowDialog() == true)
                {
                    directorytext.Text = op.FileName;
                    string path = op.FileName;
                    string csv_file_path = path;

                    string csvData = File.ReadAllText(csv_file_path);

                    dt.Columns.AddRange(new DataColumn[10] { new DataColumn("Date and Time", typeof(string)),
                 new DataColumn("ignore1", typeof(string)),
                 new DataColumn("ignore2", typeof(string)),
                 new DataColumn("RSSI",typeof(string)),
                 new DataColumn("Temperature", typeof(string)),
                 new DataColumn("DO", typeof(string)),
                 new DataColumn("EC", typeof(string)),
                 new DataColumn("pH", typeof(string)),
                 new DataColumn("WaterLevel", typeof(string)),
                 new DataColumn("ignore3", typeof(string))
               });

                    //Execute a loop over the rows.
                    foreach (string row in csvData.Split('\n'))
                    {
                        if (!string.IsNullOrEmpty(row))
                        {
                            dt.Rows.Add();
                            int i = 0;

                            //Execute a loop over the columns.
                            foreach (string cell in row.Split(','))
                            {
                                dt.Rows[dt.Rows.Count - 1][i] = cell;
                                i++;
                            }
                        }
                    }
                }
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        private void importBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MLA_Aerodyne.BuisnessAccessLayer.Lora_Water LW = new BuisnessAccessLayer.Lora_Water();

                int import = 0;

                foreach (DataRow row in dt.Rows)
                {
                    LW.Time = row["Date and Time"].ToString();
                    LW.DO = row["DO"].ToString();
                    LW.EC = row["EC"].ToString();
                    LW.pH = row["pH"].ToString();
                    LW.Temperature = row["Temperature"].ToString();
                    LW.WaterLevel = row["WaterLevel"].ToString();
                    LW.RSSI = row["RSSI"].ToString();
                    LW.userID = missionUserid;
                    LW.CreatedOn = DateTime.Now;
                    LW.Paddock_ID = importPaddockCombo.SelectedValue.ToString();
                    LW.Paddock_Name = importPaddockCombo.Text;
                    //iterate through the DataTable.

                    DataAccessLayer.ViewMissionType vMt = new DataAccessLayer.ViewMissionType();
                    import = vMt.Insert_Livelora(LW);
                }
                
                if (import != 0)
                {
                    try
                    {
                        string URL = "http://www.google.com/";
                        bool NetConStatus = IsInternetConnection(URL);
                        if (NetConStatus == true)
                            ConnectionStatus(NetConStatus);
                        else
                            ConnectionStatus(NetConStatus);

                        if (Connection_Status == "true")
                        {
                            //string apiUrl = "https://localhost:44361/api/CompareTables";
                            string apiUrl = ConfigurationManager.AppSettings["MLA_WebAPI"].ToString() + "/CompareTables/GetTableValues";
                            WebClient webClient = new WebClient();
                            Stream stream = webClient.OpenRead(apiUrl);
                            StreamReader sr = new StreamReader(stream);
                            string result = string.Empty;

                            string json1 = sr.ReadToEnd().Trim();

                            //var SetValues = JArray.Parse(json1);

                            DataTable dt = JsonConvert.DeserializeObject<DataTable>(json1);

                            DataAccessLayer.DAL_ServerTableStatusCheck ClearAndInsert = new DataAccessLayer.DAL_ServerTableStatusCheck();
                            string StatusVal = ClearAndInsert.ClearServerStatusTable();

                            DataAccessLayer.DAL_ServerTableStatusCheck TableStatus = new DataAccessLayer.DAL_ServerTableStatusCheck();
                            DataTable DS_Values = TableStatus.CheckServerStatus();

                            List<BAl_TableRecords> tableRecords1 = new List<BAl_TableRecords>();

                            List<BAl_TableRecords> tableRecords2 = new List<BAl_TableRecords>();

                            List<BAl_TableRecords> tableRecords_Final = new List<BAl_TableRecords>();

                            if (DS_Values.Rows.Count > 0)
                            {
                                tableRecords1 = (from DataRow row in DS_Values.Rows
                                                 select new BAl_TableRecords
                                                 {
                                                     //Intake
                                                     TABLE_NAME = row["Table_Name"].ToString(),
                                                     TABLE_ROWS = row["Table_LastId"].ToString(),
                                                 }).ToList();

                                tableRecords2 = (from DataRow row in dt.Rows
                                                 select new BAl_TableRecords
                                                 {
                                                     //Intake
                                                     TABLE_NAME = row["TABLE_NAME"].ToString(),
                                                     TABLE_ROWS = row["TABLE_ROWS"].ToString(),
                                                 }).ToList();

                                foreach (var bItem in tableRecords2)
                                {
                                    if (tableRecords1.Any(aItem => bItem.TABLE_ROWS != aItem.TABLE_ROWS && bItem.TABLE_NAME.ToLower() == aItem.TABLE_NAME.ToLower()))
                                    {
                                        tableRecords_Final.Add(bItem);
                                    }
                                }
                            }

                            if (tableRecords_Final.Any(aTable => aTable.TABLE_NAME.ToLower() == "tbl_lg_waterdetails"))
                            {
                                var client = new HttpClient();

                                List<MLA_Aerodyne.BuisnessAccessLayer.waterdetails> EI = new List<MLA_Aerodyne.BuisnessAccessLayer.waterdetails> ();

                                DataSet dsWaterInfo = new DataSet();
                                var result1 = tableRecords_Final.Where(a => a.TABLE_NAME == "tbl_lg_waterdetails").Select(b => b.TABLE_ROWS);

                                foreach (var rowcount in result1)
                                {
                                    SQLiteConnection con = new SQLiteConnection("Data Source = " + System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                                    string strQuery = "select distinct * from tbl_lg_waterdetails where Id> " + rowcount + " and Id != " + rowcount + "";
                                    SQLiteCommand cmd = new SQLiteCommand(strQuery, con);
                                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

                                    da.Fill(dsWaterInfo);

                                    EI = (from DataRow row in dsWaterInfo.Tables[0].Rows
                                          select new MLA_Aerodyne.BuisnessAccessLayer.waterdetails
                                          {
                                              Time = row["Time"].ToString(),
                                              DO = Convert.ToDouble(row["DO"]),
                                              EC = Convert.ToDouble(row["EC"]),
                                              pH = Convert.ToDouble(row["pH"]),
                                              Temperature = Convert.ToDouble(row["Temperature"]),
                                              WaterLevel = Convert.ToDouble(row["WaterLevel"]),
                                              RSSI = Convert.ToDouble(row["RSSI"]),
                                              PaddockId = Convert.ToInt32(row["PaddockId"]),
                                              PaddockName = row["PaddockName"].ToString(),
                                              UserId = Convert.ToInt32(row["UserId"]),
                                          }).ToList();
                                    try
                                    {
                                        //client.BaseAddress = new Uri(ConfigurationManager.AppSettings["MLA_WebAPI"].ToString() + "/Inserttbllgwaterdetails");

                                        if (EI.Count != 0)
                                        {
                                            string InsertapiUrl = ConfigurationManager.AppSettings["MLA_WebAPI"].ToString() + "/CompareTables/Inserttbllgwaterdetails";

                                            client.BaseAddress = new Uri(InsertapiUrl);

                                            //client.BaseAddress = new Uri("http://172.20.8.51/MLAWEBAPI/api/CompareTables/Inserttbllgwaterdetails");

                                            var response = client.PostAsJsonAsync("Inserttbllgwaterdetails", EI).Result;

                                            if (response.IsSuccessStatusCode)
                                            {
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
                    }

                    MessageBox message = new MessageBox("Csv file imported successfully.");
                    message.Show();
                    this.Visibility = Visibility.Hidden;
                    ((MapLoad)this.Owner).closebutton();
                }
                else
                {
                    MessageBox message = new MessageBox("Csv not saved.");
                    message.Show();
                    this.Visibility = Visibility.Hidden;
                    ((MapLoad)this.Owner).closebutton();
                }
            }
            catch(Exception ex)
            {
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MissionHub", _userID);
            }
        }

        public string Connection_Status = "false";

        public class BAl_TableRecords
        {
            public string TABLE_NAME { get; set; }
            public string TABLE_ROWS { get; set; }
            public string AUTO_INCREMENT { get; set; }
            public string B_RecordCount { get; set; }
        }

        

        public static bool IsInternetConnection(String URL)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Timeout = 5000;
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public void ConnectionStatus(bool NetConStatus)
        {
            if (NetConStatus == true)
            {
                Connection_Status = "true";
                //MessageBox.Show("This computer is connected to the internet");
            }
            else
            {
                Connection_Status = "false";
                //MessageBox.Show("This computer is not connected to the internet. Please Connect Internet!!");
            }

        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            ConfirmationWindow objCnfrm = new ConfirmationWindow("Are you sure you want to delete the selected mission?");
            objCnfrm.Owner = this;
            objCnfrm.ShowDialog();


            //((ConfirmationWindow)this.Owner).closebutton();
        }

        public void _deleteMission()
        {
            DataAccessLayer.ViewMissionType objDelete= new DataAccessLayer.ViewMissionType();
            int _status=  objDelete._delete_selectedMission(missionUserid, _Mission_ID.ToString());
            if(_status==1)
            {
                MessageBox message = new MessageBox("Mission deleted successfully.");
                message.Show();
            }
            else
            {
                MessageBox message = new MessageBox("Mission not deleted.");
                message.Show();
            }
        }
    }

    internal class PortableDeviceCollection
    {
        public PortableDeviceCollection()
        {
        }

        internal void Refresh()
        {
            throw new NotImplementedException();
        }
    }

    public static class CSVUtlity
    {
        public static void ToCSV(this DataTable dtDataTable)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "csv files (*.csv)|*.csv";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.Title = "Save csv Files";
            if (saveFileDialog1.ShowDialog() == true)
            {
                using (StreamWriter sw = new StreamWriter(saveFileDialog1.FileName))
                //headers  
                {
                    for (int i = 0; i < dtDataTable.Columns.Count; i++)
                    {
                        sw.Write(dtDataTable.Columns[i]);
                        if (i < dtDataTable.Columns.Count - 1)
                        {
                            sw.Write(",");
                        }
                    }
                    sw.Write(sw.NewLine);
                    foreach (DataRow dr in dtDataTable.Rows)
                    {
                        for (int i = 0; i < dtDataTable.Columns.Count; i++)
                        {
                            if (!Convert.IsDBNull(dr[i]))
                            {
                                string value = dr[i].ToString();
                                if (value.Contains(','))
                                {
                                    value = String.Format("\"{0}\"", value);
                                    sw.Write(value);
                                }
                                else
                                {
                                    sw.Write(dr[i].ToString());
                                }
                            }
                            if (i < dtDataTable.Columns.Count - 1)
                            {
                                sw.Write(",");
                            }
                        }
                        sw.Write(sw.NewLine);
                    }
                    sw.Close();
                }
            }
        }
    }
}
