﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for MLA_List.xaml
    /// </summary>
  
    public partial class MLA_List : Window
    {
        string userID = "";
        public string ListMaparea;
        private List<MLA_Aerodyne.BusinessAccessLayer.ImageListData> lstMyObject = new List<MLA_Aerodyne.BusinessAccessLayer.ImageListData>();
        MLA_Aerodyne.BusinessAccessLayer.ImageListData imgList = new MLA_Aerodyne.BusinessAccessLayer.ImageListData();
        List<string> Lstview = new List<string>();
        System.Data.DataSet Mds = new System.Data.DataSet();
        System.Data.DataSet Lds = new System.Data.DataSet();
        System.Data.DataSet Pds = new System.Data.DataSet();
        System.Data.DataSet Wds = new System.Data.DataSet();
        DataAccessLayer.ViewMaparea Mmap = new DataAccessLayer.ViewMaparea();
        DataAccessLayer.ViewLandMark Lmap = new DataAccessLayer.ViewLandMark();
        DataAccessLayer.ViewPaddock Pmap = new DataAccessLayer.ViewPaddock();
        DataAccessLayer.ViewWaypoint Wmap = new DataAccessLayer.ViewWaypoint();
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        public MLA_List(string Limap)
        {
            try
            {
                InitializeComponent();
                userID = Limap;
                Mds = Mmap.BindCombobox_ViewMaparea(Limap);
                Lds = Lmap.List_Landmark(Limap);
                Pds = Pmap.List_Paddock(Limap);
                Wds = Wmap.List_ViewWaypoint(Limap);
                listview_Maparea();
                listview_Landmark();
                listview_Paddock();
                listview_Waypoint();
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }
        }
        public void listview_Maparea()
        {
            try
            {
                var MimgL = new List<MLA_Aerodyne.BusinessAccessLayer.ImageListData>();
                DataTable Mdt = Mds.Tables[0];
                for (int i = 0; i < Mdt.Rows.Count; i++)
                {
                    MimgL.Add(new MLA_Aerodyne.BusinessAccessLayer.ImageListData
                    {
                        ID = Mds.Tables[0].Rows[i]["Plot_ID"].ToString(),
                        MapareaName = Mds.Tables[0].Rows[i]["Plot_Name"].ToString(),
                        DateCreated = Mds.Tables[0].Rows[i]["CreatedOn"].ToString()

                    });
                }
                maparealistView1.ItemsSource = MimgL;
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }
        }
        public void listview_Paddock()
        {
            try
            {
                var PimgL = new List<MLA_Aerodyne.BusinessAccessLayer.ImageListData>();
                DataTable Pdt = Pds.Tables[0];

                for (int i = 0; i < Pdt.Rows.Count; i++)
                {
                    PimgL.Add(new MLA_Aerodyne.BusinessAccessLayer.ImageListData
                    {
                        ID = Pds.Tables[0].Rows[i]["Paddock_Id"].ToString(),
                        MapareaName = Pds.Tables[0].Rows[i]["Paddock_Name"].ToString(),
                        DateCreated = Pds.Tables[0].Rows[i]["CreatedOn"].ToString(),
                        MapName = Pds.Tables[0].Rows[i]["Plot_Name"].ToString()
                    });
                }
                paddocklistView1.ItemsSource = PimgL;
            }
          
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }
        }
        public void listview_Waypoint()
        {
            try
            {
                var WimgL = new List<MLA_Aerodyne.BusinessAccessLayer.ImageListData>();
                DataTable Wdt = Wds.Tables[0];
                for (int i = 0; i < Wdt.Rows.Count; i++)
                {
                    WimgL.Add(new MLA_Aerodyne.BusinessAccessLayer.ImageListData
                    {
                        ID = Wds.Tables[0].Rows[i]["WayPoint_Id"].ToString(),
                        MapareaName = Wds.Tables[0].Rows[i]["WayPoint_Name"].ToString(),
                        DateCreated = Wds.Tables[0].Rows[i]["CreatedOn"].ToString(),
                        MapName= Wds.Tables[0].Rows[i]["Paddock_Name"].ToString()
                    });
                }
                waypointlistView1.ItemsSource = WimgL;
            }
          
             catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }
        }
        public void listview_Landmark()
        {
            try
            {
                var LimgL = new List<MLA_Aerodyne.BusinessAccessLayer.ImageListData>();
                DataTable Ldt = Lds.Tables[0];

                for (int i = 0; i < Ldt.Rows.Count; i++)
                {
                    LimgL.Add(new MLA_Aerodyne.BusinessAccessLayer.ImageListData
                    {
                        ID = Lds.Tables[0].Rows[i]["LM_ID"].ToString(),
                        MapareaName = Lds.Tables[0].Rows[i]["Landmark_Name"].ToString(),
                        DateCreated = Lds.Tables[0].Rows[i]["CreatedOn"].ToString(),
                        //MapName = Lds.Tables[0].Rows[i]["Paddock_Name"].ToString()
                    });
                }
                landlistView1.ItemsSource = LimgL;
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }
        }
        private void SelectCurrentItem(object sender, KeyboardFocusChangedEventArgs e)
        {
            try
            {
                //By this Code I got my `ListView` row Selected.
                ListViewItem item = (ListViewItem)sender;
                item.IsSelected = true;
            }

             catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }

        }
        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }
    
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }
        }

        private void ImagelistView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void BtnView_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CloseAllWindows();
                string MenuFlag = "VMaparea";
                MLA_Aerodyne.BusinessAccessLayer.ImageListData deleteList = ((Button)sender).DataContext as MLA_Aerodyne.BusinessAccessLayer.ImageListData;
                string plotID = deleteList.ID.ToString();
                MapLoad mview = new MapLoad(userID, plotID, MenuFlag);
                mview.Show();
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }
       
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CloseAllWindows();
                string MenuFlag = "editMaparea";
                MLA_Aerodyne.BusinessAccessLayer.ImageListData deleteList = ((Button)sender).DataContext as MLA_Aerodyne.BusinessAccessLayer.ImageListData;
                string plotID = deleteList.ID.ToString();
                MapLoad mview = new MapLoad(userID, plotID, MenuFlag);
                mview.Show();
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CloseAllWindows();
                MLA_Aerodyne.BusinessAccessLayer.ImageListData deleteList = ((Button)sender).DataContext as MLA_Aerodyne.BusinessAccessLayer.ImageListData;
                string plotID = deleteList.ID.ToString();
                DataAccessLayer.ViewMaparea transObj = new DataAccessLayer.ViewMaparea();
                int retResult = transObj.Delete_MapArea(plotID, userID);
                if (retResult == 1)
                {
                    listview_Maparea();
                    MessageBox message = new MessageBox("Maparea deleted successfully.");
                    message.Show();
                    ((MapLoad)this.Owner).closebutton();
                }
                else
                {
                    MessageBox message = new MessageBox("Maparea which you trying to delete was mapped to a paddock, So delete aborted.");
                    message.Show();
                    ((MapLoad)this.Owner).closebutton();
                }
                this.Visibility = Visibility.Hidden;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }
        }

        private void CloseAllWindows()
        {
            try
            {
                for (int intCounter = App.Current.Windows.Count - 1; intCounter > 0; intCounter--)
                    App.Current.Windows[intCounter].Close();
            }
          
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }
        }

        private void btnlandView_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CloseAllWindows();
                string MenuFlag = "VLandMark";
                MLA_Aerodyne.BusinessAccessLayer.ImageListData landList = ((Button)sender).DataContext as MLA_Aerodyne.BusinessAccessLayer.ImageListData;
                string landID = landList.ID.ToString();
                MapLoad mview = new MapLoad(userID, landID, MenuFlag);
                mview.Show();
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
            }
        }

        private void btnlandEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CloseAllWindows();
                string MenuFlag = "editLandMark";
                MLA_Aerodyne.BusinessAccessLayer.ImageListData landeditList = ((Button)sender).DataContext as MLA_Aerodyne.BusinessAccessLayer.ImageListData;
                string landID = landeditList.ID.ToString();
                MapLoad mview = new MapLoad(userID, landID, MenuFlag);
                mview.Show();
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }
        }

        private void btnlandDelete_Click(object sender, RoutedEventArgs e)
        {
            CloseAllWindows();
            string MenuFlag = "";
            MLA_Aerodyne.BusinessAccessLayer.ImageListData deleteList = ((Button)sender).DataContext as MLA_Aerodyne.BusinessAccessLayer.ImageListData;
            string lmID = deleteList.ID.ToString();
            DataAccessLayer.ViewLandMark transObj = new DataAccessLayer.ViewLandMark();
            int retResult = transObj.Delete_Landmark(lmID, userID);
            if (retResult == 1)
            {
                MessageBox message = new MessageBox("Landmark deleted successfully.");
                message.Show();
                MapLoad mview = new MapLoad(userID, lmID, MenuFlag);
                mview.Show();
                ((MapLoad)this.Owner).closebutton();
            }
            else
            {
                MessageBox message = new MessageBox("Landmark delete aborted.");
                message.Show();
                MapLoad mview = new MapLoad(userID, lmID, MenuFlag);
                mview.Show();
                ((MapLoad)this.Owner).closebutton();
            }
        }

        private void btnPaddockView_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CloseAllWindows();
                string MenuFlag = "Vpaddock";
                MLA_Aerodyne.BusinessAccessLayer.ImageListData padList = ((Button)sender).DataContext as MLA_Aerodyne.BusinessAccessLayer.ImageListData;
                string padID = padList.ID.ToString();
                MapLoad mview = new MapLoad(userID, padID, MenuFlag);
                mview.Show();
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }
        }

        private void btnPaddockEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CloseAllWindows();
                string MenuFlag = "editPaddock";
                MLA_Aerodyne.BusinessAccessLayer.ImageListData padeditList = ((Button)sender).DataContext as MLA_Aerodyne.BusinessAccessLayer.ImageListData;
                string padID = padeditList.ID.ToString();
                MapLoad mview = new MapLoad(userID, padID, MenuFlag);
                mview.Show();
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }
        }

        private void btnpaddockDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MLA_Aerodyne.BusinessAccessLayer.ImageListData deleteList = ((Button)sender).DataContext as MLA_Aerodyne.BusinessAccessLayer.ImageListData;
                string padID = deleteList.ID.ToString();
                DataAccessLayer.ViewPaddock transObj = new DataAccessLayer.ViewPaddock();
                int retResult = transObj.Delete_Paddock(padID, userID);
                if (retResult == 1)
                {
                    listview_Paddock();
                    MessageBox message = new MessageBox("Paddock deleted successfully.");
                    message.Show();
                    ((MapLoad)this.Owner).closebutton();

                }
                else
                {
                    MessageBox message = new MessageBox("Paddock which you trying to delete was mapped to a paddock, So delete aborted.");
                    message.Show();
                    ((MapLoad)this.Owner).closebutton();
                }
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }

        }
        private void btnwayhpointView_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CloseAllWindows();
                string MenuFlag = "Vwaypoint";
                MLA_Aerodyne.BusinessAccessLayer.ImageListData waypointList = ((Button)sender).DataContext as MLA_Aerodyne.BusinessAccessLayer.ImageListData;
                string wayID = waypointList.ID.ToString();
                MapLoad mview = new MapLoad(userID, wayID, MenuFlag);
                mview.Show();
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }

        }
        private void btnwaypointEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CloseAllWindows();
                string MenuFlag = "editWaypoint";
                MLA_Aerodyne.BusinessAccessLayer.ImageListData wayeditList = ((Button)sender).DataContext as MLA_Aerodyne.BusinessAccessLayer.ImageListData;
                string wayID = wayeditList.ID.ToString();
                MapLoad mview = new MapLoad(userID, wayID, MenuFlag);
                mview.Show();
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }
        }

        private void btnWaypointDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MLA_Aerodyne.BusinessAccessLayer.ImageListData deleteList = ((Button)sender).DataContext as MLA_Aerodyne.BusinessAccessLayer.ImageListData;
                string wayID = deleteList.ID.ToString();
                DataAccessLayer.ViewWaypoint transObj = new DataAccessLayer.ViewWaypoint();
                int retResult = transObj.Delete_Waypoint(wayID, userID);
                if (retResult == 1)
                {
                    MessageBox message = new MessageBox("Waypoint deleted successfully.");
                    message.Show();
                    listview_Waypoint();
                    ((MapLoad)this.Owner).closebutton();
                }
                else
                {
                    MessageBox message = new MessageBox("Waypoint delete aborted.");
                    message.Show(); ((MapLoad)this.Owner).closebutton();
                }
                this.Visibility = Visibility.Hidden;
            }
      
             catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MLA_List", userID);
            }
        }

     
    } 
       
}

