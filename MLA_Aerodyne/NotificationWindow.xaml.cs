﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for NotificationWindow.xaml
    /// </summary>
    public partial class NotificationWindow : Window
    {
        string _UserId = string.Empty;
        float _speedmegabyte;
        public NotificationWindow(string userID, string _folderpath)
        {
            InitializeComponent();
            _UserId = userID;
            long length = Directory.GetFiles(_folderpath, "*", SearchOption.AllDirectories).Sum(t => (new FileInfo(t).Length));
            float _filemegabyte= (length / 1024f) / 1024f;
            string filesize = FileSizeFormatter.FormatSize(length);
            if (!NetworkInterface.GetIsNetworkAvailable())
                return;
            NetworkInterface[] interfaces
                = NetworkInterface.GetAllNetworkInterfaces();

            foreach (NetworkInterface ni in interfaces)
            {
                if (ni.OperationalStatus == OperationalStatus.Up && ni.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                {

                    long Sent = ni.GetIPv4Statistics().BytesSent;
                    _speedmegabyte = (Sent / 1024f) / 1024f;
                    long Received = ni.GetIPv4Statistics().BytesReceived;
                }
            }
            float time = _filemegabyte / _speedmegabyte;
            TimeSpan time1 = TimeSpan.FromSeconds((double)(new decimal(time)));
            string str = time1.ToString(@"hh\:mm\:ss\:fff");
            messageLbl.Text = "Sync to iLAMS will take approximately  " + str + " hh:mm:ss:ms to transfer. Do you want to proceed?";
;        }
       
        private void yesBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            ((Sync_to_iLAMS)this.Owner).sync_toiLAMS();
        
        }

        private void noBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            ((MapLoad)this.Owner).closebutton();
        }
    }
}
