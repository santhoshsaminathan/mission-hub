﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using Newtonsoft.Json;
using System.Runtime.InteropServices;
using System.Data;
using System.Globalization;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for CMapAreaWindow.xaml
    /// </summary>
    [ComVisible(true)]
    public partial class CMapAreaWindow : Window
    {
        public dynamic Waypoints;

        public string User_Id;
        LoaderWindow loaderWindow;
        public string AllWaypoints;
        private const double EarthRadius = 3958.756;
        public string CapImage_Path;
        public string mapStatus;
        public string plot_id;
        string _mapType = "";
        public string UserId { get; set; }
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        public CMapAreaWindow(string map_Status,string plotid,string mapType)
        {
            InitializeComponent();
            _mapType = mapType;
            NameTxt.Text = "";
            DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

            if (DefaultMapSet.IsChecked == true)
            {
                DefaultMapSet.IsChecked = false;
            }
            NameTxt.Focus();
        }

        public void GetUserIDFromLogin(string cUserId)
        {
            UserId = cUserId;
        }

        public void ValueFromMapLoad(string UserId, string Waypoint, string CapImagePath)
        {
            try
            {
                Waypoints = JsonConvert.DeserializeObject(Waypoint);

                AllWaypoints = Waypoint;

                User_Id = UserId;

                CapImage_Path = CapImagePath;

                GetMapAreaFromDatabase();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "CMapAreaWindow", User_Id);

                return;
            }
        }

        public void GetMapAreaFromDatabase()
        {
            try
            {
                DataAccessLayer.DAL_CreateMapArea DAL_CreateMapArea = new DataAccessLayer.DAL_CreateMapArea();
                DataSet ds_Combo = DAL_CreateMapArea.GetMapAreaFromDB(User_Id);
                MapAreaListBox.ItemsSource = ds_Combo.Tables[0].DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "CMapAreaWindow", User_Id);

                return;
            }
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (loaderWindow == null)
                    loaderWindow = new LoaderWindow();
                 loaderWindow.Show();
                if (NameTxt.Text != "")
                {
                    DateTime now = DateTime.Now;
                    string CreatedOn = now.ToString();

                    string MapName = NameTxt.Text;

                    string IsDefaultMapCheck = null;

                    if (DefaultMapSet.IsChecked ?? false)
                    {
                        IsDefaultMapCheck = "1";
                    }
                    else
                    {
                        IsDefaultMapCheck = "0";
                    }
                    dynamic Way_Points = JsonConvert.DeserializeObject(AllWaypoints);
                    IList<PointLatLng> coordinates = new List<PointLatLng>();
                    foreach (var val in Way_Points)
                    {
                        PointLatLng pointLatLng = new PointLatLng();
                        pointLatLng.Latitude = val.lat;
                        pointLatLng.Longitude = val.lng;
                        coordinates.Add(pointLatLng);
                    }
                    
                    double area = MLA_Aerodyne.DataAccessLayer.CalculateAreaOfPolygon.ComputeSignedArea(coordinates);
                    area = Math.Round(area, 2);
                    area = Math.Abs(area);
                    area = area /(double)10000;
                    NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
                    nfi.NumberDecimalSeparator = " ";
                    string _area = area.ToString("N", nfi);

                    if(_mapType == "KMLmap")
                    {
                        string KML_ID= ((MapLoad)this.Owner).GetKML_ID(); 
                        DataAccessLayer.DAL_CreateMapArea DAL_CreateMapArea = new DataAccessLayer.DAL_CreateMapArea();
                        int result= DAL_CreateMapArea.Save_CreateKMLMapArea(User_Id, MapName, AllWaypoints, CapImage_Path, IsDefaultMapCheck, _area.Replace(" ", ".") + " " + "hectares",KML_ID);
                        this.Visibility = Visibility.Hidden;
                        if(result!=0)
                        {
                            MessageBox message = new MessageBox("KML saved successfully.");
                            message.Show();
                        }
                       
                    }
                    else
                    {
                        DataAccessLayer.DAL_CreateMapArea DAL_CreateMapArea = new DataAccessLayer.DAL_CreateMapArea();
                        DAL_CreateMapArea.Save_CreateMapArea(User_Id, MapName, AllWaypoints, CapImage_Path, IsDefaultMapCheck, _area.Replace(" ", ".") + " " + "hectares");
                        this.Visibility = Visibility.Hidden;
                        MessageBox message = new MessageBox("Maparea saved successfully.");
                        message.Show();
                        ((MapLoad)this.Owner).clearmap();
                    }
                                      
                    NameTxt.Text = "";
                    if (DefaultMapSet.IsChecked == true)
                    {
                        DefaultMapSet.IsChecked = false;
                    }
                }
                else
                {
                    MessageBox message = new MessageBox("Please enter map area name.");
                    message.Show();
                    return;
                }

                if (loaderWindow == null)
                    loaderWindow = new LoaderWindow();
                loaderWindow.Hide();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "CMapAreaWindow", User_Id);
                return;
            }
        }
        private double Haversine(double lat1, double lon1, double lat2, double lon2)
        {
            double dlat = DegreesToRadians(lat2 - lat1);
            double dlon = DegreesToRadians(lon2 - lon1);
            double a = Math.Sin(dlat / 2) * Math.Sin(dlat / 2) +
                Math.Cos(DegreesToRadians(lat1)) *
                Math.Cos(DegreesToRadians(lat2)) *
                Math.Sin(dlon / 2) * Math.Sin(dlon / 2);
            return 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a)) *
                EarthRadius;
        }

        // Convert degrees into radians.
        private double DegreesToRadians(double degrees)
        {
            return degrees / 180 * Math.PI;
        }
        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NameTxt.Text = "";
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).clearmap();
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "CMapAreaWindow", User_Id);
            }
        }
    }
    public static class ConvertDistance
    {
        public static double ConvertMilesToKilometers(double miles)
        {
            //
            // Multiply by this constant and return the result.
            //
            return miles * 1.609344;
        }
    }
    }
