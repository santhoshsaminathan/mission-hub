﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for VPaddockWindow.xaml
    /// </summary>
    public partial class VPaddockWindow : Window
    {
        string userID = "";
        public string Viewpaddock;
        public VPaddockWindow(string user_ID)
        {

            InitializeComponent();
            userID = user_ID;
            BindComboBox(userID);
        }

        public void BindComboBox(string vbuserID)
        {
            try
            {
                DataAccessLayer.ViewPaddock vpaddock = new DataAccessLayer.ViewPaddock();
                DataSet Vds = new DataSet();
               // Vds = vpaddock.BindCombobox_Paddock(vbuserID);
                PaddockCombo.DataContext = Vds.Tables[0].DefaultView;
                PaddockCombo.DisplayMemberPath = "Paddock_Name";
                PaddockCombo.SelectedValuePath = "Paddock_Id";
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return;
            }
        }

        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Viewpaddock = PaddockCombo.SelectedValue.ToString();
                string MenuFlag = "editPaddock";
                if (PaddockCombo.Text != "")
                {
                    CloseAllWindows();
                    if (PaddockCombo.Text != "")
                    {
                        MapLoad mview = new MapLoad(userID, Viewpaddock, MenuFlag);
                        mview.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return;
            }
        }

        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }

        private void PaddockCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            PaddockCheckbox.Content = "Disable";
        }

        private void PaddockCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            PaddockCheckbox.Content = "Enable";
        }

        private void PaddockCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                string chosenValue = PaddockCombo.Text;
                string v = PaddockCombo.SelectedValue.ToString();
                DataAccessLayer.ViewPaddock imgObj = new DataAccessLayer.ViewPaddock();
                string imgSource = imgObj.getI_magePath(v);

                if (!String.IsNullOrEmpty(imgSource))
                {
                    Uri resourceUri = new Uri(imgSource, UriKind.Absolute);
                    imgPaddock.Source = new BitmapImage(resourceUri);
                }
                else
                {
                    imgPaddock.Source = new BitmapImage(new Uri(System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImgNull"].ToString())));
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return;
            }
        }

        private void ViewBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Viewpaddock = PaddockCombo.SelectedValue.ToString();
                string MenuFlag = "Vpaddock";
                if (PaddockCombo.Text != "")
                {
                    CloseAllWindows();
                    if (PaddockCombo.Text != "")
                    {
                        MapLoad mview = new MapLoad(userID, Viewpaddock, MenuFlag);
                        mview.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return;
            }
        }

        private void CloseAllWindows()
        {
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > 0; intCounter--)
                App.Current.Windows[intCounter].Close();
        }
    }
}
