﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.IO;
using System.Data;
using MLA_Aerodyne.BusinessAccessLayer;
using RadioButton = System.Windows.Controls.RadioButton;
using Microsoft.VisualBasic.CompilerServices;
using System.Management;
using System.Configuration;
using System.Drawing;
using System.Windows.Media;
using Microsoft.WindowsAPICodePack.Shell;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for Data_Transfer.xaml
    /// </summary>
    public partial class Data_Transfer : Window
    {
        string _userID = "";
        string _imageCapturedDate = "";
        string _mission_folderName_config = Convert.ToString(ConfigurationManager.AppSettings["FilePath"].ToString());
        string _mission_folderName = "";
        string _mission_Destination_folderName_config = Convert.ToString(ConfigurationManager.AppSettings["DestinationFilePath"].ToString());
        string _mission_Destination_folderName = "";
        string _missionname = "";
        string _paddockname = "";
        string Mission_ID = "";
        string _fileName = "";
        string fileName_path = "";
        int misMissionID = 0;
        public int Filetype;
        string transferData_fileSize = "";
        FileAttachment objFileAttachment = new FileAttachment();
        ImageList imgList = new ImageList();
        VideoList vidList = new VideoList();
        ObservableCollection<ImageGridData> Imageitems = new ObservableCollection<ImageGridData>();
        ObservableCollection<VideoGridData> Videoitems = new ObservableCollection<VideoGridData>();
        DataSet vdsMissionList = new DataSet();
        DataSet ds_mission = new DataSet();
        MLA_Aerodyne.DataAccessLayer.DataProcess data = new MLA_Aerodyne.DataAccessLayer.DataProcess();
        MLA_Aerodyne.DataAccessLayer.DataProcess dp = new MLA_Aerodyne.DataAccessLayer.DataProcess();
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        public Data_Transfer(string User_ID)
        {
            InitializeComponent();
            datePicker1.Text = "";
            _userID = User_ID;
            Bind_MissionCombo();
           
        }

        public void Bind_MissionCombo()
        {
           
                misMissionID = 1;
                DataAccessLayer.ViewMissionType listMission = new DataAccessLayer.ViewMissionType();
                vdsMissionList = listMission.BindRadiobutton_ViewMission(_userID, misMissionID);
                missionCombo.DataContext = vdsMissionList.Tables[0].DefaultView;
                missionCombo.DisplayMemberPath = "Mission_Name";
                missionCombo.SelectedValuePath = "Mission_ID";
                missionCombo.SelectedIndex = 0;
          
        }

        private void RadioButtonChecked(object sender, RoutedEventArgs e)
        {
              var button = sender as RadioButton;
                string caseSwitch = button.Content.ToString();

                switch (caseSwitch)
                {
                    case "LoRa Gateway":
                        misMissionID = 1;
                        break;
                    case "Detect and Count":
                        misMissionID = 2;
                        break;
                    case "Fence Monitoring":
                        misMissionID = 3;
                        break;

                    case "Water Monitoring":
                        misMissionID = 4;
                        break;
                    case "Weed Monitoring":
                        misMissionID = 5;
                            break;
                    
                    default:
                        misMissionID = 1;
                        break;
                }
           
           
            DataAccessLayer.ViewMissionType listMission = new DataAccessLayer.ViewMissionType();
            vdsMissionList = listMission.BindRadiobutton_ViewMission(_userID, misMissionID);
            missionCombo.DataContext = vdsMissionList.Tables[0].DefaultView;
            missionCombo.DisplayMemberPath = "Mission_Name";
            missionCombo.SelectedValuePath = "Mission_ID";
            missionCombo.SelectedIndex = 0;
            
        }

      
        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
            }
        }

       

        private void browsedrive_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {              
                IEnumerable<String> selectedfilelist;
                string selectedFolder = null;
                if (rbtnImg.IsChecked == true || rbtnVid.IsChecked == true)
                {
                    if (Filetype == 1)
                    {
                        using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
                        {

                            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                            dialog.ShowNewFolderButton = true;

                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                selectedFolder = dialog.SelectedPath + "\\";
                                string[] filePaths = Directory.EnumerateFiles(selectedFolder, "*.*", SearchOption.AllDirectories)
                                .Where(s => s.EndsWith(".JPEG") || s.EndsWith(".JPG") || s.EndsWith(".jpeg") || s.EndsWith(".jpg")).ToArray();
                                foreach (String file in filePaths)
                                {
                                    string file_name = Path.GetFileName(file);
                                    Imageitems.Add(new ImageGridData(file_name, LoadImage(file), false, file.ToString()));

                                }
                                selectedfilelist = filePaths;
                                directorytext.Text = selectedFolder;
                                ImgBox.ItemsSource = Imageitems;
                            }

                        }
                    }

                    else
                    {
                        using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
                        {

                            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                            dialog.ShowNewFolderButton = true;

                            if (result == System.Windows.Forms.DialogResult.OK)
                            {
                                selectedFolder = dialog.SelectedPath + "\\";
                                string[] filePaths = Directory.EnumerateFiles(selectedFolder, "*.*", SearchOption.AllDirectories)
                                .Where(s => s.EndsWith(".MOV") || s.EndsWith(".mov") || s.EndsWith(".MP4") || s.EndsWith(".mp4")).ToArray();
                                foreach (String file in filePaths)
                                {

                                    string file_name = Path.GetFileName(file);
                                    ShellFile shellFile = ShellFile.FromFilePath(file);
                                    Bitmap bm = shellFile.Thumbnail.Bitmap;
                                    BitmapImage bmi = ConvertBitmapToBitmapImage(bm);
                                    Videoitems.Add(new VideoGridData(file_name, bmi, false, file.ToString()));

                                }
                                selectedfilelist = filePaths;
                                directorytext.Text = selectedFolder;
                                VidBox.ItemsSource = Videoitems;
                            }

                        }
                    }
                }
                else
                {
                    MessageBox message = new MessageBox("Please select the file type radion button." );
                    message.Show();
                }
                
            }
            
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Data_Transfer", _userID);
                message.Show();
            }
        }
        public static BitmapImage ConvertBitmapToBitmapImage(Bitmap bitmap)
        {
            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            ms.Seek(0, SeekOrigin.Begin);
            image.StreamSource = ms;
            image.EndInit();

            return image;
        }
        private BitmapImage LoadImage(string filename)
        {
            return new BitmapImage(new Uri(filename));
        }

 

        private void btnDataTransfer_Click(object sender, RoutedEventArgs e)
        {
          try
            {
                if (Filetype == 1)
                {
                    int result = 0;
                    int imgresult = 0;
                    if (datePicker1.SelectedDate != null)
                    {
                        if (rbtnImg.IsChecked == true)
                        {
                            _imageCapturedDate = datePicker1.Text;
                            _imageCapturedDate = _imageCapturedDate.Replace('-', '.');
                            _imageCapturedDate = _imageCapturedDate.Replace('/', '.');
                            Mission_ID = missionCombo.SelectedValue.ToString();
                            ds_mission = data.GetMissiondetails(Mission_ID);
                            if (ds_mission != null)
                            {
                                if (ds_mission.Tables[0].Rows.Count > 0)
                                {
                                    _missionname = ds_mission.Tables[0].Rows[0]["Mission_Name"].ToString().Trim();
                                    _paddockname = ds_mission.Tables[0].Rows[0]["Paddock_Name"].ToString();
                                }
                            }
                            _mission_folderName = _mission_folderName_config + "\\" + _missionname.Replace(" ", "") + "\\" + _paddockname.Replace(" ", "") + "\\" + _imageCapturedDate + "\\" + "Image";
                            _mission_Destination_folderName = _mission_Destination_folderName_config + "\\" + _missionname.Replace(" ", "") + "\\" + _paddockname.Replace(" ", "") + "\\" + _imageCapturedDate + "\\" + "Image";
                            if (!Directory.Exists(_mission_folderName))
                            {
                                // Try to create the directory.
                                DirectoryInfo di = Directory.CreateDirectory(_mission_folderName);
                            }
                            if (!Directory.Exists(_mission_Destination_folderName))
                            {
                                // Try to create the directory.
                                DirectoryInfo di = Directory.CreateDirectory(_mission_Destination_folderName);
                            }
                            var selectedData = Imageitems.Select(d => new { d.File_path, d.Title, d.ImageData, d.IsChecked }).Where(d => d.IsChecked).ToList(); ;
                            if (selectedData != null)
                            {
                                foreach (var ImageCollection in selectedData)
                                {
                                    imgList.selectedfileName = ImageCollection.Title;
                                    imgList.selectedData = ImageCollection.File_path;
                                    imgList.selectedfile = ImageCollection.ImageData;
                                    objFileAttachment.OriginalFileName = imgList.selectedfileName;
                                    _fileName = Mission_ID + "_" + imgList.selectedfileName;
                                    objFileAttachment.FileAttachmentName = _fileName;
                                    fileName_path = _mission_folderName + "\\" + objFileAttachment.FileAttachmentName;
                                    if (File.Exists(fileName_path))
                                    {
                                        File.Delete(fileName_path);
                                        int delete = dp.Delete_existingimage(objFileAttachment.FileAttachmentName);
                                    }
                                    CopyFiles(imgList.selectedData, fileName_path);
                                    objFileAttachment.CreatedBy = _userID + "_Drone SD card transfer";
                                    objFileAttachment.Transferd_Status = 0;
                                    FileInfo fi = new FileInfo(fileName_path);
                                    string size = FileSizeFormatter.FormatSize(fi.Length);
                                    FileInfo fit = new FileInfo(imgList.selectedData);
                                    transferData_fileSize = FileSizeFormatter.FormatSize(fit.Length);
                                    objFileAttachment.Size = size.ToString();
                                    objFileAttachment.Type = fi.Extension.ToString();
                                    objFileAttachment.Path = fileName_path;
                                    result = dp.SaveFileAttachment(Mission_ID, _imageCapturedDate, objFileAttachment.FileAttachmentName, objFileAttachment.Size, objFileAttachment.Type, objFileAttachment.Path, _userID, objFileAttachment.CreatedBy, objFileAttachment.Transferd_Status, objFileAttachment.OriginalFileName);
                                    if (result == 1)
                                    {
                                        if (objFileAttachment.Size == transferData_fileSize)
                                        {
                                            imgresult = dp.Update_transferedstatus(objFileAttachment.FileAttachmentName);

                                        }
                                    }
                                }
                                if (imgresult == 1)
                                {
                                    this.Visibility = Visibility.Hidden;
                                    MessageBox message = new MessageBox("Image file transfer successful.");
                                    message.Show();
                                    ((MapLoad)this.Owner).closebutton();
                                }
                                else
                                {
                                    MessageBox message2 = new MessageBox("Image file transfer interrupted. Image details not saved in Mission Hub.");
                                    message2.Show();
                                }

                            }

                            else
                            {
                                MessageBox message = new MessageBox("Please select a image.");
                                message.Show();
                            }
                        }
                        else
                        {
                            MessageBox message = new MessageBox("Please select a file type radio button.");
                            message.Show();
                        }
                    }
                    else
                    {
                        if (datePicker1.SelectedDate == null)
                        {
                            MessageBox message = new MessageBox("Please select a date of flight.");
                            message.Show();
                        }
                    }
                }
                else
                {
                    int result = 0;
                    int vidresult = 0;
                    if (datePicker1.SelectedDate != null)
                    {
                        if (rbtnVid.IsChecked == true)
                        {
                            _imageCapturedDate = datePicker1.Text;
                            _imageCapturedDate = _imageCapturedDate.Replace('-', '.');
                            _imageCapturedDate = _imageCapturedDate.Replace('/', '.');
                            Mission_ID = missionCombo.SelectedValue.ToString();
                            ds_mission = data.GetMissiondetails(Mission_ID);
                            if (ds_mission != null)
                            {
                                if (ds_mission.Tables[0].Rows.Count > 0)
                                {
                                    _missionname = ds_mission.Tables[0].Rows[0]["Mission_Name"].ToString().Trim();
                                    _paddockname = ds_mission.Tables[0].Rows[0]["Paddock_Name"].ToString();
                                }
                            }
                            _mission_folderName = _mission_folderName_config + "\\" + _missionname.Replace(" ", "") + "\\" + _paddockname.Replace(" ", "") + "\\" + _imageCapturedDate + "\\" + "Video";
                            _mission_Destination_folderName = _mission_Destination_folderName_config + "\\" + _missionname.Replace(" ", "") + "\\" + _paddockname.Replace(" ", "") + "\\" + _imageCapturedDate + "\\" + "Video";
                            if (!Directory.Exists(_mission_folderName))
                            {
                                // Try to create the directory.
                                DirectoryInfo di = Directory.CreateDirectory(_mission_folderName);
                            }
                            if (!Directory.Exists(_mission_Destination_folderName))
                            {
                                // Try to create the directory.
                                DirectoryInfo di = Directory.CreateDirectory(_mission_Destination_folderName);
                            }
                            var selectedData = Videoitems.Select(d => new { d.File_path, d.Title, d.ImageData, d.IsChecked }).Where(d => d.IsChecked).ToList(); ;
                            if (selectedData != null)
                            {
                                foreach (var VideoCollection in selectedData)
                                {
                                    vidList.selectedfileName = VideoCollection.Title;
                                    vidList.selectedData = VideoCollection.File_path;
                                    vidList.selectedfile = VideoCollection.ImageData;
                                    objFileAttachment.OriginalFileName = vidList.selectedfileName;
                                    _fileName = Mission_ID + "_" + vidList.selectedfileName;
                                    objFileAttachment.FileAttachmentName = _fileName;
                                    fileName_path = _mission_folderName + "\\" + objFileAttachment.FileAttachmentName;
                                    if (File.Exists(fileName_path))
                                    {
                                        File.Delete(fileName_path);
                                        int delete = dp.Delete_existingimage(objFileAttachment.FileAttachmentName);
                                    }
                                    CopyFiles(vidList.selectedData, fileName_path);
                                    objFileAttachment.CreatedBy = _userID + "_Drone SD card transfer";
                                    objFileAttachment.Transferd_Status = 0;
                                    FileInfo fi = new FileInfo(fileName_path);
                                    string size = FileSizeFormatter.FormatSize(fi.Length);
                                    FileInfo fit = new FileInfo(vidList.selectedData);
                                    transferData_fileSize = FileSizeFormatter.FormatSize(fit.Length);
                                    objFileAttachment.Size = size.ToString();
                                    objFileAttachment.Type = fi.Extension.ToString();
                                    objFileAttachment.Path = fileName_path;
                                    result = dp.SaveFileAttachment(Mission_ID, _imageCapturedDate, objFileAttachment.FileAttachmentName, objFileAttachment.Size, objFileAttachment.Type, objFileAttachment.Path, _userID, objFileAttachment.CreatedBy, objFileAttachment.Transferd_Status, objFileAttachment.OriginalFileName);
                                    if (result == 1)
                                    {
                                        if (objFileAttachment.Size == transferData_fileSize)
                                        {
                                            vidresult = dp.Update_transferedstatus(objFileAttachment.FileAttachmentName);

                                        }
                                    }
                                }
                                if (vidresult == 1)
                                {
                                    this.Visibility = Visibility.Hidden;
                                    MessageBox message = new MessageBox("Video file transfer successful.");
                                    message.Show();
                                    ((MapLoad)this.Owner).closebutton();
                                }
                                else
                                {
                                    MessageBox message2 = new MessageBox("Video file transfer interrupted. Video details not saved in Mission Hub.");
                                    message2.Show();
                                }

                            }

                            else
                            {
                                MessageBox message = new MessageBox("Please select a video.");
                                message.Show();
                            }
                        }
                        else
                        {                           
                        MessageBox message = new MessageBox("Please select a file type radio button.");
                        message.Show();
                        }
                    }
                    else
                    {
                        if (datePicker1.SelectedDate == null)
                        {
                            MessageBox message = new MessageBox("Please select a date of flight.");
                            message.Show();
                        }
                    }
                }
        }
             catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Data_Transfer", _userID);
            }
        }

        private void CopyFiles(string sourcePath, string destinationPath)
        {
            try
            {
                string file_name = Path.GetFileName(sourcePath);
                if (!File.Exists(destinationPath))
                {
                    System.IO.File.Copy(sourcePath, destinationPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Data_Transfer", _userID);
            }
        }

        private void chkSelectall_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (ImageGridData imgChk in ImgBox.ItemsSource)
                {
                    imgChk.IsChecked = true;

                }
                if (ImgBox.ItemsSource == null)
                {
                    MessageBox message = new MessageBox("Please select the file path.");
                    message.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
            }

        }

        private void chkSelectall_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (ImageGridData imgChk in ImgBox.ItemsSource)
                {
                    imgChk.IsChecked = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Data_Transfer", _userID);
            }
        }

        private void chkSelectall_Checked_vid(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (VideoGridData vidchk in VidBox.ItemsSource)
                {
                    vidchk.IsChecked = true;

                }
                if (VidBox.ItemsSource == null)
                {
                    MessageBox message = new MessageBox("Please select the file path.");
                    message.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Data_Transfer", _userID);
            }

        }

        private void chkSelectall_Unchecked_vid(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (VideoGridData vidchk in VidBox.ItemsSource)
                {
                    vidchk.IsChecked = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Data_Transfer", _userID);
            }
        }


        private void missionCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
        }

        private void ImgBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void rbtnImg_Checked(object sender, RoutedEventArgs e)
        {

            var button = sender as RadioButton;
            string caseSwitch = button.Content.ToString();

            switch (caseSwitch)
            {
                case "Image":
                    Filetype = 1;
                    break;
                case "Video":
                    Filetype = 2;
                    ImgBox.Visibility = Visibility.Hidden;
                    VidBox.Visibility = Visibility.Visible;
                    chkSelectallVid.Visibility = Visibility.Visible;
                    break;
            }
        }
    }


    public class ImageList
    {
        public string selectedData { get; set; }
        public string selectedfileName { get; set; }
        public BitmapImage selectedfile { get; set; }
    }

    public class VideoList
    {
        public string selectedData { get; set; }
        public string selectedfileName { get; set; }
        public BitmapImage selectedfile { get; set; }
    }
}

