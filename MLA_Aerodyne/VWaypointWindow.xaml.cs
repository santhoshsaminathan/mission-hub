﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for VWaypointWindow.xaml
    /// </summary>
    public partial class VWaypointWindow : Window
    {
        string userID = "";
        public string Viewwaypoint;

        public VWaypointWindow(string user_id)
        {
            InitializeComponent();
            userID = user_id;
            BindComboBox(userID);
        }

        public void BindComboBox(string vbuserID)
        {
            try
            {
                MLA_Aerodyne.BuisnessAccessLayer.UserDetails Vuserdetails = new MLA_Aerodyne.BuisnessAccessLayer.UserDetails();
                DataAccessLayer.ViewWaypoint vway = new DataAccessLayer.ViewWaypoint();
                DataSet Vds = new DataSet();
               // Vds = vway.BindCombobox_ViewWaypoint(vbuserID);
                WaypointCombo.DataContext = Vds.Tables[0].DefaultView;
                WaypointCombo.DisplayMemberPath = "WayPoint_Name";
                WaypointCombo.SelectedValuePath = "WayPoint_Id";
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: "+ ex.Message);
                return;
            }
        }

        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Viewwaypoint = WaypointCombo.SelectedValue.ToString();
                string MenuFlag = "editWaypoint";
                if (WaypointCombo.Text != "")
                {
                    CloseAllWindows();
                    if (WaypointCombo.Text != "")
                    {
                        MapLoad mview = new MapLoad(userID, Viewwaypoint, MenuFlag);
                        mview.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: "+ ex.Message);
                return;
            }
        }

        private void CloseAllWindows()
        {
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > 0; intCounter--)
                App.Current.Windows[intCounter].Close();
        }

        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }

        private void WaypointCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            WaypointCheckbox.Content = "Disable";
        }

        private void WaypointCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            WaypointCheckbox.Content = "Enable";
        }

        private void WaypointCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                string chosenValue = WaypointCombo.Text;
                string v = WaypointCombo.SelectedValue.ToString();
                DataAccessLayer.ViewWaypoint imgObj = new DataAccessLayer.ViewWaypoint();
                string imgSource = imgObj.getI_magePath(v);

                if (!String.IsNullOrEmpty(imgSource))
                {
                    Uri resourceUri = new Uri(imgSource, UriKind.Absolute);
                    imgwaypoint.Source = new BitmapImage(resourceUri);
                }
                else
                {
                    imgwaypoint.Source = new BitmapImage(new Uri(System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImgNull"].ToString())));
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: "+ ex.Message);
                return;
            }
        }

        private void ViewBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Viewwaypoint = WaypointCombo.SelectedValue.ToString();
                string MenuFlag = "Vwaypoint";
                if (WaypointCombo.Text != "")
                {
                    CloseAllWindows();
                    if (WaypointCombo.Text != "")
                    {
                        MapLoad mview = new MapLoad(userID, Viewwaypoint, MenuFlag);
                        mview.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: "+ ex.Message);
                return;
            }
        }
    }
}
