﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SQLite;
using System.IO;
using MLA_Aerodyne;
using System.Configuration;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for VLandMarkWindow.xaml
    /// </summary>
    public partial class VLandMarkWindow : Window
    {
        Login lv = new Login();
        string userID = "";
        String sURL = AppDomain.CurrentDomain.BaseDirectory + "Html/Maps.html";
        public string ViewLandmark;
        List<string> LstViewlandmark;
        public VLandMarkWindow(string vUserID)
        {
            InitializeComponent();
            userID = vUserID.ToString();
            combolandmark.SelectionChanged += new SelectionChangedEventHandler(Combolandmark_SelectionChanged);
        }

     

        private void viewBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ViewLandmark = combolandmark.SelectedValue.ToString();
                string MenuFlag = "VLandMark";
                if (combolandmark.Text != "")
                {
                    CloseAllWindows();
                    if (combolandmark.Text != "")
                    {
                        MapLoad mview = new MapLoad(userID, ViewLandmark, MenuFlag);
                        mview.Show();
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return;
            }
        }
        private void CloseAllWindows()
        {
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > 0; intCounter--)
                App.Current.Windows[intCounter].Close();
        }

        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ViewLandmark = combolandmark.SelectedValue.ToString();
                string MenuFlag = "editLandMark";
                if (combolandmark.Text != "")
                {
                    CloseAllWindows();
                    if (combolandmark.Text != "")
                    {
                        MapLoad mview = new MapLoad(userID, ViewLandmark, MenuFlag);
                        mview.Show();
                        //VLandMarkWindow updateLandmark = new VLandMarkWindow(userID);
                        //updateLandmark.Show();
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox message = new MessageBox("Error: "+ ex.Message);
                return;
            }
        }

        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }

        private void LandmarkCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            LandmarkCheckbox.Content = "Disable";
        }

        private void LandmarkCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            LandmarkCheckbox.Content = "Enable";
        }

        private void Combolandmark_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                string chosenValue = combolandmark.Text;
                string v = combolandmark.SelectedValue.ToString();
                DataAccessLayer.ViewLandMark imgObj = new DataAccessLayer.ViewLandMark();
                string imgSource = imgObj.getI_magePath(v);

                if (!String.IsNullOrEmpty(imgSource))
                {
                    Uri resourceUri = new Uri(imgSource, UriKind.Absolute);
                    imglandmark.Source = new BitmapImage(resourceUri);
                }
                else
                {
                    imglandmark.Source = new BitmapImage(new Uri(System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImgNull"].ToString())));
                }
            }
            catch(Exception ex)
            {
                MessageBox message = new MessageBox("Error: "+ ex.Message);
                return;
            }
        }
    }
}
