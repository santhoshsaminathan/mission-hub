﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for SelectPaddockWindow.xaml
    /// </summary>
    public partial class SelectPaddockWindow : Window, INotifyPropertyChanged
    {
        public string User_Id;
        public string SelectMapArea;
        public string SelectPaddock;
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();
        private string _selectedItem;
        public event PropertyChangedEventHandler PropertyChanged;

        public SelectPaddockWindow(string User_ID)
        {
            InitializeComponent();
            ValueFromMapLoad(User_ID);
            MapAreaComboBox.SelectedIndex = 1;
            PaddockComboBox.SelectedIndex = 1;
            this.DataContext = this;

        }
        private void MyComboBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            MapAreaComboBox.IsDropDownOpen = true;
        }


        public string SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public string NewItem
        {
            set
            {
                if (SelectedItem != null)
                {
                    return;
                }
                if (!string.IsNullOrEmpty(value))
                {
                    SelectedItem = value;
                }
            }
        }

        private void PaddockComboBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            PaddockComboBox.IsDropDownOpen = true;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void GetMapAreaFromDatabase()
        {
            try
            {
                DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                DataSet ds_Combo = DAL_CreateWayPoint.GetPaddockFromDB(User_Id);

                DataAccessLayer.DAL_CreatePaddock DAL_CreatePaddock = new DataAccessLayer.DAL_CreatePaddock();
                DataSet ds_Combo2 = DAL_CreatePaddock.GetMapAreaFromDB(User_Id);


                if (ds_Combo.Tables[0].Rows.Count != 0)
                {
                    PaddockComboBox.DataContext = ds_Combo.Tables[0].DefaultView;
                    PaddockComboBox.DisplayMemberPath = "Paddock_Name";
                    PaddockComboBox.SelectedValuePath = "Paddock_Id";

                    PaddockComboBox.SelectedIndex = 0;
                }
                else
                {
                    MessageBox message = new MessageBox("Please create paddock.");
                    message.Show();
                    this.Visibility = Visibility.Hidden;
                }

                if (ds_Combo2.Tables[0].Rows.Count != 0)
                {
                    MapAreaComboBox.DataContext = ds_Combo2.Tables[0].DefaultView;
                    MapAreaComboBox.DisplayMemberPath = "Plot_Name";
                    MapAreaComboBox.SelectedValuePath = "Plot_ID";

                    MapAreaComboBox.SelectedIndex = 0;
                }
                else
                {
                    MessageBox message = new MessageBox("Please create map area.");
                    message.Show();
                    this.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: "+ ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectPaddockWindow", User_Id);
                return;
            }
        }

        private void MapAreaComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SelectMapArea = MapAreaComboBox.SelectedValue.ToString();
                DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                DataSet ds_Combo = DAL_CreateWayPoint.GetSelectedPaddockFromDB(User_Id, SelectMapArea);

                if (ds_Combo.Tables[0].Rows.Count != 0)
                {
                    PaddockComboBox.DataContext = ds_Combo.Tables[0].DefaultView;
                    PaddockComboBox.DisplayMemberPath = "Paddock_Name";
                    PaddockComboBox.SelectedValuePath = "Paddock_Id";
                    PaddockComboBox.SelectedIndex = 0;
                }
                else
                {
                    MessageBox message = new MessageBox("Please create paddock.");
                    message.Show();
                    this.Visibility = Visibility.Hidden;
                }

            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectPaddockWindow", User_Id);
                return;
            }
        }
        public void ValueFromMapLoad(string UserId)
        {
            try
            {
                User_Id = UserId;

                GetMapAreaFromDatabase();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: "+ ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectPaddockWindow", User_Id);
                return;
            }
        }

        private void SelectPaddockBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CloseAllWindows();

                string MenuFlag = "SelPaddock";

                MapLoad MapLoad = new MapLoad(User_Id, SelectPaddock, MenuFlag);
                MapLoad.Show();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: "+ ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectPaddockWindow", User_Id);
                return;
            }
        }

        private void CloseAllWindows()
        {
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > 0; intCounter--)
                App.Current.Windows[intCounter].Close();
        }

        private void PaddockComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SelectPaddock = PaddockComboBox.SelectedValue.ToString();

                DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                string ImagePath = DAL_CreateWayPoint.GetSelectPaddockImageFromDB(User_Id, SelectPaddock);

                if (!String.IsNullOrEmpty(ImagePath))
                {
                    PaddockImage.Source = new BitmapImage(new Uri(ImagePath));
                }
                else
                {
                    PaddockImage.Source = new BitmapImage(new Uri(System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImgNull"].ToString())));
                }

                //MessageBox.Show(ImagePath);
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: "+ ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectPaddockWindow", User_Id);
                return;
            }
        }

        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }
        
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectPaddockWindow", User_Id);
            }
        }
    }
}
