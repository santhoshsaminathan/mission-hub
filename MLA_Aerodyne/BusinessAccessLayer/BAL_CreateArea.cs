﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.BuisnessAccessLayer
{
    public class BAL_CreateArea
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public string AreaType { get; set; }

        public string PointofInterest { get; set; }

        public string POT_Longitude { get; set; }

        public string POT_Latitude { get; set; }

        public string CreatedDateTime { get; set; }
    }

    public class Polygon_OuterboundaryIS
    {
        private string _ID;
        private string _Latitude;
        private string _Longitude;
        private string _CreeatedDate;
        private string _PlacemarkID;
        private string _Name;
        private string _Area;
        private string _Description;

        private string _Altitude;

        public string ID
        {
            get { return this._ID; }
            set { this._ID = value; }
        }
        public string Latitude
        {
            get { return this._Latitude; }
            set { this._Latitude = value; }
        }
        public string Longitude
        {
            get { return this._Longitude; }
            set { this._Longitude = value; }
        }
        public string Altitude
        {
            get { return this._Altitude; }
            set { this._Altitude = value; }

        }
        public string Area
        {
            get { return this._Area; }
            set { this._Area = value; }

        }
        public string PlacemarkID
        {
            get { return this._PlacemarkID; }
            set { this._PlacemarkID = value; }

        }
        public string Name
        {
            get { return this._Name; }
            set { this._Name = value; }
        }

        public string Description
        {
            get { return this._Description; }
            set { this._Description = value; }

        }

        public string CreeatedDate
        {
            get { return this._CreeatedDate; }
            set { this._CreeatedDate = value; }

        }

    }
}
