﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.BusinessAccessLayer
{
    public class clsAICount
    {
        public int UserId { get; set; }
        public string CatImg { get; set; }
        public string AttachmentName { get; set; }
        public string ImgPath { get; set; }
        public string ImgType { get; set; }
        public string ImgSize { get; set; }
        public int LiveStockId { get; set; }
        public int LiveStockCount { get; set; }
        public DateTime CreateDateTime { get; set; }
    }
}
