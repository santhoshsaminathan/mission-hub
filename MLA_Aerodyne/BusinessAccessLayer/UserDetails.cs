﻿namespace MLA_Aerodyne.BuisnessAccessLayer
{
    public  class UserDetails
    {
        public string Lusername { get; set; }
        public string Lpassword { get; set; }
        public string UserID { get; set; }
        public string ErrorLog { get; set; }
    }

    public class UserMacAddress
    {
        public string UserID { get; set; }
        public string MACAddress { get; set; }
    }

    public class GetUserData
    {
        public int User_Id { get; set; }
        public string User_Name { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LandMark { get; set; }
        public string Area { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string EmailId { get; set; }
        public int CountryCode { get; set; }
        public int LandLineNo { get; set; }
        public int MobileNo { get; set; }
        public int User_type { get; set; }
        public int Status { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedOn { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Zipcode { get; set; }
        public string Address { get; set; }
        public string Phonenumber { get; set; }
        public string Licence_Code { get; set; }
        public string MAC_Address { get; set; }
    }
}