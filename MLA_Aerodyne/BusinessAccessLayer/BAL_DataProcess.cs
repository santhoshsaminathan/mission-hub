﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.BusinessAccessLayer
{
    public class FileAttachment
    {
        public string FileAttachmentName { get; set; }
        public string OriginalFileName { get; set; }
        public string Size { get; set; }
        public string Type { get; set; }
        // public byte[] File { get; set; }
        public Stream File { get; set; }
        public string Path { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public int Transferd_Status { get; set; }

    }
    public class Filecheck
    {

        public string filetype { get; set; }
        //  public sbyte[] file { get; set; }

        public string path { get; set; }
    }
}
