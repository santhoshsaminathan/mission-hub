﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.BuisnessAccessLayer
{
    class LandmarkDetails
    {
            public string ViewImage { get; set; }
            public string LandMark_Id { get; set; }      
            public string LandMark_Name { get; set; }
            public string LandMark_Lat { get; set; }
            public string LandMark_Long { get; set; }
            public string LandMark_Alt { get; set; }
    }
}
