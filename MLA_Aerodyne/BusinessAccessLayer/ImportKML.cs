﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.BusinessAccessLayer
{
        public class ImportKML
        {
        }
        public class KMLData
        {
            private string _ID;
            private string _Name;
            private string _Description;
            private string _CreeatedDate;

            public string ID
            {
                get { return this._ID; }
                set { this._ID = value; }
            }
            public string Name
            {
                get { return this._Name; }
                set { this._Name = value; }
            }

            public string Description
            {
                get { return this._Description; }
                set { this._Description = value; }

            }
            public string CreeatedDate
            {
                get { return this._CreeatedDate; }
                set { this._CreeatedDate = value; }

            }

        }

        public class KMLPlacemark
        {
            private string _ID;
            private string _Name;
            private string _Description;
            private string _CreeatedDate;
            private string _StyleID;

            public string ID
            {
                get { return this._ID; }
                set { this._ID = value; }
            }
            public string StyleID
            {
                get { return this._StyleID; }
                set { this._StyleID = value; }
            }
            public string Name
            {
                get { return this._Name; }
                set { this._Name = value; }
            }

            public string Description
            {
                get { return this._Description; }
                set { this._Description = value; }

            }
            public string CreeatedDate
            {
                get { return this._CreeatedDate; }
                set { this._CreeatedDate = value; }

            }


        }
        public class KMLStyle
        {
            private string _ID;
            private string _Description;
            private string _CreeatedDate;
            private string _KML_ID;

            public string ID
            {
                get { return this._ID; }
                set { this._ID = value; }
            }
            public string KML_ID
            {
                get { return this._KML_ID; }
                set { this._KML_ID = value; }
            }

            public string Description
            {
                get { return this._Description; }
                set { this._Description = value; }

            }
            public string CreeatedDate
            {
                get { return this._CreeatedDate; }
                set { this._CreeatedDate = value; }

            }

        }

        public class PolyStyle
        {
            private string _ID;
            private string _Color;
            private string _fill;
            private string _CreeatedDate;
            private string _StyleID;
            private string _outline;

            public string ID
            {
                get { return this._ID; }
                set { this._ID = value; }
            }
            public string StyleID
            {
                get { return this._StyleID; }
                set { this._StyleID = value; }
            }
            public string outline
            {
                get { return this._outline; }
                set { this._outline = value; }
            }
            public string Color
            {
                get { return this._Color; }
                set { this._Color = value; }

            }
            public string fill
            {
                get { return this._fill; }
                set { this._fill = value; }

            }
            public string CreeatedDate
            {
                get { return this._CreeatedDate; }
                set { this._CreeatedDate = value; }

            }

        }

        public class LineStyle
        {
            private string _ID;
            private string _Color;
            private string _Width;
            private string _CreeatedDate;
            private string _StyleID;

            public string ID
            {
                get { return this._ID; }
                set { this._ID = value; }
            }
            public string StyleID
            {
                get { return this._StyleID; }
                set { this._StyleID = value; }
            }
            public string Color
            {
                get { return this._Color; }
                set { this._Color = value; }

            }
            public string Width
            {
                get { return this._Width; }
                set { this._Width = value; }

            }
            public string CreeatedDate
            {
                get { return this._CreeatedDate; }
                set { this._CreeatedDate = value; }

            }

        }
        public class Polygon_OuterboundaryIS
        {
            private string _ID;
            private string _Latitude;
            private string _Longitude;
            private string _CreeatedDate;
            private string _PlacemarkID;
            private string _Name;
            private string _Description;

            private string _Altitude;

            public string ID
            {
                get { return this._ID; }
                set { this._ID = value; }
            }
            public string Latitude
            {
                get { return this._Latitude; }
                set { this._Latitude = value; }
            }
            public string Longitude
            {
                get { return this._Longitude; }
                set { this._Longitude = value; }
            }
            public string Altitude
            {
                get { return this._Altitude; }
                set { this._Altitude = value; }

            }
            public string PlacemarkID
            {
                get { return this._PlacemarkID; }
                set { this._PlacemarkID = value; }

            }
            public string Name
            {
                get { return this._Name; }
                set { this._Name = value; }
            }

            public string Description
            {
                get { return this._Description; }
                set { this._Description = value; }

            }

            public string CreeatedDate
            {
                get { return this._CreeatedDate; }
                set { this._CreeatedDate = value; }

            }

        }
        public class Polygon_InnerboundaryIS
        {
            private string _ID;
            private string _Latitude;
            private string _Longitude;
            private string _CreeatedDate;
            private string _PlacemarkID;
            private string _InnerlistID;
            private string _Altitude;

            public string ID
            {
                get { return this._ID; }
                set { this._ID = value; }
            }
            public string Latitude
            {
                get { return this._Latitude; }
                set { this._Latitude = value; }
            }
            public string Longitude
            {
                get { return this._Longitude; }
                set { this._Longitude = value; }
            }
            public string Altitude
            {
                get { return this._Altitude; }
                set { this._Altitude = value; }

            }
            public string PlacemarkID
            {
                get { return this._PlacemarkID; }
                set { this._PlacemarkID = value; }

            }
            public string InnerlistID
            {
                get { return this._InnerlistID; }
                set { this._InnerlistID = value; }

            }
            public string CreeatedDate
            {
                get { return this._CreeatedDate; }
                set { this._CreeatedDate = value; }

            }

        }

    
}
