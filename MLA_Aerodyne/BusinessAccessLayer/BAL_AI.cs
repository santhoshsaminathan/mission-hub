﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace MLA_Aerodyne.BusinessAccessLayer
{
    public class ImageGridData : INotifyPropertyChanged
    {
        private string _Title;
        public string _File_path;

        public string Title
        {
            get { return this._Title; }
            set { this._Title = value; }
        }
        public string File_path
        {
            get { return this._File_path; }
            set { this._File_path = value; }
        }

        private BitmapImage _ImageData;
        public BitmapImage ImageData
        {
            get { return this._ImageData; }
            set { this._ImageData = value; }
        }
        private bool isChecked;
        public bool IsChecked
        {
            get { return isChecked; }
            set
            {
                if (isChecked == value) return;
                isChecked = value;
                RaisePropertyChanged("IsChecked");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propName)
        {
            PropertyChangedEventHandler eh = PropertyChanged; if (eh != null)
            { eh(this, new PropertyChangedEventArgs(propName)); }
        }
        public ImageGridData(string t, BitmapImage i, bool c, string f)
        {
            this.Title = t;
            this.ImageData = i;
            this.IsChecked = c;
            this.File_path = f;
        }
    }

    public class VideoGridData : INotifyPropertyChanged
    {
        private string _Title;
        public string _File_path;

        public string Title
        {
            get { return this._Title; }
            set { this._Title = value; }
        }
        public string File_path
        {
            get { return this._File_path; }
            set { this._File_path = value; }
        }

        private BitmapImage _ImageData;
        public BitmapImage ImageData
        {
            get { return this._ImageData; }
            set { this._ImageData = value; }
        }
        private bool isChecked;
        public bool IsChecked
        {
            get { return isChecked; }
            set
            {
                if (isChecked == value) return;
                isChecked = value;
                RaisePropertyChanged("IsChecked");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propName)
        {
            PropertyChangedEventHandler eh = PropertyChanged; if (eh != null)
            { eh(this, new PropertyChangedEventArgs(propName)); }
        }
        public VideoGridData(string t, BitmapImage i, bool c, string f)
        {
            this.Title = t;
            this.ImageData = i;
            this.IsChecked = c;
            this.File_path = f;
        }
    }

    public class AIListData
    {
        private string _ID;
        private string _Mission_Name;
        private string _Paddock_Name;
        private string _imageCapturedDate;
        private string _DI_ImagePath;
        private string _Paddock_Id;
        private string _File_Type;
        private string _File_Format;
        private string _Mission_Type;
        private string _Mission_ID;

        public string ID
        {
            get { return this._ID; }
            set { this._ID = value; }
        }
        public string Mission_ID
        {
            get { return this._Mission_ID; }
            set { this._Mission_ID = value; }
        }

        public string File_Format
        {
            get { return this._File_Format; }
            set { this._File_Format = value; }
        }

        public string Mission_Name
        {
            get { return this._Mission_Name; }
            set { this._Mission_Name = value; }

        }
        public string File_Type
        {
            get { return this._File_Type; }
            set { this._File_Type = value; }

        }
        public string Mission_Type
        {
            get { return this._Mission_Type; }
            set { this._Mission_Type = value; }

        }
        public string Paddock_Name
        {
            get { return this._Paddock_Name; }
            set { this._Paddock_Name = value; }

        }
        public string DI_ImagePath
        {
            get { return this._DI_ImagePath; }
            set { this._DI_ImagePath = value; }

        }
        public string Paddock_Id
        {
            get { return this._Paddock_Id; }
            set { this._Paddock_Id = value; }

        }
        public string imageCapturedDate
        {
            get { return this._imageCapturedDate; }
            set { this._imageCapturedDate = value; }
        }
        

    }
}
