﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.BusinessAccessLayer
{
    public class clsAIImage
    {
        public class FileAttachment
        {
            public string UploadedID { get; set; }
            public int FileAttachmentID { get; set; }
            public string FileAttachmentName { get; set; }
            public string OriginalFileName { get; set; }
            public int Size { get; set; }
            public string FileExtension { get; set; }
            public string Type { get; set; }
            public byte[] File { get; set; }
            public string Path { get; set; }
            public string Driver { get; set; }
            public int CreatedBy { get; set; }
            public string CreatedDate { get; set; }
            public string UpdatedBy { get; set; }
            public string UpdatedDate { get; set; }
        }
    }
}
