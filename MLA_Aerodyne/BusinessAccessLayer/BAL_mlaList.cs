﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.BusinessAccessLayer
{
    public class ImageListData
    {
        private string _ID;
        private string _MapareaName;
        private string _MapName;
        private string _DateCreated;
        private string _DateModified;
        public string ID
        {
            get { return this._ID; }
            set { this._ID = value; }
        }

        public string MapareaName
        {
            get { return this._MapareaName; }
            set { this._MapareaName = value; }

        }
        public string MapName
        {
            get { return this._MapName; }
            set { this._MapName = value; }

        }
        public string DateCreated
        {
            get { return this._DateCreated; }
            set { this._DateCreated = value; }
        }
        public string DateModified
        {
            get { return this._DateModified; }
            set { this._DateModified = value; }
        }

    }

}
