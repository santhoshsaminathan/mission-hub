﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.BuisnessAccessLayer
{
   public class MissionHubDetails
   {
        public int userID { get; set; }
        public int Mission_ID { get; set; }
        public int Missiondata_ID { get; set; }
        public int Plot_ID { get; set; }
        public int Paddock_ID { get; set; }
        public int Waypoint_ID { get; set; }
        public int Landmark_ID { get; set; }
        public int Flying_ID { get; set; }
        public int Pilot_ID { get; set; }
        public DateTime DateOfFlight { get; set; }
        public int Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string Alert { get; set; }
        public string Mission_Name { get; set; }
        public string Paddock_Name { get; set; }
        public string Mission_Type { get; set; }
        public string Mission_Select { get; set; }
        public string Waypoint_Name { get; set; }
        public string Pilot_Name { get; set; }
        public string File_Path { get; set; }
   }

    public class Lora_Water
    {
        public string Time { get; set; }
        public string DO { get; set; }
        public string EC { get; set; }
        public string pH { get; set; }
        public string Temperature { get; set; }
        public string WaterLevel { get; set; }
        public string Turbidity { get; set; }
        public string RSSI { get; set; }
        public string Paddock_ID { get; set; }
        public string Paddock_Name { get; set; }
        public String userID { get; set; }
        public DateTime CreatedOn { get; set; }
    }
	public class LiveStockInfo
    {
        public int livestockid { get; set; }
        public int livestockcount { get; set; }
        public int PaddockId { get; set; }
        public string PaddockName { get; set; }
        public int userid { get; set; }
    }

    public class LiveStockAttch
    {
        public string attachmentname { get; set; }
        public string path { get; set; }
        public string type { get; set; }
        public string size { get; set; }
        public int livestockid { get; set; }
        public int PaddockId { get; set; }
        public string PaddockName { get; set; }
        public int userid { get; set; }
    }
    public class waterdetails
    {
        public string Time { get; set; }
        public double DO { get; set; }
        public double EC { get; set; }
        public double pH { get; set; }
        public double Temperature { get; set; }
        public double WaterLevel { get; set; }
        public double RSSI { get; set; }
        public int PaddockId { get; set; }
        public string PaddockName { get; set; }
        public int UserId { get; set; }
    }
}
