﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.BusinessAccessLayer
{
    public class Paddock_Master
    {
        public int IsActive { get; set; }
        public string Paddock_Image { get; set; }
        public string CreatedOn { get; set; }
        public int User_ID { get; set; }
        public int Plot_ID { get; set; }
        public int Paddock_Id { get; set; }
        public string Paddock_Name { get; set; }
    }
    public class Paddock_Transact
    {
        public int PadTransact_ID { get; set; }
        public int Paddock_Id { get; set; }
        public string Paddock_Latitude { get; set; }
        public string Paddock_Longitude { get; set; }
        public string Paddock_Altitude { get; set; }
        public string CreatedOn { get; set; }
    }
}

