﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for ConfirmationWindow.xaml
    /// </summary>
    public partial class ConfirmationWindow : Window
    {
        int status = 0;
        public ConfirmationWindow(string message)
        {
            InitializeComponent();
            messageLbl.Inlines.Add(new Bold(new Run(message)));

        }

        private void messageBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                status = 1;
                
                ((MissionHub)this.Owner)._deleteMission();
                this.Visibility = Visibility.Hidden;
                return;
            }
            
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
            }
        }
        public int messageConfirm(int st)
        {
            try
            {
                MessageBox message = new MessageBox("Mission not deleted." );
                message.Show();
                return status;
            }
   
             catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return 0;
            }
        }

        private void nomessageBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                status = 0;
                this.Visibility = Visibility.Hidden;
                return;
            }
            
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
            }
        }
    }
}
