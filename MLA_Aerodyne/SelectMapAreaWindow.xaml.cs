﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for SelectMapAreaWindow.xaml
    /// </summary>
    public partial class SelectMapAreaWindow : Window
    {
        public string User_Id;
        public int init=1;
        public string SelectMapArea;
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();
        DataSet ds_Combo = new DataSet();

        public SelectMapAreaWindow(string User_ID)
        {
            InitializeComponent();
            ValueFromMapLoad(User_ID);
            MapAreaComboBox.SelectedIndex = 1;
        }

        private void MyComboBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            MapAreaComboBox.IsDropDownOpen = true;
        }

        public void GetMapAreaFromDatabase()
        {
            try
            {
                DataAccessLayer.DAL_CreatePaddock DAL_CreatePaddock = new DataAccessLayer.DAL_CreatePaddock();
                 ds_Combo = DAL_CreatePaddock.GetMapAreaFromDB(User_Id);

                if (ds_Combo.Tables[0].Rows.Count != 0)
                {
                    MapAreaComboBox.DataContext = ds_Combo.Tables[0].DefaultView;
                    MapAreaComboBox.DisplayMemberPath = "Plot_Name";
                    MapAreaComboBox.SelectedValuePath = "Plot_ID";
                    MapAreaComboBox.SelectedIndex = 0;
                }
                else
                {
                    MessageBox message = new MessageBox("Please create map area.");
                    message.Show();
                    this.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectMapAreaWindow", User_Id);
                return;
            }
        }

        public void ValueFromMapLoad(string UserId)
        {
            try
            {
                User_Id = UserId;
                GetMapAreaFromDatabase();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectMapAreaWindow", User_Id);
                return;
            }
        }

        private void SelectMapAreaBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CloseAllWindows();

                string MenuFlag = "SelMapArea";

                MapLoad MapLoad = new MapLoad(User_Id, SelectMapArea, MenuFlag);
                MapLoad.Show();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectMapAreaWindow", User_Id);
                return;
            }
        }

        private void CloseAllWindows()
        {
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > 0; intCounter--)
                App.Current.Windows[intCounter].Close();
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            ((MapLoad)this.Owner).closebutton();
        }

        private void MapAreaComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SelectMapArea = MapAreaComboBox.SelectedValue.ToString();

                DataAccessLayer.DAL_CreatePaddock DAL_CreatePaddock = new DataAccessLayer.DAL_CreatePaddock();
                string ImagePath = DAL_CreatePaddock.GetSelectMapAreaImageFromDB(User_Id, SelectMapArea);

                if (!String.IsNullOrEmpty(ImagePath))
                {
                    MapAreaImage.Source = new BitmapImage(new Uri(ImagePath));
                }
                else
                {
                    MapAreaImage.Source = new BitmapImage(new Uri(System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImgNull"].ToString())));
                }


                //MessageBox.Show(ImagePath);  CaptureImgNull
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectMapAreaWindow", User_Id);
                return;
            }
        }

        private void SelectBtn_MouseLeave(object sender, MouseEventArgs e)
        {
            //MessageBox.Show("llll");
        }

        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectMapAreaWindow", User_Id);
            }
        }

       
    }



}
