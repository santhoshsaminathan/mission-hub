﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.NetworkInformation;
using System.Net;
using System.Configuration;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;
using System.Data;
using MLA_Aerodyne.BuisnessAccessLayer;
using System.IO.Compression;
using Path = System.IO.Path;
using static MLA_Aerodyne.MapLoad;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    //public string GUser_Id="";
    public partial class Login : Window
    {
        public string GUser_Id { get; set; }
        public string UserName = "admin";
        public string Password = "admin";

        public string LicenseNo = "LIC0190805";

        public string OTPString = "STRlkjiofgeriuytg";

        public string Connection_Status = "false";

        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        public Login()
        {
            InitializeComponent();

            TextboxCheckText();
            UserNameTxt.Focus();
            string URL = "http://www.google.com/";
            bool NetConStatus = IsInternetConnection(URL);

            if (NetConStatus == true)
                ConnectionStatus(NetConStatus);
            else
                ConnectionStatus(NetConStatus);

            if (Connection_Status == "true")
            {
                var client = new HttpClient();

                string apiUrl = ConfigurationManager.AppSettings["MLA_WebAPI"].ToString() + "/SynToMissionHub/GetSynUserMasterToApp";
                WebClient webClient = new WebClient();
                Stream stream = webClient.OpenRead(apiUrl);
                StreamReader sr = new StreamReader(stream);
                string result = string.Empty;

                string json1 = sr.ReadToEnd().Trim();

                DataTable DS_Values = JsonConvert.DeserializeObject<DataTable>(json1);

                List<GetUserData> GetUserData = new List<GetUserData>();

                if (DS_Values.Rows.Count > 0)
                {
                    GetUserData = (from DataRow row in DS_Values.Rows
                                     select new GetUserData
                                     {
                                         //Intake
                                         User_Id = Convert.ToInt32(row["User_Id"]),
                                         User_Name = row["User_Name"].ToString(),
                                         Password = row["Password"].ToString(),
                                         FirstName = row["FirstName"].ToString(),
                                         LastName = row["LastName"].ToString(),
                                         LandMark = row["LandMark"].ToString(),
                                         Area = row["Area"].ToString(),
                                         City = row["City"].ToString(),
                                         Country = row["Country"].ToString(),
                                         EmailId = row["EmailId"].ToString(),
                                         CountryCode = row["CountryCode"].ToString() != "" ? Convert.ToInt32(row["CountryCode"]) : 0,
                                         LandLineNo = row["LandLineNo"].ToString() != "" ? Convert.ToInt32(row["LandLineNo"]) : 0,
                                         MobileNo = row["MobileNo"].ToString() != "" ? Convert.ToInt32(row["MobileNo"]) : 0,
                                         User_type = row["User_type"].ToString() != "" ? Convert.ToInt32(row["User_type"]) : 0,
                                         Status = row["Status"].ToString() != "" ? Convert.ToInt32(row["Status"]) : 0,
                                         CreatedOn = row["CreatedOn"].ToString(),
                                         UpdatedOn = row["UpdatedOn"].ToString(),
                                         Latitude = row["Latitude"].ToString(),
                                         Longitude = row["Longitude"].ToString(),
                                         Zipcode = row["Zipcode"].ToString(),
                                         Address = row["Address"].ToString(),
                                         Phonenumber = row["Phonenumber"].ToString(),
                                         Licence_Code = row["Licence_Code"].ToString(),
                                         MAC_Address = row["MAC_Address"].ToString(),
                                     }).ToList();
                }

                DataAccessLayer.Login_DAL loginUser = new DataAccessLayer.Login_DAL();
                loginUser.ClearUserMasterTable();

                foreach (var UserData in GetUserData)
                {
                    DataAccessLayer.Login_DAL login = new DataAccessLayer.Login_DAL();
                    login.Insert_UserDataFromiLams(UserData);
                }
            }
        }

        public void TextboxCheckText()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(UserNameTxt.Text))
                    UserNameTxt.Text = "Username or Email";

                if (string.IsNullOrWhiteSpace(PasswordTxt.Text))
                    PasswordTxt.Text = "Password";
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Login", "");
                return;
            }
        }

        public static bool IsInternetConnection(String URL)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Timeout = 5000;
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public void ConnectionStatus(bool NetConStatus)
        {
            if (NetConStatus == true)
            {
                Connection_Status = "true";
                //MessageBox.Show("This computer is connected to the internet");
            }
            else
            {
                Connection_Status = "false";
                //MessageBox.Show("This computer is not connected to the internet. Please Connect Internet!!");
            }
        }

        private void OnUserNameTxt_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UserNameTxt.Text.Equals("Username or Email", StringComparison.OrdinalIgnoreCase))
                {
                    UserNameTxt.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Login", "");
                return;
            }
        }
        private void OnUserNameTxt_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(UserNameTxt.Text))
                {
                    UserNameTxt.Text = "Username or Email";
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Login", "");
                return;
            }
        }
        private void OnPasswordTxt_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PasswordTxt.Text.Equals("Password", StringComparison.OrdinalIgnoreCase))
                {
                    PasswordTxt.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Login", "");
                return;
            }
        }

        private void RegSaveBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RegUserNameTxt.Text == "")
                {
                    MessageBox message = new MessageBox("Please enter username.");
                    message.Show();
                    RegUserNameTxt.Focus();
                    return;
                }
                else if (RegPasswordBoxTxt.Password == "")
                {
                    MessageBox message = new MessageBox("Please enter password.");
                    message.Show();
                    RegPasswordBoxTxt.Focus();
                    return;
                }
                else if (RegCnfPasswordBoxTxt.Password == "")
                {
                    MessageBox message = new MessageBox("Please enter confirm password.");
                    message.Show();
                    RegCnfPasswordBoxTxt.Focus();
                    return;
                }
                else if (RegPasswordBoxTxt.Password != RegCnfPasswordBoxTxt.Password)
                {
                    MessageBox message = new MessageBox("Mismatch password.");
                    message.Show();
                    RegPasswordBoxTxt.Password = "";
                    RegCnfPasswordBoxTxt.Password = "";
                    RegPasswordBoxTxt.Focus();
                    return;
                }
                else if (RegUserNameTxt.Text != "" && RegPasswordBoxTxt.Password != "" && RegCnfPasswordBoxTxt.Password != "")
                {
                    //MessageBox.Show("Form Load!!");
                    DataAccessLayer.Login_DAL regLog = new DataAccessLayer.Login_DAL();
                    regLog.Insert_Username_Password(RegUserNameTxt.Text, RegPasswordBoxTxt.Password);
                    MessageBox message = new MessageBox("Registration successfull.");
                    message.Show();
                    string MenuFlag = "";
                    string SelectMapArea = "";
                    MapLoad MapLoad = new MapLoad(GUser_Id, SelectMapArea, MenuFlag);
                    MapLoad.Show();
                    this.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Login", "");
                return;
            }
        }

        private void LicenseNo_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (LicenseNo == LicenseNoTxt.Text)
                {
                    LicenseNoTxt.IsEnabled = false;

                    OTPStringTxt.IsEnabled = true;
                    //OTPStringTxt.Focus();

                    MessageBox message = new MessageBox("OTP string sent to your e-mail. Please check your e-mail and enter OTP String.");
                    message.Show();
                }
                else
                {
                    //MessageBox.Show("Invalid License No!!");
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Login", "");
                return;
            }
        }

        private void OTPString_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (OTPString == OTPStringTxt.Text)
                {
                    LicenseNoTxt.IsEnabled = false;

                    OTPStringLbl.Visibility = Visibility.Hidden;
                    OTPStringTxt.Visibility = Visibility.Hidden;

                    UserNameLbl.Visibility = Visibility.Visible;
                    RegUserNameTxt.Visibility = Visibility.Visible;
                    PasswordLpl.Visibility = Visibility.Visible;
                    RegPasswordBoxTxt.Visibility = Visibility.Visible;
                    CnfPasswordLbl.Visibility = Visibility.Visible;
                    RegCnfPasswordBoxTxt.Visibility = Visibility.Visible;

                    RegSaveBtn.Visibility = Visibility.Visible;

                    RegUserNameTxt.Focus();
                }
                else
                {
                    //MessageBox.Show("Invalid OTP String!!");
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Login", "");
                return;
            }
        }

        private void RegCancelBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RegistrationForm.Visibility = Visibility.Hidden;
                LoginForm.Visibility = Visibility.Visible;
                UserNameTxt.Focus();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Login", "");
                return;
            }
        }

        private void Hyperlink_RequestNavigate(object sender, RoutedEventArgs e)
        {
            try
            {
                LoginForm.Visibility = Visibility.Hidden;
                RegistrationForm.Visibility = Visibility.Visible;
                LicenseNoTxt.Focus();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                return;
            }
        }

        private void LoginBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                StringBuilder SB = new StringBuilder();
                //Console.WriteLine(databasename[0]);

                SB.Append("Login Process Started" + DateTime.Now.ToString());
                SB.AppendFormat(Environment.NewLine);


                MLA_Aerodyne.BuisnessAccessLayer.UserDetails luserdetails = new MLA_Aerodyne.BuisnessAccessLayer.UserDetails();
                string username = "";
                string password = "";
                DataAccessLayer.Login_DAL login = new DataAccessLayer.Login_DAL();
                luserdetails = login.Check_Username_Password(UserNameTxt.Text, PasswordBoxTxt.Password);

                SB.Append("Authentication Process Completed" + DateTime.Now.ToString());
                SB.AppendFormat(Environment.NewLine);

                if (String.IsNullOrEmpty(luserdetails.UserID))
                {
                    MessageBox message = new MessageBox("Invalid login details.");
                    message.Show();

                    SB.Append("Invalid login details" + DateTime.Now.ToString());
                    SB.AppendFormat(Environment.NewLine);
                }
                else
                {
                    username = luserdetails.Lusername.ToString();
                    password = luserdetails.Lpassword.ToString();
                    GUser_Id = luserdetails.UserID.ToString();
                    //CMapAreaWindow cUserID = new CMapAreaWindow();
                    //cUserID.GetUserIDFromLogin(GUser_Id);
                    
                    //string MenuFlag = "";
                    //string SelectMapArea = "";

                    //MapLoad MapLoad = new MapLoad(GUser_Id, SelectMapArea, MenuFlag);
                    //VLandMarkWindow vl = new VLandMarkWindow(GUser_Id);
                    //VMapAreaWindow Vmap = new VMapAreaWindow(GUser_Id);

                    string Menu_Flag = "";
                    string Select_MapArea = "";
                    string MacStatus = "";
                    if (UserNameTxt.Text == username && PasswordBoxTxt.Password == password)
                    {
                        string MACAddress = GetMACAddress();

                        DataAccessLayer.Login_DAL MacAdd = new DataAccessLayer.Login_DAL();
                        //MacStatus = MacAdd.Check_MACAddress(GUser_Id, MACAddress);
                        MacStatus = "true";
                        if (MacStatus == "false")
                        {
                            DataAccessLayer.Login_DAL InsertMacAdd = new DataAccessLayer.Login_DAL();
                            MacStatus = InsertMacAdd.Insert_User_MacAddress(GUser_Id, MACAddress);

                            UserMacAddress UserMAC = new UserMacAddress
                            {
                                UserID = GUser_Id,
                                MACAddress = MACAddress
                            };

                            if (Connection_Status == "true")
                            {
                                var client = new HttpClient();
                                string InsertapiUrl = ConfigurationManager.AppSettings["MLA_WebAPI"].ToString() + "/UserMasterUpdate/UpdateUserMaster";
                                client.BaseAddress = new Uri(InsertapiUrl);
                                var response = client.PostAsJsonAsync("UpdateUserMaster", UserMAC).Result;

                                //if (response.IsSuccessStatusCode)
                                //{
                                //}
                            }
                        }

                        if (MacStatus == "Mismatch")
                        {
                            MessageBox message = new MessageBox("Invalid login.");
                            message.Show();
                            return;
                        }

                        if (MacStatus == "true")
                        {
                            MapLoad mapLoad = new MapLoad(GUser_Id, Select_MapArea, Menu_Flag);
                            mapLoad.Show();
                            this.Visibility = Visibility.Hidden;
                        }
                    }
                    else if (UserNameTxt.Text != username && PasswordBoxTxt.Password != password)
                    {
                        MessageBox message = new MessageBox("Invalid login details.");
                        message.Show();

                    }
                    else if (UserNameTxt.Text != username)
                    {
                        MessageBox message = new MessageBox("Invalid user name.");
                        message.Show();
                    }
                    else if (PasswordBoxTxt.Password != password)
                    {
                        MessageBox message = new MessageBox("Invalid password.");
                        message.Show();
                    }
                }

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(ConfigurationManager.AppSettings["LogFilePath"].ToString()))
                {
                    file.WriteLine(SB.ToString()); // "sb" is the StringBuilder
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Login", "");
                StringBuilder SB = new StringBuilder();
                SB.Append(ex + DateTime.Now.ToString());
                SB.AppendFormat(Environment.NewLine);

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(ConfigurationManager.AppSettings["LogFilePath"].ToString()))
                {
                    file.WriteLine(SB.ToString()); // "sb" is the StringBuilder
                }
                return;
            }
        }

        public string GetMACAddress()
        {
            string mac = "";
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {

                if (nic.OperationalStatus == OperationalStatus.Up && (!nic.Description.Contains("Virtual") && !nic.Description.Contains("Pseudo")))
                {
                    if (nic.GetPhysicalAddress().ToString() != "")
                    {
                        mac = nic.GetPhysicalAddress().ToString();
                    }
                }
            }
            return mac;
        }

        private void ClearBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UserNameTxt.Text = "";
                PasswordBoxTxt.Password = "";
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                return;
            }
        }

        private void OnPasswordTxt_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(PasswordTxt.Text))
                {
                    PasswordTxt.Text = "Password";
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Login", "");
                return;
            }
        }

    }

}
