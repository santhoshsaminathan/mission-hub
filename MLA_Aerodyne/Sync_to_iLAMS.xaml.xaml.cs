﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static MLA_Aerodyne.MapLoad;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for Sync_to_iLAMS.xaml
    /// </summary>
    public partial class Sync_to_iLAMS : Window
    {
        System.Data.DataSet sds = new System.Data.DataSet();
        DataAccessLayer.DAL_SynctoiLAMS syncDAL = new DataAccessLayer.DAL_SynctoiLAMS();
        MLA_Aerodyne.BusinessAccessLayer.AIListData iLAMSList = new MLA_Aerodyne.BusinessAccessLayer.AIListData();
       System.Data.DataSet ai_ds = new System.Data.DataSet();
        DataAccessLayer.DAL_ArtificialIntelligence ai_DAL = new DataAccessLayer.DAL_ArtificialIntelligence();
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        string _UserId = string.Empty;
        string _folderPath = string.Empty; 
        string File_Type = string.Empty;
        string File_Format = string.Empty;

        string _mission_Destination_folderName = Convert.ToString(ConfigurationManager.AppSettings["DestinationFilePath"].ToString());
        ProgressBar _progressBar;

        public string Connection_Status = "false";
        public Sync_to_iLAMS(string userID)
        {
            InitializeComponent();
            _UserId = userID;
            sds = syncDAL.Get_AIimagelist(userID);
            listview_AI();
        }
        public void listview_AI()
        {
            try
            {
                var MimgL = new List<MLA_Aerodyne.BusinessAccessLayer.AIListData>();
                DataTable Mdt = sds.Tables[0];
                for (int i = 0; i < Mdt.Rows.Count; i++)
                {
                    MimgL.Add(new MLA_Aerodyne.BusinessAccessLayer.AIListData
                    {
                        ID = sds.Tables[0].Rows[i]["File_ID"].ToString(),
                        Mission_Name = sds.Tables[0].Rows[i]["Mission_Name"].ToString(),
                        Paddock_Name = sds.Tables[0].Rows[i]["Paddock_Name"].ToString(),
                        File_Type = sds.Tables[0].Rows[i]["File_Type"].ToString(),
                        File_Format = sds.Tables[0].Rows[i]["File_Type"].ToString(),
                        Mission_Type = sds.Tables[0].Rows[i]["MissionType"].ToString(),
                        Paddock_Id = sds.Tables[0].Rows[i]["Paddock_Id"].ToString(),
                        imageCapturedDate = sds.Tables[0].Rows[i]["File_Captured_Date"].ToString(),
                        Mission_ID= sds.Tables[0].Rows[i]["Mission_ID"].ToString(),
                    });

                    if (MimgL[i].File_Type == ".jpeg" || MimgL[i].File_Type == ".jpg" || MimgL[i].File_Type == ".JPG" || MimgL[i].File_Type == ".JPEG")
                    {
                        MimgL[i].File_Type = "Image";
                    }
                    else
                    {
                        MimgL[i].File_Type = "Video";
                    }
                }

                var AI_View = CollectionViewSource.GetDefaultView(MimgL);
                AI_View.SortDescriptions.Add(new SortDescription("imageCapturedDate", ListSortDirection.Descending));
                iLAMSlistView1.ItemsSource = AI_View;
               
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show(); 
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Sync_to_iLAMS", _UserId);
            }
        }

        private void CloseAllWindows()
        {
            try
            {
                for (int intCounter = App.Current.Windows.Count - 1; intCounter > 0; intCounter--)
                    App.Current.Windows[intCounter].Close();
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Sync_to_iLAMS", _UserId);
            }
        }

        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Sync_to_iLAMS", _UserId);
            }
        }
        public static bool IsInternetConnection(String URL)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Timeout = 5000;
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public void sync_toiLAMS()
        {
            //CloseAllWindows();
            int res_message = 0;
            if (_progressBar == null)
                _progressBar = new ProgressBar("Sync to iLAMS in progress...");
            _progressBar.Show();


            string DBName = "dbmla_aerodyne_" + _UserId + ".zip";

            string DBNameZip = "dbmla_aerodyne_" + _UserId;

            string DBFileToCopy = System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString());
            string NewFileLocation = System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBSaveConfig"].ToString());
            string NewFolderLocation = System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBSaveFolder"].ToString());

            FileInfo file = new FileInfo(NewFileLocation);

            if (System.IO.Directory.Exists(NewFolderLocation))
            {
                if (System.IO.File.Exists(DBFileToCopy))
                {
                    System.IO.File.Copy(DBFileToCopy, NewFileLocation, true);
                }
            }
            else
            {
                Directory.CreateDirectory(NewFolderLocation);

                if (System.IO.File.Exists(DBFileToCopy))
                {
                    System.IO.File.Copy(DBFileToCopy, NewFileLocation, true);
                }
            }

            string sZipFile = System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBZipFolder"].ToString()) + DBName + "";

            if (System.IO.File.Exists(sZipFile))
            {
                System.IO.File.Delete(sZipFile);
                ZipFile.CreateFromDirectory(NewFolderLocation, sZipFile);
            }
            else
            {
                ZipFile.CreateFromDirectory(NewFolderLocation, sZipFile);
            }


            string URL = "http://www.google.com/";
            bool NetConStatus = IsInternetConnection(URL);

            if (NetConStatus == true)
                ConnectionStatus(NetConStatus);
            else
                ConnectionStatus(NetConStatus);
            
            if (Connection_Status == "true")
            {
                try
                {
                   
                    string InsertapiUrl = ConfigurationManager.AppSettings["MLA_WebAPI"].ToString() + "/GetSQLiteDBFile/SaveSQLiteDBFile";
                    var client = new HttpClient();
                    client.BaseAddress = new Uri(InsertapiUrl);
                    byte[] Filedata = File.ReadAllBytes(System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBZipFolder"].ToString()) + DBName + "");

                    GetData EI = new GetData
                    {
                        filedata = Filedata,
                        filename = DBName,
                        UserId = _UserId,
                        DBFileName = DBNameZip
                    };

                    var response = client.PostAsJsonAsync("SaveSQLiteDBFile", EI).Result;
                    if (response.IsSuccessStatusCode)
                    { }

                    if (File_Type == "Image")
                    {
                        var content = new MultipartFormDataContent();

                    DataAccessLayer.DAL_SaveAICattleCount SaveCat = new DataAccessLayer.DAL_SaveAICattleCount();
                    DataSet AIImages = SaveCat.Select_AIProcessedCattleCount(_UserId, iLAMSList.Mission_ID, iLAMSList.Paddock_Id, iLAMSList.imageCapturedDate, iLAMSList.File_Format);

                    for (int i = 0; i < AIImages.Tables[0].Rows.Count; i++)
                    {
                        string MissionID = AIImages.Tables[0].Rows[i]["Mission_ID"].ToString();
                        string FileCapturedDate = AIImages.Tables[0].Rows[i]["File_Captured_Date"].ToString();
                        string ImageFileName = AIImages.Tables[0].Rows[i]["File_Name"].ToString();
                        string SelectedMissionName = AIImages.Tables[0].Rows[0]["Mission_Name"].ToString();
                        string SelectedPaddockName = AIImages.Tables[0].Rows[0]["Paddock_Name"].ToString();
                        string SelectedMissionTypeName = AIImages.Tables[0].Rows[0]["MissionType"].ToString();

                        //string Folderpath = "C:\\Program Files (x86)\\MLA_Aerodyne\\MLA_Aerodyne_Setup\\AI\\Scanned\\" + SelectedMissionName.Replace(" ", "") + "\\"+ SelectedPaddockName.Replace(" ", "") + "\\"+ FileCapturedDate.Replace("/", ".") + "\\Image";

                        string Folderpath = Convert.ToString(ConfigurationManager.AppSettings["DestinationFilePath"].ToString()) + "\\" + SelectedMissionName.Replace(" ", "") + "\\" + SelectedPaddockName.Replace(" ", "") + "\\" + FileCapturedDate.Replace("/", ".") + "\\Image";

                        // Add first file content 
                        var fileContent1 = new ByteArrayContent(File.ReadAllBytes(Folderpath + "\\" + ImageFileName));
                        fileContent1.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                        {
                            FileName = ImageFileName
                        };

                        content.Add(fileContent1);

                        //AITransferData AddAIValues = new AITransferData();

                        AITransferData AddAIValues = new AITransferData
                        {
                            _AttachmentName = ImageFileName,
                            _Type = AIImages.Tables[0].Rows[i]["File_Type"].ToString(),
                            _Size = AIImages.Tables[0].Rows[i]["File_Size"].ToString(),
                            _MissionId = MissionID,
                            _Mission_Name = SelectedMissionName,
                            _Mission_Type = SelectedMissionTypeName,
                            _PaddockId = AIImages.Tables[0].Rows[0]["Paddock_ID"].ToString(),
                            _PaddockName = SelectedPaddockName,
                            _UserId = _UserId,
                            _imageCapturedDate = FileCapturedDate,
                            _imagefile = content
                        };

                        var myContent = JsonConvert.SerializeObject(AddAIValues);
                        var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                        var byteContent = new ByteArrayContent(buffer);
                        byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                        string InsertapiUrl1 = ConfigurationManager.AppSettings["MLA_WebAPI"].ToString() + "/ImageUpload/MediaUpload";

                        var client_1 = new HttpClient();

                        client_1.BaseAddress = new Uri(InsertapiUrl1);

                        //client.BaseAddress = new Uri("http://172.20.8.51/MLAWEBAPI/api/LiveStock/Inserttblailivestockinformation");

                        // Read file data
                        FileStream fs = new FileStream(Folderpath + "\\" + ImageFileName, FileMode.Open, FileAccess.Read);
                        byte[] data = new byte[fs.Length];
                        fs.Read(data, 0, data.Length);
                        fs.Close();
                        
                        // Generate post objects
                        Dictionary<string, object> postParameters = new Dictionary<string, object>();
                        postParameters.Add("_AttachmentName", ImageFileName);
                        postParameters.Add("_Type", AIImages.Tables[0].Rows[i]["File_Type"].ToString());
                        postParameters.Add("_Size", AIImages.Tables[0].Rows[i]["File_Size"].ToString());
                        postParameters.Add("_MissionId", MissionID);
                        postParameters.Add("_Mission_Type", SelectedMissionTypeName);
                        postParameters.Add("_PaddockId", AIImages.Tables[0].Rows[0]["Paddock_ID"].ToString());
                        postParameters.Add("_PaddockName", SelectedPaddockName);
                        postParameters.Add("_UserId", _UserId);
                        postParameters.Add("_imageCapturedDate", FileCapturedDate);
                        postParameters.Add("_imagefile", new FormUpload.FileParameter(data, ImageFileName, AIImages.Tables[0].Rows[i]["File_Type"].ToString(), AIImages.Tables[0].Rows[i]["File_Size"].ToString(), MissionID, SelectedMissionName, AIImages.Tables[0].Rows[0]["Paddock_ID"].ToString(), SelectedPaddockName, _UserId, FileCapturedDate));

                        // Create request and receive response
                        string postURL = InsertapiUrl1;
                        string userAgent = "Someone";
                        HttpWebResponse webResponse = FormUpload.MultipartFormDataPost(postURL, userAgent, postParameters);

                        // Process response
                        StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                        string fullResponse = responseReader.ReadToEnd();
                        webResponse.Close();

                        var response1 = client_1.PostAsJsonAsync(ConfigurationManager.AppSettings["MLA_WebAPI"].ToString() + "/ImageUpload/MediaUpload", fullResponse).Result;

                        //var response = client_1.PostAsync("Inserttblailivestockinformation", content).Result;

                        if (response1.IsSuccessStatusCode)
                        {

                            if (SelectedMissionTypeName == "Detect and Count")
                            {
                                DataAccessLayer.DAL_SaveAICattleCount UpdatedImages = new DataAccessLayer.DAL_SaveAICattleCount();
                                int StatusVal = UpdatedImages.Update_iLAMSImageStatus(ImageFileName, _UserId);
                                res_message = 1;
                            }
                            else if (SelectedMissionTypeName == "Fence Monitoring")
                            {
                                DataAccessLayer.DAL_SaveAICattleCount UpdatedImages = new DataAccessLayer.DAL_SaveAICattleCount();
                                int StatusVal = UpdatedImages.Update_iLAMSImageStatusForFence(ImageFileName, _UserId);
                                res_message = 1;
                            }
                            else if (SelectedMissionTypeName == "Weed Monitoring")
                            {
                                DataAccessLayer.DAL_SaveAICattleCount UpdatedImages = new DataAccessLayer.DAL_SaveAICattleCount();
                                int StatusVal = UpdatedImages.Update_iLAMSImageStatusForWeed(ImageFileName, _UserId);
                                res_message = 1;
                            }


                        }

                       

                    }
                    if (res_message==1)
                    {
                        MessageBox message = new MessageBox("Sync AI to iLAMS successfull.");
                        message.Show();
                        ((MapLoad)this.Owner).closebutton();
                    }
                        if (_progressBar == null)
                            _progressBar = new ProgressBar("");
                        _progressBar.Hide();
                    }

                    else
                    {
                        var content = new MultipartFormDataContent();

                        DataAccessLayer.DAL_SaveAICattleCount SaveCat = new DataAccessLayer.DAL_SaveAICattleCount();
                        DataSet AIImages = SaveCat.Select_AIProcessedCattleCount(_UserId, iLAMSList.Mission_ID, iLAMSList.Paddock_Id, iLAMSList.imageCapturedDate, iLAMSList.File_Format);

                        for (int i = 0; i < AIImages.Tables[0].Rows.Count; i++)
                        {
                            string MissionID = AIImages.Tables[0].Rows[i]["Mission_ID"].ToString();
                            string FileCapturedDate = AIImages.Tables[0].Rows[i]["File_Captured_Date"].ToString();
                            string ImageFileName = AIImages.Tables[0].Rows[i]["File_Name"].ToString();
                            string SelectedMissionName = AIImages.Tables[0].Rows[0]["Mission_Name"].ToString();
                            string SelectedPaddockName = AIImages.Tables[0].Rows[0]["Paddock_Name"].ToString();
                            string SelectedMissionTypeName = AIImages.Tables[0].Rows[0]["MissionType"].ToString();

                            string Folderpath = Convert.ToString(ConfigurationManager.AppSettings["DestinationFilePath"].ToString()) + "\\" + SelectedMissionName.Replace(" ", "") + "\\" + SelectedPaddockName.Replace(" ", "") + "\\" + FileCapturedDate.Replace("/", ".") + "\\Video";

                            // Add first file content 
                            var fileContent1 = new ByteArrayContent(File.ReadAllBytes(Folderpath + "\\" + ImageFileName));
                            fileContent1.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                            {
                                FileName = ImageFileName
                            };

                            content.Add(fileContent1);

                            //AITransferData AddAIValues = new AITransferData();

                            AITransferData AddAIValues = new AITransferData
                            {
                                _AttachmentName = ImageFileName,
                                _Type = AIImages.Tables[0].Rows[i]["File_Type"].ToString(),
                                _Size = AIImages.Tables[0].Rows[i]["File_Size"].ToString(),
                                _MissionId = MissionID,
                                _Mission_Name = SelectedMissionName,
                                _Mission_Type = SelectedMissionTypeName,
                                _PaddockId = AIImages.Tables[0].Rows[0]["Paddock_ID"].ToString(),
                                _PaddockName = SelectedPaddockName,
                                _UserId = _UserId,
                                _imageCapturedDate = FileCapturedDate,
                                _imagefile = content
                            };

                            var myContent = JsonConvert.SerializeObject(AddAIValues);
                            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                            var byteContent = new ByteArrayContent(buffer);
                            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                            string InsertapiUrl1 = ConfigurationManager.AppSettings["MLA_WebAPI"].ToString() + "/ImageUpload/MediaUpload";

                            var client_1 = new HttpClient();

                            client_1.BaseAddress = new Uri(InsertapiUrl1);

                            //client.BaseAddress = new Uri("http://172.20.8.51/MLAWEBAPI/api/LiveStock/Inserttblailivestockinformation");

                            // Read file data
                            FileStream fs = new FileStream(Folderpath + "\\" + ImageFileName, FileMode.Open, FileAccess.Read);
                            byte[] data = new byte[fs.Length];
                            fs.Read(data, 0, data.Length);
                            fs.Close();

                            // Generate post objects
                            Dictionary<string, object> postParameters = new Dictionary<string, object>();
                            postParameters.Add("_AttachmentName", ImageFileName);
                            postParameters.Add("_Type", AIImages.Tables[0].Rows[i]["File_Type"].ToString());
                            postParameters.Add("_Size", AIImages.Tables[0].Rows[i]["File_Size"].ToString());
                            postParameters.Add("_MissionId", MissionID);
                            postParameters.Add("_Mission_Type", SelectedMissionTypeName);
                            postParameters.Add("_PaddockId", AIImages.Tables[0].Rows[0]["Paddock_ID"].ToString());
                            postParameters.Add("_PaddockName", SelectedPaddockName);
                            postParameters.Add("_UserId", _UserId);
                            postParameters.Add("_imageCapturedDate", FileCapturedDate);
                            postParameters.Add("_imagefile", new FormUpload.FileParameter(data, ImageFileName, AIImages.Tables[0].Rows[i]["File_Type"].ToString(), AIImages.Tables[0].Rows[i]["File_Size"].ToString(), MissionID, SelectedMissionName, AIImages.Tables[0].Rows[0]["Paddock_ID"].ToString(), SelectedPaddockName, _UserId, FileCapturedDate));

                            // Create request and receive response
                            string postURL = InsertapiUrl1;
                            string userAgent = "Mission Hub";
                            HttpWebResponse webResponse = FormUpload.MultipartFormDataPost(postURL, userAgent, postParameters);

                            // Process response
                            StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                            string fullResponse = responseReader.ReadToEnd();
                            webResponse.Close();

                            var response1 = client_1.PostAsJsonAsync(ConfigurationManager.AppSettings["MLA_WebAPI"].ToString() + "/ImageUpload/MediaUpload", fullResponse).Result;

                            //var response = client_1.PostAsync("Inserttblailivestockinformation", content).Result;

                            if (response1.IsSuccessStatusCode)
                            {

                                if (SelectedMissionTypeName == "Detect and Count")
                                {
                                    DataAccessLayer.DAL_SaveAICattleCount UpdatedImages = new DataAccessLayer.DAL_SaveAICattleCount();
                                    int StatusVal = UpdatedImages.Update_iLAMSImageStatus(ImageFileName, _UserId);
                                    res_message = 1;
                                }
                                else if (SelectedMissionTypeName == "Fence Monitoring")
                                {
                                    DataAccessLayer.DAL_SaveAICattleCount UpdatedImages = new DataAccessLayer.DAL_SaveAICattleCount();
                                    int StatusVal = UpdatedImages.Update_iLAMSImageStatusForFence(ImageFileName, _UserId);
                                    res_message = 1;
                                }
                                else if (SelectedMissionTypeName == "Weed Monitoring")
                                {
                                    DataAccessLayer.DAL_SaveAICattleCount UpdatedImages = new DataAccessLayer.DAL_SaveAICattleCount();
                                    int StatusVal = UpdatedImages.Update_iLAMSImageStatusForWeed(ImageFileName, _UserId);
                                    res_message = 1;
                                }


                            }

                        }
                        if (res_message == 1)
                        {
                            MessageBox message = new MessageBox("Sync AI to iLAMS successfull.");
                            message.Show();
                            ((MapLoad)this.Owner).closebutton();
                        }

                        if (_progressBar == null)
                            _progressBar = new ProgressBar("");
                        _progressBar.Hide();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox message = new MessageBox("Error: " + ex.Message);
                    message.Show();
                }
            }

            if (_progressBar == null)
                _progressBar = new ProgressBar("");
            _progressBar.Hide();

        }
        private void btniLAMS_Click(object sender, RoutedEventArgs e)
        {
           // CloseAllWindows();
            MLA_Aerodyne.BusinessAccessLayer.AIListData aiList = ((Button)sender).DataContext as MLA_Aerodyne.BusinessAccessLayer.AIListData;
            iLAMSList.ID= aiList.ID.ToString();
            iLAMSList.Mission_Name = aiList.Mission_Name;
            iLAMSList.Mission_ID = aiList.Mission_ID;
            iLAMSList.Paddock_Id = aiList.Paddock_Id;
            iLAMSList.Paddock_Name = aiList.Paddock_Name;
            iLAMSList.imageCapturedDate = aiList.imageCapturedDate.ToString();
            iLAMSList.Mission_Type = aiList.Mission_Type;
            iLAMSList.File_Type = aiList.File_Type;
            iLAMSList.File_Format = aiList.File_Format;

            if (aiList.File_Type == "Image")
            {
                File_Type = "Image";
                _folderPath = _mission_Destination_folderName + "\\" + iLAMSList.Mission_Name.Replace(" ", "") + "\\" + iLAMSList.Paddock_Name.Replace(" ", "") + "\\" + iLAMSList.imageCapturedDate.Replace(" ", "") + "\\" + "Image";
            }
            else
            {
                File_Type = "Video";
                _folderPath = _mission_Destination_folderName + "\\" + iLAMSList.Mission_Name.Replace(" ", "") + "\\" + iLAMSList.Paddock_Name.Replace(" ", "") + "\\" + iLAMSList.imageCapturedDate.Replace(" ", "") + "\\" + "Video";
            }
            
            this.Visibility = Visibility.Hidden;
            NotificationWindow note = new NotificationWindow(_UserId,_folderPath);
            note.Owner = this;
            note.ShowDialog();
          
        }
        public void ConnectionStatus(bool NetConStatus)
        {
            if (NetConStatus == true)
            {
                Connection_Status = "true";
                //MessageBox.Show("This computer is connected to the internet");
            }
            else
            {
                Connection_Status = "false";
                //MessageBox.Show("This computer is not connected to the internet. Please Connect Internet!!");
            }
        }

        private void windowBorder_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
