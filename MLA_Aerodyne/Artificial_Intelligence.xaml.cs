﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Diagnostics;
using CsvHelper;
using System.Data;
using DataTable = System.Data.DataTable;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Data.SQLite;
using MLA_Aerodyne.BusinessAccessLayer;
using static MLA_Aerodyne.MissionHub;
using Newtonsoft.Json;
using System.Threading;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for Artificial_Intelligence.xaml
    /// </summary>
    public partial class Artificial_Intelligence : Window
    {
        DataSet VdsPaddock = new DataSet();
        string directory;
        //string destination_Path = @"C:\Users\lal\FRCNN\data\demo";

        string destination_Path = @"C:\Users\NEW USER\Desktop\FRCNN\data\demo";

        IEnumerable<String> selectedData;
        ObservableCollection<ImageGridData> Imageitems = new ObservableCollection<ImageGridData>();

        public string UserId;
        public Artificial_Intelligence(string User_Id)
        {
            InitializeComponent();

            UserId = User_Id;
            BindComboBox_Import_Paddock(User_Id);
        }
        public void BindComboBox_Import_Paddock(string vbuserID)
        {
            DataAccessLayer.ViewPaddock vpaddock = new DataAccessLayer.ViewPaddock();
            VdsPaddock = vpaddock.BindCombobox_Import_Paddock(vbuserID);
            importPaddockCombo.DataContext = VdsPaddock.Tables[0].DefaultView;
            importPaddockCombo.DisplayMemberPath = "Paddock_Name";
            importPaddockCombo.SelectedValuePath = "Paddock_Id";
        }
        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }
        private void BtnSelectall_Click(object sender, RoutedEventArgs e)
        {
            foreach (ImageGridData imgChk in ImgBox.ItemsSource)
            {
                imgChk.IsChecked = true;

            }
            selectedData = Imageitems.Where(d => d.IsChecked).Select(d => d.File_path);
        }

        public class ImageGridData : INotifyPropertyChanged
        {
            private string _Title;
            public string _File_path;

            public string Title
            {
                get { return this._Title; }
                set { this._Title = value; }
            }
            public string File_path
            {
                get { return this._File_path; }
                set { this._File_path = value; }
            }

            private BitmapImage _ImageData;
            public BitmapImage ImageData
            {
                get { return this._ImageData; }
                set { this._ImageData = value; }
            }
            private bool isChecked;
            public bool IsChecked
            {
                get { return isChecked; }
                set
                {
                    if (isChecked == value) return;
                    isChecked = value;
                    RaisePropertyChanged("IsChecked");
                }
            }
            public event PropertyChangedEventHandler PropertyChanged;
            private void RaisePropertyChanged(string propName)
            {
                PropertyChangedEventHandler eh = PropertyChanged; if (eh != null)
                { eh(this, new PropertyChangedEventArgs(propName)); }
            }
            public ImageGridData(string t, BitmapImage i, bool c, string f)
            {
                this.Title = t;
                this.ImageData = i;
                this.IsChecked = c;
                this.File_path = f;
            }
        }

        private void browsedrive_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            IEnumerable<String> selectedfilelist;
            //FileInfo[] info = dirInfo.GetFiles("*.*", SearchOption.AllDirectories);
            Microsoft.Win32.OpenFileDialog op = new Microsoft.Win32.OpenFileDialog();
            op.Title = "My Image Browser";
            op.Filter = "Images (*.JPG;*.JPEG)|*.JPG;*.JPEG|" +
            "All files (*.*)|*.*";
            op.Multiselect = true;

            if (op.ShowDialog() == true)
            {
                foreach (String file in op.FileNames)
                {
                    directory = Path.GetDirectoryName(file);

                    string file_name = Path.GetFileName(file);
                    Imageitems.Add(new ImageGridData(file_name, LoadImage(file), false, file.ToString()));
                }
                selectedfilelist = op.FileNames;
                directorytext.Text = directory;
                ImgBox.ItemsSource = Imageitems;
            }
        }
        private void CopyFiles(string sourcePath, string destinationPath)
        {
            string file_name = Path.GetFileName(sourcePath);
            string destfullpath = destinationPath + "\\" + file_name;
            if (!File.Exists(destfullpath))
            {
                System.IO.File.Copy(sourcePath, destfullpath);
            }
        }
        private BitmapImage LoadImage(string filename)
        {
            return new BitmapImage(new Uri(filename));
        }

        private void runAIBtn_Click(object sender, RoutedEventArgs e)
        {
            foreach (var SourcePath in selectedData)
            {
                CopyFiles(SourcePath, destination_Path);
            }

           // Process.Start(@"C:\Program Files (x86)\MLA_Aerodyne\MLA_Aerodyne_Setup\test.bat");

            Thread.Sleep(1000 * 60 * 5);

            int import = 0;

            string Paddock_Id = importPaddockCombo.SelectedValue.ToString();
            string Paddock_Name = importPaddockCombo.Text;

            DataAccessLayer.DAL_SaveAICattleCount SaveCat = new DataAccessLayer.DAL_SaveAICattleCount();
            //import = SaveCat.Select_CattleCount(UserId, Paddock_Id, Paddock_Name);

            string URL = "http://www.google.com/";
            bool NetConStatus = IsInternetConnection(URL);
            if (NetConStatus == true)
                ConnectionStatus(NetConStatus);
            else
                ConnectionStatus(NetConStatus);

            if (Connection_Status == "true")
            {
                string apiUrl = ConfigurationManager.AppSettings["MLA_WebAPI"].ToString() + "/CompareTables/GetTableValues";
                WebClient webClient = new WebClient();
                Stream stream = webClient.OpenRead(apiUrl);
                StreamReader sr = new StreamReader(stream);
                string result = string.Empty;

                string json1 = sr.ReadToEnd().Trim();

                //var SetValues = JArray.Parse(json1);

                DataTable dt1 = JsonConvert.DeserializeObject<DataTable>(json1);

                DataAccessLayer.DAL_ServerTableStatusCheck ClearAndInsert = new DataAccessLayer.DAL_ServerTableStatusCheck();
                string StatusVal = ClearAndInsert.ClearServerStatusTable();

                DataAccessLayer.DAL_ServerTableStatusCheck TableStatus = new DataAccessLayer.DAL_ServerTableStatusCheck();
                DataTable DS_Values = TableStatus.CheckServerStatus();

                List<BAl_TableRecords> tableRecords1 = new List<BAl_TableRecords>();

                List<BAl_TableRecords> tableRecords2 = new List<BAl_TableRecords>();

                List<BAl_TableRecords> tableRecords_Final = new List<BAl_TableRecords>();

                if (DS_Values.Rows.Count > 0)
                {
                    tableRecords1 = (from DataRow row in DS_Values.Rows
                                     select new BAl_TableRecords
                                     {
                                         //Intake
                                         TABLE_NAME = row["Table_Name"].ToString(),
                                         TABLE_ROWS = row["Table_LastId"].ToString(),
                                     }).ToList();

                    tableRecords2 = (from DataRow row in dt1.Rows
                                     select new BAl_TableRecords
                                     {
                                         //Intake
                                         TABLE_NAME = row["TABLE_NAME"].ToString(),
                                         TABLE_ROWS = row["TABLE_ROWS"].ToString(),
                                     }).ToList();

                    foreach (var bItem in tableRecords2)
                    {
                        if (tableRecords1.Any(aItem => bItem.TABLE_ROWS != aItem.TABLE_ROWS && bItem.TABLE_NAME.ToLower() == aItem.TABLE_NAME.ToLower()))
                        {
                            tableRecords_Final.Add(bItem);
                        }
                    }
                }


                if (tableRecords_Final.Any(aTable => aTable.TABLE_NAME.ToLower() == "tbl_ai_livestockinformation"))
                {
                    var client = new HttpClient();

                    List<LiveStockInfo> EI = new List<LiveStockInfo>();

                    DataSet dsWaterInfo = new DataSet();
                    var result1 = tableRecords_Final.Where(a => a.TABLE_NAME == "tbl_ai_livestockinformation").Select(b => b.TABLE_ROWS);

                    foreach (var rowcount in result1)
                    {
                        SQLiteConnection con = new SQLiteConnection("Data Source = " + System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                        string strQuery = "select distinct * from tbl_ai_livestockinformation where Id > " + rowcount + " and Id != " + rowcount + "";
                        SQLiteCommand cmd = new SQLiteCommand(strQuery, con);
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

                        da.Fill(dsWaterInfo);
                        con.Close();

                        EI = (from DataRow row in dsWaterInfo.Tables[0].Rows
                              select new LiveStockInfo
                              {
                                  livestockid = Convert.ToInt32(row["LiveStockId"]),
                                  livestockcount = Convert.ToInt32(row["LiveStockCount"]),
                                  PaddockId = Convert.ToInt32(row["PaddockId"]),
                                  PaddockName = row["PaddockName"].ToString(),
                                  userid = Convert.ToInt32(row["UserId"])
                              }).ToList();
                        try
                        {
                            //client.BaseAddress = new Uri(ConfigurationManager.AppSettings["MLA_WebAPI"].ToString() + "/Inserttbllgwaterdetails");

                            if (EI.Count != 0)
                            {
                                string InsertapiUrl = ConfigurationManager.AppSettings["MLA_WebAPI"].ToString() + "/LiveStock/Inserttblailivestockinformation";

                                client.BaseAddress = new Uri(InsertapiUrl);

                                //client.BaseAddress = new Uri("http://172.20.8.51/MLAWEBAPI/api/LiveStock/Inserttblailivestockinformation");

                                var response = client.PostAsJsonAsync("Inserttblailivestockinformation", EI).Result;

                                if (response.IsSuccessStatusCode)
                                {

                                }
                            }
                        }
                        catch (Exception ex) { }
                    }
                }

                if (tableRecords_Final.Any(aTable => aTable.TABLE_NAME.ToLower() == "tbl_ai_livestock_attachment"))
                {
                    var client = new HttpClient();

                    List<LiveStockAttch> EI = new List<LiveStockAttch>();

                    DataSet dsWaterInfo = new DataSet();
                    var result1 = tableRecords_Final.Where(a => a.TABLE_NAME == "tbl_ai_livestock_attachment").Select(b => b.TABLE_ROWS);

                    foreach (var rowcount in result1)
                    {
                        SQLiteConnection con = new SQLiteConnection("Data Source = " + System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                        string strQuery = "select distinct * from tbl_ai_livestock_attachment where Id> " + rowcount + " and Id != " + rowcount + "";
                        SQLiteCommand cmd = new SQLiteCommand(strQuery, con);
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

                        da.Fill(dsWaterInfo);
                        con.Close();

                        EI = (from DataRow row in dsWaterInfo.Tables[0].Rows
                              select new LiveStockAttch
                              {
                                  attachmentname = row["AttachmentName"].ToString(),
                                  path = row["Path"].ToString(),
                                  imagetype = row["Type"].ToString(),
                                  size = row["Size"].ToString(),
                                  livestockid = Convert.ToInt32(row["LiveStockId"]),
                                  PaddockId = Convert.ToInt32(row["PaddockId"]),
                                  PaddockName = row["PaddockName"].ToString(),
                                  userid = Convert.ToInt32(row["UserId"]),
                              }).ToList();
                        try
                        {
                            //client.BaseAddress = new Uri(ConfigurationManager.AppSettings["MLA_WebAPI"].ToString() + "/Inserttbllgwaterdetails");

                            if (EI.Count != 0)
                            {
                                string InsertapiUrl = ConfigurationManager.AppSettings["MLA_WebAPI"].ToString() + "/LiveStockAttachment/Inserttblailivestockattachment";

                                client.BaseAddress = new Uri(InsertapiUrl);

                                //client.BaseAddress = new Uri("http://172.20.8.51/MLAWEBAPI/api/LiveStockAttachment/Inserttblailivestockattachment");

                                var response = client.PostAsJsonAsync("Inserttblailivestockattachment", EI).Result;

                                if (response.IsSuccessStatusCode)
                                {

                                }
                            }
                        }
                        catch (Exception ex) { }
                    }
                }

                if (import != 0)
                {
                    MessageBox message = new MessageBox("Cattle count save successfully.");
                    message.Show();
                    this.Visibility = Visibility.Hidden;
                    ((MapLoad)this.Owner).closebutton();
                }
                //else
                //{
                //    MessageBox message = new MessageBox("Cattle count not saved.");
                //    message.Show();
                //    this.Visibility = Visibility.Hidden;
                //}
            }
        }

        public static bool IsInternetConnection(String URL)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Timeout = 5000;
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public string Connection_Status = "false";
        public void ConnectionStatus(bool NetConStatus)
        {
            if (NetConStatus == true)
            {
                Connection_Status = "true";
                //MessageBox.Show("This computer is connected to the internet");
            }
            else
            {
                Connection_Status = "false";
                //MessageBox.Show("This computer is not connected to the internet. Please Connect Internet!!");
            }
        }

        private void importPaddockCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
