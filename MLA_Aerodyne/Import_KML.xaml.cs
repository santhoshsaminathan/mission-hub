﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Win32;
using Convert = System.Convert;
using System.Xml.Schema;
using System.Data;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for Import_KML.xaml
    /// </summary>
    public partial class Import_KML : Window
    {

        public string User_Id;
        public string SelectedPath;
        public int Filetype;
        public int _error = 0;
        public static int _kmlType = 0;
        public string Plot_ID;

        DataSet VdsMaparea = new DataSet();
        DataSet Vdslandmark = new DataSet();
        DataAccessLayer.Login_DAL _errorDAL = new DataAccessLayer.Login_DAL();

        string _KML_folderPath = Convert.ToString(ConfigurationManager.AppSettings["KML_Path"].ToString());
        string _KMLschema_GoogleEarth = Convert.ToString(ConfigurationManager.AppSettings["KML_Schema_GoogleEarth"].ToString());
        string _KMLschema_GlobalMapper = Convert.ToString(ConfigurationManager.AppSettings["KML_Schema_GlobalMapper"].ToString());
        string _KMLschema_GeoJson = Convert.ToString(ConfigurationManager.AppSettings["KML_Schema_GeoJson"].ToString());

        ProgressBar _progressBar;

        public Import_KML(string userID)
        {
            InitializeComponent();
            User_Id = userID;
            BindComboBox_Maparea(User_Id);
        }

        public void BindComboBox_Maparea(string vbuserID)
        {
            try
            {
                DataAccessLayer.ViewMaparea vmap = new DataAccessLayer.ViewMaparea();
                VdsMaparea = vmap.BindCombobox_ViewMaparea(vbuserID);
                MmapareaCombo.DataContext = VdsMaparea.Tables[0].DefaultView;
                MmapareaCombo.DisplayMemberPath = "Plot_Name";
                MmapareaCombo.SelectedValuePath = "Plot_ID";
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _errorDAL.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Import_KML", User_Id);
            }
        }

        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            //this.Close();
            this.Visibility = Visibility.Hidden;
            ((MapLoad)this.Owner).closebutton();
        }
        private void RadioButtonChecked(object sender, RoutedEventArgs e)
        {
            var button = sender as RadioButton;
            string caseSwitch = button.Content.ToString();
            switch (caseSwitch)
            {
                case "Paddock":
                    Filetype = 1;
                    break;
                case "Landmark":
                    rbtnPolygon.Visibility = Visibility.Hidden;
                    lblPoint.Visibility = Visibility.Visible;
                    MmapareaCombo.Visibility = Visibility.Visible;
                    Filetype = 2;
                    break;
            }
        }

        private void CloseAllWindows()
        {
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > 0; intCounter--)
                App.Current.Windows[intCounter].Close();
        }
        
        public static byte[] StreamToByteArray(Stream input)
        {
            if (input == null)
                return null;
            byte[] buffer = new byte[16 * 1024];
            input.Position = 0;
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                byte[] temp = ms.ToArray();

                return temp;
            }
        }


        [Obsolete]
        private void importBtn_Click(object sender, RoutedEventArgs e)
        {
          try
            { 
            string MenuFlag = "ImportKML";

            this.Visibility = Visibility.Hidden;

            if (_progressBar == null)
                _progressBar = new ProgressBar("Import KML in progress...");
            _progressBar.Show();



                if (Filetype == 1)
                {

                    _KML_folderPath = _KML_folderPath + "\\" + User_Id + "\\" + "Paddock" + "\\" + DateTime.Now.ToString("dd.MM.yyyy");

                    if (!Directory.Exists(_KML_folderPath))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(_KML_folderPath);
                    }

                    string kml_fielname = Path.GetFileName(SelectedPath);
                    byte[] KML_bytes = System.IO.File.ReadAllBytes(SelectedPath);
                    _KML_folderPath = _KML_folderPath + "\\" + kml_fielname;
                    File.WriteAllBytes(_KML_folderPath, KML_bytes);

                    int pairValueCount = 0;
                    string readText = File.ReadAllText(SelectedPath);

                    XDocument z = XDocument.Parse(readText);
                    var result = z.Root.Attributes().
                            Where(a => a.IsNamespaceDeclaration).
                            GroupBy(a => a.Name.Namespace == XNamespace.None ? String.Empty : a.Name.LocalName,
                                    a => XNamespace.Get(a.Value)).
                            ToDictionary(g => g.Key,
                                         g => g.First());

                    foreach (var pair in result)
                    {
                        pairValueCount = result.Count;
                        string keyValue = pair.Key;
                        string pairValue = pair.Value.ToString();
                        if (keyValue == "kml")
                        {
                            _kmlType = 1;
                        }
                        if (keyValue == "globalmapper")
                        {
                            _kmlType = 2;
                        }

                    }

                    if (_kmlType == 2)
                    {
                        //string _ge_Schema1 = _KMLschema_GlobalMapper + "\\" + "GlobalMapperKML_Schema.xsd";

                        //XElement xsdMarkup = XElement.Load(_ge_Schema1);

                        //XmlSchemaSet schemas = new XmlSchemaSet();

                        //schemas.Add("http://www.opengis.net/kml/2.2", xsdMarkup.CreateReader());
                        //schemas.Compile();

                        //XmlReader rd = XmlReader.Create(SelectedPath);
                        //XDocument doc = XDocument.Load(rd);
                        //doc.Validate(schemas, ValidationEventHandler);

                        if (_error == 0)
                        {

                            MLA_Aerodyne.DataAccessLayer.DAL_ImportKML dAL_Import = new MLA_Aerodyne.DataAccessLayer.DAL_ImportKML();
                            string StyleVal = "";


                            XmlDataDocument xmldoc = new XmlDataDocument();
                            FileStream fs1 = new FileStream(SelectedPath, FileMode.Open, FileAccess.Read);
                            xmldoc.Load(fs1);


                            XmlNodeList xmlnodeDocument = xmldoc.SelectNodes("/Document");
                            XmlNodeList xmlnodeline = xmldoc.SelectNodes("/Style");
                            XmlNodeList xmlnodepoly = xmldoc.SelectNodes("/Style");
                            XmlNodeList xmlnode = xmldoc.SelectNodes("/Placemark/description");

                            xmlnodeDocument = xmldoc.GetElementsByTagName("Folder");
                            xmlnodeline = xmldoc.GetElementsByTagName("LineStyle");
                            xmlnodepoly = xmldoc.GetElementsByTagName("PolyStyle");
                            xmlnode = xmldoc.GetElementsByTagName("Placemark");

                            string docName = xmlnodeDocument[0].ChildNodes.Item(0).InnerText.Trim();
                            //string docDescription = xmlnodeDocument[0].ChildNodes.Item(1).InnerText.Trim();
                            string docDescription = "";
                            int KML_ID = dAL_Import.Insert_KMLMaster(docName, docDescription, User_Id);
                            if (KML_ID != 0)
                            {
                                int i = 0;
                                //    foreach (XmlNode node in xmldoc.GetElementsByTagName("Style"))
                                //    {
                                //        StyleVal = node.Attributes["id"].Value;
                                //    }
                                //    int KML_Style_ID = dAL_Import.Insert_KMLStyleMaster(KML_ID, StyleVal);

                                //if (KML_Style_ID != 0)
                                //{
                                //    int j = 0;
                                //    int k = 0;
                                //   
                                //    int m = 0;
                                //    for (j = 0; j <= xmlnodeline.Count - 1; j++)
                                //    {
                                //        string color = xmlnodeline[j].ChildNodes.Item(0).InnerText.Trim();
                                //        string width = xmlnodeline[j].ChildNodes.Item(1).InnerText.Trim();
                                //        int linestatus = dAL_Import.Insert_KML_lineStyle(KML_Style_ID, color, width);
                                //    }
                                //    for (k = 0; k <= xmlnodepoly.Count - 1; k++)
                                //    {
                                //        string color = xmlnodepoly[k].ChildNodes.Item(0).InnerText.Trim();
                                //        string fill = xmlnodepoly[k].ChildNodes.Item(1).InnerText.Trim();
                                //        string outline = xmlnodepoly[k].ChildNodes.Item(2).InnerText.Trim();
                                //        int polystatus = dAL_Import.Insert_KML_polyStyle(KML_Style_ID, color, fill, outline);
                                //    }
                                //}
                                XmlNodeList xmlnodeLatLngList = xmldoc.SelectNodes("/outerBoundaryIs/LinearRing");
                                xmlnodeLatLngList = xmldoc.GetElementsByTagName("outerBoundaryIs");
                                XmlNodeList xmlnodeLatLngListInner = xmldoc.SelectNodes("/innerBoundaryIs/LinearRing");
                                xmlnodeLatLngListInner = xmldoc.GetElementsByTagName("innerBoundaryIs");

                                for (i = 0; i <= xmlnode.Count - 1; i++)
                                {
                                    string description = "";
                                    // string description = xmlnode[i].ChildNodes.Item(0).InnerText.Trim();
                                    string name = xmlnode[i].ChildNodes.Item(1).InnerText.Trim();

                                    string styleUrl = xmlnode[i].ChildNodes.Item(2).InnerText.Trim();
                                    int KML_pID = dAL_Import.Insert_KML_Placemark(KML_ID, description, name, styleUrl);

                                    string latlng_Outer = xmlnodeLatLngList[i].ChildNodes.Item(0).InnerText.Trim();
                                    latlng_Outer = latlng_Outer.Replace(" ", "%");
                                    latlng_Outer = latlng_Outer.Replace("%%%%%%%%%%%%%%", "%");
                                    String[][] MultiArray = latlng_Outer.Split('%').Select(t => t.Split(',')).ToArray();

                                    dynamic Way_Points = MultiArray;
                                    IList<PointLatLng> coordinates = new List<PointLatLng>();
                                    foreach (var val in Way_Points)
                                    {
                                        PointLatLng pointLatLng = new PointLatLng();
                                        pointLatLng.Latitude = Convert.ToDouble(val[0]);
                                        pointLatLng.Longitude = Convert.ToDouble(val[1]);
                                        coordinates.Add(pointLatLng);

                                    }
                                    double area = MLA_Aerodyne.DataAccessLayer.CalculateAreaOfPolygon.ComputeSignedArea(coordinates);
                                    area = Math.Round(area, 2);
                                    area = Math.Abs(area);
                                    area = area / (double)10000;
                                    NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
                                    nfi.NumberDecimalSeparator = " ";
                                    string _area = area.ToString("N", nfi);

                                    foreach (string[] latlngVal in MultiArray)
                                    {
                                        if (latlngVal[1].Length < 2 || latlngVal[0].Length < 2)
                                        {
                                            this.Visibility = Visibility.Hidden;
                                            MapLoad MapLoad1 = new MapLoad(User_Id, "", MenuFlag);
                                            MapLoad1.Show();
                                            MessageBox message1 = new MessageBox("Invalid Latitude or Longitude.");
                                            message1.Show();
                                            _kmlType = 0;
                                        }
                                        else
                                        {
                                            int latlngOuterStatus = dAL_Import.Insert_KML_OuterPolygon(KML_pID, latlngVal[1], latlngVal[0], latlngVal[2], _area.Replace(" ", ".") + " " + "hectares");
                                        }

                                    }

                                    string innerBoundaryIs = "";
                                    int Inner_ListID = 1;

                                    foreach (XmlElement xn in xmlnode[i])
                                    {
                                        XmlNodeList xmlnodeinnerBoundary = xn.GetElementsByTagName("innerBoundaryIs");

                                        foreach (XmlElement xnl in xmlnodeinnerBoundary)
                                        {
                                            innerBoundaryIs = (xnl == null) ? "" : xnl.InnerText.Trim();
                                            innerBoundaryIs = innerBoundaryIs.Replace(" ", "%");
                                            innerBoundaryIs = innerBoundaryIs.Replace("%%%%%%%%%%%%%%", "%");

                                            String[][] InnerMultiArray = innerBoundaryIs.Split('%').Select(t => t.Split(',')).ToArray();

                                            foreach (string[] latlngVal in InnerMultiArray)
                                            {
                                                if (latlngVal[0].Length < 2 || latlngVal[1].Length < 2)
                                                {
                                                    this.Visibility = Visibility.Hidden;
                                                    MapLoad MapLoad1 = new MapLoad(User_Id, "", MenuFlag);
                                                    MapLoad1.Show();
                                                    MessageBox message1 = new MessageBox("Invalid Latitude or Longitude.");
                                                    message1.Show();
                                                    _kmlType = 0;
                                                }
                                                else
                                                {
                                                    int latlngInnerStatus = dAL_Import.Insert_KML_InnerPolygon(KML_pID, Inner_ListID, latlngVal[1], latlngVal[0], latlngVal[2]);
                                                }

                                            }
                                            Inner_ListID++;
                                        }
                                    }
                                }

                                this.Visibility = Visibility.Hidden;
                                CloseAllWindows();

                                MapLoad MapLoad = new MapLoad(User_Id, KML_ID.ToString(), MenuFlag);
                                MapLoad.Show();
                                MessageBox message = new MessageBox("Global mapper KML parsing completed. Now click Save KML button on left menu.");
                                message.Show();

                                _kmlType = 0;
                                _error = 0;
                            }
                            else
                            {
                                this.Visibility = Visibility.Hidden;
                                CloseAllWindows();
                                MapLoad MapLoad = new MapLoad(User_Id, "", MenuFlag);
                                MapLoad.Show();
                                MessageBox message = new MessageBox("Global mapper KML parsing corrupted.");
                                message.Show();
                                _kmlType = 0;
                                _error = 0;
                            }
                        }
                        else
                        {
                            this.Visibility = Visibility.Hidden;
                            CloseAllWindows();
                            MapLoad MapLoad = new MapLoad(User_Id, "", MenuFlag);
                            MapLoad.Show();
                            //MessageBox message = new MessageBox("Global mapper KML parsing aborted.");
                            //message.Show();
                            _kmlType = 0;
                            _error = 0;
                        }

                        if (_progressBar == null)
                            _progressBar = new ProgressBar("Process completed.");
                        _progressBar.Hide();
                    }
                    if (_kmlType == 1)
                    {
                        //string _ge_Schema1 = _KMLschema_GoogleEarth + "\\" + "GoogleEarthKML_Schema.xsd";
                        //string _ge_Schema2 = _KMLschema_GoogleEarth + "\\" + "GoogleEarthKML_Schema_2.xsd";
                        //XElement xsdMarkup = XElement.Load(_ge_Schema1);

                        //XElement xsdMarkup2 = XElement.Load(_ge_Schema2);

                        //XmlSchemaSet schemas = new XmlSchemaSet();

                        //schemas.Add("http://www.opengis.net/kml/2.2", xsdMarkup.CreateReader());
                        //schemas.Add("http://www.google.com/kml/ext/2.2", xsdMarkup2.CreateReader());

                        //schemas.Compile();

                        //XmlReader rd = XmlReader.Create(SelectedPath);
                        //XDocument doc = XDocument.Load(rd);
                        //doc.Validate(schemas, ValidationEventHandler);
                        if (_error == 0)
                        {
                            MLA_Aerodyne.DataAccessLayer.DAL_ImportKML dAL_Import = new MLA_Aerodyne.DataAccessLayer.DAL_ImportKML();
                            string StyleVal = "";
                            int j = 0;
                            int k = 0;
                            int i = 0;

                            XmlDataDocument xmldoc = new XmlDataDocument();
                            FileStream fs1 = new FileStream(SelectedPath, FileMode.Open, FileAccess.Read);
                            xmldoc.Load(fs1);

                            XmlNodeList xmlnodeDocument = xmldoc.SelectNodes("Document");
                            XmlNodeList xmlnode = xmldoc.SelectNodes("/Placemark/name");
                            XmlNodeList xmlnodedes = xmldoc.SelectNodes("/Placemark/name/description");

                            xmlnodeDocument = xmldoc.GetElementsByTagName("Document");
                            XmlNodeList xmlnodeline = xmldoc.GetElementsByTagName("LineStyle");
                            XmlNodeList xmlnodepoly = xmldoc.GetElementsByTagName("PolyStyle");
                            xmlnode = xmldoc.GetElementsByTagName("Placemark");
                            xmlnodedes = xmldoc.GetElementsByTagName("description");

                            string docName = xmlnodeDocument[0].ChildNodes.Item(0).InnerText.Trim();
                            // string docDescription = xmlnodeDocument[0].ChildNodes.Item(1).InnerText.Trim();
                            string docDescription = "";
                            int KML_ID = dAL_Import.Insert_KMLMaster(docName, docDescription, User_Id);
                            if (KML_ID != 0)
                            {
                                //foreach (XmlNode node in xmldoc.GetElementsByTagName("gx:CascadingStyle"))
                                //{
                                //    StyleVal = node.Attributes["kml:id"].Value;
                                //    int KML_Style_ID = dAL_Import.Insert_KMLStyleMaster(KML_ID, StyleVal);
                                //    if (KML_Style_ID != 0)
                                //    {

                                //        for (j = 0; j <= xmlnodeline.Count - 1; j++)
                                //        {
                                //            string color = xmlnodeline[j].ChildNodes.Item(0).InnerText.Trim();
                                //            string width = xmlnodeline[j].ChildNodes.Item(1).InnerText.Trim();
                                //            int linestatus = dAL_Import.Insert_KML_lineStyle(KML_Style_ID, color, width);
                                //        }
                                //        for (k = 0; k <= xmlnodepoly.Count - 1; k++)
                                //        {
                                //            string color = xmlnodepoly[k].ChildNodes.Item(0).InnerText.Trim();
                                //            string fill = "";
                                //            string outline = "";
                                //            int polystatus = dAL_Import.Insert_KML_polyStyle(KML_Style_ID, color, fill, outline);
                                //        }
                                //    }
                                //    else
                                //    {
                                //        this.Visibility = Visibility.Hidden;
                                //        CloseAllWindows();
                                //        MapLoad MapLoad1 = new MapLoad(User_Id, "", MenuFlag);
                                //        MapLoad1.Show();
                                //        MessageBox message1 = new MessageBox("Google Earth KML parsing aborted.");
                                //        message1.Show();
                                //        _kmlType = 0;
                                //        _error = 0;
                                //    }
                                //}

                                XmlNodeList xmlnodeLatLngList = xmldoc.SelectNodes("/outerBoundaryIs/LinearRing");
                                xmlnodeLatLngList = xmldoc.GetElementsByTagName("outerBoundaryIs");
                                XmlNodeList xmlnodeLatLngListInner = xmldoc.SelectNodes("/innerBoundaryIs/LinearRing");
                                xmlnodeLatLngListInner = xmldoc.GetElementsByTagName("innerBoundaryIs");
                                string description = "";

                                for (i = 0; i <= xmlnode.Count - 1; i++)
                                {

                                    string name = xmlnode[i].ChildNodes.Item(0).InnerText.Trim();
                                    string styleUrl = "";
                                    // string styleUrl = xmlnode[i].ChildNodes.Item(3).InnerText.Trim();
                                    int KML_pID = dAL_Import.Insert_KML_Placemark(KML_ID, description, name, styleUrl);

                                    string latlng_Outer = xmlnodeLatLngList[i].ChildNodes.Item(0).InnerText.Trim();
                                    latlng_Outer = latlng_Outer.Replace(" ", "%");
                                    latlng_Outer = latlng_Outer.Replace("%%%%%%%%%%%%%%", "%");
                                    String[][] MultiArray = latlng_Outer.Split('%').Select(t => t.Split(',')).ToArray();

                                    dynamic Way_Points = MultiArray;
                                    IList<PointLatLng> coordinates = new List<PointLatLng>();
                                    foreach (var val in Way_Points)
                                    {
                                        PointLatLng pointLatLng = new PointLatLng();
                                        pointLatLng.Latitude = Convert.ToDouble(val[0]);
                                        pointLatLng.Longitude = Convert.ToDouble(val[1]);
                                        coordinates.Add(pointLatLng);

                                    }
                                    double area = MLA_Aerodyne.DataAccessLayer.CalculateAreaOfPolygon.ComputeSignedArea(coordinates);
                                    area = Math.Round(area, 2);
                                    area = Math.Abs(area);
                                    area = area / (double)10000;
                                    NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
                                    nfi.NumberDecimalSeparator = " ";
                                    string _area = area.ToString("N", nfi);

                                    foreach (string[] latlngVal in MultiArray)
                                    {
                                        if (latlngVal[1].Length < 2 || latlngVal[0].Length < 2)
                                        {
                                            this.Visibility = Visibility.Hidden;
                                            MapLoad MapLoad1 = new MapLoad(User_Id, "", MenuFlag);
                                            MapLoad1.Show();
                                            MessageBox message1 = new MessageBox("Invalid Latitude or Longitude.");
                                            message1.Show();
                                            _kmlType = 0;
                                        }
                                        else
                                        {
                                            int latlngOuterStatus = dAL_Import.Insert_KML_OuterPolygon(KML_pID, latlngVal[1], latlngVal[0], latlngVal[2], _area.Replace(" ", ".") + " " + "hectares");
                                        }
                                    }

                                    string innerBoundaryIs = "";
                                    int Inner_ListID = 1;

                                    foreach (XmlElement xn in xmlnode[i])
                                    {
                                        XmlNodeList xmlnodeinnerBoundary = xn.GetElementsByTagName("innerBoundaryIs");

                                        foreach (XmlElement xnl in xmlnodeinnerBoundary)
                                        {
                                            innerBoundaryIs = (xnl == null) ? "" : xnl.InnerText.Trim();
                                            innerBoundaryIs = innerBoundaryIs.Replace(" ", "%");
                                            innerBoundaryIs = innerBoundaryIs.Replace("%%%%%%%%%%%%%%", "%");

                                            String[][] InnerMultiArray = innerBoundaryIs.Split('%').Select(t => t.Split(',')).ToArray();

                                            foreach (string[] latlngVal in InnerMultiArray)
                                            {
                                                if (latlngVal[0].Length < 2 || latlngVal[1].Length < 2)
                                                {
                                                    this.Visibility = Visibility.Hidden;
                                                    MapLoad MapLoad1 = new MapLoad(User_Id, "", MenuFlag);
                                                    MapLoad1.Show();
                                                    MessageBox message1 = new MessageBox("Invalid Latitude or Longitude.");
                                                    message1.Show();
                                                    _kmlType = 0;
                                                }
                                                else
                                                {
                                                    int latlngInnerStatus = dAL_Import.Insert_KML_InnerPolygon(KML_pID, Inner_ListID, latlngVal[1], latlngVal[0], latlngVal[2]);
                                                }
                                            }
                                            Inner_ListID++;
                                        }
                                    }
                                }

                                this.Visibility = Visibility.Hidden;
                                CloseAllWindows();
                                MapLoad MapLoad = new MapLoad(User_Id, KML_ID.ToString(), MenuFlag);
                                MapLoad.Show();
                                MessageBox message = new MessageBox("Google Earth KML parsing completed. Now click Save KML button on left menu.");
                                message.Show();
                            }
                            _kmlType = 0;
                            _error = 0;
                        }
                        else
                        {
                            this.Visibility = Visibility.Hidden;
                            CloseAllWindows();
                            MapLoad MapLoad = new MapLoad(User_Id, "", MenuFlag);
                            MapLoad.Show();
                            //MessageBox message = new MessageBox("Google Earth KML parsing corrupted.");
                            //message.Show();
                            _kmlType = 0;
                            _error = 0;
                        }


                        if (_progressBar == null)
                            _progressBar = new ProgressBar("Process completed.");
                        _progressBar.Hide();
                    }
                    if (pairValueCount == 1)
                    {
                        _kmlType = 3;
                        //string _ge_Schema1 = _KMLschema_GeoJson + "\\" + "GeoJsonKML_Schema.xsd";
                        ////string _ge_Schema2 = _KMLschema_GoogleEarth + "\\" + "GoogleEarthKML_Schema_2.xsd";
                        //XElement xsdMarkup = XElement.Load(_ge_Schema1);

                        //// XElement xsdMarkup2 = XElement.Load(_ge_Schema2);

                        //XmlSchemaSet schemas = new XmlSchemaSet();

                        //schemas.Add("http://www.opengis.net/kml/2.2", xsdMarkup.CreateReader());
                        //// schemas.Add("http://www.google.com/kml/ext/2.2", xsdMarkup2.CreateReader());

                        //schemas.Compile();

                        //XmlReader rd = XmlReader.Create(SelectedPath);
                        //XDocument doc = XDocument.Load(rd);
                        //doc.Validate(schemas, ValidationEventHandler);
                        if (_error == 0)
                        {

                            MLA_Aerodyne.DataAccessLayer.DAL_ImportKML dAL_Import = new MLA_Aerodyne.DataAccessLayer.DAL_ImportKML();
                            string StyleVal = "";

                            XmlDataDocument xmldoc = new XmlDataDocument();
                            FileStream fs1 = new FileStream(SelectedPath, FileMode.Open, FileAccess.Read);
                            xmldoc.Load(fs1);

                            XmlNodeList xmlnodeDocument = xmldoc.SelectNodes("/Document");
                            XmlNodeList xmlnodeline = xmldoc.SelectNodes("/Placemark");
                            XmlNodeList xmlnodepoly = xmldoc.SelectNodes("/Placemark");
                            XmlNodeList xmlnode = xmldoc.SelectNodes("/Placemark/ExtendedData");

                            XmlNodeList xmlnodeName = xmldoc.SelectNodes("/Placemark/name");
                            XmlNodeList xmlnodeDesc = xmldoc.SelectNodes("/Placemark/description");

                            xmlnodeDocument = xmldoc.GetElementsByTagName("Placemark");
                            xmlnodeline = xmldoc.GetElementsByTagName("ExtendedData");
                            xmlnodepoly = xmldoc.GetElementsByTagName("ExtendedData");
                            xmlnode = xmldoc.GetElementsByTagName("ExtendedData");

                            xmlnodeName = xmldoc.GetElementsByTagName("name");
                            xmlnodeDesc = xmldoc.GetElementsByTagName("description");

                            //string docName = xmlnodeDocument[0].ChildNodes.Item(0).InnerText.Trim();
                            //string docDescription = xmlnodeDocument[0].ChildNodes.Item(1).InnerText.Trim();

                            string docName = xmlnodeName[0].InnerText.Trim();
                            string docDescription = "";
                            //string docDescription = xmlnodeDesc[0].InnerText.Trim();

                            int KML_ID = dAL_Import.Insert_KMLMaster(docName, docDescription, User_Id);

                            if (KML_ID != 0)
                            {
                                int jj = 0;

                                for (jj = 0; jj < xmlnodeDocument.Count; jj++)
                                {
                                    //StyleVal = xmlnodeDocument[jj].ChildNodes.Item(0).InnerText.Trim();

                                    //StyleVal = xmlnodeName[jj].InnerText.Trim();
                                    //string StyleDesc = xmlnodeDesc[jj].InnerText.Trim();

                                    int KML_Style_ID = dAL_Import.Insert_KMLStyleMaster(KML_ID, StyleVal);

                                    // if (KML_Style_ID != 0)
                                    //{
                                    //    string linecolor = xmlnodeline[jj].ChildNodes.Item(0).InnerText.Trim();
                                    //    string width = xmlnodeline[jj].ChildNodes.Item(1).InnerText.Trim();
                                    //    int linestatus = dAL_Import.Insert_KML_lineStyle(KML_Style_ID, linecolor, width);

                                    //    string color = xmlnodepoly[jj].ChildNodes.Item(3).InnerText.Trim();
                                    //    string fill = xmlnodepoly[jj].ChildNodes.Item(4).InnerText.Trim();
                                    //    string outline = "1";
                                    //    int polystatus = dAL_Import.Insert_KML_polyStyle(KML_Style_ID, color, fill, outline);

                                        XmlNodeList xmlnodeLatLngList = xmldoc.SelectNodes("/outerBoundaryIs/LinearRing");
                                        xmlnodeLatLngList = xmldoc.GetElementsByTagName("outerBoundaryIs");
                                        XmlNodeList xmlnodeLatLngListInner = xmldoc.SelectNodes("/innerBoundaryIs/LinearRing");
                                        xmlnodeLatLngListInner = xmldoc.GetElementsByTagName("innerBoundaryIs");

                                        string name = xmlnodeDocument[jj].ChildNodes.Item(0).InnerText.Trim();
                                        string description = "";
                                        //string description = xmlnodeDocument[jj].ChildNodes.Item(1).InnerText.Trim();

                                        string styleUrl = xmlnodeDocument[jj].ChildNodes.Item(0).InnerText.Trim();
                                        int KML_pID = dAL_Import.Insert_KML_Placemark(KML_ID, description, name, styleUrl);

                                        string latlng_Outer = xmlnodeLatLngList[jj].ChildNodes.Item(0).InnerText.Trim();
                                        latlng_Outer = latlng_Outer.Replace(" ", "%");
                                        latlng_Outer = latlng_Outer.Replace("%%%%%%%%%%%%%%", "%");
                                        String[][] MultiArray = latlng_Outer.Split('%').Select(t => t.Split(',')).ToArray();

                                        dynamic Way_Points = MultiArray;

                                        IList<PointLatLng> coordinates = new List<PointLatLng>();
                                        foreach (var val in Way_Points)
                                        {
                                            PointLatLng pointLatLng = new PointLatLng();
                                            pointLatLng.Latitude = Convert.ToDouble(val[1]);
                                            pointLatLng.Longitude = Convert.ToDouble(val[0]);
                                            coordinates.Add(pointLatLng);
                                        }
                                        double area = MLA_Aerodyne.DataAccessLayer.CalculateAreaOfPolygon.ComputeSignedArea(coordinates);
                                        area = Math.Round(area, 2);
                                        area = Math.Abs(area);
                                        area = area / (double)10000;
                                        NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
                                        nfi.NumberDecimalSeparator = " ";
                                        string _area = area.ToString("N", nfi);

                                        foreach (string[] latlngVal in MultiArray)
                                        {
                                            int latlngOuterStatus = dAL_Import.Insert_KML_OuterPolygon(KML_pID, latlngVal[1], latlngVal[0], "0", _area.Replace(" ", ".") + " " + "hectares");
                                        }

                                        this.Visibility = Visibility.Hidden;
                                        CloseAllWindows();

                                        if (_progressBar == null)
                                            _progressBar = new ProgressBar("Process completed.");
                                        _progressBar.Hide();

                                        MessageBox message1 = new MessageBox("GeoJson KML parsing completed. Now click Save KML button on left menu.");
                                        message1.Show();
                                        MapLoad MapLoad = new MapLoad(User_Id, KML_ID.ToString(), MenuFlag);
                                        MapLoad.Show();
                                        _kmlType = 0;
                                        _error = 0;
                                    //}
                                    //else
                                    //{
                                    //    this.Visibility = Visibility.Hidden;
                                    //    CloseAllWindows();

                                    //    if (_progressBar == null)
                                    //        _progressBar = new ProgressBar("Process aborted.");
                                    //    _progressBar.Hide();
                                    //    MapLoad MapLoad1 = new MapLoad(User_Id, "null", MenuFlag);
                                    //    MapLoad1.Show();
                                    //    _kmlType = 0;
                                    //    _error = 0;
                                    //}



                                }
                                _kmlType = 0;
                                _error = 0;
                            }

                            else
                            {
                                this.Visibility = Visibility.Hidden;
                                CloseAllWindows();

                                if (_progressBar == null)
                                    _progressBar = new ProgressBar("Process aborted.");
                                _progressBar.Hide();
                                MessageBox message = new MessageBox("GeoJson KML parsing corrupted.");
                                message.Show();
                                MapLoad MapLoad1 = new MapLoad(User_Id, "null", MenuFlag);
                                MapLoad1.Show();
                                _kmlType = 0;
                                _error = 0;
                            }

                        }
                        _KML_folderPath = "";
                    }
                }
                else if (Filetype == 2)
                {
                    _KML_folderPath = _KML_folderPath + "\\" + User_Id + "\\" + "Landmark" + "\\" + DateTime.Now.ToString("dd.MM.yyyy");

                    if (!Directory.Exists(_KML_folderPath))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(_KML_folderPath);
                    }

                    string kml_fielname = Path.GetFileName(SelectedPath);
                    byte[] KML_bytes = System.IO.File.ReadAllBytes(SelectedPath);
                    _KML_folderPath = _KML_folderPath + "\\" + kml_fielname;
                    File.WriteAllBytes(_KML_folderPath, KML_bytes);

                    int pairValueCount = 0;
                    string readText = File.ReadAllText(SelectedPath);

                    XDocument z = XDocument.Parse(readText);
                    var result = z.Root.Attributes().
                            Where(a => a.IsNamespaceDeclaration).
                            GroupBy(a => a.Name.Namespace == XNamespace.None ? String.Empty : a.Name.LocalName,
                                    a => XNamespace.Get(a.Value)).
                            ToDictionary(g => g.Key,
                                         g => g.First());

                    foreach (var pair in result)
                    {
                        pairValueCount = result.Count;
                        string keyValue = pair.Key;
                        string pairValue = pair.Value.ToString();
                        if (keyValue == "kml")
                        {
                            _kmlType = 1;
                        }
                        if (keyValue == "globalmapper")
                        {
                            _kmlType = 2;
                        }

                    }

                    if (_kmlType == 2)

                    {
                        int _result = 0;
                        XmlDataDocument xmldoc = new XmlDataDocument();
                        FileStream fs1 = new FileStream(SelectedPath, FileMode.Open, FileAccess.Read);
                        xmldoc.Load(fs1);

                        XmlNodeList xmlnode = xmldoc.SelectNodes("/Placemark/description");
                        xmlnode = xmldoc.GetElementsByTagName("Placemark");

                        int i = 0;

                        XmlNodeList xmlnodeLatLngList = xmldoc.SelectNodes("/Point/coordinates");
                        xmlnodeLatLngList = xmldoc.GetElementsByTagName("Point");

                        for (i = 0; i <= xmlnode.Count - 1; i++)
                        {

                            string name = xmlnode[i].ChildNodes.Item(1).InnerText.Trim();

                            string latlng_landmark = xmlnodeLatLngList[i].ChildNodes.Item(0).InnerText.Trim();
                            latlng_landmark = latlng_landmark.Replace(" ", "%");
                            latlng_landmark = latlng_landmark.Replace("%%%%%%%%%%%%%%", "%");
                            String[][] MultiArray = latlng_landmark.Split('%').Select(t => t.Split(',')).ToArray();

                            foreach (string[] latlngVal in MultiArray)
                            {
                                if (latlngVal[1].Length < 2 || latlngVal[0].Length < 2)
                                {
                                    this.Visibility = Visibility.Hidden;
                                    MapLoad MapLoad1 = new MapLoad(User_Id, "", MenuFlag);
                                    MapLoad1.Show();
                                    MessageBox message1 = new MessageBox("Invalid Latitude or Longitude.");
                                    message1.Show();
                                    _kmlType = 0;
                                }
                                else
                                {
                                    DataAccessLayer.DAL_CreateLankmark DAL_CreateLankmark = new DataAccessLayer.DAL_CreateLankmark();
                                    DataAccessLayer.DAL_ImportKML DAL_LMID = new DataAccessLayer.DAL_ImportKML();
                                    _result = DAL_CreateLankmark.Import_CreateLandmark_ds(User_Id, name, latlngVal[1], latlngVal[0], "", "", Plot_ID);
                                    DAL_LMID.Insert_Land_temp(_result);
                                }

                            }

                        }
                        this.Visibility = Visibility.Hidden;
                        CloseAllWindows();
                        if (_result != 0)
                        {
                            MenuFlag = "ImportLandmarkKML";
                            if (_progressBar == null)
                                _progressBar = new ProgressBar("Process completed.");
                            _progressBar.Hide();
                            MapLoad MapLoad = new MapLoad(User_Id, Plot_ID, MenuFlag);
                            MapLoad.Show();
                            MessageBox message = new MessageBox("Global mapper landmark KML parsing completed. Click Save KML button to save the landmark.");
                            message.Show();
                            _result = 0;
                        }
                        else
                        {
                            this.Visibility = Visibility.Hidden;
                            CloseAllWindows();

                            if (_progressBar == null)
                                _progressBar = new ProgressBar("Process aborted.");
                            _progressBar.Hide();
                            MapLoad MapLoad1 = new MapLoad(User_Id, "", "");
                            MapLoad1.Show();
                            MessageBox message = new MessageBox("Global mapper landmark KML parsing aborted.");
                            message.Show();
                            _kmlType = 0;

                        }
                        _kmlType = 0;

                    }
                    if (_kmlType == 1)

                    {
                        int _result = 0;
                        XmlDataDocument xmldoc = new XmlDataDocument();
                        FileStream fs1 = new FileStream(SelectedPath, FileMode.Open, FileAccess.Read);
                        xmldoc.Load(fs1);

                        XmlNodeList xmlnode = xmldoc.SelectNodes("/Placemark/name");
                        xmlnode = xmldoc.GetElementsByTagName("Placemark");

                        int i = 0;

                        XmlNodeList xmlnodeLatLngList = xmldoc.SelectNodes("/Point/coordinates");
                        xmlnodeLatLngList = xmldoc.GetElementsByTagName("Point");

                        for (i = 0; i <= xmlnode.Count - 1; i++)
                        {
                            string name = xmlnode[i].ChildNodes.Item(0).InnerText.Trim();
                            string latlng_landmark = xmlnodeLatLngList[i].ChildNodes.Item(0).InnerText.Trim();
                            latlng_landmark = latlng_landmark.Replace(" ", "%");
                            latlng_landmark = latlng_landmark.Replace("%%%%%%%%%%%%%%", "%");
                            String[][] MultiArray = latlng_landmark.Split('%').Select(t => t.Split(',')).ToArray();

                            foreach (string[] latlngVal in MultiArray)
                            {
                                if (latlngVal[1].Length < 2 || latlngVal[0].Length < 2)
                                {
                                    this.Visibility = Visibility.Hidden;
                                    MapLoad MapLoad1 = new MapLoad(User_Id, "", MenuFlag);
                                    MapLoad1.Show();
                                    MessageBox message1 = new MessageBox("Invalid Latitude or Longitude.");
                                    message1.Show();
                                    _kmlType = 0;
                                }
                                else
                                {
                                    DataAccessLayer.DAL_CreateLankmark DAL_CreateLankmark = new DataAccessLayer.DAL_CreateLankmark();
                                    DataAccessLayer.DAL_ImportKML DAL_LMID = new DataAccessLayer.DAL_ImportKML();
                                    _result = DAL_CreateLankmark.Import_CreateLandmark_ds(User_Id, name, latlngVal[1], latlngVal[0], "", "", Plot_ID);
                                    DAL_LMID.Insert_Land_temp(_result);
                                }

                            }

                        }
                        this.Visibility = Visibility.Hidden;
                        CloseAllWindows();
                        if (_result != 0)
                        {
                            MenuFlag = "ImportLandmarkKML";
                            if (_progressBar == null)
                                _progressBar = new ProgressBar("Process aborted.");
                            _progressBar.Hide();
                            MapLoad MapLoad = new MapLoad(User_Id, Plot_ID, MenuFlag);
                            MapLoad.Show();
                            MessageBox message = new MessageBox("Google Earth landmark KML parsing completed.Click Save KML button to save the landmark.");
                            message.Show();
                            _result = 0;
                        }
                        else
                        {
                            this.Visibility = Visibility.Hidden;
                            CloseAllWindows();

                            if (_progressBar == null)
                                _progressBar = new ProgressBar("Process aborted.");
                            _progressBar.Hide();
                            MapLoad MapLoad1 = new MapLoad(User_Id, "", "");
                            MapLoad1.Show();
                            MessageBox message = new MessageBox("Google Earth landmark KML parsing aborted.");
                            message.Show();
                            _kmlType = 0;

                        }
                        _kmlType = 0;

                    }

                    if (pairValueCount == 1)

                    {
                        int _result = 0;
                        XmlDataDocument xmldoc = new XmlDataDocument();
                        FileStream fs1 = new FileStream(SelectedPath, FileMode.Open, FileAccess.Read);
                        xmldoc.Load(fs1);

                        XmlNodeList xmlnode = xmldoc.SelectNodes("/Placemark/name");
                        xmlnode = xmldoc.GetElementsByTagName("Placemark");

                        int i = 0;

                        XmlNodeList xmlnodeLatLngList = xmldoc.SelectNodes("/Point/coordinates");
                        xmlnodeLatLngList = xmldoc.GetElementsByTagName("Point");

                        for (i = 0; i <= xmlnode.Count - 1; i++)
                        {
                            string name = xmlnode[i].ChildNodes.Item(0).InnerText.Trim();
                            string latlng_landmark = xmlnodeLatLngList[i].ChildNodes.Item(0).InnerText.Trim();
                            latlng_landmark = latlng_landmark.Replace(" ", "%");
                            latlng_landmark = latlng_landmark.Replace("%%%%%%%%%%%%%%", "%");
                            String[][] MultiArray = latlng_landmark.Split('%').Select(t => t.Split(',')).ToArray();

                            foreach (string[] latlngVal in MultiArray)
                            {
                                if (latlngVal[1].Length < 2 || latlngVal[0].Length < 2)
                                {
                                    this.Visibility = Visibility.Hidden;
                                    MapLoad MapLoad1 = new MapLoad(User_Id, "", MenuFlag);
                                    MapLoad1.Show();
                                    MessageBox message1 = new MessageBox("Invalid Latitude or Longitude.");
                                    message1.Show();
                                    _kmlType = 0;
                                }
                                else
                                {
                                    DataAccessLayer.DAL_CreateLankmark DAL_CreateLankmark = new DataAccessLayer.DAL_CreateLankmark();
                                    DataAccessLayer.DAL_ImportKML DAL_LMID = new DataAccessLayer.DAL_ImportKML();
                                    _result = DAL_CreateLankmark.Import_CreateLandmark_ds(User_Id, name, latlngVal[1], latlngVal[0], "", "", Plot_ID);
                                    DAL_LMID.Insert_Land_temp(_result);
                                }

                            }

                        }
                        this.Visibility = Visibility.Hidden;
                        CloseAllWindows();
                        if (_result != 0)
                        {
                            MenuFlag = "ImportLandmarkKML";
                            if (_progressBar == null)
                                _progressBar = new ProgressBar("Process aborted.");
                            _progressBar.Hide();
                            MapLoad MapLoad = new MapLoad(User_Id, Plot_ID, MenuFlag);
                            MapLoad.Show();
                            MessageBox message = new MessageBox("GeoJson landmark KML parsing completed. Click Save KML button to save the landmark.");
                            message.Show();
                            _result = 0;
                        }
                        else
                        {
                            this.Visibility = Visibility.Hidden;
                            CloseAllWindows();

                            if (_progressBar == null)
                                _progressBar = new ProgressBar("Process aborted.");
                            _progressBar.Hide();
                            MapLoad MapLoad1 = new MapLoad(User_Id, "", "");
                            MapLoad1.Show();
                            MessageBox message = new MessageBox("GeoJson landmark KML parsing aborted.");
                            message.Show();
                            _kmlType = 0;

                        }
                        _kmlType = 0;

                    }
                }
          }   
            catch(Exception ex)
            {
                _errorDAL.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Import_KML", User_Id);
                MessageBox message = new MessageBox("An unhandled exception just occurred: " + ex.Message);
                message.Show();
            }
        }
     
        private void selectFiletypeCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void browsedrive_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a KML file";
            op.Filter = "kml files (*.kml)|*.kml";
            if (op.ShowDialog() == true)
            {
                pathText.Text = op.FileName;
                SelectedPath = pathText.Text;
                string path = op.FileName;
                string csv_file_path = path;
            }
        }

        private void MmapareaCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                 Plot_ID = MmapareaCombo.SelectedValue.ToString();              
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _errorDAL.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Import_KML", User_Id);
            }
        }

        public void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            try
            {
                if (_kmlType == 1)
                {
                    switch (e.Severity)
                    {
                        case XmlSeverityType.Error:
                            string er1 = "Error: {0}" + e.Message;
                            this.Visibility = Visibility.Hidden;
                            MessageBox message = new MessageBox("Invalid Google Earth KML file. " + er1);
                            message.Show();

                            if (_progressBar == null)
                                _progressBar = new ProgressBar("");
                            _progressBar.Hide();

                            _error = 1;
                            ((MapLoad)this.Owner).closebutton();
                            break;
                        case XmlSeverityType.Warning:
                            this.Visibility = Visibility.Hidden;
                            string wr1 = "Warning {0}" + e.Message;
                            MessageBox message1 = new MessageBox(wr1);
                            message1.Show();

                            if (_progressBar == null)
                                _progressBar = new ProgressBar("");
                            _progressBar.Hide();

                            _error = 1;
                            ((MapLoad)this.Owner).closebutton();
                            break;
                    }
                }
                if (_kmlType == 2)
                {
                    switch (e.Severity)
                    {
                        case XmlSeverityType.Error:
                            string er1 = "Error: {0}" + e.Message;
                            this.Visibility = Visibility.Hidden;
                            MessageBox message = new MessageBox("Invalid Global mapper KML file. " + er1);
                            message.Show();

                            if (_progressBar == null)
                                _progressBar = new ProgressBar("");
                            _progressBar.Hide();

                            _error = 1;
                            ((MapLoad)this.Owner).closebutton();
                            break;
                        case XmlSeverityType.Warning:
                            this.Visibility = Visibility.Hidden;
                            string wr1 = "Warning {0}" + e.Message;
                            MessageBox message1 = new MessageBox(wr1);
                            message1.Show();

                            if (_progressBar == null)
                                _progressBar = new ProgressBar("");
                            _progressBar.Hide();
                            _error = 1;
                            ((MapLoad)this.Owner).closebutton();
                            break;
                    }


                }
                if (_kmlType == 3)
                {
                    switch (e.Severity)
                    {
                        case XmlSeverityType.Error:
                            string er1 = "Error: {0}" + e.Message;
                            this.Visibility = Visibility.Hidden;
                            MessageBox message = new MessageBox("Invalid GeoJson KML file. " + er1);
                            message.Show();

                            if (_progressBar == null)
                                _progressBar = new ProgressBar("");
                            _progressBar.Hide();

                            _error = 1;
                            ((MapLoad)this.Owner).closebutton();
                            break;
                        case XmlSeverityType.Warning:
                            this.Visibility = Visibility.Hidden;
                            string wr1 = "Warning {0}" + e.Message;
                            MessageBox message1 = new MessageBox(wr1);
                            message1.Show();

                            if (_progressBar == null)
                                _progressBar = new ProgressBar("");
                            _progressBar.Hide();
                            _error = 1;
                            ((MapLoad)this.Owner).closebutton();
                            break;
                    }
                }

            }
            catch(Exception ex)
            {
                _errorDAL.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Import_KML", User_Id);
            }
        }

    }
}

    

