﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;
using MLA_Aerodyne.BuisnessAccessLayer;
using Path = System.IO.Path;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Configuration;
using System.Data;
using System.Windows.Forms;
using Microsoft.Toolkit.Wpf.UI.Controls;
using System.IO.Compression;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Diagnostics;
using System.Management;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for MapLoad.xaml
    /// </summary>
    /// 

    [ComVisible(true)]
    public partial class MapLoad : Window
    {
        StringBuilder SB = new StringBuilder();
        dynamic GetWayPoint = "";
        string[,] glb_array;
        string MapuserID;
        string User_Id;
        string MapArea_Id;
        string Paddock_Id;
        string _missiontype;
        CMapAreaWindow CreateMapAreaVal;
        CLandmarkWindow CLandmarkWindow;
        SelectMapAreaWindow SelectMapAreaWindow;
        SelectPaddockWindow SelectPaddockWindow;
		SelectPaddockForDefaultWPWindow selectPaddockForDefaultWPWindow;																
        SelectPaddockForLMWindow SelectPaddockForLMWindow;

        LoaderWindow loaderWindow;

        List<string> LstKML;
        List<string> LstLandKML;
        List<string> LstMapArea;
        List<string> LstPaddock;
        string Select_MapArea;
        string Select_Paddock;
        string Menu_Flag;
        string landmark_Index;
        bool setState = false;
        string view_Index;
        string MapStatus;
        string PaddockStatus;
        string WaypointStatus;
        string missionName;
        string KML_ID;
        string Plot_ID;

        int status = 0;
        List<string> Lstview = new List<string>();
        List<string> LstviewEdit = new List<string>();
        List<string> Lstlandmark = new List<string>();
        DataAccessLayer.Login_DAL _DALuserName = new DataAccessLayer.Login_DAL();
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();
        DataAccessLayer.DAL_SyncToCloud_StatusUpdate DAL_SyncToCloud_StatusUpdate = new DataAccessLayer.DAL_SyncToCloud_StatusUpdate();
        int is_loaded = 0;
        ProgressBar _progressBar;

        public int LastRowID = 0;

        public string Connection_Status = "false";

        DataSet dsKML = new DataSet();
        DataSet dshme = new DataSet();
        DataSet dsLandKML = new DataSet();
        
        // Prep stuff needed to remove close button on window
        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        public MapLoad(string UserId, string SelectIndex, string MenuFlag)
        {
            try
            {
                IsInternetConnection("https://fast.com/#");
                Loaded += ToolWindow_Loaded;
               
                MapuserID = UserId.ToString();
                InitializeComponent();
               
                User_Id = UserId;       
                KML_ID= SelectIndex;
                Select_MapArea = SelectIndex;
                Plot_ID = SelectIndex;
                Select_Paddock = SelectIndex;

                landmark_Index = SelectIndex;

                view_Index = SelectIndex;

                Menu_Flag = MenuFlag;

                if (Menu_Flag == "SelMapArea")
                {
                    SelctMapAreaLoad(Select_MapArea);
                }
                else if (Menu_Flag == "SelPaddock")
                {
                    SelctPaddockLoad(Select_Paddock);
                }
                else if (Menu_Flag == "SelPaddockForLM")
                {
                    SelctPaddockLoad(Select_Paddock);
                }
				else if (Menu_Flag == "SelPaddockForDefaultWP_3")
                {
                    SelctPaddockLoad(Select_Paddock);
                }
                else if (Menu_Flag == "SelPaddockForAutoWP_2")
                {
                    SelctPaddockLoad(Select_Paddock);
                }
                else if (Menu_Flag == "SelPaddockForAutoWP_5")
                {
                    SelctPaddockLoad(Select_Paddock);
                }
                else if (Menu_Flag == "SelPaddockForAutoWPLORA_1")
                {
                    SelctPaddockLoad(Select_Paddock);
                }
				else if (Menu_Flag == "SelPadForDefWPForWater_4")
                {
                    SelctPaddockLoad(Select_Paddock);
                }
               

            }
            catch (Exception ex)
            {
                _error.Insert_Errorlog("An unhandled exception just occurred: "+ ex.Message, "MapLoad", User_Id);
            }
           
        }

        void ToolWindow_Loaded(object sender, RoutedEventArgs e)
        {
            // Code to remove close box from window
           
            var hwnd = new System.Windows.Interop.WindowInteropHelper(this).Handle;
            SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
        }

       
        /// Enumerates network adapters installed on the computer.
        ///
        public void EnumerateNetworkAdapters()
        {
            // Information about network adapters can be found
            // by querying the WMI class Win32_NetworkAdapter,
            // NetConnectionStatus = 2 filters the ones that are not connected.
            // WMI related classes are located in assembly System.Management.dll
            ManagementObjectSearcher searcher = new
                ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapter" +
                                                " WHERE NetConnectionStatus=2");

            ManagementObjectCollection adapterObjects = searcher.Get();

            foreach (ManagementObject adapterObject in adapterObjects)
            {
                string name = adapterObject["Name"].ToString();

                // Create an instance of NetworkAdapter class,
                // and create performance counters for it.
                //NetworkAdapter adapter = new NetworkAdapter(name);
                string Val1 = new PerformanceCounter("Network Interface",
                                                    "Bytes Received/sec", name).ToString();
                string Val2 = new PerformanceCounter("Network Interface",
                                                        "Bytes Sent/sec", name).ToString();
                //this.adapters.Add(adapter); // Add it to ArrayList adapter
            }
        }
        public void GetUserIdFromLogin(string UserId)
        {
            try
            {
                BAL_CreateArea MapArea = new BAL_CreateArea();
                MapArea.UserId = UserId;
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                return;
            }
        }

        public void viewLandmark(string landmarkIndex)
        {
            try
            {
                landmark_Index = landmarkIndex;
                DataAccessLayer.ViewLandMark transObj = new DataAccessLayer.ViewLandMark();
                Lstlandmark = transObj.get_latlong(landmark_Index);
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                return;
            }
        }

        public void SelctMapAreaLoad(string MapAreaName)
        {
            try
            {
                MapArea_Id = MapAreaName;
                DataAccessLayer.DAL_CreatePaddock DAL_CreatePaddock = new DataAccessLayer.DAL_CreatePaddock();
                LstMapArea = DAL_CreatePaddock.GetSelectMapAreaPointFromDB(MapArea_Id);
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                return;
            }
        }

        public void SelctPaddockLoad(string PaddockName)
        {
            try
            {
                Paddock_Id = PaddockName;
                DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                LstPaddock = DAL_CreateWayPoint.GetSelectPaddockPointFromDB(Paddock_Id);
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                return;
            }
        }

        public void DockPanel_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (loaderWindow == null)
                    loaderWindow = new LoaderWindow();
                loaderWindow.Show();

                System.Threading.Thread.Sleep(2000);

                if (is_loaded == 0)
                {
                    StreamReader sr = new StreamReader(Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["MapTextFile"].ToString()));
                    string text = sr.ReadToEnd();
                    sr.Close();
                    webBrowser1.NavigateToString(text);
                    is_loaded = 1;
                }
                else
                {
                    //Not Html Load
                }

              
            }
            catch (Exception ex)
            {
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
            }

        }

        private void WebView_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
               
                System.Threading.Thread.Sleep(2000);
                string _userName = _DALuserName._get_username(User_Id);
                this.webBrowser1.InvokeScript("getUsername", _userName);
               
                if (MapuserID != "")
                {
                    DataAccessLayer.DAL_DefaultMapLoad DAL_DefaultMapLoad = new DataAccessLayer.DAL_DefaultMapLoad();
                    DataTable MapData = DAL_DefaultMapLoad.UserLocationLoad(User_Id);

                    string LastSyncCloudDT = DAL_SyncToCloud_StatusUpdate.GetLastSyncCloudDateTime(User_Id);

                    if(LastSyncCloudDT != "")
                    {
                        DateTime SyncdateTime = new DateTime();
                        SyncdateTime = Convert.ToDateTime(LastSyncCloudDT);
                        LastSyncCloudDT = SyncdateTime.ToString("MMMM dd, yyyy hh:mm tt");
                    }
                    else
                    {
                        LastSyncCloudDT = "No updates.";
                    }
                    

                    if (MapData.Columns[0].ColumnName == "Latitude")
                    {
                        foreach (DataRow row in MapData.Rows)
                        {
                            string lat1 = row["Latitude"].ToString();
                            string lng1 = row["Longitude"].ToString();
                            string maparea = "";

                            if (!String.IsNullOrEmpty(lat1))
                            {
                                // do something
                                System.Threading.Thread.Sleep(3000);
                                string val = this.webBrowser1.InvokeScript("GetMapLocation", lat1, lng1, maparea, LastSyncCloudDT);
                                //System.Threading.Thread.Sleep(1000);
                            }
                        }
                    }
                    else if (MapData.Columns[0].ColumnName == "Plot_Latitude")
                    {
                        string lat2 = MapData.Rows[0]["Plot_Latitude"].ToString();
                        string lng2 = MapData.Rows[0]["Plot_Longitude"].ToString();
                        string maparea = MapData.Rows[0]["Plot_Name"].ToString();
                        if (!String.IsNullOrEmpty(lat2))
                        {
                            // do something
                            System.Threading.Thread.Sleep(3000);
                            string val = this.webBrowser1.InvokeScript("GetMapLocation", lat2, lng2, maparea, LastSyncCloudDT);
                            //System.Threading.Thread.Sleep(1000);
                        }

                        
                    }

                }

                if (setState != true)
                {
                    ButtonAutomationPeer peer = new ButtonAutomationPeer(HiddenBtn);
                    IInvokeProvider invokeProv = peer.GetPattern(PatternInterface.Invoke) as IInvokeProvider;
                    invokeProv.Invoke();
                    setState = true;
                }

                if (loaderWindow == null)
                    loaderWindow = new LoaderWindow();
                loaderWindow.Hide();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                SB.Append(ex.Message.ToString());
            }

            File.WriteAllText(Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["ErrorLog"].ToString()), SB.ToString());
        }

        private void CloseAllWindows()
        {
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > 0; intCounter--)
                App.Current.Windows[intCounter].Close();
        }

        public void WebBrowser_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            try
            {
                if (setState != true)
                {
                    ButtonAutomationPeer peer = new ButtonAutomationPeer(HiddenBtn);
                    IInvokeProvider invokeProv = peer.GetPattern(PatternInterface.Invoke) as IInvokeProvider;
                    invokeProv.Invoke();
                    setState = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
             
            }

        }


        public void CreateCaptureImage()
        {
            try
            {
                DateTime nowDT = DateTime.Now;

                string now_DT = nowDT.ToString("yyyyMMddHHmmss");

                string ImgFileName = "Cap_" + now_DT + ".png";

                string FilePath = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImg"].ToString());

                string path = FilePath + ImgFileName;

                CreateBitmapFromVisual(this.webBrowser1, path);
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                return;
            }
        }

        public void CreateBitmapFromVisual(Visual target, string filename)
        {
            try
            {
                Visual target2 = webBrowser1;

                if (target2 == null)
                {
                    return;
                }
                Rect bounds = VisualTreeHelper.GetDescendantBounds(target);

                Type t = target2.GetType();
                if (t.Name == "WebView")
                {
                    
                    System.Windows.Point p0 = target2.PointToScreen(bounds.TopLeft);
                    System.Drawing.Point p1 = new System.Drawing.Point((int)p0.X, (int)p0.Y);
                    using (Bitmap image = new Bitmap((int)bounds.Width, (int)bounds.Height))
                    {
                        Graphics imgGraphics = Graphics.FromImage(image);
                        imgGraphics.CopyFromScreen(p1.X, p1.Y, 0, 0, new System.Drawing.Size((int)bounds.Width, (int)bounds.Height));
                        image.Save(filename, ImageFormat.Png);
                    }
                }
                else
                {
                    RenderTargetBitmap rtb = new RenderTargetBitmap((Int32)bounds.Width, (Int32)bounds.Height, 96, 96, PixelFormats.Default);

                    DrawingVisual dv = new DrawingVisual();
                    using (DrawingContext dc = dv.RenderOpen())
                    {
                        VisualBrush vb = new VisualBrush(target);
                        dc.DrawRectangle(vb, null, new Rect(new System.Windows.Point(), bounds.Size));
                    }
                    rtb.Render(dv);
                    PngBitmapEncoder png = new PngBitmapEncoder();
                    png.Frames.Add(BitmapFrame.Create(rtb));
                    using (Stream stm = File.Create(filename))
                    {
                        png.Save(stm);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                return;
            }
        }

        private void Reload()
        {
            try
            {
                LstMapArea = null;
                LstPaddock = null;
                Lstlandmark = null;
                GetWayPoint = "";
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                return;
            }
        }

        public void LogOut()
        {
            System.Windows.Application.Current.Shutdown();
        }

        public void View_Landmark(string[,] array_LatLong_view)
        {
            try
            {
                glb_array = array_LatLong_view;
                this.webBrowser1.InvokeScript("initMap");
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                return;
            }
        }

        [System.Runtime.InteropServices.ComVisibleAttribute(true)]
        public class HtmlInteropInternal
        {
            public void endDragMarkerCS(Decimal Lat, Decimal Lng)
            {
                //((MainWindow)Application.Current.MainWindow).tbLocation.Text = Math.Round(Lat, 5) + "," + Math.Round(Lng, 5);
            }

            public void AddMarkerValues(string WayPoint)
            {
                
                dynamic Waypoints = JsonConvert.DeserializeObject(WayPoint);

                foreach (var val in Waypoints)
                {
                    string Name = val.name;
                    string CreatedBy = val.CreatedBy;
                   
                }
            }
        }

        public async void Maparea()
        {
            try
            {
                string mapType = "Create Maparea";
                string WayPoint = GetWayPoint;

                if (WayPoint != "[]")
                {
                    DataAccessLayer.DAL_CreateMapArea DAL_CreateMapArea = new DataAccessLayer.DAL_CreateMapArea();
                    string plotid = DAL_CreateMapArea.Check_MapArea(view_Index, User_Id);                  
                    if (MapStatus == "Update")
                    {
                        if (plotid == view_Index)
                        {
                            
                            DateTime nowDT = DateTime.Now;
                            string now_DT = nowDT.ToString("yyyyMMddHHmmss");
                            string ImgFileName = "MapArea_" + now_DT + ".png";
                            string FilePath = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImg"].ToString());
                            string CapImagePath = FilePath + ImgFileName;
                            dynamic WayValues = JsonConvert.DeserializeObject(WayPoint);

                            foreach (var val in WayValues)
                            {
                                string Name = val.name;
                                string CreatedBy = val.CreatedBy;
                                string[] points = val.points;
                            }
                            string ValWaypoints = JsonConvert.DeserializeObject(WayPoint).ToString();

                            System.Threading.Thread.Sleep(1000);

                            try
                            {
                                CreateBitmapFromVisual(webBrowser1, CapImagePath);
                            }
                            catch (Exception ex)
                            {
                                MessageBox message1 = new MessageBox("Error: " + ex.Message);
                                message1.Show();
                            }

                            if (CreateMapAreaVal == null)
                                CreateMapAreaVal = new CMapAreaWindow(MapStatus, view_Index, mapType);

                            DAL_CreateMapArea.Update_CreateMapArea(User_Id, ValWaypoints, CapImagePath, view_Index);

                            this.webBrowser1.InvokeScript("initMap");
                            this.webBrowser1.InvokeScript("clearMarkers");
                            ValWaypoints = "";
                            view_Index = "";
                            MessageBox message = new MessageBox("Maparea updated successfully.");
                            message.Show();
                            this.webBrowser1.InvokeScript("MapArea_text", "Reload");
                            LstMapArea = null;
                        }
                    }
                    else
                    {
                        this.webBrowser1.InvokeScript("MapArea_text", "Save");
                        DateTime nowDT = DateTime.Now;
                        string now_DT = nowDT.ToString("yyyyMMddHHmmss");
                        string ImgFileName = "MapArea_" + now_DT + ".png";
                        string FilePath = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImg"].ToString());
                        string CapImagePath = FilePath + ImgFileName;
                        dynamic WayValues = JsonConvert.DeserializeObject(WayPoint);

                        foreach (var val in WayValues)
                        {
                            string Name = val.name;
                            string CreatedBy = val.CreatedBy;
                            string[] points = val.points;
                        }
                        string ValWaypoints = JsonConvert.DeserializeObject(WayPoint).ToString();

                        System.Threading.Thread.Sleep(1000);

                        try
                        {
                            CreateBitmapFromVisual(webBrowser1, CapImagePath);
                        }
                        catch (Exception ex)
                        {
                            MessageBox message2 = new MessageBox("Error: " + ex.Message);
                            message2.Show();
                            _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                        }
                        if (CreateMapAreaVal == null)
                            CreateMapAreaVal = new CMapAreaWindow(MapStatus, view_Index, mapType);
                        CreateMapAreaVal.ValueFromMapLoad(User_Id, ValWaypoints, CapImagePath);
                        CreateMapAreaVal.Owner = this;
                        CreateMapAreaVal.ShowDialog();
                        ValWaypoints = "";
                        view_Index = "";
                        this.webBrowser1.InvokeScript("MapArea_text", "Reload");
                        LstMapArea = null;

                    }
                    LstMapArea = null;
                }
                else
                {
                  
                    MessageBox message = new MessageBox("Please mark your map area.");
                    message.Show();
                }
                LstMapArea = null;

            }
            catch (AggregateException err)
            {
                foreach (var errInner in err.InnerExceptions)
                {
                    MessageBox message = new MessageBox(errInner.ToString());
                    message.Show();
                    _error.Insert_Errorlog("An unhandled exception just occurred: " + err.Message, "MapLoad", User_Id);
                }
            }

        }

        public void closebutton()
        {
            this.webBrowser1.InvokeScript("ReloadclearMarkers");
        }

        public void clearmap()
        {
             this.webBrowser1.InvokeScript("initMap");
             this.webBrowser1.InvokeScript("clearMarkers");
             this.webBrowser1.InvokeScript("MapArea_text", "Reload");
        }

        public void clearpaddock()
        {
            this.webBrowser1.InvokeScript("initMap");
            this.webBrowser1.InvokeScript("clearMarkers");
            this.webBrowser1.InvokeScript("Paddock_text", "Reload");
        }

        public void clearWaypoint()
        {
            this.webBrowser1.InvokeScript("initMap");
            this.webBrowser1.InvokeScript("clearMarkers");
            this.webBrowser1.InvokeScript("Waypoint_text", "Reload");
        }
        public void clearLandmark()
        {
            this.webBrowser1.InvokeScript("initMap");
            this.webBrowser1.InvokeScript("clearMarkers");
            this.webBrowser1.InvokeScript("Landmark_text", "Reload");
        }

        private void LandMark()
        {
            try
            {
                if (LstPaddock != null)
                {
                    string WayPoint = GetWayPoint;

                    if (WayPoint != "[]")
                    {
                        dynamic Waypoints = JsonConvert.DeserializeObject(WayPoint);

                        DateTime nowDT = DateTime.Now;

                        string now_DT = nowDT.ToString("yyyyMMddHHmmss");

                        string ImgFileName = "Landmark_" + now_DT + ".png";

                        string FilePath = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImg"].ToString());

                        string CapImagePath = FilePath + ImgFileName;

                        foreach (var val in Waypoints)
                        {
                            string Name = val.name;
                            string CreatedBy = val.CreatedBy;
                            string[] points = val.points;
                        }

                        string ValWaypoints = JsonConvert.DeserializeObject(WayPoint).ToString();

                        System.Threading.Thread.Sleep(1000);

                        try
                        {
                            CreateBitmapFromVisual(webBrowser1, CapImagePath);
                        }
                        catch (Exception ex)
                        {
                            MessageBox message = new MessageBox("Error: " + ex.Message);
                            message.Show();
                            _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                        }

                        this.webBrowser1.InvokeScript("initMap");
                        this.webBrowser1.InvokeScript("clearMarkers");

                        System.Threading.Thread.Sleep(2000);

                        if (CLandmarkWindow == null)
                            CLandmarkWindow = new CLandmarkWindow();

                        CLandmarkWindow.ValueFromMapLoad(User_Id, ValWaypoints, CapImagePath, Paddock_Id);
                        CLandmarkWindow.Owner = this;
                        CLandmarkWindow.ShowDialog();

                        ValWaypoints = "";
                        Paddock_Id = "";
                        this.webBrowser1.InvokeScript("MapArea_text", "Reload");
                        //CreateBitmapFromVisual(webBrowser1, path);
                    }
                    else
                    {
                        MessageBox message = new MessageBox("Please mark your landmark.");
                        message.Show();
                    }
 
                }
                else
                {
                    if (SelectPaddockForLMWindow == null)
                    SelectPaddockForLMWindow = new SelectPaddockForLMWindow();
                    SelectPaddockForLMWindow.Owner = this;
                    SelectPaddockForLMWindow.ValueFromMapLoad(User_Id);
                    SelectPaddockForLMWindow.Show();
                    LstPaddock = null;
                }
               
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
            }
        }

        private void Paddock()
        {
            try
            {
                if (LstMapArea != null)
                {
                    string WayPoint = GetWayPoint;

                    if (WayPoint != "[]")
                    {
                        string[] array = { };

                        DataAccessLayer.DAL_CreatePaddock DAL_CreatePaddock = new DataAccessLayer.DAL_CreatePaddock();
                        string padID = DAL_CreatePaddock.Check_Paddock(view_Index, User_Id);
                        if (PaddockStatus == "Update")
                        {
                            if (padID == view_Index)
                            {

                                dynamic Waypoints = JsonConvert.DeserializeObject(WayPoint);

                                DateTime nowDT = DateTime.Now;

                                string now_DT = nowDT.ToString("yyyyMMddHHmmss");

                                string ImgFileName = "Paddock_" + now_DT + ".png";

                                string FilePath = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImg"].ToString());

                                string CapImagePath = FilePath + ImgFileName;

                                foreach (var val in Waypoints)
                                {
                                    string Name = val.name;
                                    string CreatedBy = val.CreatedBy;
                                    string[] points = val.points;
                                }

                                string ValWaypoints = JsonConvert.DeserializeObject(WayPoint).ToString();

                                System.Threading.Thread.Sleep(1000);

                                try
                                {
                                    CreateBitmapFromVisual(webBrowser1, CapImagePath);
                                }
                                catch (Exception ex)
                                {
                                    MessageBox message1 = new MessageBox("Error: " + ex.Message);
                                    message1.Show();
                                    _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                                }

                                CPaddockWindow cPaddockWindow = new CPaddockWindow();
                                DAL_CreatePaddock.Update_Paddock(User_Id, ValWaypoints, CapImagePath, view_Index);
                                this.webBrowser1.InvokeScript("initMap");
                                this.webBrowser1.InvokeScript("clearMarkers");
                                ValWaypoints = "";
                                view_Index = "";
                                MessageBox message = new MessageBox("Paddock updated successfully.");
                                message.Show();
                                this.webBrowser1.InvokeScript("Paddock_text", "Reload");
                                LstMapArea = null;
                            }
                        }
                        else
                        {
                            dynamic Waypoints = JsonConvert.DeserializeObject(WayPoint);

                            DateTime nowDT = DateTime.Now;

                            string now_DT = nowDT.ToString("yyyyMMddHHmmss");

                            string ImgFileName = "Paddock_" + now_DT + ".png";

                            string FilePath = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImg"].ToString());

                            string CapImagePath = FilePath + ImgFileName;

                            foreach (var val in Waypoints)
                            {
                                string Name = val.name;
                                string CreatedBy = val.CreatedBy;
                                string[] points = val.points;
                            }

                            string ValWaypoints = JsonConvert.DeserializeObject(WayPoint).ToString();

                            System.Threading.Thread.Sleep(1000);

                            try
                            {
                                CreateBitmapFromVisual(webBrowser1, CapImagePath);
                            }
                            catch (Exception ex)
                            {
                                MessageBox message1 = new MessageBox("Error: " + ex.Message);
                                message1.Show();
                                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                            }

                            CPaddockWindow cPaddockWindow = new CPaddockWindow();
                            cPaddockWindow.ValueFromMapLoad(User_Id, ValWaypoints, CapImagePath, MapArea_Id);
                            cPaddockWindow.Owner = this;
                            cPaddockWindow.ShowDialog();                         
                            ValWaypoints = "";
                            MapArea_Id = "";
                            LstMapArea = null;
                            this.webBrowser1.InvokeScript("Paddock_text", "Reload");
                        }
                    }
                    else
                    {
                        MessageBox message = new MessageBox("Please mark your Paddock.");
                        message.Show();
                    }
                }
                else
                {
                    if (SelectMapAreaWindow == null)
                        SelectMapAreaWindow = new SelectMapAreaWindow(User_Id);
                    SelectMapAreaWindow.Owner = this;
                    
                    SelectMapAreaWindow.ValueFromMapLoad(User_Id);
                    SelectMapAreaWindow.Show();

                    LstMapArea = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
            }
        }

        private void Waypoint()
        {
            try
            {
                if (LstPaddock != null)
                {
                    string WayPoint = GetWayPoint;

                    if (WayPoint != "[]")
                    {
                        string[] array = { };
                        DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                        string wayID = DAL_CreateWayPoint.Check_Waypoint(view_Index, User_Id);
                        if (WaypointStatus == "Update")
                        {
                            if (wayID == view_Index)
                            {

                                dynamic Waypoints = JsonConvert.DeserializeObject(WayPoint);

                                DateTime nowDT = DateTime.Now;

                                string now_DT = nowDT.ToString("yyyyMMddHHmmss");

                                string ImgFileName = "Waypoint_" + now_DT + ".png";

                                string FilePath = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImg"].ToString());

                                string CapImagePath = FilePath + ImgFileName;

                                string ValWaypoints = JsonConvert.DeserializeObject(WayPoint).ToString();

                                System.Threading.Thread.Sleep(1000);

                                try
                                {
                                    CreateBitmapFromVisual(webBrowser1, CapImagePath);
                                }
                                catch (Exception ex)
                                {
                                    MessageBox message1 = new MessageBox("Error: " + ex.Message);
                                    message1.Show();
                                    _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                                }



                                DAL_CreateWayPoint.Update_Waypoint(User_Id, ValWaypoints, CapImagePath, view_Index);
                                this.webBrowser1.InvokeScript("initMap");
                                this.webBrowser1.InvokeScript("clearMarkers");
                                ValWaypoints = "";
                                view_Index = "";
                                MessageBox message = new MessageBox("Waypoint updated successfully");
                                message.Show();
                                this.webBrowser1.InvokeScript("Waypoint_text", "Reload");
                            }
                        }
                        else
                        {
                            dynamic Waypoints = JsonConvert.DeserializeObject(WayPoint);
                            IList<PointLatLng> coordinates = new List<PointLatLng>();
                            foreach (var val in Waypoints)
                            {
                                if (val != null)
                                {
                                    PointLatLng pointLatLng = new PointLatLng();
                                    pointLatLng.Latitude = val.lat;
                                    pointLatLng.Longitude = val.lng;
                                    coordinates.Add(pointLatLng);
                                }
                            }
                            if (coordinates.Count <= 99)
                            {

                                DateTime nowDT = DateTime.Now;

                                string now_DT = nowDT.ToString("yyyyMMddHHmmss");

                                string ImgFileName = "Waypoint_" + now_DT + ".png";

                                string FilePath = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImg"].ToString());

                                string CapImagePath = FilePath + ImgFileName;

                                string ValWaypoints = JsonConvert.DeserializeObject(WayPoint).ToString();

                                System.Threading.Thread.Sleep(1000);

                                try
                                {
                                    CreateBitmapFromVisual(webBrowser1, CapImagePath);
                                }
                                catch (Exception ex)
                                {
                                    MessageBox message1 = new MessageBox("Error: " + ex.Message);
                                    message1.Show();
                                    _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                                }

                                CWaypointWindow cWaypointWindow = new CWaypointWindow();
                                cWaypointWindow.ValueFromMapLoad(User_Id, ValWaypoints, CapImagePath, Paddock_Id, _missiontype);
                                cWaypointWindow.Owner = this;
                                cWaypointWindow.ShowDialog();
                                ValWaypoints = "";
                                Paddock_Id = "";
                                this.webBrowser1.InvokeScript("Waypoint_text", "Reload");
                            }
                            else
                            {
                                MessageBox message1 = new MessageBox("Please mark less than 99 Geo points.");
                                message1.Show();
                            }
                        }
                    }
                    else
                    {
                        
                        MessageBox message = new MessageBox("Please mark your Waypoint!!");
                        message.Show();
                    }
                }
                else
                {
                    if (SelectPaddockWindow == null)
                        SelectPaddockWindow = new SelectPaddockWindow(User_Id);
                    SelectPaddockWindow.Owner = this;
                    SelectPaddockWindow.ValueFromMapLoad(User_Id);
                    SelectPaddockWindow.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
            }
        }

		private void DefaultWaypoint()
        {
            try
            {
                if (LstPaddock != null)
                {
                    string WayPoint = GetWayPoint;

                    if (WayPoint != "[]")
                    {
                        string[] array = { };
                        DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                        string wayID = DAL_CreateWayPoint.Check_Waypoint(view_Index, User_Id);
                        if (WaypointStatus == "Update")
                        {
                            if (wayID == view_Index)
                            {

                                dynamic Waypoints = JsonConvert.DeserializeObject(WayPoint);

                                DateTime nowDT = DateTime.Now;

                                string now_DT = nowDT.ToString("yyyyMMddHHmmss");

                                string ImgFileName = "Waypoint_" + now_DT + ".png";

                                string FilePath = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImg"].ToString());

                                string CapImagePath = FilePath + ImgFileName;

                                foreach (var val in Waypoints)
                                {
                                    string Name = val.name;
                                    string CreatedBy = val.CreatedBy;
                                    string[] points = val.points;
                                }

                                string ValWaypoints = JsonConvert.DeserializeObject(WayPoint).ToString();

                                System.Threading.Thread.Sleep(1000);

                                try
                                {
                                    CreateBitmapFromVisual(webBrowser1, CapImagePath);
                                }
                                catch (Exception ex)
                                {
                                    MessageBox message1 = new MessageBox("Error: " + ex.Message);
                                    message1.Show();
                                    _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                                }
                                DAL_CreateWayPoint.Update_Waypoint(User_Id, ValWaypoints, CapImagePath, view_Index);
                                this.webBrowser1.InvokeScript("initMap");
                                this.webBrowser1.InvokeScript("clearMarkers");
                                ValWaypoints = "";
                                view_Index = "";
                                MessageBox message = new MessageBox("Waypoint updated successfully");
                                message.Show();
                                this.webBrowser1.InvokeScript("DefaultWaypoint_text", "Reload");
                            }
                        }
                        else
                        {
                            dynamic Waypoints = JsonConvert.DeserializeObject(WayPoint);

                            DateTime nowDT = DateTime.Now;

                            string now_DT = nowDT.ToString("yyyyMMddHHmmss");

                            string ImgFileName = "Waypoint_" + now_DT + ".png";

                            string FilePath = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImg"].ToString());

                            string CapImagePath = FilePath + ImgFileName;

                            string ValWaypoints = JsonConvert.DeserializeObject(WayPoint).ToString();

                            System.Threading.Thread.Sleep(1000);

                            try
                            {
                                CreateBitmapFromVisual(webBrowser1, CapImagePath);
                            }
                            catch (Exception ex)
                            {
                                MessageBox message1 = new MessageBox("Error: " + ex.Message);
                                message1.Show();
                                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                            }

                            CWaypointWindow cWaypointWindow = new CWaypointWindow();
                            cWaypointWindow.ValueFromMapLoad(User_Id, ValWaypoints, CapImagePath, Paddock_Id, _missiontype);
                            cWaypointWindow.Owner = this;
                            cWaypointWindow.ShowDialog();
                            ValWaypoints = "";
                            Paddock_Id = "";
                            this.webBrowser1.InvokeScript("DefaultWaypoint_text", "Reload");
                        }
                    }
                    else
                    {
                        MessageBox message = new MessageBox("Please mark your Waypoint!!");
                        message.Show();
                    }

                    LstPaddock = null;
                }
                else
                {
                    if (selectPaddockForDefaultWPWindow == null)
                        selectPaddockForDefaultWPWindow = new SelectPaddockForDefaultWPWindow(User_Id);
                    selectPaddockForDefaultWPWindow.Owner = this;
                    selectPaddockForDefaultWPWindow.ValueFromMapLoad(User_Id);
                    selectPaddockForDefaultWPWindow.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
            }
        }
        private void MissionHub(string Mission_Name)
        {
            try
            {
                missionName = Mission_Name;
                if (status == 1)
                {
                    MissionHub missionHubWindow = new MissionHub(MapuserID, Mission_Name, Paddock_Id, status);
                    missionHubWindow.Owner = this;
                    missionHubWindow.ShowDialog();

                    Paddock_Id = "";
                }
                else
                {
                    MissionHub missionHubWindow = new MissionHub(MapuserID, Mission_Name, Paddock_Id, status);
                    missionHubWindow.Owner = this;
                    missionHubWindow.ShowDialog();

                    Paddock_Id = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                return;
            }
        }
     

        private void HiddenBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Convert to string array.

                string[] array = { };
                string[] arrayEdit = { };

                if (Menu_Flag == "SelMapArea")
                {
                    //CMapAreaWindow cUpdate = new CMapAreaWindow(MapStatus);
                    DataAccessLayer.DAL_CreatePaddock DAL_CreatePaddock = new DataAccessLayer.DAL_CreatePaddock();
                    LstMapArea = DAL_CreatePaddock.GetSelectMapAreaPointFromDB(MapArea_Id);
                    string[] ArMapArea = LstMapArea.ToArray();
                    this.webBrowser1.InvokeScript("MapValFromDB", JsonConvert.SerializeObject(ArMapArea));
                }
                else if (Menu_Flag == "SelPaddock")
                {
                    DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                    LstPaddock = DAL_CreateWayPoint.GetSelectPaddockPointFromDB(Paddock_Id);
                    string[] ArMapArea = LstPaddock.ToArray();
                    _missiontype = "Customize";
                    this.webBrowser1.InvokeScript("PaddockValFromDB", JsonConvert.SerializeObject(ArMapArea), _missiontype);
                }

                else if (Menu_Flag == "ImportKML")
                {
                    DataAccessLayer.DAL_ImportKML DAL_kml = new DataAccessLayer.DAL_ImportKML();
                    dsKML = DAL_kml.GetPlacemarkdetails(KML_ID, User_Id);
                    DataTable Mdt = dsKML.Tables[0];
                    for (int i = 0; i < Mdt.Rows.Count; i++)
                    {
                        string _pID = dsKML.Tables[0].Rows[i]["Placemark_ID"].ToString();
                        LstKML = DAL_kml.GetSelectedPolygonFromDB(_pID, KML_ID, User_Id);
                        string[] ArKML = LstKML.ToArray();
                        this.webBrowser1.InvokeScript("KML_ValFromDB", JsonConvert.SerializeObject(ArKML));
                        LstKML.Clear();
                    }

                    this.webBrowser1.InvokeScript("KML_GetVal");
                }
                else if (Menu_Flag == "ImportLandmarkKML")
                {
                    DataAccessLayer.DAL_ImportKML DAL_kml = new DataAccessLayer.DAL_ImportKML();
                    dsLandKML = DAL_kml.GetLM_temp_details();
                    DataTable Mdt = dsLandKML.Tables[0];
                    for (int i = 0; i < Mdt.Rows.Count; i++)
                    {
                        string _landID = dsLandKML.Tables[0].Rows[i]["LM_ID"].ToString();
                        LstLandKML = DAL_kml.GetSelectedLandmarkFromDB(Plot_ID, _landID, User_Id);
                        string splitAll = "";
                        string latitude = "";
                        string longitude = "";
                        for (int j = 0; j < LstLandKML.Count; j++)
                        {
                            // Part B: access element with index.
                            splitAll = LstLandKML[0];
                            string[] latlng = splitAll.Split(',');
                            latitude = latlng[0];
                            longitude = latlng[1];
                        }
                        string[] ArKML = LstLandKML.ToArray();
                        this.webBrowser1.InvokeScript("drawCircleLandmark", latitude, longitude, _landID);
                        LstLandKML.Clear();
                    }
                    DAL_kml.Clear_Land_temp();
                }


                else if (Menu_Flag == "SelPaddockForDefaultWP_3")
                {
                    DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                    LstPaddock = DAL_CreateWayPoint.GetSelectPaddockPointFromDB(Paddock_Id);
                    string[] ArMapArea = LstPaddock.ToArray();
                    _missiontype = "Fence Monitoring";
                    this.webBrowser1.InvokeScript("PaddockValFromDBForDefaultWP", JsonConvert.SerializeObject(ArMapArea), _missiontype);
                }
                else if (Menu_Flag == "SelPaddockForAutoWP_2")
                {
                    DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                    LstPaddock = DAL_CreateWayPoint.GetSelectPaddockPointFromDB(Paddock_Id);
                    string[] ArMapArea = LstPaddock.ToArray();
                    _missiontype = "Detect and Count";
                    this.webBrowser1.InvokeScript("PaddockValFromDBForDefaultWP_auto", JsonConvert.SerializeObject(ArMapArea), _missiontype);
                }
                else if (Menu_Flag == "SelPaddockForAutoWP_5")
                {
                    DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                    LstPaddock = DAL_CreateWayPoint.GetSelectPaddockPointFromDB(Paddock_Id);
                    string[] ArMapArea = LstPaddock.ToArray();
                    _missiontype = "Weed Monitoring";
                    this.webBrowser1.InvokeScript("PaddockValFromDBForDefaultWP_auto", JsonConvert.SerializeObject(ArMapArea), _missiontype);
                }
                else if (Menu_Flag == "SelPaddockForAutoWPLORA_1")
                {
                    DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                    LstPaddock = DAL_CreateWayPoint.GetSelectPaddockPointFromDB(Paddock_Id);
                    string[] ArMapArea = LstPaddock.ToArray();
                    _missiontype = "LoRa Gateway";
                    this.webBrowser1.InvokeScript("PaddockValFromDBForDefaultLoRaWP", JsonConvert.SerializeObject(ArMapArea), _missiontype);
                }
                else if (Menu_Flag == "SelPadForDefWPForWater_4")
                {
                    DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                    LstPaddock = DAL_CreateWayPoint.GetSelectPaddockPointFromDB(Paddock_Id);
                    string[] ArMapArea = LstPaddock.ToArray();
                    _missiontype = "Water Monitoring";
                    this.webBrowser1.InvokeScript("PaddockValFromDBForWaterLM", JsonConvert.SerializeObject(ArMapArea), _missiontype, "LandMarker");
                }
                else if (Menu_Flag == "SelPaddockForLM")
                {
                    DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                    LstPaddock = DAL_CreateWayPoint.GetSelectPaddockPointFromDB(Paddock_Id);
                    string[] ArMapArea = LstPaddock.ToArray();
                    string val = this.webBrowser1.InvokeScript("PaddockValFromDBForLM", JsonConvert.SerializeObject(ArMapArea), "LandMarker");
                }
                else if (Menu_Flag == "VLandMark")
                {
                    DataAccessLayer.ViewLandMark transObj = new DataAccessLayer.ViewLandMark();
                    Lstview = transObj.get_latlong(view_Index);
                    array = Lstview.ToArray();
                    this.webBrowser1.InvokeScript("landmarkValFromDB", JsonConvert.SerializeObject(array));
                }
                else if (Menu_Flag == "editLandMark")
                {
                    DataAccessLayer.ViewLandMark transObj = new DataAccessLayer.ViewLandMark();
                    Lstview = transObj.get_latlong(view_Index);
                    array = Lstview.ToArray();
                    this.webBrowser1.InvokeScript("addMarker_array", JsonConvert.SerializeObject(array), "1");
                }
                else if (Menu_Flag == "VMaparea")
                {
                    DataAccessLayer.ViewMaparea transObj = new DataAccessLayer.ViewMaparea();
                    Lstview = transObj.get_latlong(view_Index);
                    array = Lstview.ToArray();
                    this.webBrowser1.InvokeScript("ViewMapFromDB", JsonConvert.SerializeObject(array));
                }
                else if (Menu_Flag == "editMaparea")
                {
                    MapStatus = "Update";
                    DataAccessLayer.ViewMaparea transObj = new DataAccessLayer.ViewMaparea();
                    Lstview = transObj.get_latlong(view_Index);
                    array = Lstview.ToArray();
                    this.webBrowser1.InvokeScript("addMarker_array_maparea", JsonConvert.SerializeObject(array), "1");
                }
                else if (Menu_Flag == "Vpaddock")
                {
                    Paddock_Id = view_Index;
                    DataAccessLayer.ViewPaddock transObj = new DataAccessLayer.ViewPaddock();
                    Lstview = transObj.get_latlong(view_Index);
                    array = Lstview.ToArray();
                    this.webBrowser1.InvokeScript("ViewPaddockFromDB", JsonConvert.SerializeObject(array));
                    // MissionHub missionHubWindow = new MissionHub(MapuserID, missionName, Paddock_Id, status);
                }
                else if (Menu_Flag == "SimulateFlight")
                {
                    DataAccessLayer.ViewWaypoint transObj = new DataAccessLayer.ViewWaypoint();
                    Lstview = transObj.get_latlong_simulate(view_Index);
                    array = Lstview.ToArray();
                    this.webBrowser1.InvokeScript("Simulateflight", JsonConvert.SerializeObject(array));
                }
                else if (Menu_Flag == "editPaddock")
                {
                    PaddockStatus = "Update";
                    DataAccessLayer.DAL_CreatePaddock DAL_CreatePaddock = new DataAccessLayer.DAL_CreatePaddock();
                    string plotID = DAL_CreatePaddock.Get_MapID(view_Index, User_Id);
                    DataAccessLayer.ViewMaparea transObjmap = new DataAccessLayer.ViewMaparea();
                    DataAccessLayer.ViewPaddock transObj = new DataAccessLayer.ViewPaddock();
                    LstviewEdit = transObjmap.get_latlong_edit(plotID, view_Index);
                    LstMapArea = LstviewEdit;
                    arrayEdit = LstviewEdit.ToArray();
                    this.webBrowser1.InvokeScript("EditMapFromDB", JsonConvert.SerializeObject(arrayEdit));
                    transObj = new DataAccessLayer.ViewPaddock();
                    Lstview = transObj.get_latlong(view_Index);
                    array = Lstview.ToArray();
                    this.webBrowser1.InvokeScript("addMarker_array_paddock", JsonConvert.SerializeObject(array), "1");
                }
                else if (Menu_Flag == "Vwaypoint")
                {
                    DataAccessLayer.ViewWaypoint transObj = new DataAccessLayer.ViewWaypoint();
                    Lstview = transObj.get_latlong(view_Index);
                    array = Lstview.ToArray();
                    this.webBrowser1.InvokeScript("ViewWaypointFromDB", JsonConvert.SerializeObject(array));
                }
                else if (Menu_Flag == "editWaypoint")
                {
                    WaypointStatus = "Update";
                    DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                    string padID = DAL_CreateWayPoint.Get_WayID(view_Index, User_Id);
                    DataAccessLayer.ViewPaddock transObjmap = new DataAccessLayer.ViewPaddock();
                    DataAccessLayer.ViewWaypoint transObj = new DataAccessLayer.ViewWaypoint();
                    LstviewEdit = transObjmap.get_latlong_edit(padID, view_Index);
                    LstPaddock = LstviewEdit;
                    arrayEdit = LstviewEdit.ToArray();
                    this.webBrowser1.InvokeScript("EditWayFromDB", JsonConvert.SerializeObject(arrayEdit));
                    transObj = new DataAccessLayer.ViewWaypoint();
                    Lstview = transObj.get_latlong(view_Index);
                    array = Lstview.ToArray();
                    this.webBrowser1.InvokeScript("addMarker_array_paddock", JsonConvert.SerializeObject(array), "1");
                }
            }
            catch (AggregateException ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                return;
            }
        }

        private void Waypoint_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (LstPaddock != null)
                {
                    string WayPoint = this.webBrowser1.InvokeScript("createMapAreaObject").ToString();

                    if (WayPoint != "Null")
                    {
                        dynamic Waypoints = JsonConvert.DeserializeObject(WayPoint);

                        DateTime nowDT = DateTime.Now;

                        string now_DT = nowDT.ToString("yyyyMMddHHmmss");

                        string ImgFileName = "Waypoint_" + now_DT + ".png";

                        string FilePath = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImg"].ToString());

                        string CapImagePath = FilePath + ImgFileName;

                        foreach (var val in Waypoints)
                        {
                            string Name = val.name;
                            string CreatedBy = val.CreatedBy;
                            string[] points = val.points;
                        }

                        string ValWaypoints = JsonConvert.DeserializeObject(WayPoint).ToString();

                        try
                        {
                            CreateBitmapFromVisual(webBrowser1, CapImagePath);
                        }
                        catch (Exception ex)
                        {
                            MessageBox message = new MessageBox("Error: " + ex.Message);
                            message.Show();
                            _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                        }

                        this.webBrowser1.InvokeScript("initMap");
                        this.webBrowser1.InvokeScript("clearMarkers");

                        CWaypointWindow cWaypointWindow = new CWaypointWindow();
                        cWaypointWindow.ValueFromMapLoad(User_Id, ValWaypoints, CapImagePath, Paddock_Id, _missiontype);
                        cWaypointWindow.Show();
                    }
                    else
                    {
                        MessageBox message = new MessageBox("Please mark your Waypoint.");
                        message.Show();
                    }
                }
                else
                {
                    if (SelectPaddockWindow == null)
                        SelectPaddockWindow = new SelectPaddockWindow(User_Id);

                    SelectPaddockWindow.ValueFromMapLoad(User_Id);
                    SelectPaddockWindow.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
            }
        }

        public void MLAlist()
        {
            try
            {
                MLA_List mlist = new MLA_List(User_Id);
                mlist.Owner = this;
                mlist.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                return;
            }
        }

        public void RunAI()
        {
            try
            {
                AI_list AI = new AI_list(User_Id);
                AI.Owner = this;
                AI.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                return;
            }
        }
        public void Sync_to_cloud()
        {
            try
            {
                if (_progressBar == null)
                    _progressBar = new ProgressBar("Sync to cloud in progress...");
                _progressBar.Show();


                LastRowID = DAL_SyncToCloud_StatusUpdate.Insert_SyncCloudStatus(User_Id);

                string DBName = "dbmla_aerodyne_" + User_Id + ".zip";

                string DBNameZip = "dbmla_aerodyne_" + User_Id;

                string DBFileToCopy = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString());
                string NewFileLocation = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBSaveConfig"].ToString());
                string NewFolderLocation = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBSaveFolder"].ToString());

                FileInfo file = new FileInfo(NewFileLocation);

                if (System.IO.Directory.Exists(NewFolderLocation))
                {
                    if (System.IO.File.Exists(DBFileToCopy))
                    {
                        System.IO.File.Copy(DBFileToCopy, NewFileLocation, true);
                    }
                }
                else
                {
                    Directory.CreateDirectory(NewFolderLocation);

                    if (System.IO.File.Exists(DBFileToCopy))
                    {
                        System.IO.File.Copy(DBFileToCopy, NewFileLocation, true);
                    }
                }

                string sZipFile = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBZipFolder"].ToString()) + DBName + "";

                if (System.IO.File.Exists(sZipFile))
                {
                    System.IO.File.Delete(sZipFile);
                    ZipFile.CreateFromDirectory(NewFolderLocation, sZipFile);
                }
                else
                {
                    ZipFile.CreateFromDirectory(NewFolderLocation, sZipFile);
                }

                string URL = "http://www.google.com/";
                bool NetConStatus = IsInternetConnection(URL);
                if (NetConStatus == true)
                    ConnectionStatus(NetConStatus);
                else
                    ConnectionStatus(NetConStatus);

                var client = new HttpClient();

                if (Connection_Status == "true")
                {
                    string InsertapiUrl = ConfigurationManager.AppSettings["MLA_WebAPI"].ToString() + "/GetSQLiteDBFile/SaveSQLiteDBFile";

                    client.BaseAddress = new Uri(InsertapiUrl);

                    byte[] Filedata = File.ReadAllBytes(Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBZipFolder"].ToString()) + DBName + "");

                    GetData EI = new GetData
                    {
                        filedata = Filedata,
                        filename = DBName,
                        UserId = User_Id,
                        DBFileName = DBNameZip
                    };

                    var response = client.PostAsJsonAsync("SaveSQLiteDBFile", EI).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        DAL_SyncToCloud_StatusUpdate.Update_SyncCloudStatus(User_Id, LastRowID);
                        MessageBox message = new MessageBox("Data synced successfully.");
                        message.Show();
                        closebutton();
                    }
                }
                if (_progressBar == null)
                    _progressBar = new ProgressBar("");
                _progressBar.Hide();

            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                closebutton();
                return;
            }
        }
        public void SynciLAMS()
        {
            try
            {
                Sync_to_iLAMS iLAMS = new Sync_to_iLAMS(User_Id);
                iLAMS.Owner = this;
                iLAMS.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                return;
            }
        }

        public void Home()
        {
            try
            {
                DataAccessLayer.DAL_CreateMapArea DAL_hme = new DataAccessLayer.DAL_CreateMapArea();
                dshme = DAL_hme.GetMapAreaFromDBHome(User_Id);
                DataTable Mdt = dshme.Tables[0];
                for (int i = 0; i < Mdt.Rows.Count; i++)
                {
                    string _pID = dshme.Tables[0].Rows[i]["Paddock_Id"].ToString();
                    LstKML = DAL_hme.GetSelectedPolygonFromDBhme(_pID, User_Id);
                    string[] ArKML = LstKML.ToArray();
                    this.webBrowser1.InvokeScript("Plot_Home_ValFromDB", JsonConvert.SerializeObject(ArKML));
                    LstKML.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                return;
            }
        }
        public void IMarker_Max()
        {
            MessageBox message = new MessageBox("Maximum allowed marker is 99. So marker disabled." );
            message.Show();
        }
        public void Import_KML()
         {
            try
            {
                string mapType = "KMLmap";
                string WayPoint = GetWayPoint;

                if (WayPoint != "[]")
                {
                    DateTime nowDT = DateTime.Now;
                    string now_DT = nowDT.ToString("yyyyMMddHHmmss");
                    string ImgFileName = "MapArea_" + now_DT + ".png";
                    string FilePath = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImg"].ToString());
                    string CapImagePath = FilePath + ImgFileName;
                    dynamic WayValues = JsonConvert.DeserializeObject(WayPoint);

                    
                    string ValWaypoints = JsonConvert.DeserializeObject(WayPoint).ToString();

                    System.Threading.Thread.Sleep(1000);

                    try
                    {
                        CreateBitmapFromVisual(webBrowser1, CapImagePath);
                    }
                    catch (Exception ex)
                    {
                        MessageBox message2 = new MessageBox("Error: " + ex.Message);
                        message2.Show();
                        _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                    }

                    if (CreateMapAreaVal == null)
                    CreateMapAreaVal = new CMapAreaWindow(MapStatus, view_Index, mapType);
                    CreateMapAreaVal.ValueFromMapLoad(User_Id, ValWaypoints, CapImagePath);
                    CreateMapAreaVal.Owner = this;
                    CreateMapAreaVal.ShowDialog();
                    ValWaypoints = "";
                    view_Index = "";
                    this.webBrowser1.InvokeScript("ImportKML_text", "Reload");
                    LstMapArea = null;
                }
                else
                {
                    Import_KML importKML = new Import_KML(User_Id);
                    importKML.Owner = this;
                    importKML.ShowDialog();
                }
                LstMapArea = null;
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                return;
            }
        }

        public string GetKML_ID()
        {            
            return KML_ID;
        }


        public void Data_Transfer()
        {
            try
            {
                Data_Transfer datatrs = new Data_Transfer(User_Id);
                datatrs.Owner = this;
                datatrs.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                return;
            }
        }
        private void WebBrowser1_ScriptNotify(object sender, Microsoft.Toolkit.Win32.UI.Controls.Interop.WinRT.WebViewControlScriptNotifyEventArgs e)
        {
            try
            {
                string MissionName = null;
                string MapValues = e.Value;
                string MenuType = null;

                MapValues = MapValues.Trim();
                if ((MapValues.StartsWith("{") && MapValues.EndsWith("}")) || //For object
                    (MapValues.StartsWith("[") && MapValues.EndsWith("]"))) //For array
                {
                    try
                    {
                        dynamic Way_Points = JsonConvert.DeserializeObject(MapValues);
                        GetWayPoint = JsonConvert.DeserializeObject(MapValues);
                        MenuType = GetWayPoint.Name;
                        MissionName = GetWayPoint.MissionName;
                        _missiontype = GetWayPoint.missiontype;
                        GetWayPoint = GetWayPoint.WayPoints;

                        switch (MenuType)
                        {
                            case "maparea":
                                Maparea();
                                break;
                            case "Landmark":
                                LandMark();
                                break;
                            case "Paddock":
                                Paddock();
                                break;
                            case "Waypoint":
                                Waypoint();
                                break;
                            case "DefaultWaypoint":
                                DefaultWaypoint();
                                break;
                            case "MissionHub":
                                MissionHub(MissionName);
                                break;
                            case "MLAlist":
                                MLAlist();
                                break;
                            case "RunAI":
                                RunAI();
                                break;
                            case "SynciLAMS":
                                SynciLAMS();
                                break;
                            case "SyncCloud":
                                Sync_to_cloud();
                                break;
                            case "ImportKML":
                                Import_KML();
                                break;
                            case "Marker":
                                IMarker_Max();
                                break;
                            case "Home":
                                Home();
                                break;
                            case "ImportLandmarkKML":
                                Import_KML_Landmark();
                                break;
                            case "DataTransfer":
                                Data_Transfer();
                                break;
                            case "LogOut":
                                LogOut();
                                break;
                            case "Reload":
                                Reload();
                                break;
                           
                        }
                        
                    }
                    catch (JsonReaderException jex)
                    {
                        //Exception in parsing json
                        MessageBox messageBox = new MessageBox(MapValues);
                        messageBox.Show();
                        _error.Insert_Errorlog("An unhandled exception just occurred: " + jex.Message, "MapLoad", User_Id);
                    }
                    catch (AggregateException ex) //some other exception
                    {
                        MessageBox messageBox = new MessageBox("Error: " + ex.Message);
                        messageBox.Show();
                        _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
                    }
                }
                else
                {
                    MessageBox messageBox = new MessageBox(MapValues);
                    messageBox.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "MapLoad", User_Id);
            }
        }

        private void Import_KML_Landmark()
        {
            int _result=0;
            string WayPoint = GetWayPoint;
            dynamic WayValues = JsonConvert.DeserializeObject(WayPoint);
            if (_progressBar == null)
                _progressBar = new ProgressBar("Saving landmark in progress...");
            _progressBar.Show();
            foreach (var val in WayValues)
            {
               string Latitude = val.lat;
               string Longitude = val.lng;
               string LM_ID = val.name;
               DataAccessLayer.DAL_CreateLankmark DAL_CreateLankmark = new DataAccessLayer.DAL_CreateLankmark();
               _result = DAL_CreateLankmark.Import_Landmarktransact(LM_ID, Latitude, Longitude);
            }
            if(_result==1)
            {
                if (_progressBar == null)
                    _progressBar = new ProgressBar("");
                _progressBar.Hide();

                MessageBox message = new MessageBox("Landmark saved successfully into the mission hub.");
                message.Show();
                closebutton();
            }
            else
            {
                MessageBox message = new MessageBox("Landmark not saved into the mission hub.");
                message.Show();
                closebutton();
            }
        }

        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            if (loaderWindow == null)
                loaderWindow = new LoaderWindow();
            loaderWindow.Show();

         
            System.Windows.Application.Current.Shutdown();

            if (loaderWindow == null)
                loaderWindow = new LoaderWindow();
            loaderWindow.Hide();
        }

        public class AITransferData
        {
            public string _AttachmentName { get; set; }
            public string _Type { get; set; }
            public string _Size { get; set; }
            public string _MissionId { get; set; }
            public string _Mission_Name { get; set; }
            public string _Mission_Type { get; set; }
            public string _PaddockId { get; set; }
            public string _PaddockName { get; set; }
            public string _UserId { get; set; }
            public string _imageCapturedDate { get; set; }
            public HttpContent _imagefile { get; set; }
            // public Stream _imagefile { get; set; }
            // public List<Filecheck> files { get; set; }
        }

        public class GetData
        {
            public byte[] filedata { get; set; }
            public string filename { get; set; }
            public string UserId { get; set; }
            public string DBFileName { get; set; }
        }

        public static class FormUpload 
        {
            private static readonly Encoding encoding = Encoding.UTF8;
            public static HttpWebResponse MultipartFormDataPost(string postUrl, string userAgent, Dictionary<string, object> postParameters)
            {
                string formDataBoundary = String.Format("----------{0:N}", Guid.NewGuid());
                string contentType = "multipart/form-data; boundary=" + formDataBoundary;

                byte[] formData = GetMultipartFormData(postParameters, formDataBoundary);

                return PostForm(postUrl, userAgent, contentType, formData);
            }
            private static HttpWebResponse PostForm(string postUrl, string userAgent, string contentType, byte[] formData)
            {
                HttpWebRequest request = WebRequest.Create(postUrl) as HttpWebRequest;

                if (request == null)
                {
                    throw new NullReferenceException("request is not a http request");
                }

                // Set up the request properties.
                request.Method = "POST";
                request.ContentType = contentType;
                request.UserAgent = userAgent;
                request.CookieContainer = new CookieContainer();
                request.ContentLength = formData.Length;

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(formData, 0, formData.Length);
                    requestStream.Close();
                }

                return request.GetResponse() as HttpWebResponse;
            }

            private static byte[] GetMultipartFormData(Dictionary<string, object> postParameters, string boundary)
            {
                Stream formDataStream = new System.IO.MemoryStream();
                bool needsCLRF = false;
                try
                {
                   
                    foreach (var param in postParameters)
                    {
                        // Thanks to feedback from commenters, add a CRLF to allow multiple parameters to be added.
                        // Skip it on the first parameter, add it to subsequent parameters.
                        if (needsCLRF)
                            formDataStream.Write(encoding.GetBytes("\r\n"), 0, encoding.GetByteCount("\r\n"));

                        needsCLRF = true;

                        if (param.Value is FileParameter)
                        {
                            FileParameter fileToUpload = (FileParameter)param.Value;
                            MemoryStream memory = new MemoryStream(fileToUpload.imagefile);
                            BinaryReader binReader = new BinaryReader(memory);
                            // Add just the first part of this param, since we will write the file data directly to the Stream
                            string header = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; Size=\"{4}\"; MissionId=\"{5}\"; MissionType=\"{6}\"; PaddockId=\"{7}\"; PaddockName=\"{8}\"; UserId=\"{9}\"; imageCapturedDate=\"{10}\"; filename=\"{3}\"\r\nContent-Type: {11}\r\n\r\n",
                                boundary,
                                param.Key,
                                binReader.ToString() ?? param.Key,
                                fileToUpload.AttachmentName ?? param.Key,
                                 fileToUpload.Size ?? param.Key,
                                  fileToUpload.MissionId ?? param.Key,
                                  fileToUpload.MissionType ?? param.Key,
                                   fileToUpload.PaddockId ?? param.Key,
                                    fileToUpload.PaddockName ?? param.Key,
                                     fileToUpload.UserId ?? param.Key,
                                      fileToUpload.imageCapturedDate ?? param.Key,
                                       fileToUpload.Type ?? param.Key);

                            formDataStream.Write(encoding.GetBytes(header), 0, encoding.GetByteCount(header));


                            // Write the file data directly to the Stream, rather than serializing it to a string.
                            formDataStream.Write(fileToUpload.imagefile, 0, fileToUpload.imagefile.Length);
                        }
                        else
                        {
                            string postData = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}",
                                boundary,
                                param.Key,
                                param.Value);
                            formDataStream.Write(encoding.GetBytes(postData), 0, encoding.GetByteCount(postData));
                        }
                    }

                    // Add the end of the request.  Start with a newline
                    string footer = "\r\n--" + boundary + "--\r\n";
                    formDataStream.Write(encoding.GetBytes(footer), 0, encoding.GetByteCount(footer));

                    // Dump the Stream into a byte[]
                    formDataStream.Position = 0;
                    byte[] formData = new byte[formDataStream.Length];
                    formDataStream.Read(formData, 0, formData.Length);
                    formDataStream.Close();

                    return formData;
                }
                catch (System.OutOfMemoryException)
                {
                    System.Runtime.GCSettings.LargeObjectHeapCompactionMode = System.Runtime.GCLargeObjectHeapCompactionMode.CompactOnce;
                    GC.Collect();
                    return null;
                }
                finally
                {
                    System.Runtime.GCSettings.LargeObjectHeapCompactionMode = System.Runtime.GCLargeObjectHeapCompactionMode.CompactOnce;
                    GC.Collect();
                }
            }

            public class FileParameter
            {
                public byte[] imagefile { get; set; }
                public string AttachmentName { get; set; }
                public string Type { get; set; }
                public string Size { get; set; }
                public string MissionId { get; set; }
                public string MissionType { get; set; }
                public string PaddockId { get; set; }
                public string PaddockName { get; set; }
                public string UserId { get; set; }
                public string imageCapturedDate { get; set; }
                public FileParameter(byte[] _imagefile) : this(_imagefile, null) { }
                public FileParameter(byte[] _imagefile, string _AttachmentName) : this(_imagefile, _AttachmentName, null, null, null, null, null, null, null, null) { }
                public FileParameter(byte[] _imagefile, string _AttachmentName, string _Type, string _Size, string _MissionId, string _Mission_Type, string _PaddockId, string _PaddockName, string _UserId, string _imageCapturedDate)
                {
                    imagefile = _imagefile;
                    AttachmentName = _AttachmentName;
                    Type = _Type;
                    Size = _Size;
                    MissionId = _MissionId;
                    MissionType = _Mission_Type;
                    PaddockId = _PaddockId;
                    PaddockName = _PaddockName;
                    UserId = _UserId;
                    imageCapturedDate = _imageCapturedDate;
                }
            }
        }



        protected virtual bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }

        public static bool IsInternetConnection(String URL)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Timeout = 5000;
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        public void ConnectionStatus(bool NetConStatus)
        {
            if (NetConStatus == true)
            {
                Connection_Status = "true";
                //MessageBox.Show("This computer is connected to the internet");
            }
            else
            {
                Connection_Status = "false";
                //MessageBox.Show("This computer is not connected to the internet. Please Connect Internet!!");
            }
        }
        private void minimizeTopBtn_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        //private async void WebView_NavigationCompleted(WebView sender, Microsoft.Toolkit.Win32.UI.Controls.Interop.WinRT.WebViewControlNavigationCompletedEventArgs args)
        //{
        //    string result = await sender.InvokeScriptAsync("eval", new string[] { "window.alert = function(AlertMessage) {window.external.notify(AlertMessage)}" });
        //}
    }
}