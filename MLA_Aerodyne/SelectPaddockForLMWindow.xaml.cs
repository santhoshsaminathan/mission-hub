﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for SelectPaddockForLMWindow.xaml
    /// </summary>
    public partial class SelectPaddockForLMWindow : Window
    {
        public string User_Id;
        public string SelectMapArea;
        public string SelectPaddock;
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        public SelectPaddockForLMWindow()
        {
            InitializeComponent();
        }

        public void GetpaddockFromDatabase()
        {
            try
            {
                DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                DataSet ds_Combo = DAL_CreateWayPoint.GetPaddockFromDB(User_Id);

                DataAccessLayer.DAL_CreatePaddock DAL_CreatePaddock = new DataAccessLayer.DAL_CreatePaddock();
                DataSet ds_Combo2 = DAL_CreatePaddock.GetMapAreaFromDB(User_Id);

                if (ds_Combo.Tables[0].Rows.Count != 0)
                {
                    PaddockComboBox.DataContext = ds_Combo.Tables[0].DefaultView;
                    PaddockComboBox.DisplayMemberPath = "Paddock_Name";
                    PaddockComboBox.SelectedValuePath = "Paddock_Id";

                    PaddockComboBox.SelectedIndex = 0;
                }
                else
                {
                    MessageBox message = new MessageBox("Please create paddock.");
                    message.Show();
                    this.Visibility = Visibility.Hidden;
                }

                if (ds_Combo2.Tables[0].Rows.Count != 0)
                {
                    MapAreaComboBox.DataContext = ds_Combo2.Tables[0].DefaultView;
                    MapAreaComboBox.DisplayMemberPath = "Plot_Name";
                    MapAreaComboBox.SelectedValuePath = "Plot_ID";

                    MapAreaComboBox.SelectedIndex = 0;
                }
                else
                {
                    MessageBox message = new MessageBox("Please create map area.");
                    message.Show();
                    this.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectPaddockForLMWindow", User_Id);
                return;
            }
        }

        public void ValueFromMapLoad(string UserId)
        {
            try
            {
                User_Id = UserId;

                GetpaddockFromDatabase();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectPaddockForLMWindow", User_Id);
                return;
            }
        }

        private void MapAreaComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SelectMapArea = MapAreaComboBox.SelectedValue.ToString();
                DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                DataSet ds_Combo = DAL_CreateWayPoint.GetSelectedPaddockFromDB(User_Id, SelectMapArea);

                if (ds_Combo.Tables[0].Rows.Count != 0)
                {
                    PaddockComboBox.DataContext = ds_Combo.Tables[0].DefaultView;
                    PaddockComboBox.DisplayMemberPath = "Paddock_Name";
                    PaddockComboBox.SelectedValuePath = "Paddock_Id";
                    PaddockComboBox.SelectedIndex = 0;
                }
                else
                {
                    MessageBox message = new MessageBox("Please create paddock.");
                    message.Show();
                    this.Visibility = Visibility.Hidden;
                }

            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectPaddockForLMWindow", User_Id);
                return;
            }
        }

        private void SelectPaddockBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CloseAllWindows();

                string MenuFlag = "SelPaddockForLM";

                MapLoad MapLoad = new MapLoad(User_Id, SelectPaddock, MenuFlag);
                MapLoad.Show();

                //TestWindow testWindow = new TestWindow(User_Id, SelectMapArea, MenuFlag);
                //testWindow.Show();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectPaddockForLMWindow", User_Id);
                return;
            }
        }

        private void CloseAllWindows()
        {
            for (int intCounter = App.Current.Windows.Count - 1; intCounter > 0; intCounter--)
                App.Current.Windows[intCounter].Close();
        }

        private void PaddockComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SelectPaddock = PaddockComboBox.SelectedValue.ToString();

                DataAccessLayer.DAL_CreateWayPoint DAL_CreateWayPoint = new DataAccessLayer.DAL_CreateWayPoint();
                string ImagePath = DAL_CreateWayPoint.GetSelectPaddockImageFromDB(User_Id, SelectPaddock);

                if (!String.IsNullOrEmpty(ImagePath))
                {
                    PaddockImage.Source = new BitmapImage(new Uri(ImagePath));
                }
                else
                {
                    PaddockImage.Source = new BitmapImage(new Uri(System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CaptureImgNull"].ToString())));
                }

                //MessageBox.Show(ImagePath);
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectPaddockForLMWindow", User_Id);
                return;
            }
        }

        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).closebutton();
            }
            
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "SelectPaddockForLMWindow", User_Id);
                message.Show();

            }
        }
    }
}
