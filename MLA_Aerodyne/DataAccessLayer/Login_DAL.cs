﻿using MLA_Aerodyne.BuisnessAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MLA_Aerodyne.DataAccessLayer
{
   public class Login_DAL
    {
     

        public MLA_Aerodyne.BuisnessAccessLayer.UserDetails Check_Username_Password(string username, string password)
        {
            try
            {
                StringBuilder SB = new StringBuilder();
                SB.Append("Log Started");

                MLA_Aerodyne.BuisnessAccessLayer.UserDetails Userdata = new MLA_Aerodyne.BuisnessAccessLayer.UserDetails();

                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source =" + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()) ))

                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    SB.Append("Connection");

                    SB.AppendFormat(Environment.NewLine);

                    SB.Append("Connection Open Before");
                    SB.AppendFormat(Environment.NewLine);
                    con.Open();
                    SB.Append("Connection Open After");
                    SB.AppendFormat(Environment.NewLine);

                    SB.Append("Connection Ok" + con.ConnectionString);
                    SB.AppendFormat(Environment.NewLine);

                    command.CommandText = "SELECT User_Id,User_Name,Password FROM User_Master where User_Name=@Username and Password=@Password";
                    command.Parameters.Add(new SQLiteParameter("@Username") { Value = username });
                    command.Parameters.Add(new SQLiteParameter("@Password") { Value = password });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader reader;
                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        SB.Append("Reader Line Executed");
                        SB.AppendFormat(Environment.NewLine);
                        Userdata.Lusername = reader["User_Name"].ToString();
                        Userdata.Lpassword = reader["Password"].ToString();
                        Userdata.UserID = reader["User_Id"].ToString();
                        System.Windows.Application.Current.Properties["User_Id"] = Userdata.UserID.ToString();
																			 
                        SB.Append("Reader Line Executed");
                        SB.AppendFormat(Environment.NewLine);
                    }
                    File.WriteAllText(Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["ErrorLog"].ToString()), SB.ToString());
                    reader.Close();
                    con.Close();
                }
                return Userdata;
            }
            catch (Exception ex) {
                MLA_Aerodyne.BuisnessAccessLayer.UserDetails Userdata = new MLA_Aerodyne.BuisnessAccessLayer.UserDetails();
                Userdata.ErrorLog = ex.ToString();
                DataAccessLayer.Login_DAL _error1 = new DataAccessLayer.Login_DAL();
                File.WriteAllText(Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["ErrorLog"].ToString()), ex.ToString());
                _error1.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Login_DAL", Userdata.UserID);
                return Userdata;
            }
        }

        public string _get_username(string _userID)
        {
            try
            {
                string _username = "";

                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source =" + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "SELECT User_Id,User_Name,Password FROM User_Master where User_Id=@User_Id";
                    command.Parameters.Add(new SQLiteParameter("@User_Id") { Value = _userID });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader reader;
                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        _username = reader["User_Name"].ToString();
                         System.Windows.Application.Current.Properties["User_Id"] = _username;
                    }
                  
                    reader.Close();
                    con.Close();
                }
                return _username;
            }
            catch (Exception ex)
            {
                DataAccessLayer.Login_DAL _error1 = new DataAccessLayer.Login_DAL();
                _error1.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Login_DAL", "");
                return ex.ToString();
            }
        }

        public string Check_MACAddress(string UserId, string MacAddress)
        {
            string MAC_Address = null;
            try
            {
                MLA_Aerodyne.BuisnessAccessLayer.UserDetails Userdata = new MLA_Aerodyne.BuisnessAccessLayer.UserDetails();

                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source =" + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "SELECT MAC_Address FROM User_Master where User_Id=@UserId";
                    //command.CommandText = "SELECT MAC_Address FROM User_Master where User_Name=@Username and MAC_Address=@MacAddress and Status=1";
                    command.Parameters.Add(new SQLiteParameter("@UserId") { Value = UserId });
                    //command.Parameters.Add(new SQLiteParameter("@MacAddress") { Value = MacAddress });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader reader;
                    reader = command.ExecuteReader();
                    
                    if (reader.Read())
                    {
                        MAC_Address = reader["MAC_Address"].ToString();
                    }
                    reader.Close();
                    con.Close();
                }

                string Status = null;


                if (String.IsNullOrEmpty(MAC_Address))
                {
                    Status = "false";
                }
                else if(MAC_Address == MacAddress)
                {
                    Status = "true";
                }
                else if (MAC_Address != MacAddress)
                {
                    Status = "Mismatch";
                }

                return Status;
            }
            catch(Exception ex)
            {
                DataAccessLayer.Login_DAL _error1 = new DataAccessLayer.Login_DAL();
                _error1.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Login_DAL", UserId);
                return "false";
            }
        }

        public string Insert_User_MacAddress(string UserId, string MACAddress)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "UPDATE User_Master SET MAC_Address=@MACAddress, Status=1 WHERE User_Id=@UserId";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@UserId",Convert.ToInt32( UserId)));
                    command.Parameters.Add(new SQLiteParameter("@MACAddress", MACAddress));
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                }
                return "true";
            }
            catch(Exception ex)
            {
                DataAccessLayer.Login_DAL _error1 = new DataAccessLayer.Login_DAL();
                _error1.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Login_DAL", UserId);
                return "false";
            }
        }

        public void Insert_Username_Password(string regUsername, string regPassword)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "INSERT INTO User_Master (User_Name,Password,CreatedOn) VALUES (@rUsername,@rPassword,@CreatedOn)";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@rUsername", regUsername));
                command.Parameters.Add(new SQLiteParameter("@rPassword", regPassword));
                command.Parameters.Add(new SQLiteParameter("@CreatedOn", Convert.ToString(DateTime.Now)));
                command.CommandType = System.Data.CommandType.Text;
                command.ExecuteNonQuery();
                con.Close();
            }
        }

        public void Insert_Errorlog(string _details, string _createdBy,string _userID)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "INSERT INTO Errorlog_Master (Details,CreatedBy,CreatedTime,User_Id) VALUES (@Details,@CreatedBy,@CreatedTime,@User_Id)";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@Details", _details));
                command.Parameters.Add(new SQLiteParameter("@CreatedBy", _createdBy));
                command.Parameters.Add(new SQLiteParameter("@User_Id", _userID));
                command.Parameters.Add(new SQLiteParameter("@CreatedTime", Convert.ToString(DateTime.Now)));
                command.CommandType = System.Data.CommandType.Text;
                command.ExecuteNonQuery();
                con.Close();
            }
        }

        public string Insert_UserDataFromiLams(GetUserData getUserData)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO User_Master(User_Id, User_Name, Password, FirstName, LastName, LandMark, Area, City, Country, EmailId, CountryCode, " +
                                "LandLineNo, MobileNo, User_type, Status, CreatedOn, UpdatedOn, Latitude, Longitude, Zipcode, Address, Phonenumber, Licence_Code, MAC_Address) VALUES " +
                                "(@User_Id, @User_Name, @Password, @FirstName, @LastName, @LandMark, @Area, @City, @Country, @EmailId, @CountryCode, @LandLineNo, @MobileNo, " +
                                "@User_type, @Status, @CreatedOn, @UpdatedOn, @Latitude, @Longitude, @Zipcode, @Address, @Phonenumber, @Licence_Code, @MAC_Address)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@User_Id", getUserData.User_Id));
                    command.Parameters.Add(new SQLiteParameter("@User_Name", getUserData.User_Name));
                    command.Parameters.Add(new SQLiteParameter("@Password", getUserData.Password));
                    command.Parameters.Add(new SQLiteParameter("@FirstName", getUserData.FirstName));
                    command.Parameters.Add(new SQLiteParameter("@LastName", getUserData.LastName));
                    command.Parameters.Add(new SQLiteParameter("@LandMark", getUserData.LandMark));
                    command.Parameters.Add(new SQLiteParameter("@Area", getUserData.Area));
                    command.Parameters.Add(new SQLiteParameter("@City", getUserData.City));
                    command.Parameters.Add(new SQLiteParameter("@Country", getUserData.Country));
                    command.Parameters.Add(new SQLiteParameter("@EmailId", getUserData.EmailId));
                    command.Parameters.Add(new SQLiteParameter("@CountryCode", getUserData.CountryCode));
                    command.Parameters.Add(new SQLiteParameter("@LandLineNo", getUserData.LandLineNo));
                    command.Parameters.Add(new SQLiteParameter("@MobileNo", getUserData.MobileNo));
                    command.Parameters.Add(new SQLiteParameter("@User_type", getUserData.User_type));
                    command.Parameters.Add(new SQLiteParameter("@Status", getUserData.Status));
                    command.Parameters.Add(new SQLiteParameter("@CreatedOn", getUserData.CreatedOn));
                    command.Parameters.Add(new SQLiteParameter("@UpdatedOn", getUserData.UpdatedOn));
                    command.Parameters.Add(new SQLiteParameter("@Latitude", getUserData.Latitude));
                    command.Parameters.Add(new SQLiteParameter("@Longitude", getUserData.Longitude));
                    command.Parameters.Add(new SQLiteParameter("@Zipcode", getUserData.Zipcode));
                    command.Parameters.Add(new SQLiteParameter("@Address", getUserData.Address));
                    command.Parameters.Add(new SQLiteParameter("@Phonenumber", getUserData.Phonenumber));
                    command.Parameters.Add(new SQLiteParameter("@Licence_Code", getUserData.Licence_Code));
                    command.Parameters.Add(new SQLiteParameter("@MAC_Address", getUserData.MAC_Address));
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                    con.Close();
                    return "Inserted";
                }
            }
            catch(Exception ex)
            {
                DataAccessLayer.Login_DAL _error1 = new DataAccessLayer.Login_DAL();
                _error1.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "Login_DAL", getUserData.User_Id.ToString());
                return ex.Message;
            }
        }

        public string ClearUserMasterTable()
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                {
                    con.Open();
                    using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                    {
                        command.CommandText = "DELETE from User_Master";
                        command.Prepare();
                        command.CommandType = System.Data.CommandType.Text;
                        command.ExecuteNonQuery();
                        con.Close();
                    }
                }

                using (System.Data.SQLite.SQLiteConnection con1 = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                {
                    con1.Open();
                    using (System.Data.SQLite.SQLiteCommand command1 = con1.CreateCommand())
                    {
                        command1.CommandText = "UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='User_Master'";
                        command1.Prepare();
                        command1.CommandType = System.Data.CommandType.Text;
                        command1.ExecuteNonQuery();
                        con1.Close();
                    }
                }
                return "Cleared";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
