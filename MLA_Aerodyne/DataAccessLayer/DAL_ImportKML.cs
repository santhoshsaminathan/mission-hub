﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.DataAccessLayer
{
    public class DAL_ImportKML
    {
        DataSet ds = new DataSet();
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        public int Insert_KMLMaster(string _docName, string _description, string User_ID)
        {
            try
            {
                int _rowsAffected = 0;
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO KML_Master (Name, Description, createddate,User_ID) VALUES (@Name,@Description,@createddate,@User_ID)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@Name", _docName));
                    command.Parameters.Add(new SQLiteParameter("@Description", _description));
                    command.Parameters.Add(new SQLiteParameter("@createddate", DateTime.Now));
                    command.Parameters.Add(new SQLiteParameter("@User_ID", User_ID));
                    command.CommandType = System.Data.CommandType.Text;
                    _rowsAffected = command.ExecuteNonQuery();

                    command.CommandText = "select last_insert_rowid()";
                    Int64 LastRowID64 = (Int64)command.ExecuteScalar();
                    int LastRowID = (int)LastRowID64;
                    con.Close();
                    return LastRowID;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ImportKML", User_ID);
                return 0;
            }
        }

        public int Insert_KMLStyleMaster(int _KML_ID, string _description)
        {
            try
            {
                int _rowsAffected = 0;
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO KML_Style(Description, KML_ID, CreatedDate) VALUES (@Description, @KML_ID, @CreatedDate)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@KML_ID", _KML_ID));
                    command.Parameters.Add(new SQLiteParameter("@Description", _description));
                    command.Parameters.Add(new SQLiteParameter("@CreatedDate", DateTime.Now));
                    command.CommandType = System.Data.CommandType.Text;
                    _rowsAffected = command.ExecuteNonQuery();
                    command.CommandText = "select last_insert_rowid()";
                    Int64 LastRowID64 = (Int64)command.ExecuteScalar();
                    int LastRowID = (int)LastRowID64;
                    con.Close();
                    return LastRowID;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ImportKML", "");
                return 0;
            }
        }

        public int Insert_KML_lineStyle(int _Style_ID, string _color, string _width)
        {
            try
            {
                int _rowsAffected = 0;
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO Line_Style(color, width, Style_ID, CreatedDate) VALUES (@color, @width, @Style_ID, @CreatedDate)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@Style_ID", _Style_ID));
                    command.Parameters.Add(new SQLiteParameter("@color", _color));
                    command.Parameters.Add(new SQLiteParameter("@width", _width));
                    command.Parameters.Add(new SQLiteParameter("@CreatedDate", DateTime.Now));
                    command.CommandType = System.Data.CommandType.Text;
                    _rowsAffected = command.ExecuteNonQuery();
                    con.Close();
                    return _rowsAffected;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ImportKML", "");
                return 0;
            }
        }

        public int Insert_KML_polyStyle(int _Style_ID, string _color, string _fill, string _outline)
        {
            try
            {
                int _rowsAffected = 0;
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO Poly_Style(color, fill, outline, Style_ID, CreatedDate) VALUES(@color, @fill, @outline, @Style_ID, @CreatedDate)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@Style_ID", _Style_ID));
                    command.Parameters.Add(new SQLiteParameter("@color", _color));
                    command.Parameters.Add(new SQLiteParameter("@fill", _fill));
                    command.Parameters.Add(new SQLiteParameter("@outline", _outline));
                    command.Parameters.Add(new SQLiteParameter("@CreatedDate", DateTime.Now));
                    command.CommandType = System.Data.CommandType.Text;
                    _rowsAffected = command.ExecuteNonQuery();
                    con.Close();
                    return _rowsAffected;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return 0;
            }
        }

        public int Insert_KML_Placemark(int _KML_ID, string _description, string _name, string _style_ID)
        {
            try
            {
                int _rowsAffected = 0;
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO KML_Placemark(Description, Name, Style_ID, CreatedDate,KML_ID) VALUES (@Description, @Name, @Style_ID, @CreatedDate,@KML_ID)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@KML_ID", _KML_ID));
                    command.Parameters.Add(new SQLiteParameter("@Description", _description));
                    command.Parameters.Add(new SQLiteParameter("@Name", _name));
                    command.Parameters.Add(new SQLiteParameter("@Style_ID", _style_ID));
                    command.Parameters.Add(new SQLiteParameter("@CreatedDate", DateTime.Now));
                    command.CommandType = System.Data.CommandType.Text;
                    _rowsAffected = command.ExecuteNonQuery();
                    command.CommandText = "select last_insert_rowid()";
                    Int64 LastRowID64 = (Int64)command.ExecuteScalar();
                    int LastRowID = (int)LastRowID64;
                    con.Close();
                    return LastRowID;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ImportKML", "");
                return 0;
            }
        }

        public int Insert_KML_OuterPolygon(int _KML_pID, string _lat_Outer, string _lng_Outer, string _Alt_Outer,string Area)
        {
            try
            {
                int _rowsAffected = 0;
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO Polygon_outerBoundaryIs(Latitude, Longitude, Altitude, Placemark_ID, CreatedDate) VALUES (@Latitude, @Longitude, @Altitude, @Placemark_ID, @CreatedDate)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@Latitude", _lat_Outer));
                    command.Parameters.Add(new SQLiteParameter("@Longitude", _lng_Outer));
                    command.Parameters.Add(new SQLiteParameter("@Altitude", _Alt_Outer));
                    command.Parameters.Add(new SQLiteParameter("@Placemark_ID", _KML_pID));
                    command.Parameters.Add(new SQLiteParameter("@CreatedDate", DateTime.Now));
                    command.CommandType = System.Data.CommandType.Text;
                    _rowsAffected = command.ExecuteNonQuery();
                    con.Close();
                    con.Open();
                    command.CommandText = "Update KML_Placemark SET Area=@Area where Placemark_ID=@_KML_pID";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@_KML_pID", _KML_pID));
                    command.Parameters.Add(new SQLiteParameter("@Area", Area));
                    command.CommandType = System.Data.CommandType.Text;
                    _rowsAffected = command.ExecuteNonQuery();
                    con.Close();
                    return _rowsAffected;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ImportKML", "");
                return 0;
            }
        }

        public int Insert_KML_InnerPolygon(int _KML_pID, int Inner_ListID, string _lat_Outer, string _lng_Outer, string _Alt_Outer)
        {
            try
            {
                int _rowsAffected = 0;
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO Polygon_innerBoundaryIs(Latitude, Longitude, Altitude, Placemark_ID, Inner_ListID, CreatedDate) VALUES (@Latitude, @Longitude, @Altitude, @Placemark_ID, @Inner_ListID, @CreatedDate)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@Latitude", _lat_Outer));
                    command.Parameters.Add(new SQLiteParameter("@Longitude", _lng_Outer));
                    command.Parameters.Add(new SQLiteParameter("@Altitude", _Alt_Outer));
                    command.Parameters.Add(new SQLiteParameter("@Placemark_ID", _KML_pID));
                    command.Parameters.Add(new SQLiteParameter("@Inner_ListID", Inner_ListID));
                    command.Parameters.Add(new SQLiteParameter("@CreatedDate", DateTime.Now));
                    command.CommandType = System.Data.CommandType.Text;
                    _rowsAffected = command.ExecuteNonQuery();
                    con.Close();
                    return _rowsAffected;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ImportKML", "");
                return 0;
            }
        }

        public int Insert_Land_temp(int _LM_ID)
        {
            try
            {
                int _rowsAffected = 0;
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO tbl_Landmark_temp(LM_ID) VALUES(@LM_ID)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@LM_ID", _LM_ID));
                  
                    command.CommandType = System.Data.CommandType.Text;
                    _rowsAffected = command.ExecuteNonQuery();
                    con.Close();
                    return _rowsAffected;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                 _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ImportKML", "");
                return 0;
            }
        }

        public List<string> GetSelectedPolygonFromDB(string _PiD,string _kML_id,string User_ID)
        {
            try
            {
                List<string> LstKML = new List<string>();
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    DataTable dt = new DataTable();
                    con.Open();
                    command.CommandText = "	SELECT PO.Latitude,PO.Longitude,PO.Placemark_ID,PM.name,KM.User_Id FROM Polygon_outerBoundaryIs AS PO " + 
                                            " INNER JOIN KML_Placemark AS PM ON PM.Placemark_ID = PO.Placemark_ID " +
                                            " INNER JOIN KML_Master AS KM ON KM.KML_ID = PM.KML_ID " +
                                            " WHERE PM.KML_ID = @KML_ID  AND PO.Placemark_ID = @Placemark_ID AND KM.User_Id=@User_Id";
                    command.Parameters.Add(new SQLiteParameter("@KML_ID") { Value = _kML_id });
                    command.Parameters.Add(new SQLiteParameter("@Placemark_ID") { Value = _PiD });
                    command.Parameters.Add(new SQLiteParameter("@User_Id") { Value = User_ID });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader dbr1;
                    dbr1 = command.ExecuteReader();
                    while (dbr1.Read())
                    {
                        string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() + "," + dbr1[2].ToString() + "," + dbr1[3].ToString() ;
                        //string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() ;
                        LstKML.Add(ValPoints);
                    }
                }
                return LstKML;
            }


            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ImportKML", User_ID);
                return null;
            }
        }


     
        public List<string> GetSelectedLandmarkFromDB(string _PiD,string _landID, string User_ID)
        {
            try
            {
                List<string> LstKML = new List<string>();
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    DataTable dt = new DataTable();
                    con.Open();
                    command.CommandText = "	SELECT LT.LMtransact_Latitude,LT.LMtransact_Longitude,LM.Landmark_Name,LM.User_Id,LM.Plot_ID FROM Landmark_Master AS LM "+ 
                                           " LEFT JOIN Landmark_Transact AS LT ON LM.LM_ID = LT.LM_ID " +
                                           " WHERE LM.Plot_ID = @Plot_ID AND LM.User_Id = @User_Id AND LM.LM_ID=@LM_ID AND LM.IsActive=1";
                    command.Parameters.Add(new SQLiteParameter("@Plot_ID") { Value = _PiD });
                    command.Parameters.Add(new SQLiteParameter("@LM_ID") { Value = _landID });
                    command.Parameters.Add(new SQLiteParameter("@User_Id") { Value = User_ID });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader dbr1;
                    dbr1 = command.ExecuteReader();
                    while (dbr1.Read())
                    {
                        string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() + "," + dbr1[2].ToString() + "," + dbr1[3].ToString();
                        //string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() ;
                        LstKML.Add(ValPoints);
                    }
                }
                return LstKML;
            }


            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ImportKML", User_ID);
                return null;
            }
        }


        public List<string> GetAllPolygonFromDB( string _kML_id, string User_ID)
        {
            try
            {
                List<string> LstKML = new List<string>();
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    DataTable dt = new DataTable();
                    con.Open();
                    command.CommandText = "	SELECT PO.Latitude,PO.Longitude,PO.Placemark_ID,PM.name,KM.User_Id FROM Polygon_outerBoundaryIs AS PO " +
                                            " INNER JOIN KML_Placemark AS PM ON PM.Placemark_ID = PO.Placemark_ID " +
                                            " INNER JOIN KML_Master AS KM ON KM.KML_ID = PM.KML_ID " +
                                            " WHERE PM.KML_ID = @KML_ID  AND KM.User_Id=@User_Id";
                    command.Parameters.Add(new SQLiteParameter("@KML_ID") { Value = _kML_id });
                    command.Parameters.Add(new SQLiteParameter("@User_Id") { Value = User_ID });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader dbr1;
                    dbr1 = command.ExecuteReader();
                    while (dbr1.Read())
                    {
                        string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() + "," + dbr1[2].ToString() + "," + dbr1[3].ToString();
                        //string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() ;
                        LstKML.Add(ValPoints);
                    }
                }
                return LstKML;
            }


            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ImportKML", User_ID);
                return null;
            }
        }



        public void Clear_Land_temp()
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));

                con.Open();
                SQLiteCommand command = new SQLiteCommand("DELETE from tbl_Landmark_temp", con);
                command.ExecuteNonQuery();
                con.Close();

                con.Open();
                SQLiteCommand command1 = new SQLiteCommand("UPDATE SQLITE_SEQUENCE SET SEQ = 0 WHERE NAME = 'tbl_Landmark_temp'", con);
                command1.ExecuteNonQuery();
                con.Close();
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ImportKML", "");
            }
        }

        public DataSet GetLM_temp_details()
        {
            try
            {

                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source =" + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))

                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {

                    command.CommandText = "SELECT * FROM tbl_Landmark_temp";                 
                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();

                }
                return ds;
            }
            catch (Exception ex)
            {
                return ds;
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ImportKML", "");
            }

        }
        public DataSet GetPlacemarkdetails(string KML_ID,string User_Id)
        {
            try
            {

                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source =" + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))

                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {

                    command.CommandText = "SELECT Placemark_ID FROM KML_Placemark AS KP "+
                                          " INNER JOIN KML_Master AS KM ON KM.KML_ID = KP.KML_ID "+
                                          " WHERE KP.KML_ID = @KML_ID AND KM.User_Id = @User_Id  ";
                    command.Parameters.Add(new SQLiteParameter("@KML_ID") { Value = KML_ID });
                    command.Parameters.Add(new SQLiteParameter("@User_Id") { Value = User_Id });
                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();

                }
                return ds;
            }
            catch (Exception ex)
            {
                return ds;
            }

        }

        public DataSet GetLandmarkdetails(string Plot_ID, string User_Id)
        {
            try
            {

                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source =" + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))

                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {

                    command.CommandText = "SELECT LM_ID FROM Landmark_Master WHERE Plot_ID=@Plot_ID AND User_Id=@User_Id";
                    command.Parameters.Add(new SQLiteParameter("@Plot_ID") { Value = Plot_ID });
                    command.Parameters.Add(new SQLiteParameter("@User_Id") { Value = User_Id });
                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();

                }
                return ds;
            }
            catch (Exception ex)
            {
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ImportKML", "");
                return ds;
            }

        }
    }
}
