﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;

namespace MLA_Aerodyne.DataAccessLayer
{
    class DAL_SyncToCloud_StatusUpdate
    {

        public int Insert_SyncCloudStatus(string User_Id)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "INSERT INTO SyncToCloud_StatusUpdate (UserId, Sync_StartDateTime) VALUES (@pUserId, @pSync_StartDateTime)";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@pUserId", User_Id));
                command.Parameters.Add(new SQLiteParameter("@pSync_StartDateTime", Convert.ToString(DateTime.Now)));
                command.CommandType = System.Data.CommandType.Text;
                int Status = command.ExecuteNonQuery();
                

                command.CommandText = "select last_insert_rowid()";

                // The row ID is a 64-bit value - cast the Command result to an Int64.
                //
                Int64 LastRowID64 = (Int64)command.ExecuteScalar();

                // Then grab the bottom 32-bits as the unique ID of the row.
                //
                int LastRowID = (int)LastRowID64;

                con.Close();

                return LastRowID;
            }
        }

        public void Update_SyncCloudStatus(string User_Id, int LastRowID)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "UPDATE SyncToCloud_StatusUpdate SET Sync_Status=1, Sync_EndDateTime=@pSync_EndDateTime WHERE Id=@pLastRowID AND UserId=@pUser_Id";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@pSync_EndDateTime", Convert.ToString(DateTime.Now)));
                command.Parameters.Add(new SQLiteParameter("@pUser_Id", User_Id));
                command.Parameters.Add(new SQLiteParameter("@pLastRowID", LastRowID));
                command.CommandType = System.Data.CommandType.Text;
                command.ExecuteNonQuery();
                con.Close();
            }
        }

        public string GetLastSyncCloudDateTime(string User_Id)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "SELECT MAX(Id), Sync_EndDateTime FROM SyncToCloud_StatusUpdate WHERE UserId=@pUserId AND Sync_Status=1";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@pUserId", User_Id));
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader reader;
                reader = command.ExecuteReader();

                string Sync_EndDateTime = null;

                if (reader.Read())
                {
                    Sync_EndDateTime = reader["Sync_EndDateTime"].ToString();
                }

                reader.Close();
                con.Close();

                return Sync_EndDateTime;
            }
        }
    }
}
