﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.BusinessAccessLayer
{
    public class clsWaypointTransact
    {
        public string WpointTransact_ID { get; set; }

        public string WayPoint_ID { get; set; }

        public string Wpoint_Latitude { get; set; }

        public string Wpoint_Longitude { get; set; }

        public string Wpoint_Altitude { get; set; }

        public string CreatedOn { get; set; }
    }

    public class waterdetails
    {
        public string Time { get; set; }
        public double DO { get; set; }
        public double EC { get; set; }
        public double pH { get; set; }
        public double Temperature { get; set; }
        public double WaterLevel { get; set; }
        public double RSSI { get; set; }
        public int PaddockId { get; set; }
        public string PaddockName { get; set; }
        public int UserId { get; set; }
    }

    public class LiveStockInfo
    {
        public int livestockid { get; set; }
        public int livestockcount { get; set; }
        public int PaddockId { get; set; }
        public string PaddockName { get; set; }
        public int userid { get; set; }
    }

    public class LiveStockAttch
    {
        public string attachmentname { get; set; }
        public string path { get; set; }
        public string imagetype { get; set; }
        public string size { get; set; }
        public int livestockid { get; set; }
        public int PaddockId { get; set; }
        public string PaddockName { get; set; }
        public int userid { get; set; }
        public byte[] Img_Bytes { get; set; }
    }


    public class LiveStockAttchbytes
    {
        public byte[] Img_Bytes { get; set; }
    }


}
