﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.DataAccessLayer
{
    class DAL_AIFenceMonitoring
    {
        int Fenceinformation_LastRowID = 0;
        int AIFenceCount = 0;
        int AIFenceCountImg = 0;
        int _AI_Status = 0;
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();
        public int Select_DamageCount(string UserId, string PaddockId, string PaddockName, string File_Captured_Date, string Mission_Name, string Mission_Type,int File_type)
        {
          
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command1 = new SQLiteCommand("select * from tbl_pythonAI_FenceDamageCount", con);
                DataTable dt1 = new DataTable();
                SQLiteDataAdapter da1 = new SQLiteDataAdapter(command1);
                da1.Fill(dt1);
                int insert_status = 0;

                //DataTable Map_dataTable = new DataTable();

                if (dt1.Rows != null && !string.IsNullOrEmpty(dt1.Rows.ToString()))
                {
                    foreach (DataRow row in dt1.Rows)
                    {
                        object value = row["DamageCount"];
                        string ai_Imagename = row["AI_Img_Name"].ToString();
                        string FenceStatus= row["FenceStatus"].ToString();
                        string FenceType = row["FenceType"].ToString();
                        string Ftype_ID = GetFenceID(FenceType);
                        if (value != DBNull.Value)
                        {
                            // do something
                            AIFenceCount= InsertFenceDamageCount(row["DamageCount"].ToString(), FenceStatus, Ftype_ID, UserId, FenceType, PaddockId, PaddockName);
                            AIFenceCountImg= InsertAIFenceAttachment(row["AI_Img_Name"].ToString(), UserId, row["Id"].ToString(), PaddockId, PaddockName, File_Captured_Date, Mission_Name, Mission_Type,File_type);
                            _AI_Status= Update_AI_Status(UserId, row["AI_Img_Name"].ToString());
                        }
                    }
                    ClearAIPythonDamageCount();
                    if ((AIFenceCount == 1) && (AIFenceCountImg == 1) && (_AI_Status == 1))
                    {
                        insert_status = 1;

                    }
                    else
                    {
                        insert_status = 0;
                    }
                   

                }
                else
                {
                    insert_status = 0;
                }

                return insert_status;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_AIFenceMonitoring", UserId);
                return 0;
            }
        }

        public string GetFenceID(string FenceType)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "SELECT Ftype_ID FROM tbl_AI_Fencetype WHERE FenceType=@FenceType";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@FenceType", FenceType));
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader reader;
                reader = command.ExecuteReader();
                if (reader.Read())
                {
                    FenceType = reader["FenceType"].ToString();
                }
                reader.Close();
                con.Close();
                return FenceType;
            }
        }

        public int InsertFenceDamageCount(string DamageCount,string FenceStatus,string Ftype_ID, string UserId, string FenceType, string PaddockId, string PaddockName)
        {
            int rowsAffected = 0;
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO tbl_AI_FenceMonitoring (Ftype_ID, DamageCount, PaddockId, PaddockName, UserId, CreatedDate,FenceStatus,FenceType) VALUES (@Ftype_ID,@DamageCount,@PaddockId,@PaddockName,@UserId,@CreatedDate,@FenceStatus,@FenceType)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@Ftype_ID", Ftype_ID));
                    command.Parameters.Add(new SQLiteParameter("@DamageCount", DamageCount));
                    command.Parameters.Add(new SQLiteParameter("@PaddockId", PaddockId));
                    command.Parameters.Add(new SQLiteParameter("@PaddockName", PaddockName));
                    command.Parameters.Add(new SQLiteParameter("@UserId", UserId));
                    command.Parameters.Add(new SQLiteParameter("@CreatedDate", DateTime.Now));
                    command.Parameters.Add(new SQLiteParameter("@FenceStatus", FenceStatus));
                    command.Parameters.Add(new SQLiteParameter("@FenceType", FenceType));
                    command.CommandType = System.Data.CommandType.Text;
                    rowsAffected = command.ExecuteNonQuery();

                    con.Close();
                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_AIFenceMonitoring", UserId);
                return 0;
            }
        }

        public int InsertAIFenceAttachment(string AI_Image, string UserId, string Ftype_ID, string PaddockId, string PaddockName, string File_Captured_Date, string Mission_Name, string Mission_Type,int File_type)
        {
            int rowsAffected = 0;
            try
            {
                string _mission_Destination_folderName = "";
                string fileName_path = "";
                if (File_type == 1)
                {
                    _mission_Destination_folderName = Convert.ToString(ConfigurationManager.AppSettings["DestinationFilePath"].ToString());
                    fileName_path = _mission_Destination_folderName + "\\" + Mission_Name.Replace(" ", "") + "\\" + PaddockName.Replace(" ", "") + "\\" + File_Captured_Date.Replace(" ", "") + "\\" + "Image" + "\\" + AI_Image;
                }
                if (File_type == 2)
                {
                    _mission_Destination_folderName = Convert.ToString(ConfigurationManager.AppSettings["DestinationFilePath"].ToString());
                    fileName_path = _mission_Destination_folderName + "\\" + Mission_Name.Replace(" ", "") + "\\" + PaddockName.Replace(" ", "") + "\\" + File_Captured_Date.Replace(" ", "") + "\\" + "Video" + "\\" + AI_Image;
                }
                FileInfo fi = new FileInfo(fileName_path);
                string filetype = fi.Extension;
                string filesize = FileSizeFormatter.FormatSize(fi.Length);
                string iLAMS_Status = "0";
                string isImageTransfered = "0";

                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO tbl_AI_FenceMonitoring_Attachment (AttachmentName, Path, Type, Size, Ftype_ID, PaddockId, PaddockName, UserId, CreatedDate,FileCapturedDate,Mission_Name,Mission_Type,iLAMS_Status,Fence_ID,IsImgTransfered) VALUES (@AttachmentName, @Path, @Type, @Size, @Ftype_ID, @PaddockId, @PaddockName, @UserId, @CreatedDate,@FileCapturedDate,@Mission_Name,@Mission_Type,@iLAMS_Status,@Fence_ID,@IsImgTransfered)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@AttachmentName", AI_Image));
                    command.Parameters.Add(new SQLiteParameter("@Path", fileName_path));
                    command.Parameters.Add(new SQLiteParameter("@Type", filetype));
                    command.Parameters.Add(new SQLiteParameter("@Size", filesize));
                    command.Parameters.Add(new SQLiteParameter("@Ftype_ID", Ftype_ID));
                    command.Parameters.Add(new SQLiteParameter("@PaddockId", PaddockId));
                    command.Parameters.Add(new SQLiteParameter("@PaddockName", PaddockName));
                    command.Parameters.Add(new SQLiteParameter("@UserId", UserId));
                    command.Parameters.Add(new SQLiteParameter("@FileCapturedDate", File_Captured_Date));
                    command.Parameters.Add(new SQLiteParameter("@Mission_Name", Mission_Name));
                    command.Parameters.Add(new SQLiteParameter("@Mission_Type", Mission_Type));
                    command.Parameters.Add(new SQLiteParameter("@iLAMS_Status", iLAMS_Status));
                    command.Parameters.Add(new SQLiteParameter("@IsImgTransfered", isImageTransfered));
                    command.Parameters.Add(new SQLiteParameter("@Fence_ID", Fenceinformation_LastRowID));
                    command.Parameters.Add(new SQLiteParameter("@CreatedDate", DateTime.Now));
                    command.CommandType = System.Data.CommandType.Text;
                    rowsAffected= command.ExecuteNonQuery();
                    con.Close();
                    Fenceinformation_LastRowID = 0;
                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_AIFenceMonitoring", UserId);
                return 0;
            }
        }

        public int Update_AI_Status(string User_Id, string AI_Imagename)
        {
            int rowsAffected = 0;
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "UPDATE Drone_Files SET AI_Status=1 WHERE File_Name=@AI_Img_Name AND User_Id=@User_Id";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@User_Id", User_Id));
                    command.Parameters.Add(new SQLiteParameter("@AI_Img_Name", AI_Imagename));
                    command.CommandType = System.Data.CommandType.Text;
                    rowsAffected= command.ExecuteNonQuery();
                    con.Close();
                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_AIFenceMonitoring", User_Id);
                return 0;
            }
        }

        public void ClearAIPythonDamageCount()
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));

                con.Open();
                SQLiteCommand command = new SQLiteCommand("DELETE from tbl_pythonAI_FenceDamageCount", con);
                command.ExecuteNonQuery();
                con.Close();

                con.Open();
                SQLiteCommand command1 = new SQLiteCommand("UPDATE SQLITE_SEQUENCE SET SEQ = 0 WHERE NAME = 'tbl_pythonAI_FenceDamageCount'", con);
                command1.ExecuteNonQuery();
                con.Close();
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_AIFenceMonitoring", "");
            }
        }
    }
}
