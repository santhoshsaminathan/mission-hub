﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;

namespace MLA_Aerodyne.DataAccessLayer
{
    class DAL_CreatePaddock
    {
        public string Paddock_ID { get; set; }

        public string Plot_ID { get; set; }

        public string UserId { get; set; }

        public string Plot_Image { get; set; }

        public string Select_MapArea { get; set; }

        public string GetSelectMapAreaImageFromDB(string User_Id, string SelectMapArea)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "SELECT Plot_ID, User_Id, Plot_Image FROM Plot_Master where User_ID=@pUser_ID and Plot_ID=@pPlot_ID AND IsActive=1";
                command.Parameters.Add(new SQLiteParameter("@pUser_ID") { Value = User_Id });
                command.Parameters.Add(new SQLiteParameter("@pPlot_ID") { Value = SelectMapArea });
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader reader;
                reader = command.ExecuteReader();
                if (reader.Read())
                {
                    Plot_ID = reader["Plot_ID"].ToString();
                    UserId = reader["User_Id"].ToString();
                    Plot_Image = reader["Plot_Image"].ToString();
                }
                reader.Close();
                con.Close();
                return Plot_Image;
            }
        }

        public List<string> GetSelectMapAreaPointFromDB(string SelectMapArea)
        {
            List<string> LstMapArea = new List<string>();
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                DataTable dt = new DataTable();
                con.Open();
                command.CommandText = "select PT.Plot_Latitude,PT.Plot_Longitude,PM.Plot_Name from Plot_Transact as PT INNER JOIN Plot_Master as PM on PM.Plot_ID=PT.Plot_ID WHERE PM.Plot_ID=@Plotid";
                command.Parameters.Add(new SQLiteParameter("@Plotid") { Value = SelectMapArea });
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader dbr1;
                dbr1 = command.ExecuteReader();
                while (dbr1.Read())
                {
                    string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() + "," + dbr1[2].ToString();
                    LstMapArea.Add(ValPoints);
                }
            }
            if (LstMapArea.Count == 0)
            {
                return null;
            }
            else
            {
                return LstMapArea;
            }
        }
        public string Check_Paddock(string padId, string userid)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "SELECT Paddock_Id FROM Paddock_Master WHERE Paddock_Id=@Paddock_Id AND IsActive=1";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@Paddock_Id", padId));
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader reader;
                reader = command.ExecuteReader();
                if (reader.Read())
                {
                    Paddock_ID = reader["Paddock_Id"].ToString();
                }
                reader.Close();
                con.Close();
                return Paddock_ID;
            }
        }

        public string Get_MapID(string padId, string userid)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "SELECT Plot_ID FROM Paddock_Master WHERE Paddock_Id=@Paddock_Id AND IsActive=1";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@Paddock_Id", padId));
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader reader;
                reader = command.ExecuteReader();
                if (reader.Read())
                {
                    Plot_ID = reader["Plot_ID"].ToString();
                }
                reader.Close();
                con.Close();
                return Plot_ID;
            }
        }

        public void Update_Paddock(string User_Id, string Waypoints, string CapImagePath, string padID)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "DELETE FROM Paddock_Transact WHERE Paddock_Id=@Paddock_Id";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@Paddock_Id", padID));
                command.CommandType = System.Data.CommandType.Text;
                command.ExecuteNonQuery();
                con.Close();
                con.Open();
                command.CommandText = "UPDATE Paddock_Master SET Paddock_Image=@CapImagePath WHERE Paddock_Id =@Paddock_Id AND User_Id=@User_Id AND IsActive=1";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@CapImagePath", CapImagePath));
                command.Parameters.Add(new SQLiteParameter("@Paddock_Id", padID));
                command.Parameters.Add(new SQLiteParameter("@User_Id", User_Id));
                command.CommandType = System.Data.CommandType.Text;
                command.ExecuteNonQuery();
                con.Close();
                dynamic Way_Points = JsonConvert.DeserializeObject(Waypoints);

                foreach (var val in Way_Points)
                {
                    con.Open();
                    command.CommandText = "INSERT INTO Paddock_Transact (Paddock_Id,Paddock_Latitude,Paddock_Longitude,CreatedOn) VALUES (@Paddock_Id,@Paddock_Latitude,@Paddock_Longitude,@CreatedOn)";
                    command.Prepare();
                    //command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                    command.Parameters.Add(new SQLiteParameter("@Paddock_Id", padID));
                    command.Parameters.Add(new SQLiteParameter("@Paddock_Latitude", val.lat));
                    command.Parameters.Add(new SQLiteParameter("@Paddock_Longitude", val.lng));
                    command.Parameters.Add(new SQLiteParameter("@CreatedOn", Convert.ToString(DateTime.Now)));
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
        public string GetSelectMapAreaNameFromDB(string SelectMapArea)
        {
            string PaddockName = null;

            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                DataTable dt = new DataTable();
                con.Open();
                command.CommandText = "SELECT Plot_Name FROM plot_master where Plot_ID=@pPlot_ID";
                command.Parameters.Add(new SQLiteParameter("@pPlot_ID") { Value = SelectMapArea });
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader dbr1;
                dbr1 = command.ExecuteReader();
                
                while (dbr1.Read())
                {
                    PaddockName = dbr1["Plot_Name"].ToString();
                }
                con.Close();
            }
            return PaddockName;
        }

        public string GetSelectPaddockNameFromDB(string SelectPaddock, string User_Id)
        
            {
            string PaddockName = null;

            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                DataTable dt = new DataTable();
                con.Open();
                command.CommandText = "SELECT Paddock_Name FROM Paddock_Master where Paddock_Id=@pPaddock_Id AND User_ID=@User_ID";
                command.Parameters.Add(new SQLiteParameter("@pPaddock_Id") { Value = SelectPaddock });
                command.Parameters.Add(new SQLiteParameter("@User_ID") { Value = User_Id });
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader dbr1;
                dbr1 = command.ExecuteReader();
                
                while (dbr1.Read())
                {
                    PaddockName = dbr1["Paddock_Name"].ToString();
                }
                con.Close();
            }
            return PaddockName;
        }

        public string GetPlotIDFromDB(string SelectPaddock, string User_Id)
        {
            string PaddockName = null;

            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                DataTable dt = new DataTable();
                con.Open();
                command.CommandText = "SELECT Plot_ID FROM Paddock_Master where Paddock_Id=@pPaddock_Id AND User_ID=@User_ID";
                command.Parameters.Add(new SQLiteParameter("@pPaddock_Id") { Value = SelectPaddock });
                command.Parameters.Add(new SQLiteParameter("@User_ID") { Value = User_Id });
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader dbr1;
                dbr1 = command.ExecuteReader();

                while (dbr1.Read())
                {
                    PaddockName = dbr1["Plot_ID"].ToString();
                }
                con.Close();
            }
            return PaddockName;
        }

        public DataSet GetPaddockFromDB(string User_Id)
        {
            System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));

            con.Open();
            SQLiteCommand command = new SQLiteCommand("select Paddock_Name from Paddock_Master WHERE User_ID=" + Convert.ToInt32(User_Id) + " AND IsActive=1 Order By Paddock_Name ASC", con);
            DataSet ds = new DataSet();
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(ds);
            con.Close();
            return ds;
        }

        public DataSet GetMapAreaFromDB(string User_Id)
        {
            System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));

            con.Open();
            SQLiteCommand command = new SQLiteCommand("select Plot_ID,Plot_Name from Plot_Master WHERE User_ID=" + Convert.ToInt32(User_Id) + " AND IsActive=1 Order By Plot_Name ASC", con);
            DataSet ds = new DataSet();
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(ds);
            con.Close();
            return ds;
        }

        public DataSet GetMapAreaFromDB_Auto(string User_Id,string Auto)
        {
            System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));

            con.Open();
            SQLiteCommand command = new SQLiteCommand("select Plot_ID,Plot_Name from Plot_Master WHERE User_ID=@User_ID AND IsActive=1 AND Plot_Name like @Auto", con);
            command.Parameters.Add(new SQLiteParameter("@User_ID") { Value = User_Id });
            command.Parameters.Add(new SQLiteParameter("@Auto") { Value = Auto });
            DataSet ds = new DataSet();
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(ds);
            con.Close();
            return ds;
        }


        public void Save_CreateMapArea(string User_Id, string PaddockName, string Waypoints, string CapImagePath, string SelectMapArea, string Area)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO Paddock_Master (Plot_ID,Paddock_Name,User_ID,CreatedOn,Paddock_Image,IsActive,Area) VALUES (@pPlot_ID,@pPaddock_Name,@pUser_ID,@pCreatedOn,@pPaddock_Image,1,@Area)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@pPlot_ID", SelectMapArea));
                    command.Parameters.Add(new SQLiteParameter("@pPaddock_Name", PaddockName));
                    command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                    command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));
                    command.Parameters.Add(new SQLiteParameter("@pPaddock_Image", CapImagePath));
                    command.Parameters.Add(new SQLiteParameter("@Area", Area));
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                    con.Close();

                    con.Open();
                    command.CommandText = "SELECT User_Id,Paddock_Id FROM Paddock_Master where User_ID=@pUser_ID and Paddock_Name=@pPaddock_Name AND IsActive=1";
                    command.Parameters.Add(new SQLiteParameter("@pUser_ID") { Value = User_Id });
                    command.Parameters.Add(new SQLiteParameter("@pPaddock_Name", PaddockName));
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader reader;
                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        Paddock_ID = reader["Paddock_Id"].ToString();
                    }
                    reader.Close();
                    con.Close();

                    dynamic Way_Points = JsonConvert.DeserializeObject(Waypoints);

                    foreach (var val in Way_Points)
                    {
                        con.Open();
                        command.CommandText = "INSERT INTO Paddock_Transact (Paddock_ID,Paddock_Latitude,Paddock_Longitude,CreatedOn) VALUES (@pPaddock_ID,@pPaddock_Latitude,@pPaddock_Longitude,@pCreatedOn)";
                        command.Prepare();
                        //command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                        command.Parameters.Add(new SQLiteParameter("@pPaddock_ID", Paddock_ID));
                        command.Parameters.Add(new SQLiteParameter("@pPaddock_Latitude", val.lat));
                        command.Parameters.Add(new SQLiteParameter("@pPaddock_Longitude", val.lng));
                        command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));
                        command.CommandType = System.Data.CommandType.Text;
                        command.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
            }
        }
    }
}
