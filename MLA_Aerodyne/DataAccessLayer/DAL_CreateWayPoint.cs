﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;

namespace MLA_Aerodyne.DataAccessLayer
{
    class DAL_CreateWayPoint
    {
        public string Waypoint_ID { get; set; }

        public string Paddock_ID { get; set; }

        public string UserId { get; set; }

        public string Paddock_Image { get; set; }

        public string Select_Paddock { get; set; }
          DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        public string GetSelectPaddockImageFromDB(string User_Id, string SelectPaddock)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "SELECT Paddock_Id, User_Id, Paddock_Image FROM Paddock_Master where User_ID=@pUser_ID and Paddock_Id=@pPaddock_Id and IsActive=1";
                    command.Parameters.Add(new SQLiteParameter("@pUser_ID") { Value = User_Id });
                    command.Parameters.Add(new SQLiteParameter("@pPaddock_Id") { Value = SelectPaddock });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader reader;
                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        Paddock_ID = reader["Paddock_Id"].ToString();
                        UserId = reader["User_Id"].ToString();
                        Paddock_Image = reader["Paddock_Image"].ToString();
                    }
                    reader.Close();
                    con.Close();
                    return Paddock_Image;
                }
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateWayPoint", User_Id);
                return null;
            }
        }

        public string Check_Waypoint(string wayId, string userid)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "SELECT Waypoint_ID FROM WayPoint_Master WHERE Waypoint_ID=@Waypoint_ID AND IsActive=1";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@Waypoint_ID", wayId));
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader reader;
                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        Waypoint_ID = reader["Waypoint_ID"].ToString();
                    }
                    reader.Close();
                    con.Close();
                    return Waypoint_ID;
                }
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateWayPoint", userid);
                return null;
            }
        }
        public string Get_WayID(string padId, string userid)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "SELECT Paddock_Id FROM WayPoint_Master WHERE WayPoint_Id=@WayPoint_Id AND IsActive=1";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@WayPoint_Id", padId));
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader reader;
                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        Paddock_ID = reader["Paddock_Id"].ToString();
                    }
                    reader.Close();
                    con.Close();
                    return Paddock_ID;
                }
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateWayPoint", userid);
                return null;
            }
        }
        public void Update_Waypoint(string User_Id, string Waypoints, string CapImagePath, string wayID)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "DELETE FROM Waypoint_Transact WHERE WayPoint_Id=@WayPoint_Id";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@WayPoint_Id", wayID));
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                    con.Close();
                    con.Open();
                    command.CommandText = "UPDATE WayPoint_Master SET Wpoint_Image=@CapImagePath WHERE WayPoint_Id =@WayPoint_Id AND User_Id=@User_Id AND IsActive=1";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@CapImagePath", CapImagePath));
                    command.Parameters.Add(new SQLiteParameter("@WayPoint_Id", wayID));
                    command.Parameters.Add(new SQLiteParameter("@User_Id", User_Id));
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                    con.Close();
                    dynamic Way_Points = JsonConvert.DeserializeObject(Waypoints);

                    foreach (var val in Way_Points)
                    {
                        if (val != null)
                        {
                            con.Open();
                            command.CommandText = "INSERT INTO Waypoint_Transact (WayPoint_Id,Wpoint_Latitude,Wpoint_Longitude,CreatedOn) VALUES (@WayPoint_Id,@Wpoint_Latitude,@Wpoint_Longitude,@CreatedOn)";
                            command.Prepare();
                            //command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                            command.Parameters.Add(new SQLiteParameter("@WayPoint_Id", wayID));
                            command.Parameters.Add(new SQLiteParameter("@Wpoint_Latitude", val.lat));
                            command.Parameters.Add(new SQLiteParameter("@Wpoint_Longitude", val.lng));
                            command.Parameters.Add(new SQLiteParameter("@CreatedOn", Convert.ToString(DateTime.Now)));
                            command.CommandType = System.Data.CommandType.Text;
                            command.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateWayPoint", User_Id);
            }
        }
        public List<string> GetSelectPaddockPointFromDB(string SelectPaddock)
        {
            try
            {
                List<string> LstPaddock = new List<string>();
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    DataTable dt = new DataTable();
                    con.Open();
                    command.CommandText = "select PT.Paddock_Latitude, PT.Paddock_Longitude,PM.Paddock_Name,PL.Plot_Name from Paddock_Master as PM "
                                           +" LEFT JOIN Paddock_Transact as PT on PM.Paddock_Id = PT.Paddock_Id"
                                           + " LEFT JOIN Plot_Master AS PL ON PL.Plot_ID = PM.Plot_ID"
                                           + " WHERE PT.Paddock_Id = @Paddockid";
                    command.Parameters.Add(new SQLiteParameter("@Paddockid") { Value = SelectPaddock });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader dbr1;
                    dbr1 = command.ExecuteReader();
                    while (dbr1.Read())
                    {
                        string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() + "," + dbr1[2].ToString() + "," + dbr1[3].ToString();
                        LstPaddock.Add(ValPoints);
                    }
                }
                return LstPaddock;
            }


            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateWayPoint", "");
                return null;
            }
        }

        public string GetSelectPaddockNameFromDB(string PaddockId)
        {
            try
            {
                string PaddockName = null;

                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    DataTable dt = new DataTable();
                    con.Open();
                    command.CommandText = "SELECT Paddock_Name FROM Paddock_Master where Paddock_Id=@pPaddock_Id";
                    command.Parameters.Add(new SQLiteParameter("@pPaddock_Id") { Value = PaddockId });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader dbr1;
                    dbr1 = command.ExecuteReader();
                    while (dbr1.Read())
                    {
                        PaddockName = dbr1["Paddock_Name"].ToString();
                    }
                }
                return PaddockName;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateWayPoint", "");
                return null;
            }
        }

        public DataSet GetPaddockFromDB(string User_Id)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("select Paddock_Id, Paddock_Name from Paddock_Master WHERE User_ID=" + Convert.ToInt32(User_Id) + " AND IsActive=1 Order By Paddock_Name ASC", con);
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateWayPoint", User_Id);
                return null;
            }
        }

        public DataSet GetSelectedPaddockFromDB(string User_Id,string PlotID)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("select Paddock_Id, Paddock_Name from Paddock_Master WHERE User_ID=" + Convert.ToInt32(User_Id) + " AND IsActive=1 AND Plot_ID=" + Convert.ToInt32(PlotID) + " Order By Paddock_Name ASC ", con);
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateWayPoint", User_Id);
                return null;
            }
        }


        public DataSet GetMissionFromDB()
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("select MisMaster_ID, MissionType from Mission_Master Order By MissionType ASC", con);
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateWayPoint", "");
                return null;
            }
        }

        public DataSet GetWayPointsFromDB(string User_Id)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("select WayPoint_Name from WayPoint_Master WHERE User_ID=" + Convert.ToInt32(User_Id) + " AND IsActive=1 Order By WayPoint_Name ASC", con);
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateWayPoint", User_Id);
                return null;
            }
        }

        public void Save_CreateWayPoint(string User_Id, string WayPointName, string Waypoints, string CapImagePath, string SelectPaddock,string Area,string distancekm,string missiontype)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO WayPoint_Master (Paddock_ID,WayPoint_Name,User_ID,CreatedOn,Wpoint_Image,IsActive,Area,Distance,Mission_type) VALUES (@pPaddock_ID,@pWayPoint_Name,@pUser_ID,@pCreatedOn,@pWpoint_Image,1,@Area,@Distance,@Mission_type)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@pPaddock_ID", SelectPaddock));
                    command.Parameters.Add(new SQLiteParameter("@pWayPoint_Name", WayPointName));
                    command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                    command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));
                    command.Parameters.Add(new SQLiteParameter("@pWpoint_Image", CapImagePath));
                    command.Parameters.Add(new SQLiteParameter("@Area", Area));
                    command.Parameters.Add(new SQLiteParameter("@Distance", distancekm));
                    command.Parameters.Add(new SQLiteParameter("@Mission_type", missiontype));
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                    con.Close();

                    con.Open();
                    command.CommandText = "SELECT User_Id,WayPoint_Id FROM WayPoint_Master where User_ID=@pUser_ID and WayPoint_Name=@pWayPoint_Name and IsActive=1";
                    command.Parameters.Add(new SQLiteParameter("@pUser_ID") { Value = User_Id });
                    command.Parameters.Add(new SQLiteParameter("@pWayPoint_Name", WayPointName));
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader reader;
                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        Waypoint_ID = reader["WayPoint_Id"].ToString();
                    }
                    reader.Close();
                    con.Close();

                    dynamic Way_Points = JsonConvert.DeserializeObject(Waypoints);

                    foreach (var val in Way_Points)
                    {
                        if (val != null)
                        {
                            con.Open();
                            command.CommandText = "INSERT INTO Waypoint_Transact (WayPoint_ID,Wpoint_Latitude,Wpoint_Longitude,CreatedOn) VALUES (@pWayPoint_ID,@pWpoint_Latitude,@pWpoint_Longitude,@pCreatedOn)";
                            command.Prepare();
                            //command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                            command.Parameters.Add(new SQLiteParameter("@pWayPoint_ID", Waypoint_ID));
                            command.Parameters.Add(new SQLiteParameter("@pWpoint_Latitude", val.lat));
                            command.Parameters.Add(new SQLiteParameter("@pWpoint_Longitude", val.lng));
                            command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));
                            command.CommandType = System.Data.CommandType.Text;
                            command.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateWayPoint", User_Id);
            }
        }
    }
}
