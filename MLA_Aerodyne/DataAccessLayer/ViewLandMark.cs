﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.DataAccessLayer
{
    class ViewLandMark
    {
        public DataSet BindCombobox_ViewLandmark(string userId)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));

                con.Open();
                SQLiteCommand command = new SQLiteCommand("select LM_ID,Landmark_Name,Landmark_Image,CreatedOn from Landmark_Master where User_Id=@UserID AND IsActive = 1 Order By Landmark_Name ASC", con);
                command.Parameters.Add(new SQLiteParameter("@UserID") { Value = userId });
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }

        public DataSet BindCombobox_Landmark(string userId, string padID)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("select LM_ID, Landmark_Name, Landmark_Image, CreatedOn from Landmark_Master where User_Id = @UserID AND Paddock_Id = @Paddock_Id AND IsActive = 1 Order By Landmark_Name ASC", con);
                command.Parameters.Add(new SQLiteParameter("@UserID") { Value = userId });
                command.Parameters.Add(new SQLiteParameter("@Paddock_Id") { Value = padID });
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }
        public DataSet List_Landmark(string userId)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("select LM_ID, Landmark_Name, Landmark_Image, CreatedOn from Landmark_Master where User_Id = @UserID  AND IsActive = 1 ORDER BY Landmark_Name ASC", con);
                command.Parameters.Add(new SQLiteParameter("@UserID") { Value = userId });
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }

        public string getI_magePath(string index)
        {
            string imgPath = "";
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "select LM_ID,Landmark_Name,Landmark_Image from Landmark_Master WHERE LM_ID=@LMid";
                    command.Parameters.Add(new SQLiteParameter("@LMid") { Value = index });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader reader;
                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        imgPath = reader["Landmark_Image"].ToString();
                    }
                }

                return imgPath;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }

        public List<string> get_latlong(string index)
        {
            try
            {
                List<string> LstMapArea = new List<string>();
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    DataTable dt = new DataTable();
                    con.Open();
                    command.CommandText = "select lt.LMtransact_Latitude, lt.LMtransact_Longitude,lm.Landmark_Name,PL.Plot_Name from Landmark_Transact as lt "
                        + " INNER JOIN Landmark_Master as lm on lm.LM_ID = lt.LM_ID"
                        + " INNER JOIN Plot_Master AS PL ON PL.Plot_ID = lm.Plot_ID"
                        + " WHERE lm.LM_ID = @LMid";
                    command.Parameters.Add(new SQLiteParameter("@LMid") { Value = index });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader dbr1;
                    dbr1 = command.ExecuteReader();
                    while (dbr1.Read())
                    {
                        string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() + "," + dbr1[2].ToString() + "," + dbr1[3].ToString();
                        LstMapArea.Add(ValPoints);
                    }

                }
                return LstMapArea;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }

        public int Delete_Landmark(string lmID, string userid)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "UPDATE Landmark_Master SET IsActive = 0 WHERE LM_ID =@LM_ID AND User_Id=@Userid";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@LM_ID", lmID));
                command.Parameters.Add(new SQLiteParameter("@Userid", userid));
                command.CommandType = System.Data.CommandType.Text;
                int retVal = command.ExecuteNonQuery();
                con.Close();
                return retVal;
            }
        }
    }
}
