﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.DataAccessLayer
{
    public class DAL_ArtificialIntelligence
    {
        public string AI_ID { get; set; }
        public string Mission_ID { get; set; }
        public string Paddockname { get; set; }
        public string capturedimage_date { get; set; }
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        public DataSet Get_AIimagelist(string User_Id)
        {
            DataSet ds = new DataSet();
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "SELECT DT.File_ID,CM.Mission_Name,PM.Paddock_Name,DT.File_Captured_Date,MM.MissionType,DT.File_Type,PM.Paddock_Id FROM Drone_Files AS DT "
                                            + " INNER JOIN Create_Mission AS CM ON DT.Mission_ID = CM.Mission_ID"
                                            + " INNER JOIN Paddock_Master AS PM ON CM.Paddock_Id = PM.Paddock_Id"
                                            + " INNER JOIN Mission_Master AS MM ON CM.MisMaster_ID = MM.MisMaster_ID"
                                            + " WHERE DT.User_Id =" + User_Id + " AND DT.Transferd_Status = 1 AND DT.AI_Status = 0 AND CM.IsActive=1"
                                            + " GROUP BY DT.File_Captured_Date, PM.Paddock_Name,CM.Mission_ID";
                    command.Parameters.Add(new SQLiteParameter("@User_Id") { Value = User_Id });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                    return ds;
                }
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ArtificialIntelligence", User_Id);
                return null;
            }
        }
        public DataSet Get_AIdetails(string User_Id,string AIid)
        {
            DataSet ds = new DataSet();
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "SELECT DT.Dimg_ID,CM.Mission_Name,DT.DI_ImagePath,PM.Paddock_Name,PM.Paddock_Id,DT.imageCapturedDate FROM DroneImage_Table AS DT INNER JOIN Create_Mission AS CM ON DT.Mission_ID=CM.Mission_ID INNER JOIN Paddock_Master AS PM ON CM.Paddock_Id=PM.Paddock_Id WHERE DT.User_Id =@User_Id AND DT.Transferd_Status=1 AND DT.AI_Status=0 AND DT.Dimg_ID=@Dimg_ID AND CM.IsActive=1";
                    command.Parameters.Add(new SQLiteParameter("@User_Id") { Value = User_Id });
                    command.Parameters.Add(new SQLiteParameter("@Dimg_ID") { Value = AIid });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                    return ds;
                }
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ArtificialIntelligence", User_Id);
                return null;
            }
        }
    }
}
