﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.DataAccessLayer
{
    public class DataProcess
    {
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();
        DataSet ds_mission = new DataSet();
        public int SaveFileAttachment(string Mission_ID, string imageCapturedDate, string FileName, string Size, string Filetype, string Path, string User_ID, string createdBy, int Transferd_Status, string OriginalFileName)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO Drone_Files (Mission_ID,File_Name,File_Size,File_Type,File_Path,User_ID,CreatedBy,Created_Date,Transferd_Status,OriginalFileName,File_Captured_Date) VALUES (@Mission_ID,@DI_Imagename,@DI_FileSize,@DI_FileType,@DI_ImagePath,@User_ID,@CreatedBy,@CreatedDate,@Transferd_Status,@OriginalFileName,@imageCapturedDate)";
                    // command.Parameters.Add(new SQLiteParameter("@p_file_attachment_name", Convert.ToString(FileAttachmentName)));
                    command.Parameters.Add(new SQLiteParameter("@Mission_ID", Convert.ToString(Mission_ID)));
                    command.Parameters.Add(new SQLiteParameter("@DI_Imagename", Convert.ToString(FileName)));
                    command.Parameters.Add(new SQLiteParameter("@DI_FileSize", Convert.ToString(Size)));
                    command.Parameters.Add(new SQLiteParameter("@DI_FileType", Convert.ToString(Filetype)));
                    command.Parameters.Add(new SQLiteParameter("@DI_ImagePath", Convert.ToString(Path)));
                    command.Parameters.Add(new SQLiteParameter("@User_ID", User_ID));
                    command.Parameters.Add(new SQLiteParameter("@CreatedBy", Convert.ToString(createdBy)));
                    command.Parameters.Add(new SQLiteParameter("@CreatedDate", Convert.ToString(DateTime.Now)));
                    command.Parameters.Add(new SQLiteParameter("@Transferd_Status", Convert.ToString(Transferd_Status)));
                    command.Parameters.Add(new SQLiteParameter("@OriginalFileName", Convert.ToString(OriginalFileName)));
                    command.Parameters.Add(new SQLiteParameter("@imageCapturedDate", Convert.ToString(imageCapturedDate)));
                    command.CommandType = System.Data.CommandType.Text;
                    int _result = command.ExecuteNonQuery();
                    con.Close();
                    return _result;
                }
            }
            catch (Exception ex)
            {
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DataProcess", User_ID);
                throw ex;
            }
        }

        public int Delete_existingimage(string FileAttachmentName)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "DELETE FROM Drone_Files WHERE File_Name = @DI_Imagename";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@DI_Imagename", Convert.ToString(FileAttachmentName)));
                    command.CommandType = System.Data.CommandType.Text;
                    int _result = command.ExecuteNonQuery();
                    con.Close();
                    return _result;
                }
            }
            catch (Exception ex)
            {
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DataProcess", "");
                throw ex;

            }
        }
        public int Update_transferedstatus(string FileAttachmentName)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + System.IO.Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "UPDATE Drone_Files SET Transferd_Status = '1', AI_Status='0' WHERE File_Name = @DI_Imagename";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@DI_Imagename", Convert.ToString(FileAttachmentName)));
                    command.CommandType = System.Data.CommandType.Text;
                    int _result = command.ExecuteNonQuery();
                    con.Close();

                    return _result;
                }
            }
            catch (Exception ex)
            {
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DataProcess", "");
                throw ex;

            }
        }
       

        public DataSet GetMissiondetails(string Mission_ID)
        {
            try
            {

                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source =" + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))

                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {

                    command.CommandText = "SELECT CM.Mission_Name,PM.Paddock_Name FROM Create_Mission AS CM INNER JOIN Paddock_Master AS PM ON CM.Paddock_Id=PM.Paddock_Id WHERE Mission_ID=@Mission_ID";
                    command.Parameters.Add(new SQLiteParameter("@Mission_ID") { Value = Mission_ID });
                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds_mission);
                    con.Close();

                }
                return ds_mission;
            }
            catch (Exception ex)
            {
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DataProcess", "");
                return ds_mission;
            }

        }
    }
}

