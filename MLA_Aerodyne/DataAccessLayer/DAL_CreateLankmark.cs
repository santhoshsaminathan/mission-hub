﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;

namespace MLA_Aerodyne.DataAccessLayer
{
    class DAL_CreateLankmark
    {
        public string LM_ID { get; set; }

        public string Plot_ID { get; set; }

        public string UserId { get; set; }

        public string LM_Image { get; set; }

        public string Select_MapArea { get; set; }

        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        public string GetSelectMapAreaImageFromDB(string User_Id, string SelectMapArea)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "SELECT Plot_ID, User_Id, Plot_Image FROM Plot_Master where User_ID=@pUser_ID and Plot_ID=@pPlot_ID AND IsActive=1";
                    command.Parameters.Add(new SQLiteParameter("@pUser_ID") { Value = User_Id });
                    command.Parameters.Add(new SQLiteParameter("@pPlot_ID") { Value = SelectMapArea });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader reader;
                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        Plot_ID = reader["Plot_ID"].ToString();
                        UserId = reader["User_Id"].ToString();
                        LM_Image = reader["Plot_Image"].ToString();
                    }
                    reader.Close();
                    con.Close();
                    return LM_Image;
                }
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateLankmark", User_Id);
                return null;
            }
        }

        public List<string> GetSelectMapAreaPointFromDB(string SelectMapArea)
        {
            List<string> LstMapArea = new List<string>();
            try
            {
              
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    DataTable dt = new DataTable();
                    con.Open();
                    command.CommandText = "SELECT Plot_Latitude, Plot_Longitude FROM Plot_Transact where Plot_ID=@pPlot_ID";
                    command.Parameters.Add(new SQLiteParameter("@pPlot_ID") { Value = SelectMapArea });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader dbr1;
                    dbr1 = command.ExecuteReader();
                    while (dbr1.Read())
                    {
                        string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString();
                        LstMapArea.Add(ValPoints);
                    }
                }
                return LstMapArea;
            }
            catch(Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateLankmark", "");
                return null;
            }
           
        }

        public DataSet GetLandmarkFromDB(string User_Id)
        {
            DataSet ds = new DataSet();
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("select Landmark_Name from Landmark_Master WHERE User_ID=" + Convert.ToInt32(User_Id) + " Order By Landmark_Name ASC", con);              
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateLankmark", User_Id);
                return null;
            }
        }

        public DataSet GetMapAreaFromDB(string User_Id)
        {
            DataSet ds = new DataSet();
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));

                con.Open();
                SQLiteCommand command = new SQLiteCommand("select Plot_ID,Plot_Name from Plot_Master WHERE User_ID=" + Convert.ToInt32(User_Id) + "AND IsActive=1", con);

                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }
            catch(Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show(); 
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateLankmark", User_Id);
                return null;
            }
            
        }

        public void Save_CreateLandmark(string User_Id, string LandmarkName, string Waypoints, string CapImagePath, string SelectMapArea,string Plot_ID)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO Landmark_Master (Paddock_ID,Landmark_Name,User_ID,CreatedOn,Landmark_Image,IsActive,Plot_ID) VALUES (@pPaddock_ID,@pLandmark_Name,@pUser_ID,@pCreatedOn,@pLandmark_Image,1,@Plot_ID)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@pPaddock_ID", SelectMapArea));
                    command.Parameters.Add(new SQLiteParameter("@pLandmark_Name", LandmarkName));
                    command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                    command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));
                    command.Parameters.Add(new SQLiteParameter("@pLandmark_Image", CapImagePath));
                    command.Parameters.Add(new SQLiteParameter("@Plot_ID", Plot_ID));
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                    con.Close();

                    con.Open();
                    command.CommandText = "SELECT User_Id,LM_ID FROM Landmark_Master where User_ID=@pUser_ID and Landmark_Name=@pLandmark_Name AND IsActive=1";
                    command.Parameters.Add(new SQLiteParameter("@pUser_ID") { Value = User_Id });
                    command.Parameters.Add(new SQLiteParameter("@pLandmark_Name", LandmarkName));
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader reader;
                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        LM_ID = reader["LM_ID"].ToString();
                    }
                    reader.Close();
                    con.Close();

                    dynamic Way_Points = JsonConvert.DeserializeObject(Waypoints);

                    foreach (var val in Way_Points)
                    {
                        con.Open();
                        command.CommandText = "INSERT INTO Landmark_Transact (LM_ID,LMtransact_Latitude,LMtransact_Longitude,CreatedOn) VALUES (@pLM_ID,@pLMtransact_Latitude,@pLMtransact_Longitude,@pCreatedOn)";
                        command.Prepare();
                        //command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                        command.Parameters.Add(new SQLiteParameter("@pLM_ID", LM_ID));
                        command.Parameters.Add(new SQLiteParameter("@pLMtransact_Latitude", val.lat));
                        command.Parameters.Add(new SQLiteParameter("@pLMtransact_Longitude", val.lng));
                        command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));
                        command.CommandType = System.Data.CommandType.Text;
                        command.ExecuteNonQuery();
                        con.Close();
                    }
                }
                
            }
            catch(Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateLankmark", User_Id);
                return;
            }
           
        }

        public int Import_CreateLandmark(string User_Id, string LandmarkName, string latitude,string longitude, string CapImagePath, string SelectMapArea, string Plot_ID)
        {
            int _rowsAffected;
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO Landmark_Master (Paddock_ID,Landmark_Name,User_ID,CreatedOn,Landmark_Image,IsActive,Plot_ID) VALUES (@pPaddock_ID,@pLandmark_Name,@pUser_ID,@pCreatedOn,@pLandmark_Image,1,@Plot_ID)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@pPaddock_ID", SelectMapArea));
                    command.Parameters.Add(new SQLiteParameter("@pLandmark_Name", LandmarkName));
                    command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                    command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));
                    command.Parameters.Add(new SQLiteParameter("@pLandmark_Image", CapImagePath));
                    command.Parameters.Add(new SQLiteParameter("@Plot_ID", Plot_ID));
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                    command.CommandText = "select last_insert_rowid()";

                    // The row ID is a 64-bit value - cast the Command result to an Int64.
                    //
                    Int64 LastRowID64 = (Int64)command.ExecuteScalar();
                    LM_ID = LastRowID64.ToString();
                    command.ExecuteNonQuery();
                    con.Close();

                    con.Open();
                    command.CommandText = "INSERT INTO Landmark_Transact (LM_ID,LMtransact_Latitude,LMtransact_Longitude,CreatedOn) VALUES (@pLM_ID,@pLMtransact_Latitude,@pLMtransact_Longitude,@pCreatedOn)";
                    command.Prepare();
                    //command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                    command.Parameters.Add(new SQLiteParameter("@pLM_ID", LM_ID));
                    command.Parameters.Add(new SQLiteParameter("@pLMtransact_Latitude", latitude));
                    command.Parameters.Add(new SQLiteParameter("@pLMtransact_Longitude", longitude));
                    command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));
                    command.CommandType = System.Data.CommandType.Text;
                    _rowsAffected=command.ExecuteNonQuery();
                    con.Close();
                }
                return _rowsAffected;

            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return 0;
            }

        }

        public int Import_CreateLandmark_ds(string User_Id, string LandmarkName, string latitude, string longitude, string CapImagePath, string SelectMapArea, string Plot_ID)
        {
            int _rowsAffected;
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO Landmark_Master (Paddock_ID,Landmark_Name,User_ID,CreatedOn,Landmark_Image,IsActive,Plot_ID) VALUES (@pPaddock_ID,@pLandmark_Name,@pUser_ID,@pCreatedOn,@pLandmark_Image,1,@Plot_ID)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@pPaddock_ID", SelectMapArea));
                    command.Parameters.Add(new SQLiteParameter("@pLandmark_Name", LandmarkName));
                    command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                    command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));
                    command.Parameters.Add(new SQLiteParameter("@pLandmark_Image", CapImagePath));
                    command.Parameters.Add(new SQLiteParameter("@Plot_ID", Plot_ID));
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                    command.CommandText = "select last_insert_rowid()";

                    // The row ID is a 64-bit value - cast the Command result to an Int64.
                    //
                    Int64 LastRowID64 = (Int64)command.ExecuteScalar();
                    LM_ID = LastRowID64.ToString();
                    command.ExecuteNonQuery();
                    con.Close();

                    con.Open();
                    command.CommandText = "INSERT INTO Landmark_Transact (LM_ID,LMtransact_Latitude,LMtransact_Longitude,CreatedOn) VALUES (@pLM_ID,@pLMtransact_Latitude,@pLMtransact_Longitude,@pCreatedOn)";
                    command.Prepare();
                    //command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                    command.Parameters.Add(new SQLiteParameter("@pLM_ID", LM_ID));
                    command.Parameters.Add(new SQLiteParameter("@pLMtransact_Latitude", latitude));
                    command.Parameters.Add(new SQLiteParameter("@pLMtransact_Longitude", longitude));
                    command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));
                    command.CommandType = System.Data.CommandType.Text;
                    _rowsAffected = command.ExecuteNonQuery();
                    con.Close();
                }
                return Convert.ToInt32(LM_ID);

            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateLankmark", User_Id);
                return 0;
            }

        }

        public int Import_Landmarktransact(string LM_ID, string latitude, string longitude)
        {
            int _rowsAffected;
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    
                    con.Open();
                    command.CommandText = "INSERT INTO Landmark_Transact (LM_ID,LMtransact_Latitude,LMtransact_Longitude,CreatedOn) VALUES (@pLM_ID,@pLMtransact_Latitude,@pLMtransact_Longitude,@pCreatedOn)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@pLM_ID", LM_ID));
                    command.Parameters.Add(new SQLiteParameter("@pLMtransact_Latitude", latitude));
                    command.Parameters.Add(new SQLiteParameter("@pLMtransact_Longitude", longitude));
                    command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));
                    command.CommandType = System.Data.CommandType.Text;
                    _rowsAffected = command.ExecuteNonQuery();
                    con.Close();
                }
                return _rowsAffected;

            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateLankmark", "");
                return 0;
            }

        }
    }
}
