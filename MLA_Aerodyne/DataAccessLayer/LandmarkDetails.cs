﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.DataAccessLayer
{
    class LandmarkDetails
    {
            public string ViewImage { get; set; }
            public string LandMark_Id { get; set; }      
            public string LandMark_Name { get; set; }
    }
}
