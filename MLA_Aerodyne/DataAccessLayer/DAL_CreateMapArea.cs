﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;

namespace MLA_Aerodyne.DataAccessLayer
{
    public  class DAL_CreateMapArea
    {
        DataSet ds = new DataSet();

        DataTable dt = new DataTable();
        DataTable dtPlacemark = new DataTable();
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        MLA_Aerodyne.BuisnessAccessLayer.Polygon_OuterboundaryIS importKML= new BuisnessAccessLayer.Polygon_OuterboundaryIS();
        public string Plot_ID { get; set; }
        public void Save_CreateMapArea(string User_Id, string MapName, string Waypoints, string CapImagePath, string IsDefaultMapCheck,string Area)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "INSERT INTO Plot_Master (Plot_Name,User_ID,CreatedOn,Plot_Image,IsDefaultMap,IsActive,Area) VALUES (@pPlot_Name,@pUser_ID,@pCreatedOn,@pPlot_Image,@pIsDefaultMap,1,@Area)";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@pPlot_Name", MapName));
                command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));
                command.Parameters.Add(new SQLiteParameter("@pPlot_Image", CapImagePath));
                command.Parameters.Add(new SQLiteParameter("@pIsDefaultMap", IsDefaultMapCheck));
                command.Parameters.Add(new SQLiteParameter("@Area", Area));
                command.CommandType = System.Data.CommandType.Text;
                command.ExecuteNonQuery();
                con.Close();

                con.Open();
                command.CommandText = "SELECT User_Id,Plot_ID FROM Plot_Master where User_ID=@pUser_ID and Plot_Name=@pPlot_Name";
                command.Parameters.Add(new SQLiteParameter("@pUser_ID") { Value = User_Id });
                command.Parameters.Add(new SQLiteParameter("@pPlot_Name", MapName));
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader reader;
                reader = command.ExecuteReader();
                if (reader.Read())
                {
                    Plot_ID = reader["Plot_ID"].ToString();
                }
                reader.Close();
                con.Close();

                dynamic Way_Points = JsonConvert.DeserializeObject(Waypoints);

                foreach (var val in Way_Points)
                {
                    con.Open();
                    command.CommandText = "INSERT INTO Plot_Transact (Plot_ID,Plot_Latitude,Plot_Longitude,CreatedOn) VALUES (@pPlot_ID,@pPlot_Latitude,@pPlot_Longitude,@pCreatedOn)";
                    command.Prepare();
                    //command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                    command.Parameters.Add(new SQLiteParameter("@pPlot_ID", Plot_ID));
                    command.Parameters.Add(new SQLiteParameter("@pPlot_Latitude", val.lat));
                    command.Parameters.Add(new SQLiteParameter("@pPlot_Longitude", val.lng));
                    command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                    con.Close();
                }

                Check_DefaultMapArea(Plot_ID, User_Id);
            }
        }

       
        public int Save_CreateKMLMapArea(string User_Id, string MapName, string Waypoints, string CapImagePath, string IsDefaultMapCheck, string Area,string KML_ID)
        {
            int _rowsAffected = 0;
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "INSERT INTO Plot_Master (Plot_Name,User_ID,CreatedOn,Plot_Image,IsDefaultMap,IsActive,Area) VALUES (@pPlot_Name,@pUser_ID,@pCreatedOn,@pPlot_Image,@pIsDefaultMap,1,@Area)";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@pPlot_Name", MapName));
                command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));
                command.Parameters.Add(new SQLiteParameter("@pPlot_Image", CapImagePath));
                command.Parameters.Add(new SQLiteParameter("@pIsDefaultMap", IsDefaultMapCheck));
                command.Parameters.Add(new SQLiteParameter("@Area", Area));
                command.CommandType = System.Data.CommandType.Text;
                command.ExecuteNonQuery();
                con.Close();

                con.Open();
                command.CommandText = "SELECT User_Id,Plot_ID FROM Plot_Master where User_ID=@pUser_ID and Plot_Name=@pPlot_Name";
                command.Parameters.Add(new SQLiteParameter("@pUser_ID") { Value = User_Id });
                command.Parameters.Add(new SQLiteParameter("@pPlot_Name", MapName));
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader reader;
                reader = command.ExecuteReader();
                if (reader.Read())
                {
                    Plot_ID = reader["Plot_ID"].ToString();
                }
                reader.Close();
                con.Close();

                dynamic Way_Points = JsonConvert.DeserializeObject(Waypoints);

                foreach (var val in Way_Points)
                {
                    con.Open();
                    command.CommandText = "INSERT INTO Plot_Transact (Plot_ID,Plot_Latitude,Plot_Longitude,CreatedOn) VALUES (@pPlot_ID,@pPlot_Latitude,@pPlot_Longitude,@pCreatedOn)";
                    command.Prepare();
                    //command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                    command.Parameters.Add(new SQLiteParameter("@pPlot_ID", Plot_ID));
                    command.Parameters.Add(new SQLiteParameter("@pPlot_Latitude", val.lat));
                    command.Parameters.Add(new SQLiteParameter("@pPlot_Longitude", val.lng));
                    command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                    con.Close();
                }

                Check_DefaultMapArea(Plot_ID, User_Id);
            }

            _rowsAffected= Select_KMLpaddocks(User_Id, Plot_ID, KML_ID);
            return _rowsAffected;

        }

        public int Select_KMLpaddocks(string User_Id, string MapID, string KML_ID)
        {
            int _rowsAffected = 0;
            int Paddock_ID =0;
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {

                con.Open();
                command.CommandText = "SELECT KP.name,KP.Placemark_ID,KP.Area, KM.User_Id from KML_Placemark AS KP " +
                                       " INNER JOIN KML_Master AS KM ON KM.KML_ID = KP.KML_ID "+
                                       " WHERE KP.KML_ID =@KML_ID AND KM.User_Id = @User_Id";
                command.Parameters.Add(new SQLiteParameter("@User_Id") { Value = User_Id });
                command.Parameters.Add(new SQLiteParameter("@KML_ID", KML_ID));
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(dt);
                con.Close();
                if (dt.Rows != null && !string.IsNullOrEmpty(dt.Rows.ToString()))
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        con.Open();
                        command.CommandText = "INSERT INTO Paddock_Master (Plot_ID,Paddock_Name,User_ID,CreatedOn,IsActive,Area) VALUES (@pPlot_ID,@pPaddock_Name,@pUser_ID,@pCreatedOn,1,@Area)";
                        command.Prepare();
                        command.Parameters.Add(new SQLiteParameter("@pPlot_ID", MapID));
                        command.Parameters.Add(new SQLiteParameter("@pPaddock_Name", row["name"].ToString()));
                        command.Parameters.Add(new SQLiteParameter("@Area", row["Area"].ToString()));
                        command.Parameters.Add(new SQLiteParameter("@pUser_ID", row["User_Id"]));
                        command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));

                        command.CommandType = System.Data.CommandType.Text;
                        _rowsAffected = command.ExecuteNonQuery();
                        command.CommandText = "select last_insert_rowid()";
                        Int64 LastRowID64 = (Int64)command.ExecuteScalar();
                        int LastRowID = (int)LastRowID64;
                        Paddock_ID = LastRowID;
                        con.Close();

                        con.Open();
                        command.CommandText = "SELECT PO.Latitude,PO.Longitude,PO.altitude,PO.Placemark_ID FROM Polygon_outerBoundaryIs AS PO " +
                                                "INNER JOIN KML_Placemark AS PK ON PK.Placemark_ID = PO.Placemark_ID " +
                                               " INNER JOIN KML_Master AS KM ON KM.KML_ID = PK.KML_ID " +
                                                "WHERE PK.Placemark_ID = @Placemark_ID AND KM.User_Id = @User_Id";
                        command.Parameters.Add(new SQLiteParameter("@User_Id") { Value = User_Id });
                        command.Parameters.Add(new SQLiteParameter("@Placemark_ID", row["Placemark_ID"]));
                        command.CommandType = System.Data.CommandType.Text;
                        SQLiteDataAdapter da1 = new SQLiteDataAdapter(command);
                        da1.Fill(dtPlacemark);
                        con.Close();

                        if (dtPlacemark.Rows != null && !string.IsNullOrEmpty(dtPlacemark.Rows.ToString()))
                        {
                            foreach (DataRow rowP in dtPlacemark.Rows)
                            {
                                con.Open();
                                command.CommandText = "INSERT INTO Paddock_Transact (Paddock_Id,Paddock_Latitude,Paddock_Longitude,CreatedOn) VALUES (@Paddock_Id,@Paddock_Latitude,@Paddock_Longitude,@CreatedOn)";
                                command.Prepare();
                                command.Parameters.Add(new SQLiteParameter("@Paddock_Id", Paddock_ID));
                                command.Parameters.Add(new SQLiteParameter("@Paddock_Latitude", rowP["Latitude"]));
                                command.Parameters.Add(new SQLiteParameter("@Paddock_Longitude", rowP["Longitude"]));
                                command.Parameters.Add(new SQLiteParameter("@CreatedOn", Convert.ToString(DateTime.Now)));
                                command.CommandType = System.Data.CommandType.Text;
                                _rowsAffected= command.ExecuteNonQuery();
                                con.Close();

                            }
                            dtPlacemark.Clear(); 
                        }
                    }
                }

            }
            return _rowsAffected;
        }

            public DataSet GetMapAreaFromDB(string User_Id)
            {

            System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));

            con.Open();
            SQLiteCommand command = new SQLiteCommand("select Plot_Name from Plot_Master WHERE User_ID=" + Convert.ToInt32(User_Id) + " AND IsActive=1 Order By Plot_Name ASC", con);
            DataSet ds = new DataSet();
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(ds);
            con.Close();
            return ds;
        }


        public List<string> GetSelectedPolygonFromDBhme(string _PiD, string User_ID)
        {
            try
            {
                List<string> LstKML = new List<string>();
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    DataTable dt = new DataTable();
                    con.Open();
                    command.CommandText = "	SELECT PT.Paddock_Latitude,PT.Paddock_Longitude,PM.Paddock_Id,PM.Paddock_Name,PM.User_Id FROM Paddock_Master as PM"+
                    " INNER JOIN Paddock_Transact AS PT ON PM.Paddock_Id = PT.Paddock_Id WHERE PM.Paddock_Id = @Paddock_Id AND PM.User_Id = @User_Id";

                    command.Parameters.Add(new SQLiteParameter("@Paddock_Id") { Value = _PiD });
                    command.Parameters.Add(new SQLiteParameter("@User_Id") { Value = User_ID });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader dbr1;
                    dbr1 = command.ExecuteReader();
                    while (dbr1.Read())
                    {
                        string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() + "," + dbr1[2].ToString() + "," + dbr1[3].ToString();
                        //string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() ;
                        LstKML.Add(ValPoints);
                    }
                }
                return LstKML;
            }


            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_CreateMaparea", User_ID);
                return null;
            }
        }

        public DataSet GetMapAreaFromDBHome(string User_Id)
        {
            string plotID = "";
            System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));

            con.Open();
            SQLiteCommand command = new SQLiteCommand("select Plot_ID from Plot_Master WHERE User_ID=" + Convert.ToInt32(User_Id) + " AND IsActive=1 AND IsDefaultMap=1", con);
            SQLiteDataReader reader;
            reader = command.ExecuteReader();
            if (reader.Read())
            {
                plotID = reader["Plot_ID"].ToString();
               
            }
            reader.Close();
            con.Close();

            con.Open();
            SQLiteCommand command1 = new SQLiteCommand("SELECT Paddock_Id FROM Paddock_Master WHERE Plot_ID=" + Convert.ToInt32(plotID) + " AND IsActive=1", con);
            DataSet ds = new DataSet();
            SQLiteDataAdapter da = new SQLiteDataAdapter(command1);
            da.Fill(ds);
            con.Close();
            return ds;
        }

        public string Check_DefaultMapArea(string plotId, string userid)
        {
            string UpdatePlotID = null;
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "SELECT Plot_ID FROM Plot_Master WHERE User_ID=@pUser_ID AND Plot_ID<>@pPlotid AND IsDefaultMap=1";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@pUser_ID", userid));
                command.Parameters.Add(new SQLiteParameter("@pPlotid", plotId));
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader reader;
                reader = command.ExecuteReader();

                if (reader.Read())
                {
                    Plot_ID = reader["Plot_ID"].ToString();
                    UpdatePlotID = reader["Plot_ID"].ToString();
                }

                reader.Close();
                con.Close();

                if (UpdatePlotID != null)
                {
                    Update_DefaultMapArea(userid, Plot_ID);
                }

                return Plot_ID;
            }
        }

        public void Update_DefaultMapArea(string User_Id, string plotID)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "UPDATE Plot_Master SET IsDefaultMap=0 WHERE Plot_ID=@pPlot_ID AND User_Id=@pUser_Id";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@pPlot_ID", plotID));
                command.Parameters.Add(new SQLiteParameter("@pUser_Id", User_Id));
                command.CommandType = System.Data.CommandType.Text;
                command.ExecuteNonQuery();
                con.Close();
            }
        }

        public string Check_MapArea(string plotId, string userid)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "SELECT Plot_ID FROM Plot_Master WHERE Plot_ID=@Plotid AND IsActive=1";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@Plotid", plotId));
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader reader;
                reader = command.ExecuteReader();
                if (reader.Read())
                {
                    Plot_ID = reader["Plot_ID"].ToString();
                }
                reader.Close();
                con.Close();
                return Plot_ID;
            }
        }

        public void Update_CreateMapArea(string User_Id, string Waypoints, string CapImagePath,string plotID)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "DELETE FROM Plot_Transact WHERE Plot_ID=@Plotid";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@Plotid", plotID));
                command.CommandType = System.Data.CommandType.Text;
                command.ExecuteNonQuery();
                con.Close();
                con.Open();
                command.CommandText = "UPDATE Plot_Master SET Plot_Image=@CapImagePath WHERE Plot_ID =@plotID AND User_Id=@User_Id AND IsActive=1";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@CapImagePath", CapImagePath));
                command.Parameters.Add(new SQLiteParameter("@plotID", plotID));
                command.Parameters.Add(new SQLiteParameter("@User_Id", User_Id));
                command.CommandType = System.Data.CommandType.Text;
                command.ExecuteNonQuery();
                con.Close();
                dynamic Way_Points = JsonConvert.DeserializeObject(Waypoints);

                foreach (var val in Way_Points)
                {
                    con.Open();
                    command.CommandText = "INSERT INTO Plot_Transact (Plot_ID,Plot_Latitude,Plot_Longitude,CreatedOn) VALUES (@pPlot_ID,@pPlot_Latitude,@pPlot_Longitude,@pCreatedOn)";
                    command.Prepare();
                    //command.Parameters.Add(new SQLiteParameter("@pUser_ID", User_Id));
                    command.Parameters.Add(new SQLiteParameter("@pPlot_ID", Plot_ID));
                    command.Parameters.Add(new SQLiteParameter("@pPlot_Latitude", val.lat));
                    command.Parameters.Add(new SQLiteParameter("@pPlot_Longitude", val.lng));
                    command.Parameters.Add(new SQLiteParameter("@pCreatedOn", Convert.ToString(DateTime.Now)));
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
    }
}
