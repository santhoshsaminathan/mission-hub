﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.DataAccessLayer
{
    class ViewMissionType
    {
        public DataSet BindCombobox_ViewMission()
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("select MisMaster_ID,MissionType from Mission_Master", con);
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }

        public DataSet BindCombobox_SelectMission(string selectedmission)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("select MisMaster_ID,MissionType from Mission_Master WHERE MissionType=@MissionType", con);
                command.Parameters.Add(new SQLiteParameter("@MissionType") { Value = selectedmission });
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }
        public string Mission_Count()
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                int Num1 = 0;
                SQLiteCommand command = new SQLiteCommand("Select MAX(Mission_ID) from Create_Mission", con);
                SQLiteDataReader reader;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (!String.IsNullOrEmpty(reader[0].ToString()))
                    {
                        Num1 = int.Parse(reader[0].ToString());
                    }
                }
                reader.Close();
                DateTime nowDT = DateTime.Now;
                string now_DT = nowDT.ToString("MM-dd-yyyy hh:mm tt");
                now_DT = now_DT.Replace(':', '.');
                string count = "Mission_" + (Num1 + 1).ToString() + "_" + now_DT;
                count = count.Replace('/', '-');
                con.Close();
                return count;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }

        }

        public string GetMission(string selectMission)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                string mission = null;
                SQLiteCommand command = new SQLiteCommand("select Mission_type from WayPoint_Master WHERE Waypoint_ID=" + Convert.ToInt32(selectMission) + "", con);
                SQLiteDataReader reader;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (!String.IsNullOrEmpty(reader[0].ToString()))
                    {
                        mission = reader[0].ToString();
                    }
                }
                reader.Close();
                con.Close();
                return mission;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }
        public DataSet BindListBox_ViewMission(string userId)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("select Mission_ID,Mission_Name from Create_Mission WHERE User_Id=" + Convert.ToInt32(userId) + " AND IsActive=1", con);
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }
        public DataSet BindRadiobutton_ViewMission(string userId, int misMissionID)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("Select Mission_ID,Mission_Name from Create_Mission WHERE User_Id=" + Convert.ToInt32(userId) + " AND IsActive=1 AND MisMaster_ID=" + Convert.ToInt32(misMissionID) + "", con);
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }
        public DataSet BindTextBlock_ViewMission(int Mission_Id)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("SELECT c.Mission_ID, c.Alert, c.Mission_Name, c.DateOfFlight, c.Csv_FilePath, p.Paddock_Name,p.Paddock_Id, pm.Plot_Name," +
                    "ft.MissionType, w.WayPoint_Name,w.WayPoint_Id, pi.Pilot_Name FROM Create_Mission AS c INNER JOIN Plot_Master AS pm ON c.Plot_ID = pm.Plot_ID" +
                    " INNER JOIN WayPoint_Master AS w ON c.WayPoint_Id = w.WayPoint_Id INNER JOIN Paddock_Master AS P ON c.Paddock_ID = p.Paddock_ID" +
                   " INNER JOIN Pilot_Master AS Pi ON c.Pilot_Id = pi.Pilot_Id INNER JOIN Mission_Master AS Ft ON c.MisMaster_ID = Ft.MisMaster_ID WHERE c.Mission_ID = " + Convert.ToInt32(Mission_Id) + " AND c.IsActive=1", con);
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }

        public DataSet ExportCSV_ViewMission(int Mission_Id)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("SELECT A.Mission_ID,B.Wpoint_Latitude, B.Wpoint_Longitude," +
                "C.* FROM Create_Mission AS A LEFT JOIN Waypoint_Transact B on B.WayPoint_ID = A.WayPoint_ID JOIN Mission_Master C on C.MisMaster_ID = A.MisMaster_ID " +
                " WHERE A.Mission_ID = " + Convert.ToInt32(Mission_Id) + " AND A.IsActive=1", con);
                //SQLiteCommand command = new SQLiteCommand("SELECT wt.Wpoint_Latitude,wt.Wpoint_Longitude,wt.WpointTransact_ID,c.WayPoint_Id FROM Waypoint_Transact AS wt LEFT JOIN Create_Mission as c on wt.WayPoint_Id = c.WayPoint_Id WHERE c.Mission_ID= " + Convert.ToInt32(Mission_Id) + "", con);
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }

        public int Insert_CreateMission(MLA_Aerodyne.BuisnessAccessLayer.MissionHubDetails objMission)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO Create_Mission (User_Id,Plot_ID,Paddock_ID,WayPoint_Id,MisMaster_ID,Pilot_Id,DateOfFlight,CreatedOn,Alert,Status,Mission_Name,IsActive) VALUES (@User_Id,@Plot_ID,@Paddock_ID,@WayPoint_Id,@MisMaster_ID,@Pilot_Id,@DateOfFlight,@CreatedOn,@Alert,@Status,@Mission_Name,@IsActive)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@User_Id", objMission.userID));
                    command.Parameters.Add(new SQLiteParameter("@Plot_ID", objMission.Plot_ID));
                    command.Parameters.Add(new SQLiteParameter("@Paddock_ID", objMission.Paddock_ID));
                    command.Parameters.Add(new SQLiteParameter("@WayPoint_Id", objMission.Waypoint_ID));
                    command.Parameters.Add(new SQLiteParameter("@MisMaster_ID", objMission.Flying_ID));
                    command.Parameters.Add(new SQLiteParameter("@Pilot_Id", objMission.Pilot_ID));
                    command.Parameters.Add(new SQLiteParameter("@DateOfFlight", objMission.DateOfFlight.ToString()));
                    command.Parameters.Add(new SQLiteParameter("@CreatedOn", objMission.CreatedOn.ToString()));
                    command.Parameters.Add(new SQLiteParameter("@Alert", objMission.Alert));
                    command.Parameters.Add(new SQLiteParameter("@Status", objMission.Status));
                    command.Parameters.Add(new SQLiteParameter("@Mission_Name", objMission.Mission_Name));
                    command.Parameters.Add(new SQLiteParameter("@IsActive", 1));
                    command.CommandType = System.Data.CommandType.Text;
                    int insert_status = command.ExecuteNonQuery();
                    con.Close();
                    return insert_status;
                }
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return 0;
            }
        }

        public int Insert_Livelora(MLA_Aerodyne.BuisnessAccessLayer.Lora_Water objwater)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO tbl_lg_waterdetails (time,DO,EC,pH,Temperature,WaterLevel,RSSI,PaddockId,PaddockName,UserId,createddate) VALUES (@time,@DO,@EC,@pH,@Temperature,@WaterLevel,@RSSI,@PaddockId,@PaddockName,@UserId,@createddate)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@time", objwater.Time));
                    command.Parameters.Add(new SQLiteParameter("@DO", objwater.DO));
                    command.Parameters.Add(new SQLiteParameter("@EC", objwater.EC));
                    command.Parameters.Add(new SQLiteParameter("@pH", objwater.pH));
                    command.Parameters.Add(new SQLiteParameter("@Temperature", objwater.Temperature));
                    command.Parameters.Add(new SQLiteParameter("@WaterLevel", objwater.WaterLevel));
                    command.Parameters.Add(new SQLiteParameter("@RSSI", objwater.RSSI));
                    command.Parameters.Add(new SQLiteParameter("@PaddockId", objwater.Paddock_ID));
                    command.Parameters.Add(new SQLiteParameter("@PaddockName", objwater.Paddock_Name));
                    command.Parameters.Add(new SQLiteParameter("@UserId", objwater.userID));
                    command.Parameters.Add(new SQLiteParameter("@createddate", objwater.CreatedOn));
                    command.CommandType = System.Data.CommandType.Text;
                    int insert_status = command.ExecuteNonQuery();
                    con.Close();
                    return insert_status;
                }
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return 0;
            }
        }


        public int _delete_selectedMission(string User_Id, string Mission_ID)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "UPDATE Create_Mission SET IsActive=0 WHERE Mission_ID=@Mission_ID AND User_Id=@User_Id";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@User_Id", User_Id));
                    command.Parameters.Add(new SQLiteParameter("@Mission_ID", Mission_ID));
                    command.CommandType = System.Data.CommandType.Text;
                    int status = command.ExecuteNonQuery();
                    con.Close();
                    con.Open();
                    return status;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return 0;
            }
        }

    }
}
