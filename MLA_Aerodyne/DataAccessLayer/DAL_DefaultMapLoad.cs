﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;

namespace MLA_Aerodyne.DataAccessLayer
{
    class DAL_DefaultMapLoad
    {
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();
        public DataTable UserLocationLoad(string User_Id)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command1 = new SQLiteCommand("select Plot_ID from Plot_Master WHERE User_ID=" + Convert.ToInt32(User_Id) + " AND IsDefaultMap=1", con);
                DataTable dt1 = new DataTable();
                SQLiteDataAdapter da1 = new SQLiteDataAdapter(command1);
                da1.Fill(dt1);
                DataTable Map_dataTable = new DataTable();

                if (dt1.Rows.Count != 0 && dt1.Rows != null && !string.IsNullOrEmpty(dt1.Rows.ToString()))
                {
                    foreach (DataRow row in dt1.Rows)
                    {
                        object value = row["Plot_ID"];

                        if (value != DBNull.Value)
                        {
                            Map_dataTable = UserMapAreaLoad(row["Plot_ID"].ToString());
                        }
                    }
                }
                else
                {
                    Map_dataTable = UserBasedMapLoad(User_Id);
                }

                con.Close();
                return Map_dataTable;
            }
            
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_DefaultMapLoad", User_Id);
                return null;
            }
        }

        public DataTable UserMapAreaLoad(string Plot_ID)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("select PT.Plot_Latitude, PT.Plot_Longitude,PM.Plot_Name from Plot_Transact AS PT INNER JOIN Plot_Master AS PM ON PM.Plot_ID = PT.Plot_ID WHERE PT.Plot_ID=" + Convert.ToInt32(Plot_ID) + "", con);
                DataTable dt = new DataTable();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(dt);
                con.Close();
                return dt;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_DefaultMapLoad", "");
                return null;
            }
        }

        public DataTable UserBasedMapLoad(string User_Id)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("select Latitude, Longitude,Area  from User_Master WHERE User_ID=" + Convert.ToInt32(User_Id) + "", con);
                DataTable dt = new DataTable();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(dt);
                con.Close();
                return dt;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_DefaultMapLoad", User_Id);
                return null;
            }
        }
    }
}
