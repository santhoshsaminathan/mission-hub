﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;

namespace MLA_Aerodyne.DataAccessLayer
{
    class DAL_ServerTableStatusCheck
    {
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        public DataTable CheckServerStatus()
        {
            System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
            DataTable ds = new DataTable();
            try
            {
                con.Open();
                SQLiteCommand command = new SQLiteCommand("select Table_Name,Table_LastId from Synctablestatus where Table_Name in ('tbl_lg_waterdetails','tbl_ai_livestockinformation','tbl_ai_livestock_attachment') order by 1 desc", con);

                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ServerTableStatusCheck", "");
                con.Close();
            }
            return null;
        }

        public string ClearServerStatusTable()
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));

                con.Open();
                SQLiteCommand command = new SQLiteCommand("DELETE from Synctablestatus;", con);
                command.ExecuteNonQuery();

                SQLiteCommand command1 = new SQLiteCommand("UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='Synctablestatus'", con);
                command1.ExecuteNonQuery();
                con.Close();
                InsertServerStatusTable();
                return "Cleared";
            }
            catch (Exception ex)
            {
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ServerTableStatusCheck", "");
                return ex.Message;
            }
        }

        public void InsertServerStatusTable()
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO Synctablestatus (Table_Name, Table_LastId) SELECT masterTab.name as Table_Name, (select seq from sqlite_sequence where name=masterTab.name) as Table_LastId FROM sqlite_master as masterTab WHERE type = 'table' AND name NOT LIKE 'sqlite_%'";
                    command.Prepare();
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ServerTableStatusCheck", "");
            }
        }

        public DataSet SyncTables(string TableName, string LastId)
        {
            System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
            DataSet ds = new DataSet();
            try
            {
                if (TableName.ToLower() == "create_mission")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where Mission_ID<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "droneimage_master")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where DI_Id<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "droneimage_transact")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where DiTransact_ID<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "dronevideo_master")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where DV_Id<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "dronevideo_transact")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where DVTransact_ID<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "errorlog_master")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where Errorlog_Id<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "flyingtype_master")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where Flying_ID<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "flying_transact")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where FTransact_ID<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "landmark_master")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where LM_ID<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "landmark_transact")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where LMtransact_ID<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "login_master")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where User_Id<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "paddock_master")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where Paddock_Id<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "paddock_transact")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where PadTransact_ID<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "pilot_master")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where Pilot_Id<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "pilot_transact")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where Ptransact_ID<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "plot_master")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where Plot_ID<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "plot_transact")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where PlotTransact_ID<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "synctablestatus")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where Id<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "user_master")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where User_Id<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "user_transact")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where UserTrasact_ID<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "waypoint_master")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where WayPoint_Id<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }
                else if (TableName.ToLower() == "waypoint_transact")
                {
                    con.Open();
                    SQLiteCommand command = new SQLiteCommand("select * from '" + TableName + "' where WpointTransact_ID<='" + LastId + "'", con);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                }

                return ds;
            }
            catch (Exception ex)
            {
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_ServerTableStatusCheck", "");
                con.Close();
            }
            return null;
        }
    }
}
