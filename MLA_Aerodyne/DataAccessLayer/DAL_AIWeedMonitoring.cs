﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.DataAccessLayer
{
    public class DAL_AIWeedMonitoring
    {
        int Weedinformation_LastRowID = 0;
        int Weedspectral_LastRowID = 0;
        int AIWeedCount = 0;
        int AIWeedCountImg = 0;
        int _AI_Status_Weed = 0;
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        public int Select_WeedCount(string UserId, string PaddockId, string PaddockName, string File_Captured_Date, string Mission_Name, string Mission_Type,int File_type)
        {

            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command1 = new SQLiteCommand("select * from tbl_pythonAI_WeedCount", con);
                DataTable dt1 = new DataTable();
                SQLiteDataAdapter da1 = new SQLiteDataAdapter(command1);
                da1.Fill(dt1);
                int insert_status = 0;

                //DataTable Map_dataTable = new DataTable();

                if (dt1.Rows != null && !string.IsNullOrEmpty(dt1.Rows.ToString()))
                {
                    foreach (DataRow row in dt1.Rows)
                    {
                        object value = row["AI_Count"];
                        string ai_Imagename = row["AI_Img_Name"].ToString();

                        if (value != DBNull.Value)
                        {
                            // do something
                            AIWeedCount= InsertWeedCount(row["AI_Count"].ToString(), UserId, PaddockId, PaddockName);
                            AIWeedCountImg= InsertAIWeedAttachment(row["AI_Img_Name"].ToString(), UserId, row["Id"].ToString(), PaddockId, PaddockName, File_Captured_Date, Mission_Name, Mission_Type, File_type);
                            _AI_Status_Weed=Update_AI_Status(UserId, row["AI_Img_Name"].ToString());
                        }
                    }

                    ClearAIPythonWeedCount();
                    if ((AIWeedCount == 1) && (AIWeedCountImg == 1) && (_AI_Status_Weed == 1))
                    {
                        insert_status = 1;

                    }
                    else
                    {
                        insert_status = 0;
                    }
                   
                }

                return insert_status;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_AIWeedMonitoring", UserId);
                return 0;
            }
        }
        public void InsertWeedSpectral(string Area, string UserId, string PaddockId, string PaddockName)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "INSERT INTO tbl_AI_WeedMonitoringSpectral (Area, PaddockId, PaddockName, UserId, CreatedDate) VALUES (@Area,@PaddockId,,@PaddockName,@UserId,@CreatedDate)";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@Area", Area));
                command.Parameters.Add(new SQLiteParameter("@PaddockId", PaddockId));
                command.Parameters.Add(new SQLiteParameter("@PaddockName", PaddockName));
                command.Parameters.Add(new SQLiteParameter("@UserId", UserId));
                command.Parameters.Add(new SQLiteParameter("@CreatedDate", DateTime.Now));
                command.CommandType = System.Data.CommandType.Text;
                command.ExecuteNonQuery();

                command.CommandText = "select last_insert_rowid()";

                // The row ID is a 64-bit value - cast the Command result to an Int64.
                //
                Int64 LastRowID64 = (Int64)command.ExecuteScalar();
                Weedspectral_LastRowID = (int)LastRowID64;
                command.ExecuteNonQuery();
                con.Close();
            }
        }


        public int InsertWeedCount(string AI_Count, string UserId, string PaddockId, string PaddockName)
        {
            int rowsAffected = 0;
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "INSERT INTO tbl_AI_WeedMonitoring (AI_Count, PaddockId, PaddockName, UserId, CreatedDate) VALUES (@AI_Count,@PaddockId,@PaddockName,@UserId,@CreatedDate)";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@AI_Count", AI_Count));
                command.Parameters.Add(new SQLiteParameter("@PaddockId", PaddockId));
                command.Parameters.Add(new SQLiteParameter("@PaddockName", PaddockName));
                command.Parameters.Add(new SQLiteParameter("@UserId", UserId));
                command.Parameters.Add(new SQLiteParameter("@CreatedDate", DateTime.Now));
                command.CommandType = System.Data.CommandType.Text;
                rowsAffected= command.ExecuteNonQuery();
                con.Close();
                return rowsAffected;
            }
        }
        public void InsertAIWeedspectralAttachment(string AI_Image, string UserId, string Ftype_ID, string PaddockId, string PaddockName, string File_Captured_Date, string Mission_Name, string Mission_Type)
        {
            try
            {
                string _mission_Destination_folderName = Convert.ToString(ConfigurationManager.AppSettings["DestinationFilePath"].ToString());
                string fileName_path = _mission_Destination_folderName + "\\" + Mission_Name.Replace(" ", "") + "\\" + PaddockName.Replace(" ", "") + "\\" + File_Captured_Date.Replace(" ", "") + "\\" + "Image" + "\\" + AI_Image;
                FileInfo fi = new FileInfo(fileName_path);
                string filetype = fi.Extension;
                string filesize = FileSizeFormatter.FormatSize(fi.Length);
                string iLAMS_Status = "0";

                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO tbl_AI_WeedMonitoring_SpectralAttachment (AttachmentName, Path, Type, Size,PaddockId, PaddockName, UserId, CreatedDate,FileCapturedDate,Mission_Name,Mission_Type,iLAMS_Status,Weed_ID) VALUES (@AttachmentName, @Path, @Type, @Size, @PaddockId, @PaddockName, @UserId, @CreatedDate,@FileCapturedDate,@Mission_Name,@Mission_Type,@iLAMS_Status,@Weed_ID)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@AttachmentName", "Mission Hub"));
                    command.Parameters.Add(new SQLiteParameter("@Path", AI_Image));
                    command.Parameters.Add(new SQLiteParameter("@Type", filetype));
                    command.Parameters.Add(new SQLiteParameter("@Size", filesize));
                    command.Parameters.Add(new SQLiteParameter("@PaddockId", PaddockId));
                    command.Parameters.Add(new SQLiteParameter("@PaddockName", PaddockName));
                    command.Parameters.Add(new SQLiteParameter("@UserId", UserId));
                    command.Parameters.Add(new SQLiteParameter("@FileCapturedDate", File_Captured_Date));
                    command.Parameters.Add(new SQLiteParameter("@Mission_Name", Mission_Name));
                    command.Parameters.Add(new SQLiteParameter("@Mission_Type", Mission_Type));
                    command.Parameters.Add(new SQLiteParameter("@iLAMS_Status", iLAMS_Status));
                    command.Parameters.Add(new SQLiteParameter("@Weed_ID", Weedspectral_LastRowID));
                    command.Parameters.Add(new SQLiteParameter("@CreatedDate", DateTime.Now));
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                    con.Close();
                    Weedspectral_LastRowID = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_AIWeedMonitoring", UserId);
            }
        }
        public int InsertAIWeedAttachment(string AI_Image, string UserId, string Ftype_ID, string PaddockId, string PaddockName, string File_Captured_Date, string Mission_Name, string Mission_Type,int File_type)
        {
            try
            {
                int rowsAffected = 0;
                string _mission_Destination_folderName = "";
                string fileName_path = "";
                if (File_type==1)
                {
                    _mission_Destination_folderName = Convert.ToString(ConfigurationManager.AppSettings["DestinationFilePath"].ToString());
                     fileName_path = _mission_Destination_folderName + "\\" + Mission_Name.Replace(" ", "") + "\\" + PaddockName.Replace(" ", "") + "\\" + File_Captured_Date.Replace(" ", "") + "\\" + "Image" + "\\" + AI_Image;
                }
                if (File_type == 2)
                {
                    _mission_Destination_folderName = Convert.ToString(ConfigurationManager.AppSettings["DestinationFilePath"].ToString());
                    fileName_path = _mission_Destination_folderName + "\\" + Mission_Name.Replace(" ", "") + "\\" + PaddockName.Replace(" ", "") + "\\" + File_Captured_Date.Replace(" ", "") + "\\" + "Video" + "\\" + AI_Image;
                }

                FileInfo fi = new FileInfo(fileName_path);
                string filetype = fi.Extension;
                string filesize = FileSizeFormatter.FormatSize(fi.Length);
                string iLAMS_Status = "0";
                string isImageTransfered = "0";

                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO tbl_AI_WeedMonitoring_Attachment (AttachmentName, Path, Type, Size,PaddockId, PaddockName, UserId, CreatedDate,FileCapturedDate,Mission_Name,Mission_Type,iLAMS_Status,Weed_ID,IsImgTransfered) VALUES (@AttachmentName, @Path, @Type, @Size, @PaddockId, @PaddockName, @UserId, @CreatedDate,@FileCapturedDate,@Mission_Name,@Mission_Type,@iLAMS_Status,@Weed_ID,@IsImgTransfered)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@AttachmentName", AI_Image));
                    command.Parameters.Add(new SQLiteParameter("@Path", fileName_path));
                    command.Parameters.Add(new SQLiteParameter("@Type", filetype));
                    command.Parameters.Add(new SQLiteParameter("@Size", filesize));
                    command.Parameters.Add(new SQLiteParameter("@PaddockId", PaddockId));
                    command.Parameters.Add(new SQLiteParameter("@PaddockName", PaddockName));
                    command.Parameters.Add(new SQLiteParameter("@UserId", UserId));
                    command.Parameters.Add(new SQLiteParameter("@FileCapturedDate", File_Captured_Date));
                    command.Parameters.Add(new SQLiteParameter("@Mission_Name", Mission_Name));
                    command.Parameters.Add(new SQLiteParameter("@IsImgTransfered", isImageTransfered));
                    command.Parameters.Add(new SQLiteParameter("@Mission_Type", Mission_Type));
                    command.Parameters.Add(new SQLiteParameter("@iLAMS_Status", iLAMS_Status));
                    command.Parameters.Add(new SQLiteParameter("@Weed_ID", Weedinformation_LastRowID));
                    command.Parameters.Add(new SQLiteParameter("@CreatedDate", DateTime.Now));
                    command.CommandType = System.Data.CommandType.Text;
                    rowsAffected= command.ExecuteNonQuery();
                    con.Close();
                    Weedinformation_LastRowID = 0;
                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_AIWeedMonitoring", UserId);
                return 0;
            }
        }

        public int Update_AI_Status(string User_Id, string AI_Imagename)
        {
            try
            {
                int rowsAffected = 0;
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "UPDATE Drone_Files SET AI_Status=1 WHERE File_Name=@AI_Img_Name AND User_Id=@User_Id";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@User_Id", User_Id));
                    command.Parameters.Add(new SQLiteParameter("@AI_Img_Name", AI_Imagename));
                    command.CommandType = System.Data.CommandType.Text;
                    rowsAffected=command.ExecuteNonQuery();
                    con.Close();
                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_AIWeedMonitoring", User_Id);
                return 0;
            }
        }

        public void ClearAIPythonWeedspectral()
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));

                con.Open();
                SQLiteCommand command = new SQLiteCommand("DELETE from tbl_pythonAI_WeedSpectral", con);
                command.ExecuteNonQuery();
                con.Close();

                con.Open();
                SQLiteCommand command1 = new SQLiteCommand("UPDATE SQLITE_SEQUENCE SET SEQ = 0 WHERE NAME = 'tbl_pythonAI_WeedSpectral'", con);
                command1.ExecuteNonQuery();
                con.Close();
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
            }
        }

        public void ClearAIPythonWeedCount()
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));

                con.Open();
                SQLiteCommand command = new SQLiteCommand("DELETE from tbl_pythonAI_WeedCount", con);
                command.ExecuteNonQuery();
                con.Close();

                con.Open();
                SQLiteCommand command1 = new SQLiteCommand("UPDATE SQLITE_SEQUENCE SET SEQ = 0 WHERE NAME = 'tbl_pythonAI_WeedCount'", con);
                command1.ExecuteNonQuery();
                con.Close();
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_AIWeedMonitoring", "");
            }
        }
    }
}
