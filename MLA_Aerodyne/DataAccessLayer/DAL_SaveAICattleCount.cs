﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Drawing;

namespace MLA_Aerodyne.DataAccessLayer
{
    class DAL_SaveAICattleCount
    {
        int livestockinformation_LastRowID = 0;
        int AICattleCount = 0;
        int AICattleCountImg = 0;
        int _AI_Status = 0;
        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        public int Select_CattleCount(string UserId, string PaddockId, string PaddockName, string File_Captured_Date, string Mission_Name, string Mission_Type,int File_type)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command1 = new SQLiteCommand("select * from tbl_python_AICattleCount", con);
                DataTable dt1 = new DataTable();
                SQLiteDataAdapter da1 = new SQLiteDataAdapter(command1);
                da1.Fill(dt1);
                int insert_status = 0;

                //DataTable Map_dataTable = new DataTable();

                if (dt1.Rows != null && !string.IsNullOrEmpty(dt1.Rows.ToString()))
                {
                    foreach (DataRow row in dt1.Rows)
                    {
                        object value = row["AI_Count"];
                        string ai_Imagename = row["AI_Img_Name"].ToString();

                        if (value != DBNull.Value)
                        {
                            // do something
                            AICattleCount= InsertAICattleCount(row["AI_Count"].ToString(), UserId,"1" , PaddockId, PaddockName);
                            AICattleCountImg= InsertAICattleCountImg(row["AI_Img_Name"].ToString(), UserId, row["Id"].ToString(), PaddockId, PaddockName, File_Captured_Date, Mission_Name, Mission_Type,File_type);
                            _AI_Status= Update_AI_Status(UserId, row["AI_Img_Name"].ToString());
                        }
                    }

                    ClearAIPythonCattleCount();
                    if((AICattleCount==1) && (AICattleCountImg==1) && (_AI_Status==1))
                    {
                        insert_status = 1;

                    }
                    else
                    {
                        insert_status = 0;
                    }
                    
                }
                else
                {
                    // do something else
                    //Map_dataTable = UserBasedMapLoad(User_Id);
                    insert_status = 0;
                }

                return insert_status;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_SaveAICattleCount", "");
                return 0;
            }
        }

        public int Update_AI_Status(string User_Id, string AI_Imagename)
        {
            try
            {
                int rowsAffected = 0;
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "UPDATE Drone_Files SET AI_Status=1 WHERE File_Name=@AI_Img_Name AND User_Id=@User_Id";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@User_Id", User_Id));
                    command.Parameters.Add(new SQLiteParameter("@AI_Img_Name", AI_Imagename));
                    command.CommandType = System.Data.CommandType.Text;
                    rowsAffected = command.ExecuteNonQuery();
                    con.Close();
                }
                return rowsAffected;
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_SaveAICattleCount", User_Id);
                return 0;
            }
        }

        public int InsertAICattleCount(string AI_Count, string UserId, string LiveStockId, string PaddockId, string PaddockName)
        {
            int rowsAffected = 0;
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO tbl_ai_livestockinformation (LiveStockId, LiveStockCount, PaddockId, PaddockName, UserId, CreatedDate) VALUES (@LiveStockId,@LiveStockCount,@PaddockId,@PaddockName,@UserId,@CreatedDate)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@LiveStockId", LiveStockId));
                    command.Parameters.Add(new SQLiteParameter("@LiveStockCount", AI_Count));
                    command.Parameters.Add(new SQLiteParameter("@PaddockId", PaddockId));
                    command.Parameters.Add(new SQLiteParameter("@PaddockName", PaddockName));
                    command.Parameters.Add(new SQLiteParameter("@UserId", UserId));
                    command.Parameters.Add(new SQLiteParameter("@CreatedDate", DateTime.Now));
                    command.CommandType = System.Data.CommandType.Text;
                    rowsAffected = command.ExecuteNonQuery();
                    con.Close();
                }
                return rowsAffected;
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_SaveAICattleCount", UserId);
                return 0;
            }
        }

        public int InsertAICattleCountImg(string AI_Image, string UserId, string LiveStockId, string PaddockId, string PaddockName, string File_Captured_Date, string Mission_Name, string Mission_Type,int File_type)
        {
            try
            {
                int rowsAffected = 0;
               string _mission_Destination_folderName = "";
                string fileName_path = "";
                if (File_type==1)
                {
                    _mission_Destination_folderName = Convert.ToString(ConfigurationManager.AppSettings["DestinationFilePath"].ToString());
                     fileName_path = _mission_Destination_folderName + "\\" + Mission_Name.Replace(" ", "") + "\\" + PaddockName.Replace(" ", "") + "\\" + File_Captured_Date.Replace(" ", "") + "\\" + "Image" + "\\" + AI_Image;
                }
                if (File_type == 2)
                {
                    _mission_Destination_folderName = Convert.ToString(ConfigurationManager.AppSettings["DestinationFilePath"].ToString());
                    fileName_path = _mission_Destination_folderName + "\\" + Mission_Name.Replace(" ", "") + "\\" + PaddockName.Replace(" ", "") + "\\" + File_Captured_Date.Replace(" ", "") + "\\" + "Video" + "\\" + AI_Image;
                }
                FileInfo fi = new FileInfo(fileName_path);
                string filetype = fi.Extension;
                string filesize = FileSizeFormatter.FormatSize(fi.Length);
                string iLAMS_Status = "0";
                string isImageTransfered = "0";

                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "INSERT INTO tbl_ai_livestock_attachment (AttachmentName, Path, Type, Size, LiveStockId, PaddockId, PaddockName, UserId, CreatedDate,FileCapturedDate,Mission_Name,Mission_Type,iLAMS_Status,LiveStockInfoId,IsImgTransfered) VALUES (@AttachmentName, @Path, @Type, @Size, @LiveStockId, @PaddockId, @PaddockName, @UserId, @CreatedDate,@FileCapturedDate,@Mission_Name,@Mission_Type,@iLAMS_Status,@LiveStockInfoId,@IsImgTransfered)";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@AttachmentName", AI_Image));
                    command.Parameters.Add(new SQLiteParameter("@Path", fileName_path));
                    command.Parameters.Add(new SQLiteParameter("@Type", filetype));
                    command.Parameters.Add(new SQLiteParameter("@Size", filesize));
                    command.Parameters.Add(new SQLiteParameter("@LiveStockId", LiveStockId));
                    command.Parameters.Add(new SQLiteParameter("@PaddockId", PaddockId));
                    command.Parameters.Add(new SQLiteParameter("@PaddockName", PaddockName));
                    command.Parameters.Add(new SQLiteParameter("@UserId", UserId));
                    command.Parameters.Add(new SQLiteParameter("@FileCapturedDate", File_Captured_Date));
                    command.Parameters.Add(new SQLiteParameter("@Mission_Name", Mission_Name));
                    command.Parameters.Add(new SQLiteParameter("@Mission_Type", Mission_Type));
                    command.Parameters.Add(new SQLiteParameter("@CreatedDate", DateTime.Now));
                    command.Parameters.Add(new SQLiteParameter("@iLAMS_Status", iLAMS_Status));
                    command.Parameters.Add(new SQLiteParameter("@IsImgTransfered", isImageTransfered));
                    command.Parameters.Add(new SQLiteParameter("@LiveStockInfoId", livestockinformation_LastRowID));
                    command.CommandType = System.Data.CommandType.Text;
                    rowsAffected= command.ExecuteNonQuery();
                    con.Close();
                    livestockinformation_LastRowID = 0;
                }
                return rowsAffected;

            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_SaveAICattleCount",UserId);
                return 0;
            }
        }

        public void ClearAIPythonCattleCount()
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));

                con.Open();
                SQLiteCommand command = new SQLiteCommand("DELETE from tbl_python_AICattleCount", con);
                command.ExecuteNonQuery();
                con.Close();

                con.Open();
                SQLiteCommand command1 = new SQLiteCommand("UPDATE SQLITE_SEQUENCE SET SEQ = 0 WHERE NAME = 'tbl_python_AICattleCount'", con);
                command1.ExecuteNonQuery();
                con.Close();
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_SaveAICattleCount","");
            }
        }

        public int Insert_CattleCount(MLA_Aerodyne.BusinessAccessLayer.clsAICount objCattle)
        {
            int insert_status = 0;
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "INSERT INTO tbl_ai_livestockinformation (LiveStockId, LiveStockCount, UserId, CreatedDate) VALUES (@LiveStockId,@LiveStockCount,@UserId,@CreatedDate)";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@LiveStockId", objCattle.LiveStockId));
                command.Parameters.Add(new SQLiteParameter("@LiveStockCount", objCattle.LiveStockCount));
                command.Parameters.Add(new SQLiteParameter("@UserId", objCattle.UserId));
                command.Parameters.Add(new SQLiteParameter("@CreatedDate", objCattle.CreateDateTime));
                command.CommandType = System.Data.CommandType.Text;
                insert_status= command.ExecuteNonQuery();
                con.Close();

                con.Open();
                command.CommandText = "INSERT INTO tbl_ai_livestock_attachment (AttachmentName, Path, Type, Size, LiveStockId, UserId, CreatedDate) VALUES (@AttachmentName, @Path, @Type, @Size, @LiveStockId, @UserId, @CreatedDate)";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@AttachmentName", objCattle.AttachmentName));
                command.Parameters.Add(new SQLiteParameter("@Path", objCattle.ImgPath));
                command.Parameters.Add(new SQLiteParameter("@Type", objCattle.ImgType));
                command.Parameters.Add(new SQLiteParameter("@Size", objCattle.ImgSize));
                command.Parameters.Add(new SQLiteParameter("@LiveStockId", objCattle.LiveStockId));
                command.Parameters.Add(new SQLiteParameter("@UserId", objCattle.UserId));
                command.Parameters.Add(new SQLiteParameter("@CreatedDate", objCattle.CreateDateTime));
                command.CommandType = System.Data.CommandType.Text;
                insert_status = command.ExecuteNonQuery();
                con.Close();
                return insert_status;
            }
        }

        public DataSet Select_AIProcessedCattleCount(string UserId,string Mission_ID, string Paddock_ID, string File_Captured_Date,string File_Type)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source= " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "SELECT DT.File_ID,CM.Mission_Name,PM.Paddock_Name,DT.File_Captured_Date,MM.MissionType,DT.File_Type,PM.Paddock_Id,DT.File_Size,CM.Mission_ID,PM.Paddock_Id,DT.File_Name FROM Drone_Files AS DT"
                                                            + " INNER JOIN Create_Mission AS CM ON DT.Mission_ID = CM.Mission_ID"
                                                            + " INNER JOIN Paddock_Master AS PM ON CM.Paddock_Id = PM.Paddock_Id"
                                                            + " INNER JOIN Mission_Master AS MM ON CM.MisMaster_ID = MM.MisMaster_ID"
                                                            + " WHERE DT.User_Id = @User_Id AND DT.File_Type=@File_Type AND DT.Transferd_Status = 1 AND DT.AI_Status = '1' AND CM.IsActive=1"
                                                            + " AND DT.File_Captured_Date = @File_Captured_Date AND CM.Mission_ID=@Mission_ID  AND PM.Paddock_Id=@Paddock_Id"
                                                            + " GROUP BY DT.File_Captured_Date, PM.Paddock_Name, CM.Mission_ID, DT.File_Name";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@User_Id", UserId));
                    command.Parameters.Add(new SQLiteParameter("@File_Captured_Date", File_Captured_Date));
                    command.Parameters.Add(new SQLiteParameter("@Mission_ID", Mission_ID));
                    command.Parameters.Add(new SQLiteParameter("@Paddock_Id", Paddock_ID));
                    command.Parameters.Add(new SQLiteParameter("@File_Type", File_Type));
                    command.CommandType = System.Data.CommandType.Text;
                    DataSet ds = new DataSet();

                    SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                    da.Fill(ds);
                    con.Close();
                    return ds;
                }
            }
            catch(Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "DAL_SaveAICattleCount", UserId);
                return null;
            }
                    
        }

        public DataSet Select_MissionAIProcessed(string MissionID)
        {
            System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));

            con.Open();
            SQLiteCommand command = new SQLiteCommand("select A.Mission_Name, A.Paddock_ID, B.Paddock_Name, c.MissionType from create_mission A INNER JOIN paddock_master B ON " +
                "B.Paddock_Id = A.Paddock_ID INNER JOIN mission_master C ON C.MisMaster_ID = A.MisMaster_ID WHERE A.Mission_ID=" + MissionID + " AND A.IsActive=1", con);
            DataSet ds = new DataSet();
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(ds);
            con.Close();
            return ds;
        }

        public int Update_iLAMSImageStatus(string ImageFileName, string UserId)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "UPDATE tbl_ai_livestock_attachment SET iLAMS_Status = 1 WHERE AttachmentName=@AttachmentName AND UserId=@UserId";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@AttachmentName", ImageFileName));
                command.Parameters.Add(new SQLiteParameter("@UserId", UserId));
                command.CommandType = System.Data.CommandType.Text;
                int retVal = command.ExecuteNonQuery();
                con.Close();
                return retVal;
            }
        }

        public int Update_iLAMSImageStatusForFence(string ImageFileName, string UserId)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "UPDATE tbl_AI_FenceMonitoring_Attachment SET iLAMS_Status = 1 WHERE AttachmentName=@AttachmentName AND UserId=@UserId";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@AttachmentName", ImageFileName));
                command.Parameters.Add(new SQLiteParameter("@UserId", UserId));
                command.CommandType = System.Data.CommandType.Text;
                int retVal = command.ExecuteNonQuery();
                con.Close();
                return retVal;
            }
        }
        public int Update_iLAMSImageStatusForWeed(string ImageFileName, string UserId)
        {
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "UPDATE tbl_AI_WeedMonitoring_Attachment SET iLAMS_Status = 1 WHERE AttachmentName=@AttachmentName AND UserId=@UserId";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@AttachmentName", ImageFileName));
                command.Parameters.Add(new SQLiteParameter("@UserId", UserId));
                command.CommandType = System.Data.CommandType.Text;
                int retVal = command.ExecuteNonQuery();
                con.Close();
                return retVal;
            }
        }
    }
}
