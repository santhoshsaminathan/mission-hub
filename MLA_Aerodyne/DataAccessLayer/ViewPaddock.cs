﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.DataAccessLayer
{
    class ViewPaddock
    {
        public string Pad_ID { get; set; }
        public DataSet BindCombobox_Paddock(string userId, string plotID)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("SELECT Paddock_Id,Paddock_Name,Paddock_Image,CreatedOn from Paddock_Master where User_Id=@UserID AND Plot_ID=@Plot_ID AND IsActive = 1 Order By Paddock_Name ASC", con);
                command.Parameters.Add(new SQLiteParameter("@UserID") { Value = userId });
                command.Parameters.Add(new SQLiteParameter("@Plot_ID") { Value = plotID });
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }

        }
        public DataSet BindCombobox_Import_Paddock(string userId)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("SELECT Paddock_Id,Paddock_Name,Paddock_Image,CreatedOn from Paddock_Master where User_Id=@UserID AND IsActive = 1 Order By Paddock_Name ASC", con);
                command.Parameters.Add(new SQLiteParameter("@UserID") { Value = userId });
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;

            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }
        public DataSet Bind_Paddock(string userId, string padID)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("SELECT Paddock_Id,Paddock_Name,Paddock_Image,CreatedOn,Plot_ID from Paddock_Master where User_Id=@UserID AND Paddock_Id=@Paddock_Id AND IsActive = 1 Order By Landmark_Name ASC", con);
                command.Parameters.Add(new SQLiteParameter("@UserID") { Value = userId });
                command.Parameters.Add(new SQLiteParameter("@Paddock_Id") { Value = padID });
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }

        public DataSet List_Paddock(string userId)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("SELECT A.Paddock_Id,A.Paddock_Name,A.Paddock_Image,A.CreatedOn,B.Plot_ID,B.Plot_Name from Paddock_Master as A LEFT JOIN Plot_Master as B where A.User_Id=@UserID AND A.IsActive = 1 AND A.Plot_ID=B.Plot_ID ORDER BY Paddock_Name ASC", con);
                command.Parameters.Add(new SQLiteParameter("@UserID") { Value = userId });
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }
            
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }

        }

        public string getI_magePath(string index)
        {
            string imgPath = "";
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "select Paddock_Id,Paddock_Name,Paddock_Image from Paddock_Master WHERE Paddock_Id=@Paddockid";
                    command.Parameters.Add(new SQLiteParameter("@Paddockid") { Value = index });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader reader;
                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        imgPath = reader["Paddock_Image"].ToString();
                    }
                }

                return imgPath;
            }
           
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }
        public int Delete_Paddock(string padId, string userid)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "select Paddock_Id FROM WayPoint_Master WHERE Paddock_Id=@Paddock_Id and IsActive=1";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@Paddock_Id", padId));
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader reader;
                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        Pad_ID = reader["Paddock_Id"].ToString();
                    }
                    reader.Close();
                    con.Close();
                    if (Pad_ID == null || Pad_ID == "")
                    {
                        con.Open();
                        command.CommandText = "UPDATE Paddock_Master SET IsActive = 0 WHERE Paddock_Id =@Paddock_Id AND User_Id=@Userid";
                        command.Prepare();
                        command.Parameters.Add(new SQLiteParameter("@Paddock_Id", padId));
                        command.Parameters.Add(new SQLiteParameter("@Userid", userid));
                        command.CommandType = System.Data.CommandType.Text;
                        int retVal = command.ExecuteNonQuery();
                        con.Close();
                        return retVal;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
          
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return 0;
            }
        }
        public List<string> get_latlong(string index)
        {
            try
            {
                List<string> LstLandmark = new List<string>();
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    DataTable dt = new DataTable();
                    con.Open();
                    command.CommandText = "select PT.Paddock_Latitude, PT.Paddock_Longitude, PM.Paddock_Name, PL.Plot_Name, PM.Area from Paddock_Master as PM "
                        + " LEFT JOIN Paddock_Transact as PT on PM.Paddock_Id=PT.Paddock_Id"
                        + " LEFT JOIN Plot_Master AS PL ON PL.Plot_ID = PM.Plot_ID WHERE PT.Paddock_Id = @Paddockid";
                    command.Parameters.Add(new SQLiteParameter("@Paddockid") { Value = index });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader dbr1;
                    dbr1 = command.ExecuteReader();
                    while (dbr1.Read())
                    {
                        string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() + "," + dbr1[2].ToString() + "," + dbr1[3].ToString() + "," + dbr1[4].ToString();
                        LstLandmark.Add(ValPoints);
                    }
                }
                return LstLandmark;
            }
           
              catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }
        public List<string> get_latlong_edit(string index,string wayid)
        {
            try
            {
                List<string> LstLandmark = new List<string>();
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    DataTable dt = new DataTable();
                    con.Open();
                    command.CommandText = "select PT.Paddock_Latitude, PT.Paddock_Longitude,PM.Paddock_Name,PL.Plot_Name,WM.WayPoint_Name from Paddock_Master as PM "
                        + " LEFT JOIN Paddock_Transact as PT on PM.Paddock_Id=PT.Paddock_Id"
                        + " LEFT JOIN Plot_Master AS PL ON PL.Plot_ID = PM.Plot_ID " 
                        + " LEFT JOIN WayPoint_Master AS WM ON PM.Paddock_Id = WM.Paddock_Id"
                        + " WHERE PT.Paddock_Id = @Paddockid AND WM.Waypoint_ID=@Waypoint_ID";
                    command.Parameters.Add(new SQLiteParameter("@Paddockid") { Value = index });
                    command.Parameters.Add(new SQLiteParameter("@Waypoint_ID") { Value = wayid });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader dbr1;
                    dbr1 = command.ExecuteReader();
                    while (dbr1.Read())
                    {
                        string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() + "," + dbr1[2].ToString() + "," + dbr1[3].ToString() + "," + dbr1[4].ToString();
                        LstLandmark.Add(ValPoints);
                    }
                }
                return LstLandmark;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }
    }


}
