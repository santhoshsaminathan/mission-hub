﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.DataAccessLayer
{
    class ViewMaparea
    {
        public string Plot_ID { get; set; }
        public DataSet BindCombobox_ViewMaparea(string userId)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));

                con.Open();
                SQLiteCommand command = new SQLiteCommand("SELECT Plot_ID, Plot_Name, Plot_Image, CreatedOn from Plot_Master where User_Id=@UserID AND IsActive = 1 Order By Plot_Name ASC", con);
                command.Parameters.Add(new SQLiteParameter("@UserID") { Value = userId });
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }
        public DataSet Bind_Maparea(string userId,string plotId)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("SELECT Plot_ID,Plot_Name,Plot_Image,CreatedOn from Plot_Master where User_Id=@UserID AND Plot_ID=@Plot_ID AND IsActive = 1 Order By Plot_Name ASC", con);
                command.Parameters.Add(new SQLiteParameter("@UserID") { Value = userId });
                command.Parameters.Add(new SQLiteParameter("@Plot_ID") { Value = plotId });
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }

        public string getI_magePath(string index)
        {
            string imgPath = "";

            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "select Plot_ID,Plot_Name,Plot_Image from Plot_Master WHERE Plot_ID=@Plotid";
                command.Parameters.Add(new SQLiteParameter("@Plotid") { Value = index });
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader reader;
                reader = command.ExecuteReader();
                if (reader.Read())
                {
                    imgPath = reader["Plot_Image"].ToString();
                }
                con.Close();
            }
            return imgPath;
        }

        public List<string> get_latlong(string index)
        {
            List<string> LstLandmark = new List<string>();
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                DataTable dt = new DataTable();
                con.Open();
                command.CommandText = "select PT.Plot_Latitude,PT.Plot_Longitude,PM.Plot_Name,PM.Area from Plot_Transact as PT INNER JOIN Plot_Master as PM on PM.Plot_ID=PT.Plot_ID WHERE PM.Plot_ID=@Plotid";
                command.Parameters.Add(new SQLiteParameter("@Plotid") { Value = index });
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader dbr1;
                dbr1 = command.ExecuteReader();
                while (dbr1.Read())
                {
                    string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() + "," + dbr1[2].ToString() + "," + dbr1[3].ToString();
                    LstLandmark.Add(ValPoints);
                }
            }
            return LstLandmark;
        }

        public List<string> get_latlong_edit(string index,string padid)
        {
            List<string> LstLandmark = new List<string>();
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                DataTable dt = new DataTable();
                con.Open();
                command.CommandText = "select PT.Plot_Latitude,PT.Plot_Longitude,PM.Plot_Name,PD.Paddock_Name from Plot_Transact as PT"
                                        +" INNER JOIN Plot_Master as PM on PM.Plot_ID = PT.Plot_ID"
                                        + " INNER JOIN Paddock_Master AS PD ON PM.Plot_ID = PD.Plot_ID"
                                        + " WHERE PM.Plot_ID = @Plotid AND PD.Paddock_Id=@Paddock_Id";
                command.Parameters.Add(new SQLiteParameter("@Plotid") { Value = index });
                command.Parameters.Add(new SQLiteParameter("@Paddock_Id") { Value = padid });
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader dbr1;
                dbr1 = command.ExecuteReader();
                while (dbr1.Read())
                {
                    string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() + "," + dbr1[2].ToString() + "," + dbr1[3].ToString();
                    LstLandmark.Add(ValPoints);
                }
            }
            return LstLandmark;
        }
        public int Delete_MapArea(string plotId,string userid)
        {
            string plot_id;
            using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
            using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
            {
                con.Open();
                command.CommandText = "select Plot_ID FROM Paddock_Master WHERE Plot_ID=@Plot_ID and IsActive=1";
                command.Prepare();
                command.Parameters.Add(new SQLiteParameter("@Plot_ID", plotId));
                command.CommandType = System.Data.CommandType.Text;
                SQLiteDataReader reader;
                reader = command.ExecuteReader();
                if (reader.Read())
                {
                    Plot_ID = reader["Plot_ID"].ToString();
                }
                reader.Close();
                con.Close();
                if (Plot_ID == null||Plot_ID=="")
                {
                    con.Open();
                    command.CommandText = "UPDATE Plot_Master SET IsActive = 0 WHERE Plot_ID =@Plotid AND User_Id=@Userid";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@Plotid", plotId));
                    command.Parameters.Add(new SQLiteParameter("@Userid", userid));
                    command.CommandType = System.Data.CommandType.Text;
                    int retVal = command.ExecuteNonQuery();
                    con.Close();
                    return retVal;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
