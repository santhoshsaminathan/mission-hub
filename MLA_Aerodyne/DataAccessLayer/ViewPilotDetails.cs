﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.DataAccessLayer
{
    class ViewPilotDetails
    {
        public DataSet BindCombobox_ViewPilotDetails()
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));

                con.Open();
                SQLiteCommand command = new SQLiteCommand("select Pilot_ID,Pilot_Name from Pilot_Master", con);
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }        
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }

        }
    }
}
