﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MLA_Aerodyne.DataAccessLayer
{
    class ViewWaypoint
    {
        public DataSet BindCombobox_ViewWaypoint(string userId, string padID)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand("SELECT WayPoint_Id, WayPoint_Name, Wpoint_Image, CreatedOn from WayPoint_Master where User_Id = @UserID AND  Paddock_Id = @Paddock_Id AND IsActive = 1 Order By WayPoint_Name ASC", con);
                command.Parameters.Add(new SQLiteParameter("@UserID") { Value = userId });
                command.Parameters.Add(new SQLiteParameter("@Paddock_Id") { Value = padID });
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }

        public DataSet List_ViewWaypoint(string userId)
        {
            try
            {
                System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString()));
                con.Open();
                SQLiteCommand command = new SQLiteCommand(" SELECT WM.WayPoint_Id,WM.WayPoint_Name,WM.Wpoint_Image, WM.CreatedOn,PM.Paddock_Name from WayPoint_Master AS WM" 
                                                            +" INNER JOIN Paddock_Master AS PM ON PM.Paddock_Id = WM.Paddock_Id"
                                                           + "  where  WM.User_Id = @UserID AND  WM.IsActive = 1 Order By  WM.WayPoint_Name ASC", con);
                command.Parameters.Add(new SQLiteParameter("@UserID") { Value = userId });
                DataSet ds = new DataSet();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(ds);
                con.Close();
                return ds;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }

        public string getI_magePath(string index)
        {
            string imgPath = "";
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "select WayPoint_Id,WayPoint_Name,Wpoint_Image from WayPoint_Master WHERE WayPoint_Id=@Waypointid";
                    command.Parameters.Add(new SQLiteParameter("@Waypointid") { Value = index });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader reader;
                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        imgPath = reader["Wpoint_Image"].ToString();
                    }
                }

                return imgPath;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }

        public List<string> get_latlong(string index)
        {
            try
            {
                List<string> LstLandmark = new List<string>();
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    DataTable dt = new DataTable();
                    con.Open();
                    command.CommandText = "select wt.Wpoint_Latitude, wt.Wpoint_Longitude,wm.WayPoint_Name,PD.Paddock_Name,PM.Plot_Name,wm.Area,wm.Distance from Waypoint_Transact as wt "
                                               + " INNER JOIN WayPoint_Master as wm on wm.WayPoint_Id = wt.WayPoint_Id"
                                               + " INNER JOIN Paddock_Master AS PD ON PD.Paddock_Id = WM.Paddock_Id"
                                               + " INNER JOIN Plot_Master AS PM ON PM.Plot_ID = PD.Plot_ID"
                                               + " WHERE wm.WayPoint_Id = @Waypointid";
                    command.Parameters.Add(new SQLiteParameter("@Waypointid") { Value = index });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader dbr1;
                    dbr1 = command.ExecuteReader();
                    while (dbr1.Read())
                    {
                        string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() + "," + dbr1[2].ToString() + "," + dbr1[3].ToString() + "," + dbr1[4].ToString() + "," + dbr1[5].ToString() + "," + dbr1[6].ToString();
                        LstLandmark.Add(ValPoints);
                    }
                    con.Close();
                }
                return LstLandmark;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }

        public List<string> get_latlong_simulate(string index)
        {
            try
            {
                string speed = "50 kph";
                double _dspeed = 50;
                double distance;
                string _distance;
                List<string> LstLandmark = new List<string>();
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    DataTable dt = new DataTable();
                    con.Open();
                    command.CommandText = "select wt.Wpoint_Latitude, wt.Wpoint_Longitude,wm.WayPoint_Name,PD.Paddock_Name,PM.Plot_Name,wm.Area,wm.Distance from Waypoint_Transact as wt "
                                               + " INNER JOIN WayPoint_Master as wm on wm.WayPoint_Id = wt.WayPoint_Id"
                                               + " INNER JOIN Paddock_Master AS PD ON PD.Paddock_Id = WM.Paddock_Id"
                                               + " INNER JOIN Plot_Master AS PM ON PM.Plot_ID = PD.Plot_ID"
                                               + " WHERE wm.WayPoint_Id = @Waypointid";
                    command.Parameters.Add(new SQLiteParameter("@Waypointid") { Value = index });
                    command.CommandType = System.Data.CommandType.Text;
                    SQLiteDataReader dbr1;
                    dbr1 = command.ExecuteReader();
                    while (dbr1.Read())
                    {                       
                        _distance = dbr1[6].ToString();
                        _distance = _distance.Replace("km", "");
                        distance = Convert.ToDouble(_distance);
                        double _flighttime= cal_time(distance, _dspeed);
                        TimeSpan interval = TimeSpan.FromHours(_flighttime);
                        string _interval = Verbose(interval);
                        string ValPoints = dbr1[0].ToString() + "," + dbr1[1].ToString() + "," + dbr1[2].ToString() + "," + dbr1[3].ToString() + "," + dbr1[4].ToString() + "," + dbr1[5].ToString() + ","  + dbr1[6].ToString() + "," + speed + "," + _interval;
                        LstLandmark.Add(ValPoints);
                    }
                    con.Close();
                }
                return LstLandmark;
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return null;
            }
        }
        static double cal_time(double dist, double speed)
        {
            return dist/ speed ;
        }
        public static String Verbose(TimeSpan timeSpan)
        {
            var hours = timeSpan.Hours;
            var minutes = timeSpan.Minutes;
            var seconds = timeSpan.Seconds;

            if (hours > 0) return String.Format("{0} hrs {1} min {2} sec", hours, minutes, seconds);
            return String.Format("{0} min {1} sec", minutes,seconds);
        }
        public int Delete_Waypoint(string wayID, string userid)
        {
            try
            {
                using (System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection("Data Source = " + Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["DBConfig"].ToString())))
                using (System.Data.SQLite.SQLiteCommand command = con.CreateCommand())
                {
                    con.Open();
                    command.CommandText = "UPDATE WayPoint_Master SET IsActive = 0 WHERE WayPoint_Id =@WayPoint_Id AND User_Id=@Userid";
                    command.Prepare();
                    command.Parameters.Add(new SQLiteParameter("@WayPoint_Id", wayID));
                    command.Parameters.Add(new SQLiteParameter("@Userid", userid));
                    command.CommandType = System.Data.CommandType.Text;
                    int retVal = command.ExecuteNonQuery();
                    con.Close();
                    return retVal;
                }
            }

            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                return 0;
            }
        }
    }
  
}