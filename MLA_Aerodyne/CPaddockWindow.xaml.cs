﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using System.Globalization;

namespace MLA_Aerodyne
{
    /// <summary>
    /// Interaction logic for CPaddockWindow.xaml
    /// </summary>
    public partial class CPaddockWindow : Window
    {
        public dynamic Waypoints;

        public string User_Id;

        public string AllWaypoints;

        public string CapImage_Path;

        public string SelectMapArea;

        public string MapArea_Id;

        DataAccessLayer.Login_DAL _error = new DataAccessLayer.Login_DAL();

        public string UserId { get; set; }

        public CPaddockWindow()
        {
            InitializeComponent();

            NameTxt.Focus();
        }

        public void GetUserIDFromLogin(string cUserId)
        {
            UserId = cUserId;
        }

        public void GetpaddockFromDatabase()
        {
            try
            {
                DataAccessLayer.DAL_CreatePaddock DAL_CreatePaddock = new DataAccessLayer.DAL_CreatePaddock();
                DataSet ds_Combo = DAL_CreatePaddock.GetPaddockFromDB(User_Id);
                PaddockListBox.ItemsSource = ds_Combo.Tables[0].DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: "+ ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "CPaddockWindow", User_Id);
                return;
            }
        }

        public void ValueFromMapLoad(string UserId, string Waypoint, string CapImagePath, string MapAreaId)
        {
            try
            {
                Waypoints = JsonConvert.DeserializeObject(Waypoint);

                AllWaypoints = Waypoint;

                User_Id = UserId;

                CapImage_Path = CapImagePath;

                MapArea_Id = MapAreaId;

                DataAccessLayer.DAL_CreatePaddock DAL_CreatePaddock = new DataAccessLayer.DAL_CreatePaddock();
                string LoadMapName = DAL_CreatePaddock.GetSelectMapAreaNameFromDB(MapAreaId);

                foreach (var val in Waypoints)
                {
                    string Name = val.name;
                    string CreatedBy = val.CreatedBy;
                    string[] points = val.points;
                }

                MapAreaNameTxt.Text = LoadMapName;

                GetpaddockFromDatabase();

                //GetMapAreaFromDatabase();
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: "+ ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "CPaddockWindow", User_Id);
                return;
            }
        }
       

        private static double ConvertToRadian(double input)
        {
            return input * Math.PI / 180;
        }
        private void SaveBtn_Click(object sender, RoutedEventArgs e)
            
            {
            try
            {
                if (NameTxt.Text != "")
                {
                    DateTime now = DateTime.Now;
                    string CreatedOn = now.ToString();
                    string PaddockName = NameTxt.Text;

                    DataAccessLayer.DAL_CreatePaddock DAL_CreatePaddock = new DataAccessLayer.DAL_CreatePaddock();
                    dynamic Way_Points = JsonConvert.DeserializeObject(AllWaypoints);
                    IList<PointLatLng> coordinates = new List<PointLatLng>();
                    foreach (var val in Way_Points)
                    {
                        PointLatLng pointLatLng = new PointLatLng();
                        pointLatLng.Latitude = val.lat;
                        pointLatLng.Longitude = val.lng;
                        coordinates.Add(pointLatLng);

                    }
                    double area = MLA_Aerodyne.DataAccessLayer.CalculateAreaOfPolygon.ComputeSignedArea(coordinates);
                    area = Math.Round(area,2);
                    area = Math.Abs(area);
                    area = area / (double)10000;
                    NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
                    nfi.NumberDecimalSeparator = " ";
                    string _area = area.ToString("N", nfi);
                    DAL_CreatePaddock.Save_CreateMapArea(User_Id, PaddockName, AllWaypoints, CapImage_Path, MapArea_Id, _area.Replace(" ",".") + " " + "hectares");

                    this.Visibility = Visibility.Hidden;
					MessageBox message = new MessageBox("Paddock saved successfully.");
					message.Show();
                    ((MapLoad)this.Owner).clearpaddock();
                }
                else
                {
                    MessageBox message = new MessageBox("Please enter paddock name.");
					message.Show();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: "+ ex.Message);
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "CPaddockWindow", User_Id);
                return;
            }
        }
       
      
        private void CloseTopBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NameTxt.Text = "";
                this.Visibility = Visibility.Hidden;
                ((MapLoad)this.Owner).clearpaddock();

            }
            catch (Exception ex)
            {
                MessageBox message = new MessageBox("Error: " + ex.Message);
                message.Show();
                _error.Insert_Errorlog("An unhandled exception just occurred: " + ex.Message, "CPaddockWindow", User_Id);
            }

        }
    }
    public class PointLatLng 
    {
        private double _Longitude;
        public double _Latitude;

        public double Longitude
        {
            get { return this._Longitude; }
            set { this._Longitude = value; }
        }
        public double Latitude
        {
            get { return this._Latitude; }
            set { this._Latitude = value; }
        }
    }
}
